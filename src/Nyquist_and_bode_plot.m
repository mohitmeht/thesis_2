%% Load Data from the database
clear all;
Impedance=ld_eis_db();
experiment=6;           % Experiment number
ZreZimohm=cell2mat(Impedance(3,experiment));
names=char(Impedance{4,experiment});
num=cell2mat(Impedance(1,experiment));
ZreZimohmX(:,:)=ZreZimohm(:,1,:);
ZreZimohmY(:,:)=ZreZimohm(:,2,:);
ocv=num(:,10:11:end);
frq(:,:)=Impedance{1,experiment}(:,3:11:end);
Zre_Abs(:,:)=Impedance{1,experiment}(:,7:11:end);
Zre_Phase(:,:)=Impedance{1,experiment}(:,8:11:end);
%% Nyquist Plot

hold off
i=5;            % Plot number/reading number
tot_readings=2*size(ZreZimohm,3);
cc=[0,0,0;0,0,1];
prnt=0;
plotlinewidth=1.5;
markertype='-';
figvis='off';
markersize=7;
fontsize = 20;
range=1:size(ZreZimohm,1);

figure(1);
subplot(1,2,1);
plot(ZreZimohmX(:,i),ZreZimohmY(:,i),'-','LineWidth',plotlinewidth);
grid off; box on;
axis square;               % Axis Size and font type
set(gcf,'Color','w'); 
ylabel('-Z'''' [\Omega]','FontSize',fontsize);
xlabel('Z'' [\Omega]','FontSize',fontsize);
set(gca,'FontSize',fontsize);
axis([-50 300 0 350])
set(gca,'YTick',[0 100 200 300 400])
%% Bode Plot
tot_readings=2*size(ZreZimohm,3);
cc=[0,0,0;0,0,1];
prnt=0;
plotlinewidth=1.5;
figvis='off';
markersize=7;
fontsize = 24;
range=1:size(ZreZimohm,1);
plot_freq = frq(:,i);
plot_Abs = Zre_Abs(:,i);
plot_phase = Zre_Phase(:,i);
subplot(1,2,2);
[AX,H1,H2] =plotyy(frq(:,i),Zre_Abs(:,i),frq(:,i),Zre_Phase(:,i),'semilogx');
set(H1,'Marker','v','LineWidth',plotlinewidth,'MarkerFaceColor',cc(1,:),'Color',cc(1,:));
set(H2,'Marker','o','LineWidth',plotlinewidth,'MarkerFaceColor',cc(2,:),'Color',cc(2,:));
set(H1,'LineStyle','-');
set(H2,'LineStyle','-');
set(AX(2),'ylim',[-50 20],'YTick',[-40 -30 -20 -10 0 10 20],'YColor', cc(2,:));
set(AX(1),'YColor',cc(1,:));
set(AX(2),'YColor',cc(2,:));
set(AX(1),'ylim',[0 300],'YTick',[100 200 300 400 500 600 700]);
grid off; box off;
axis square;               % Axis Size and font type
set(gcf,'Color','w'); 
set(get(AX(1),'Ylabel'),'String','Amplitude [\Omega]','FontSize',fontsize,'FontName','Times New Roman');
set(get(AX(2),'Ylabel'),'String','Phase [\circ]','FontSize',fontsize,'FontName','Times New Roman');
set(get(AX(1),'Xlabel'),'String','Frequency [Hz]','FontSize',fontsize,'FontName','Times New Roman');
set(get(AX(2),'Xlabel'),'String','Frequency [Hz]','FontSize',fontsize,'FontName','Times New Roman');
set(AX(1),'FontSize',fontsize,'FontName','Times New Roman');
set(AX(2),'FontSize',fontsize,'FontName','Times New Roman');  
set(AX(1),'PlotBoxAspectRatio',[1 1 1]);
set(AX(2),'PlotBoxAspectRatio',[1 1 1]);