#LyX 2.0 created this file. For more info see http://www.lyx.org/
\lyxformat 413
\begin_document
\begin_header
\textclass article
\begin_preamble
\usepackage{babel}
\end_preamble
\use_default_options false
\maintain_unincluded_children false
\language english
\language_package default
\inputencoding auto
\fontencoding T1
\font_roman default
\font_sans default
\font_typewriter default
\font_default_family default
\use_non_tex_fonts false
\font_sc false
\font_osf false
\font_sf_scale 100
\font_tt_scale 100

\graphics default
\default_output_format default
\output_sync 0
\bibtex_command default
\index_command default
\paperfontsize default
\spacing single
\use_hyperref false
\papersize default
\use_geometry false
\use_amsmath 1
\use_esint 1
\use_mhchem 0
\use_mathdots 0
\cite_engine basic
\use_bibtopic false
\use_indices false
\paperorientation portrait
\suppress_date false
\use_refstyle 0
\index Index
\shortcut idx
\color #008000
\end_index
\secnumdepth 3
\tocdepth 3
\paragraph_separation indent
\paragraph_indentation default
\quotes_language english
\papercolumns 1
\papersides 1
\paperpagestyle default
\tracking_changes false
\output_changes false
\html_math_output 0
\html_css_as_file 0
\html_be_strict false
\end_header

\begin_body

\begin_layout Section
Introduction
\end_layout

\begin_layout Standard
The Butler-Volmer relation can describe charging and discharging mechanism
 at the electrode/electrolyte interface.
 Although one can use either eqn
\begin_inset space ~
\end_inset


\begin_inset CommandInset ref
LatexCommand ref
reference "eq:ZF_final_sinh"

\end_inset

 or eqn.
\begin_inset space ~
\end_inset


\begin_inset CommandInset ref
LatexCommand ref
reference "eq:ZF_final_FBV_non_sinh"

\end_inset

 to describe EIS during charge and discharge, however, these equations implicitl
y assumes that either electron transfer coefficient or standard reaction
 rate at the electrode is the same for charging and discharge respectively,
 which is unlikely in a practical battery.
 In this chapter, the Butler-Volmer with different forward and reverse reaction
 rate, non-equal electron transfer factor is considered and is given as
 
\begin_inset Formula 
\begin{equation}
k=a\left(k_{0}^{f}e^{\frac{-\alpha n}{V_{T}}\eta}-k_{0}^{r}e^{\frac{(1-\alpha)n}{V_{T}}\eta}\right)\label{eq:k_BV_w_beta}
\end{equation}

\end_inset

where 
\begin_inset Formula $k_{0}^{f}$
\end_inset

 is the standard forward reaction rate, 
\begin_inset Formula $k_{0}^{r}$
\end_inset

 is the standard reverse reaction rate, 
\begin_inset Formula $\alpha$
\end_inset

 is the electron transfer rate (
\begin_inset Formula $0<\alpha<1$
\end_inset

), 
\begin_inset Formula $n$
\end_inset

 is the number of electrons transferred in electrochemical reaction, 
\begin_inset Formula $\eta=E^{0}-\Delta V_{\textrm{\ce{Li2O2}}}$
\end_inset

 is the over-potential (the potential drop between the electrode surface
 and the bulk layer)
\end_layout

\begin_layout Section
Modeling
\end_layout

\begin_layout Standard
The same methodology, specifying boundary conditions, obtaining an expression
 for the spatial distribution of oxygen in the cathode, computing the impedance
 spectra under steady-state conditions, and finally the small-signal analysis
 to obtain the final analytical expression, is used to develop the analytical
 model to compute the EIS under charge and discharge in Li-air batteries
 with organic electrolyte.
 The chemical reaction in the cathode of organic Li-air battery is given
 by, 
\begin_inset Formula 
\begin{equation}
\ce{2Li++O2+2e-<=>Li2O2}
\end{equation}

\end_inset

In order to reduce the length of the equations and to reduce number of variables
 new variables are introduced and used henceforth in this chapter, the original
 variable will be reintroduced in the final impedance expression.
 The forward reaction rate constant with the inclusion of specific capacity
 of the cathode (
\begin_inset Formula $a$
\end_inset

) is given by, 
\begin_inset Formula 
\begin{equation}
k^{f}=ak_{0}^{f}e^{\frac{-\beta n}{V_{T}}\eta}\label{eq:kf_def_FBV}
\end{equation}

\end_inset

and the reaction rate constant with the addition of oxygen diffusion coefficient
 term is given by, 
\begin_inset Formula 
\begin{equation}
k_{\textrm{wD}_{\textrm{eff}}}^{f}=\frac{k^{f}}{D_{\textrm{eff}}}\label{eq:kfwDeff_FBV}
\end{equation}

\end_inset

The reverse reaction rate constant with the specific capacity of the cathode
 given by 
\begin_inset Formula 
\begin{equation}
k^{r}=ak_{0}^{r}e^{\frac{(1-\beta)n}{V_{T}}\eta}\label{eq:kr_def_FBV}
\end{equation}

\end_inset

and the reverse reaction rate constant with the addition of oxygen diffusion
 coefficient term 
\begin_inset Formula 
\begin{equation}
k_{\textrm{wD}_{\textrm{eff}}}^{r}=\frac{k^{r}}{D_{\textrm{eff}}}\label{eq:krwDeff_FBV}
\end{equation}

\end_inset

The complete reaction rate in the cathode is given by 
\begin_inset Formula 
\begin{equation}
R_{c}=nF\left[k^{f}c_{o_{2}}(x)-k^{r}\right]\label{eqn:chap5_FBV_Rc}
\end{equation}

\end_inset


\end_layout

\begin_layout Subsection
Steady-state analysis
\end_layout

\begin_layout Standard
The mass transport of oxygen inside the cathode is described using Fick's
 second law of diffusion.
 For this analysis the effects of convection are neglected, since it is
 assumed that the setup is not disturbed while the impedance spectrum is
 measured.
 The convection term in the Nernst-Plank becomes important while computing
 the impedance response of Li-air flow type of batteries.
 The mass transport of oxygen in Li-air batteries under steady state analysis
 is given by 
\begin_inset Formula 
\begin{equation}
\frac{\partial^{2}c_{o_{2}}(x)}{\partial x^{2}}-k_{\textrm{wD}_{\textrm{eff}}}^{f}c_{o_{2}}(x)+k_{\textrm{wD}_{\textrm{eff}}}^{r}=0\label{eq:Ficks_second_law_FBV_steady_state}
\end{equation}

\end_inset

The solution to the above equation (eqn.
\begin_inset space ~
\end_inset


\begin_inset CommandInset ref
LatexCommand ref
reference "eq:Ficks_second_law_FBV_steady_state"

\end_inset

) is, 
\begin_inset Formula 
\begin{equation}
c_{o_{2}}(x)=C_{1}\cosh{\left(\sqrt{k_{\textrm{wD}_{\textrm{eff}}}^{f}}x\right)}+C_{2}\sinh{\left(\sqrt{k_{\textrm{wD}_{\textrm{eff}}}^{f}}x\right)}+\frac{k_{\textrm{wD}_{\textrm{eff}}}^{r}}{k_{\textrm{wD}_{\textrm{eff}}}^{f}}\label{eq:Solution_to_Ficks_second_law_FBV_steady_state}
\end{equation}

\end_inset

where C
\begin_inset Formula $_{1}$
\end_inset

 and C
\begin_inset Formula $_{2}$
\end_inset

 are the integration constants.
 The integration constants are computed using the boundary conditions: 
\begin_inset ERT
status collapsed

\begin_layout Plain Layout


\backslash
begin{subequations}
\end_layout

\end_inset

 
\end_layout

\begin_layout Enumerate
(a) 
\series bold
Boundary condition at 
\begin_inset Formula $x=0$
\end_inset


\series default
 
\begin_inset Formula 
\begin{align}
c_{o_{2}}(0)=\frac{p_{o_{2}}}{k_{H}}\label{bc:FBV_c_at_0}
\end{align}

\end_inset


\end_layout

\begin_layout Enumerate
(b) 
\series bold
Boundary condition at 
\begin_inset Formula $x=l$
\end_inset


\series default
 
\begin_inset Formula 
\begin{align}
\left.\frac{\textrm{d}c_{o_{2}}(x)}{\textrm{d}x}\right|_{x=l}=0\label{bc:FBV_c_at_l}
\end{align}

\end_inset


\end_layout

\begin_layout Standard
\begin_inset ERT
status collapsed

\begin_layout Plain Layout


\backslash
end{subequations}
\end_layout

\end_inset

 Using boundary conditions (eqns.
\begin_inset space ~
\end_inset


\begin_inset CommandInset ref
LatexCommand ref
reference "bc:FBV_c_at_0"

\end_inset

 and
\begin_inset space ~
\end_inset


\begin_inset CommandInset ref
LatexCommand ref
reference "bc:FBV_c_at_l"

\end_inset

) and eqn.
\begin_inset space ~
\end_inset


\begin_inset CommandInset ref
LatexCommand ref
reference "eq:Solution_to_Ficks_second_law_FBV_steady_state"

\end_inset

, the expressions for the integration constants become 
\begin_inset Formula 
\begin{align}
C_{1} & =\frac{p_{o_{2}}}{k_{H}}-\frac{k_{\textrm{wD}_{\textrm{eff}}}^{r}}{k_{\textrm{wD}_{\textrm{eff}}}^{f}}\\
C_{2} & =\left(\frac{k_{\textrm{wD}_{\textrm{eff}}}^{r}}{k_{\textrm{wD}_{\textrm{eff}}}^{f}}-\frac{p_{o_{2}}}{k_{H}}\right)\tanh{\left(\sqrt{k_{\textrm{wD}_{\textrm{eff}}}^{f}}l\right)}
\end{align}

\end_inset

The final expression for the oxygen concentration distribution inside the
 cathode is 
\begin_inset Formula 
\begin{equation}
\boxed{c_{o_{2}}(x)=\left(\frac{p_{o_{2}}}{k_{H}}-\frac{k_{\textrm{wD}_{\textrm{eff}}}^{r}}{k_{\textrm{wD}_{\textrm{eff}}}^{f}}\right)\left[\cosh\left(\sqrt{k_{\textrm{wD}_{\textrm{eff}}}^{f}}x\right)-\tanh\left(\sqrt{k_{\textrm{wD}_{\textrm{eff}}}^{f}}l\right)\sinh\left(\sqrt{k_{\textrm{wD}_{\textrm{eff}}}^{f}}x\right)\right]+\frac{k_{\textrm{wD}_{\textrm{eff}}}^{r}}{k_{\textrm{wD}_{\textrm{eff}}}^{f}}}\label{eqn:chap5_co2_solution}
\end{equation}

\end_inset

The Faradaic component (
\begin_inset Formula $I_{F}$
\end_inset

) of the discharge current is computed using 
\begin_inset Formula 
\begin{equation}
I_{F}=A\int_{0}^{l}R_{c}\,\textrm{d}x
\end{equation}

\end_inset

Substituting 
\begin_inset Formula $R_{c}$
\end_inset

 from eqn
\begin_inset space ~
\end_inset


\begin_inset CommandInset ref
LatexCommand ref
reference "eqn:chap5_FBV_Rc"

\end_inset

 and separating the integrals 
\begin_inset Formula 
\begin{equation}
I_{F}=nFA\left[k^{f}\int_{0}^{l}c_{o_{2}}(x)\,\textrm{d}x-\int_{0}^{l}k^{r}\,\textrm{d}x\right]\label{eq:FBV_I_F_w_integrals}
\end{equation}

\end_inset

The equation for the steady-state Faradaic current in Li-air batteries with
 organic electrolyte during charge and discharge after solving the integrals
 in the above equation is, 
\begin_inset Formula 
\begin{equation}
I_{F}=nFA\sqrt{k^{f}D_{\textrm{eff}}}\left(\frac{p_{o_{2}}}{k_{H}}-\frac{k^{r}}{k^{f}}\right)\tanh\left(\sqrt{k_{\textrm{wD}_{\textrm{eff}}}^{f}}l\right)
\end{equation}

\end_inset

Now replacing
\family roman
\series medium
\shape up
\size normal
\emph off
\bar no
\strikeout off
\uuline off
\uwave off
\noun off
\color none
 
\begin_inset Formula $k_{\textrm{wD}_{\textrm{eff}}}^{f}$
\end_inset


\family default
\series default
\shape default
\size default
\emph default
\bar default
\strikeout default
\uuline default
\uwave default
\noun default
\color inherit
 from eqn.~
\begin_inset CommandInset ref
LatexCommand ref
reference "eq:kfwDeff_FBV"

\end_inset

 into the above equation.
 The final expression for steady-state Faradaic current is, 
\end_layout

\begin_layout Standard
\begin_inset Formula 
\begin{equation}
I_{F}=nFA\sqrt{k^{f}D_{\textrm{eff}}}\left(\frac{p_{o_{2}}}{k_{H}}-\frac{k^{r}}{k^{f}}\right)\tanh\left(\sqrt{\frac{k^{f}}{D_{\textrm{eff}}}}l\right)
\end{equation}

\end_inset


\end_layout

\begin_layout Standard
The Faradaic current contributed due to charging or discharging dc current
 is a function of forward and backward reaction rate constant, effective
 diffusion coefficient of oxygen in the electrolyte, length of the cathode,
 dissolvation of oxygen in the organic electrolyte, partial pressure of
 oxygen.
 
\end_layout

\begin_layout Subsection
Small-signal analysis
\end_layout

\begin_layout Standard
When an external sinusoidal signal with angular frequency 
\begin_inset Formula $\omega$
\end_inset

 is applied around the discharge current, in the limit of small perturbations
 the oxygen concentration, over-potential, and the Faradaic current can
 be linearized as 
\begin_inset Formula 
\begin{equation}
c_{o_{2}}(x)+\delta c_{o_{2}}(x)\, e^{j\omega t}\textrm{, }\eta+\delta\eta\, e^{j\omega t}\textrm{, and }I_{F}+\delta I_{F}\, e^{j\omega t}\textrm{ respectively.}\label{eq:ss_rep_FBV}
\end{equation}

\end_inset

where 
\begin_inset Formula $j=\sqrt{-1}$
\end_inset

 and 
\begin_inset Formula $\delta c_{o_{2}}(x)$
\end_inset

, 
\begin_inset Formula $\delta\eta$
\end_inset

, and 
\begin_inset Formula $\delta I_{F}$
\end_inset

 are the perturbation amplitudes.
 The oxygen concentration (in response to input signal perturbation) in
 the cathode is again described using Fick's second law of diffusion
\begin_inset Formula 
\[
\frac{\partial c_{o_{2}}(x)+\delta c_{o_{2}}(x)\, e^{j\omega t}}{\partial t}=D_{\textrm{eff}}\frac{\partial^{2}c_{o_{2}}(x)+\delta c_{o_{2}}(x)\, e^{j\omega t}}{\partial x^{2}}-\left[k^{f}+\delta k^{f}\, e^{j\omega t}\right]\left[c_{o_{2}}(x)+\delta c_{o_{2}}(x)\, e^{j\omega t}\right]+\left[k^{r}+\delta k^{r}\, e^{j\omega t}\right]
\]

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula 
\begin{equation}
\frac{\partial c_{o_{2}}(x)}{\partial t}=D_{\textrm{eff}}\frac{\partial^{2}c_{o_{2}}(x)}{\partial x^{2}}-k^{f}c_{o_{2}}(x)+k^{r}\label{eq:Ficks_second_law_FBV_small_signal}
\end{equation}

\end_inset

where 
\family roman
\series medium
\shape up
\size normal
\emph off
\bar no
\strikeout off
\uuline off
\uwave off
\noun off
\color none

\begin_inset Formula $k^{f}$
\end_inset

 and 
\begin_inset Formula $k^{r}$
\end_inset

 are defined by eqns.
\begin_inset CommandInset ref
LatexCommand ref
reference "eq:kf_def_FBV"

\end_inset

 and 
\begin_inset CommandInset ref
LatexCommand ref
reference "eq:kr_def_FBV"

\end_inset

 respectively.
 The substitution of eqn.
 
\begin_inset CommandInset ref
LatexCommand ref
reference "eq:ss_rep_FBV"

\end_inset

 into eqn.
 
\begin_inset CommandInset ref
LatexCommand ref
reference "eq:Ficks_second_law_FBV_small_signal"

\end_inset

 yields:
\family default
\series default
\shape default
\size default
\emph default
\bar default
\strikeout default
\uuline default
\uwave default
\noun default
\color inherit
 
\end_layout

\begin_layout Standard
\begin_inset Formula 
\begin{equation}
\frac{\partial^{2}\delta c_{o_{2}}(x)}{\partial x^{2}}-\frac{\left(k^{f}+j\omega\right)\delta c_{o_{2}}(x)}{D_{\textrm{eff}}}+\frac{\delta k^{f}c_{o_{2}}(x)}{D_{\textrm{eff}}}+\frac{\delta k^{r}}{D_{\textrm{eff}}}=0
\end{equation}

\end_inset

The ordinary differential equation can be solved analytically upon the impositio
n of two boundary conditions, as follows: 
\begin_inset ERT
status collapsed

\begin_layout Plain Layout


\backslash
begin{subequations}
\end_layout

\end_inset

 
\end_layout

\begin_layout Enumerate

\series bold
Linearized boundary condition at 
\begin_inset Formula $x=0$
\end_inset


\series default
 
\begin_inset Formula 
\begin{equation}
\delta c_{o_{2}}(0)=0
\end{equation}

\end_inset


\end_layout

\begin_layout Enumerate

\series bold
Linearized boundary condition at 
\begin_inset Formula $x=l$
\end_inset


\series default
 
\begin_inset Formula 
\begin{equation}
\left.\frac{\textrm{d}\delta c_{o_{2}}(x)}{\textrm{d}x}\right|_{x=l}=0
\end{equation}

\end_inset


\end_layout

\begin_layout Standard
\begin_inset ERT
status collapsed

\begin_layout Plain Layout


\backslash
end{subequations}
\end_layout

\end_inset


\end_layout

\end_body
\end_document
