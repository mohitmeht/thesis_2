\documentclass[../thesis.tex]{subfiles}
\providecommand{\main}{../}
\begin{document}
\chapter{Effect of finite oxygen dissolution on the impedance spectra}

\label{chap:Dissolution_kinetics}


\section{Introduction}

In the previous chapters, the concentration of oxygen at $x=0$ is
considered to be a constant and equal to $c_{o}^{*}$, however, in
reality in an open system such as Li-air, the external partial pressure
of oxygen, the solubility of oxygen in the electrolyte, and the value
finite value of the discharge current should play an important role
in this battery. The amount of oxygen dissolved into an electrolyte
solution from the atmosphere, at low concentrations, can be described
using Henry's law, 
\begin{equation}
p_{o}=k_{H}c_{o}
\end{equation}
where $p_{o}$ is partial pressure of oxygen in the atmosphere, $c_{o}$
is the dissolved oxygen concentration in the electrolyte, and $k_{H}$
is the Henry's law constant and it depends on the solute and solvent
in the electrolyte and the ambient temperature. Another common method
of quantifying solubility of gas in a liquid is called Bunsen solubility
constant. Bunsen solubility coefficient $\alpha$ is described as
the volume of the gas (at STP conditions) absorbed by a liter of the
electrolyte at a temperature $T$. The relation between Bunsen solubility
coefficient, partial pressure of oxygen, and dissolved oxygen concentration
is\cite{Breitbarth2004}, 
\begin{equation}
c_{o}=\frac{\alpha p_{o}}{RT_{0}}\label{Bunsen coefficient relation}
\end{equation}
where $\alpha$ is the Bunsen constant (\SI[per-mode=symbol]{}{\centi\meter\cubed \textrm{ gas}\per\centi\meter\cubed \textrm{ liquid}}),
$R$ is the gas constant (\SI[per-mode=symbol]{8314.4621}{\kilo\pascal\centi\meter\cubed\per\mole\per\kelvin}),
$p_{o}$ is the partial pressure of the ambient oxygen (\SI{}{\kilo\pascal}),
$T_{0}$ is the STP temperature (273.15 K). The Bunsen solubility
constant can be related to Henry's law constant using the following
relation 
\begin{equation}
\alpha=\frac{RT_{0}}{k_{H}}=\frac{c_{o}}{p_{o}}
\end{equation}
\section{Model for dissolution kinetics}

Under equilibrium conditions, the dissolution of oxygen in the electrolyte
is described by Henry's law, however, under non-equilibrium conditions,
for instance when the battery is under charge or discharge, Henry's
law fails and it is important to the kinetic effects. By following
the approach presented in \cite{Horstmann2013}, the dissolution of
oxygen is modeled as a chemical reaction and the flux density of oxygen
is expressed as 
\begin{equation}
N_{o}=k_{f}p_{o}-k_{b}c_{o}(0)\label{eq:oxygen_flux}
\end{equation}
where $N_{o}$ is the flux of gas entering the system, $k_{f}$ is
the rate of oxygen entering into the battery, $k_{b}$ is the rate
of oxygen exiting the battery, $p_{o}$ is the partial pressure of
the gas, $c_{o}(0)$ is the concentration of the gas dissolved in
the solvent at $x=0$. Figure \ref{fig:oxygen_dissolvation} shows
a simplified view of the dissolution process, in which fluxes are
represented using arrows and oxygen molecules are represented using
circles (entering the porous cathode at $x=0$). During discharge
there is a flux of oxygen, , whose strength depends on the rate of
the electrochemical reaction. The direction of the flux depends whether
the battery is discharging or charging. 
\begin{figure}[bt]
	\begin{center} \includegraphics[width=0.32\linewidth,angle=90]{\main figures/oxygen_dissolvation_better_cathode}\end{center}
	\protect\caption{Dissolution kinetics in a Lithium-air battery with oxygen dissolving
		at $x=0$}
	\label{fig:oxygen_dissolvation} 
\end{figure}

The forward rate constant can be determined by using Hertz-Knudsen
equation and assuming that only a small fraction of the gas particles,
$f$, hitting the electrolyte/gas interface ($x=0$) enters the liquid
\begin{equation}
k_{f}=f\left(2\pi M_{O_{2}}RT\right)
\end{equation}
where $M_{O_{2}}$ is the molecular weight of oxygen. At equilibrium
($N_{o}=0$), the concentration of oxygen in the electrolyte is computed
using Henry's law. The value of the Henry's law constant can be obtained
by imposing equilibrium conditions. Setting $N_{o}=0$, eqn. \ref{eq:oxygen_flux}
gives 
\begin{equation}
\frac{k_{f}}{k_{b}}=\frac{c_{o}(0)}{p_{o}}=k_{H}^{-1}\label{eq:oxygen_flux_equilibrium}
\end{equation}
The concentration at the electrode/gas interface ($x=0$) is computed
using Fickian diffusion 
\begin{equation}
N_{o}=-D_{\textrm{eff}}\nabla c_{o}(0)
\end{equation}
where $D_{\textrm{eff}}$ is the diffusion coefficient of oxygen in
the electrolyte. Assuming 1-D 
\begin{equation}
N_{o}=-D_{\textrm{eff}}\frac{\partial c_{o}(0)}{\partial x}\label{eq:Fickian_diffusion_x_0}
\end{equation}
The expression for dissolved oxygen concentration at $x=0$ is obtained
by using equations \ref{eq:oxygen_flux} and \ref{eq:Fickian_diffusion_x_0}
\[
k_{f}p_{o}-k_{b}c_{o}(0)=-D_{\textrm{eff}}\frac{\partial c_{o}(0)}{\partial x}
\]
$k_{f}$ and $k_{b}$ are related using eqn. \ref{eq:oxygen_flux_equilibrium}
\[
k_{f}\left(p_{o}-k_{H}c_{o}(0)\right)=-D_{\textrm{eff}}\frac{\partial c_{o}(0)}{\partial x}
\]
After rearranging the above equation, the final expression for oxygen
concentration dissolved at the electrode/gas boundary is 
\begin{equation}
\left.\frac{\partial c_{o}(x)}{\partial x}\right|_{x=0}=\left(\frac{k_{f}k_{H}}{D_{\textrm{eff}}}\right)c_{o}(0)-\frac{k_{f}p_{o}}{D_{\textrm{eff}}}
\end{equation}



\subsection{Steady-state analysis}

Under steady-state conditions the modified Fick's second law of diffusion
reduces to 
\begin{equation}
\frac{d^{2}c_{o}(x)}{dx^{2}}-\frac{kc_{o}(x)}{D_{\text{eff}}}=0\label{eq:Ficks_second_law_diffusion_kinetics}
\end{equation}
where $k$ is the reaction rate that can be described using the Butler-Volmer
equation. In this chapter we will assume the simplified case of $\beta=0.5$, but the same analysis can be performed using more accurate equations
for the reaction rate such as the one employed in the previous chapter.
Hence, we will assume 
\begin{equation}
k=-2k_{0}a\sinh\left(\frac{n}{2V_{T}}\eta\right)\label{eq:k_diffusion_kinetics}
\end{equation}
The solution of eqn.~ \ref{eq:Ficks_second_law_diffusion_kinetics}
is 
\begin{equation}
c_{o}(x)=C_{1}\cosh\left(\frac{x}{\lambda}\right)+C_{2}\sinh\left(\frac{x}{\lambda}\right)\label{eq:Li2O2_model_1_dc_ficks_second_law_solution_diff_kinetics}
\end{equation}
where $\lambda=\sqrt{\frac{D_{\textrm{eff}}}{k}}$, $\lambda$ describes
the diffusion length for oxygen in the electrolyte and $C_{1}$ and
$C_{2}$ are integration constants. These integration constants are
obtained by applying the following boundary conditions:\begin{subequations} 
	\begin{enumerate}
		\item []\textbf{Boundary condition at $x=0$} 
		\begin{equation}
		\left.\frac{\partial c_{o}\left(x\right)}{\partial x}\right|_{x=0}=\left(\frac{k_{f}k_{H}}{D_{\text{eff}}}\right)c_{o}(0)-\frac{k_{f}p_{o}}{D_{\text{eff}}}\label{bc:x=00003D0}
		\end{equation}
		
		\item []\textbf{Boundary condition at $x=l$} 
		\begin{equation}
		\left.\frac{dc_{o}\left(x\right)}{dx}\right|_{x=l}=0\label{bc:x=00003Dl}
		\end{equation}
	\end{enumerate}
\end{subequations} $C_{2}$ is obtained by applying boundary condition
(\ref{bc:x=00003Dl}) to eqn.~\ref{eq:Li2O2_model_1_dc_ficks_second_law_solution_diff_kinetics}
\[
\left.\frac{dc_{o}\left(x\right)}{dx}\right|_{x=l}=\frac{C_{1}}{\lambda}\sinh\left(\frac{x}{\lambda}\right)+\frac{C_{2}}{\lambda}\cosh\left(\frac{x}{\lambda}\right)=0
\]
and, after rearranging
\begin{equation}
C_{2}=-C_{1}\tanh\left(\frac{l}{\lambda}\right)
\end{equation}
Next, substitute the expression for $C_{2}$ into eqn~\ref{eq:Li2O2_model_1_dc_ficks_second_law_solution_diff_kinetics}
\begin{equation}
c_{o}\left(x\right)=C_{1}\left[\cosh\left(\frac{x}{\lambda}\right)-\tanh\left(\frac{l}{\lambda}\right)\sinh\left(\frac{x}{\lambda}\right)\right]\label{eq:diff_kinetics_co_partial}
\end{equation}
The integration constant $C_{1}$ is obtained by applying boundary
condition (eqn. \ref{bc:x=00003D0}) to eqn.~\ref{eq:diff_kinetics_co_partial}
and get 
\[
\left.\frac{\partial c_{o}\left(x\right)}{\partial x}\right|_{x=0}=-\frac{C_{1}}{\lambda}\tanh\left(\frac{l}{\lambda}\right)=\left(\frac{k_{f}k_{H}}{D_{\text{eff}}}\right)c_{o}(0)-\frac{k_{f}p_{o}}{D_{\text{eff}}}.
\]
Rewriting the above equation and using $c_{o}\left(0\right)$ from
eqn. \ref{eq:diff_kinetics_co_partial}, the above equation reduces
to 
\[
C_{1}=\frac{\lambda}{\tanh\left(\frac{l}{\lambda}\right)}\left[\frac{k_{f}p_{o}}{D_{\text{eff}}}-\left(\frac{k_{f}k_{H}}{D_{\text{eff}}}\right)\times C_{1}\right]
\]
After rearranging, the final expression for integration constant $C_{1}$
is 
\begin{equation}
C_{1}=C_{o}=\left[\frac{\lambda k_{f}p_{o}}{\tanh\left(\frac{l}{\lambda}\right)D_{\text{eff}}+\lambda k_{f}k_{H}}\right]\label{eq:diss_kinetics_C1_Co}
\end{equation}
The final expression to compute the spatial variation of the oxygen
concentration in the cathode of Li-air batteries by assuming finite
oxygen dissolution kinetics is 
\begin{equation}
c_{o}\left(x\right)=C_{o}\left[\cosh\left(\frac{x}{\lambda}\right)-\tanh\left(\frac{l}{\lambda}\right)\sinh\left(\frac{x}{\lambda}\right)\right]\label{eq:dc_oxygen_concentration_diffusion_kinetics}
\end{equation}
where $C_{o}$ is a constant that is given by eqn. \ref{eq:diss_kinetics_C1_Co}
and depends upon diffusion length of the oxygen ($\lambda$), diffusion
coefficient of oxygen in the electrolyte ($D_{\text{eff}}$), partial
pressure of oxygen in the region just outside the cathode ($p_{o}$),
rate of oxygen dissolving in the electrolyte ($k_{f}$), and Henry's
law constant ($k_{H}$). 

The Faradaic current is computed using 
\begin{equation}
i_{F}=A\int_{0}^{l}R_{c}\,\textrm{d}x\label{eq:diss_kinetics_dc_iF_integral}
\end{equation}
where $R_{c}$ is given by 
\begin{equation}
R_{c}=nFkc_{o}\left(x\right)
\end{equation}
Using eqn. \ref{eq:dc_oxygen_concentration_diffusion_kinetics} and
$R_{c}$, the relation for Faradaic current becomes 
\[
i_{F}=nFAkC_{o}\int_{0}^{l}\left[\cosh\left(\frac{x}{\lambda}\right)-\tanh\left(\frac{l}{\lambda}\right)\sinh\left(\frac{x}{\lambda}\right)\right]\,\textrm{d}x
\]
After integration, applying boundary conditions, and using $\lambda^{2}=\tfrac{D_{\textrm{eff}}}{k}$
the final expression for the Faradaic current becomes 
\begin{equation}
i_{F}=\frac{nFAC_{o}D_{\textrm{eff}}}{\lambda}\tanh\left(\frac{l}{\lambda}\right)\label{eq:faradaic_current_w_kinetics_diffusion}
\end{equation}



\section{Impedance spectra under d.c. discharge}

The small-signal Faradaic current is computed by applying the perturbation
theory to eqn. \ref{eq:diss_kinetics_dc_iF_integral}. The small-signal
representation of Faradaic current, over-potential and oxygen concentration
is $\delta i_{F}e^{j\omega t}$, $\delta\eta e^{j\omega t}$, and
$\delta c_{o}(x)e^{j\omega t}$ respectively. Substituting $i_{F}+\delta i_{F}e^{j\omega t}$,
$k+\delta ke^{j\omega t}$, and $c_{o}\left(x\right)+\delta c_{o}\left(x\right)e^{j\omega t}$
into
\begin{equation}
i_{F}=nFAk\int_{0}^{l}c_{o}\left(x\right)\,\textrm{d}x,
\end{equation}
leads to
\begin{equation}
i_{F}+\delta i_{F}e^{j\omega t}=nFA\left(k+\delta ke^{j\omega t}\right)\int_{0}^{l}\left(c_{o}\left(x\right)+\delta c_{o}\left(x\right)e^{j\omega t}\right)\;dx
\end{equation}
\begin{multline}
i_{F}+\delta i_{F}e^{j\omega t}=nFA\left[k\int_{0}^{l}c_{o}\left(x\right)\;dx+k\int_{0}^{l}\delta c_{o}\left(x\right)e^{j\omega t}\;dx\right. \\
\left.+\delta ke^{j\omega t}\int_{0}^{l}\delta c_{o}\left(x\right)e^{j\omega t}\;dx+\delta ke^{j\omega t}\int_{0}^{l}c_{o}\left(x\right)\;dx\right]\label{eq:Small_signal}
\end{multline}
Using equation~\ref{eq:diss_kinetics_dc_iF_integral} and neglecting
the third term (since the product $\delta c_{o}\times\delta k$ is
negligible) in eqn. \ref{eq:Small_signal} results in the following
expression 
\begin{equation}
\delta i_{F}=nFA\left[k\int_{0}^{l}\delta c_{o}\left(x\right)\;dx+\delta k\int_{0}^{l}c_{o}\left(x\right)\;dx\right]\label{eq:small_signal_faradaic_current_for_diffusion_kinetics}
\end{equation}
The last equation gives the expression of small-signal Faradaic current,
where $k$ is given by eqn. \ref{eq:k_diffusion_kinetics}, $c_{o}\left(x\right)$
is given by eqn. \ref{eq:dc_oxygen_concentration_diffusion_kinetics},
$\delta k$ is obtained by linearizing eqn. \ref{eq:k_diffusion_kinetics},
and$\delta c_{o}\left(x\right)$ is obtained by solving the small-signal
form of the Fick's second law of diffusion 
\begin{equation}
\frac{d^{2}\delta c_{o}\left(x\right)}{dx^{2}}-\frac{\left(k+j\omega\epsilon_{0}\right)}{D_{\text{eff}}}\delta c_{o}\left(x\right)=\frac{\delta k\,c_{o}\left(x\right)}{D_{\text{eff}}}\label{eq:small_signal_ficks_second_law_for_diffusion_kinetics}
\end{equation}
The homogeneous solution of the previous differential equation is
\begin{equation}
\delta c_{o,c}=C_{1}\cosh\left(\frac{x}{\delta\lambda}\right)+C_{2}\sinh\left(\frac{x}{\delta\lambda}\right)\label{eq:complementary_solution_ficks_second_law_for_diffusion_kinetics}
\end{equation}
where $C_{1}$ and $C_{2}$ are the integration constants and $\delta\lambda=\sqrt{\frac{D_{\text{eff}}}{\left(k+j\omega\epsilon_{0}\right)}}$.
The particular solution of the second-order non-homogeneous differential
equation is 
\begin{equation}
\delta c_{o,p}=C_{3}\cosh\left(\frac{x}{\lambda}\right)+C_{4}\sinh\left(\frac{x}{\lambda}\right)\label{eq:particular_solution_ficks_second_law_for_diffusion_kinetics}
\end{equation}
where $C_{3}$ and $C_{4}$ are the integration constants. The complete
solution is 
\begin{equation}
\delta c_{o}\left(x\right)=C_{1}\cosh\left(\frac{x}{\delta\lambda}\right)+C_{2}\sinh\left(\frac{x}{\delta\lambda}\right)+C_{3}\cosh\left(\frac{x}{\lambda}\right)+C_{4}\sinh\left(\frac{x}{\lambda}\right)\label{eq:total_small_signal_oxygen_concentration_solution_for_diffusion_kinetics}
\end{equation}
The expressions of integration constants $C_{3}$ and $C_{4}$ are
obtained by substituting eqn.~\ref{eq:particular_solution_ficks_second_law_for_diffusion_kinetics}
into eqn.~\ref{eq:small_signal_ficks_second_law_for_diffusion_kinetics}
and then comparing the left and right sides of the resulting equation.
Substituting \ref{eq:particular_solution_ficks_second_law_for_diffusion_kinetics}
into the L.H.S of eqn. \ref{eq:small_signal_ficks_second_law_for_diffusion_kinetics},
the L.H.S. of this equation becomes 
\[
L.H.S=\frac{d^{2}}{dx^{2}}\left[C_{3}\cosh\left(\frac{x}{\lambda}\right)+C_{4}\sinh\left(\frac{x}{\lambda}\right)\right]-\frac{\left(k+j\omega\epsilon_{0}\right)}{D_{\text{eff}}}\left[C_{3}\cosh\left(\frac{x}{\lambda}\right)+C_{4}\sinh\left(\frac{x}{\lambda}\right)\right]
\]
and after simplification 
\begin{equation}
L.H.S=-\frac{j\omega\epsilon_{0}}{D_{\text{eff}}}\left[C_{3}\cosh\left(\frac{x}{\lambda}\right)+C_{4}\sinh\left(\frac{x}{\lambda}\right)\right]
\end{equation}
Now, substituting \ref{eq:particular_solution_ficks_second_law_for_diffusion_kinetics}
into the R.H.S of eqn. \ref{eq:small_signal_ficks_second_law_for_diffusion_kinetics}
gives 
\begin{equation}
R.H.S=\frac{\delta k\,C_{o}}{D_{\text{eff}}}\left[\cosh\left(\frac{x}{\lambda}\right)-\tanh\left(\frac{l}{\lambda}\right)\sinh\left(\frac{x}{\lambda}\right)\right]
\end{equation}
Equating the L.H.S. and the R.H.S. gives 
\[
-j\omega\epsilon_{0}\left[C_{3}\cosh\left(\frac{x}{\lambda}\right)+C_{4}\sinh\left(\frac{x}{\lambda}\right)\right]=\delta k\,C_{o}\left[\cosh\left(\frac{x}{\lambda}\right)-\tanh\left(\frac{l}{\lambda}\right)\sinh\left(\frac{x}{\lambda}\right)\right]
\]
The expressions for integration constants $C_{3}$ and $C_{4}$ are
obtained by comparing 
\begin{align}
C_{3} & =-\frac{\delta k\,C_{o}}{j\omega\epsilon_{0}}\\
C_{4} & =\frac{\delta k\,C_{o}}{j\omega\epsilon_{0}}\tanh\left(\frac{l}{\lambda}\right)
\end{align}
Substituting the expressions of $C_{3}$ and $C_{4}$ into eqn. \ref{eq:total_small_signal_oxygen_concentration_solution_for_diffusion_kinetics},
the complete solution for eqn. \ref{eq:small_signal_ficks_second_law_for_diffusion_kinetics}
becomes
\begin{equation}
\delta c_{o}\left(x\right)=C_{1}\cosh\left(\frac{x}{\delta\lambda}\right)+C_{2}\sinh\left(\frac{x}{\delta\lambda}\right)-\frac{\delta k\,C_{o}}{j\omega\epsilon_{0}}\cosh\left(\frac{x}{\lambda}\right)+\frac{\delta k\,C_{o}}{j\omega\epsilon_{0}}\tanh\left(\frac{l}{\lambda}\right)\sinh\left(\frac{x}{\lambda}\right)\label{eq:small_signal_oxygen_concentration_w_C3_C4_diffusion_kinetics}
\end{equation}
The values of integration constants $C_{1}$ and $C_{2}$ are obtained
by imposing the following boundary conditions

\begin{subequations} 
	\begin{enumerate}
		\item []\textbf{Boundary condition at $x=0$} 
		\begin{align}
		\left.\frac{\partial\,\delta c_{o}\left(x\right)}{\partial x}\right|_{x=0}=\left(\frac{k_{f}k_{H}}{D_{\text{eff}}}\right) & \delta c_{o}\left(0\right)\label{bc:small_signal_x=00003D0}
		\end{align}
		
		\item []\textbf{Boundary condition at $x=l$} 
		\begin{align}
		\left.\frac{d\,\delta c_{o}(0)}{dx}\right|_{x=l}= & 0\label{bc:small_signal_x=00003Dl}
		\end{align}
	\end{enumerate}
\end{subequations} 

Integration constant $C_{2}$ is obtained using the boundary condition
\ref{bc:small_signal_x=00003Dl} 
\begin{equation}
C_{2}=-C_{1}\tanh\left(\frac{l}{\delta\lambda}\right)\label{eq:small_signal_C2_intermediate_diffusion_kinetics}
\end{equation}
and $C_{1}$ is obtained by applying boundary condition~\ref{bc:small_signal_x=00003D0}.
Substituting eqn. \ref{eq:small_signal_oxygen_concentration_w_C3_C4_diffusion_kinetics}
into eqn. \ref{bc:small_signal_x=00003Dl}, taking the derivative,
and applying the boundary conditions results in 
\[
\frac{C_{2}}{\delta\lambda}+\frac{\delta k\,C_{o}}{j\omega\epsilon_{0}\lambda}\tanh\left(\frac{l}{\lambda}\right)=\left(\frac{k_{f}k_{H}}{D_{\text{eff}}}\right)\delta c_{o}(0)
\]
Substituting $C_{2}$ from eqn. \ref{eq:small_signal_C2_intermediate_diffusion_kinetics}
leads to 
\[
\frac{-C_{1}}{\delta\lambda}\tanh\left(\frac{l}{\delta\lambda}\right)+\frac{\delta k\,C_{o}}{j\omega\epsilon_{0}\lambda}\tanh\left(\frac{l}{\lambda}\right)=\left(\frac{k_{f}k_{H}}{D_{\text{eff}}}\right)\delta c_{o}(0)
\]
Next, by evaluating $\delta c_{o}(x)$ at $x=0$ in eqn.~\ref{eq:small_signal_oxygen_concentration_w_C3_C4_diffusion_kinetics}
and using the last equation gives 
\[
\frac{C_{1}}{\delta\lambda}\tanh\left(\frac{l}{\delta\lambda}\right)=\frac{\delta k\,C_{o}}{j\omega\epsilon_{0}\lambda}\tanh\left(\frac{l}{\lambda}\right)-\left(\frac{k_{f}k_{H}}{D_{\text{eff}}}\right)\left[C_{1}-\frac{\delta k\,C_{o}}{j\omega\epsilon_{0}}\right]
\]
Rearranging the above equation yields the value of integration constant
$C_{1}$ 
\begin{equation}
C_{1}=\frac{\delta k\,C_{o}}{j\omega\epsilon_{0}}\frac{\frac{1}{\lambda}\tanh\left(\frac{l}{\lambda}\right)+\left(\frac{k_{f}k_{H}}{D_{\text{eff}}}\right)}{\frac{1}{\delta\lambda}\tanh\left(\frac{l}{\delta\lambda}\right)+\left(\frac{k_{f}k_{H}}{D_{\text{eff}}}\right)}\label{eq:small_signal_C1_intermediate_diffusion_kinetics}
\end{equation}
By replacing constant $C_{1}$ in eqn. \ref{eq:small_signal_C2_intermediate_diffusion_kinetics}
results in the final expression for integration constant $C_{2}$
\begin{equation}
C_{2}=-\frac{\delta k\,C_{o}}{j\omega\epsilon_{0}}\frac{\frac{1}{\lambda}\tanh\left(\frac{l}{\lambda}\right)+\left(\frac{k_{f}k_{H}}{D_{\text{eff}}}\right)}{\frac{1}{\delta\lambda}\tanh\left(\frac{l}{\delta\lambda}\right)+\left(\frac{k_{f}k_{H}}{D_{\text{eff}}}\right)}\tanh\left(\frac{l}{\delta\lambda}\right)\label{eq:small_signal_C2_diffusion_kinetics}
\end{equation}
The complete solution of the differential eqn. \ref{eq:small_signal_ficks_second_law_for_diffusion_kinetics}
after replacing the integration constants $C_{1}$ (eqn. \ref{eq:small_signal_C1_intermediate_diffusion_kinetics})
and $C_{2}$ (eqn. \ref{eq:small_signal_C2_diffusion_kinetics}) into
eqn. \ref{eq:small_signal_oxygen_concentration_w_C3_C4_diffusion_kinetics}
is 
\begin{multline*}
\delta c_{o}\left(x\right) =\frac{\delta k\,C_{o}}{j\omega\epsilon_{0}}\left[\frac{\frac{1}{\lambda}\tanh\left(\frac{l}{\lambda}\right)+\left(\frac{k_{f}k_{H}}{D_{\text{eff}}}\right)}{\frac{1}{\delta\lambda}\tanh\left(\frac{l}{\delta\lambda}\right)+\left(\frac{k_{f}k_{H}}{D_{\text{eff}}}\right)}\cosh\left(\frac{x}{\delta\lambda}\right)+\tanh\left(\frac{l}{\lambda}\right)\sinh\left(\frac{x}{\lambda}\right)\right.\\
\left.-\cosh\left(\frac{x}{\lambda}\right)-\frac{\frac{1}{\lambda}\tanh\left(\frac{l}{\lambda}\right)+\left(\frac{k_{f}k_{H}}{D_{\text{eff}}}\right)}{\frac{1}{\delta\lambda}\tanh\left(\frac{l}{\delta\lambda}\right)+\left(\frac{k_{f}k_{H}}{D_{\text{eff}}}\right)}\tanh\left(\frac{l}{\delta\lambda}\right)\sinh\left(\frac{x}{\delta\lambda}\right)\right]
\end{multline*}
The final expression for the small-signal oxygen concentration $\delta c_{o}\left(x\right)$
is 
\begin{multline}
\delta c_{o}\left(x\right)=\frac{\delta k\,C_{o}}{j\omega\epsilon_{0}} \frac{\frac{1}{\lambda}\tanh\left(\frac{l}{\lambda}\right)+\left(\frac{k_{f}k_{H}}{D_{\text{eff}}}\right)}{\frac{1}{\delta\lambda}\tanh\left(\frac{l}{\delta\lambda}\right)+\left(\frac{k_{f}k_{H}}{D_{\text{eff}}}\right)}\left[\cosh\left(\frac{x}{\delta\lambda}\right)-\tanh\left(\frac{l}{\delta\lambda}\right)\sinh\left(\frac{x}{\delta\lambda}\right)\right] \\
+\frac{\delta k\,C_{o}}{j\omega\epsilon_{0}}\left[-\cosh\left(\frac{x}{\lambda}\right)+\tanh\left(\frac{l}{\lambda}\right)\sinh\left(\frac{x}{\lambda}\right)\right]\label{eq:final_equation_for_small_signal_oxygen_concentration_under_diffusion_kinetics}
\end{multline}
Finally, the small-signal Faradaic current $\delta i_{F}$ can be
computed 
\begin{align}
\delta i_{F} & =nFA\left[k\int_{0}^{l}\delta c_{o}(x)\;dx+\delta k\int_{0}^{l}c_{o}(x)\;dx\right]\nonumber \\
& =\qquad\qquad\qquad\textrm{int}_{1}\qquad\quad\,\,+\qquad\textrm{int}_{2}\label{eq:diff_kinetics_di_f_with_int1_and_int2}
\end{align}
where the integral was divided into two parts: $\textrm{int}_{1}$
and $\textrm{int}_{2}$. Integral int$_{2}$ is computed by substituting
$c_{o}(x)$ from eqn. \ref{eq:dc_oxygen_concentration_diffusion_kinetics}
into eqn. \ref{eq:diff_kinetics_di_f_with_int1_and_int2} 
\[
\textrm{int}_{2}=nFA\delta kC_{o}\int_{0}^{l}\cosh\left(\frac{x}{\lambda}\right)\;dx-nFA\delta kC_{o}\int_{0}^{l}\tanh\left(\frac{l}{\lambda}\right)\sinh\left(\frac{x}{\lambda}\right)\;dx
\]
Solving the integral results in
\begin{equation}
\textrm{int}_{2}=nFA\lambda\delta kC_{o}\tanh\left(\frac{l}{\lambda}\right)\label{eq:int_2_diffusion_kinetics_df}
\end{equation}
Next, $\textrm{int}_{1}$ is computed by substituting $\delta c_{o}\left(x\right)$
with eqn. \ref{eq:final_equation_for_small_signal_oxygen_concentration_under_diffusion_kinetics}
\begin{multline*}
\textrm{int}_{1}=\frac{nFkA\delta k\,C_{o}}{j\omega\epsilon_{0}}\frac{\frac{1}{\lambda}\tanh\left(\frac{l}{\lambda}\right)+\left(\frac{k_{f}k_{H}}{D_{\text{eff}}}\right)}{\frac{1}{\delta\lambda}\tanh\left(\frac{l}{\delta\lambda}\right)+\left(\frac{k_{f}k_{H}}{D_{\text{eff}}}\right)}\left[\int_{0}^{l}\cosh\left(\frac{x}{\delta\lambda}\right)\;dx-\int_{0}^{l}\tanh\left(\frac{l}{\delta\lambda}\right)\sinh\left(\frac{x}{\delta\lambda}\right)\;dx\right]\\
+\frac{nFkA\delta k\,C_{o}}{j\omega\epsilon_{0}}\left[\int_{0}^{l}\tanh\left(\frac{l}{\lambda}\right)\sinh\left(\frac{x}{\lambda}\right)\;dx-\int_{0}^{l}\cosh\left(\frac{x}{\lambda}\right)\;dx\right]
\end{multline*}
after integrating and applying boundary conditions the final expression
for $\textrm{int}_{1}$ is 
\begin{equation}
\textrm{int}_{1}=nFkA\delta k\,C_{o}\left[\frac{\delta\lambda}{j\omega\epsilon_{0}}\frac{\frac{1}{\lambda}\tanh\left(\frac{l}{\lambda}\right)+\left(\frac{k_{f}k_{H}}{D_{\text{eff}}}\right)}{\frac{1}{\delta\lambda}\tanh\left(\frac{l}{\delta\lambda}\right)+\left(\frac{k_{f}k_{H}}{D_{\text{eff}}}\right)}\tanh\left(\frac{l}{\delta\lambda}\right)-\frac{\lambda}{j\omega\epsilon_{0}}\tanh\left(\frac{l}{\lambda}\right)\right]\label{eq:int_1_diffusion_kinetics_df}
\end{equation}
Substituting integrals int$_{1}$ (eqn. \ref{eq:int_1_diffusion_kinetics_df})
and int$_{2}$ (eqn. \ref{eq:int_2_diffusion_kinetics_df}) back into
eqn. \ref{eq:diff_kinetics_di_f_with_int1_and_int2} and rearranging
the expression for $\delta i_{F}$ gives 
\[
\delta i_{F}=nFk\delta kA\,C_{o}\left[\frac{\delta\lambda}{j\omega\epsilon_{0}}\frac{\frac{1}{\lambda}\tanh\left(\frac{l}{\lambda}\right)+\left(\frac{k_{f}k_{H}}{D_{\text{eff}}}\right)}{\frac{1}{\delta\lambda}\tanh\left(\frac{l}{\delta\lambda}\right)+\left(\frac{k_{f}k_{H}}{D_{\text{eff}}}\right)}\tanh\left(\frac{l}{\delta\lambda}\right)-\frac{\lambda}{j\omega\epsilon_{0}}\tanh\left(\frac{l}{\lambda}\right)+\frac{\lambda}{k}\tanh\left(\frac{l}{\lambda}\right)\right]
\]
The final form of the small-signal Faradaic current $\delta i_{F}$
is 
\begin{equation}
\delta i_{F}=\frac{i_{F}\delta k}{k}\left[\frac{k\delta\lambda}{j\omega\epsilon_{0}\lambda}\frac{\frac{1}{\lambda}\tanh\left(\frac{l}{\lambda}\right)+\left(\frac{k_{f}k_{H}}{D_{\text{eff}}}\right)}{\frac{1}{\delta\lambda}\tanh\left(\frac{l}{\delta\lambda}\right)+\left(\frac{k_{f}k_{H}}{D_{\text{eff}}}\right)}\frac{\tanh\left(\frac{l}{\delta\lambda}\right)}{\tanh\left(\frac{l}{\lambda}\right)}-\frac{k}{j\omega\epsilon_{0}}+1\right]\label{eq:di_dk_diffusion_kinetics}
\end{equation}
The Faradaic impedance is given by, 
\begin{equation}
Z_{F}\left(\omega\right)=-\frac{\delta n}{\delta i_{F}}=-\frac{\delta\eta}{\delta k}\frac{\delta k}{\delta i_{F}}
\end{equation}
where $\frac{\delta k}{\delta i_{F}}$ is given by eqn.~\ref{eq:di_dk_diffusion_kinetics}
and $\frac{\delta\eta}{\delta k}$ is obtained by linearizing eqn.~\ref{eq:k_diffusion_kinetics}
and is given by 
\begin{equation}
\frac{\delta k}{\delta\eta}=\frac{2V_{T}}{nk}\tanh\left(\frac{n}{2V_{T}}\eta\right)\label{eq:dn_dk_diffusion_kinetics}
\end{equation}
The Faradaic impedance is given by 
\[
Z_{F}\left(\omega\right)=-\frac{2V_{T}}{ni_{F}}\frac{\tanh\left(\frac{n}{2V_{T}}\eta\right)}{\frac{k\delta\lambda}{j\omega\epsilon_{0}\lambda}\frac{\frac{1}{\lambda}\tanh\left(\frac{l}{\lambda}\right)+\left(\frac{k_{f}k_{H}}{D_{\text{eff}}}\right)}{\frac{1}{\delta\lambda}\tanh\left(\frac{l}{\delta\lambda}\right)+\left(\frac{k_{f}k_{H}}{D_{\text{eff}}}\right)}\frac{\tanh\left(\frac{l}{\delta\lambda}\right)}{\tanh\left(\frac{l}{\lambda}\right)}-\frac{k}{j\omega\epsilon_{0}}+1}
\]
\[
Z_{F}\left(\omega\right)=-\frac{2V_{T}}{ni_{F}}\frac{\tanh\left(\frac{n}{2V_{T}}\eta\right)}{\frac{k\delta\lambda^{2}}{j\omega\epsilon_{0}\lambda^{2}}\frac{\tanh\left(\frac{l}{\lambda}\right)+\lambda\left(\frac{k_{f}k_{H}}{D_{\text{eff}}}\right)}{\tanh\left(\frac{l}{\delta\lambda}\right)+\delta\lambda\left(\frac{k_{f}k_{H}}{D_{\text{eff}}}\right)}\frac{\tanh\left(\frac{l}{\delta\lambda}\right)}{\tanh\left(\frac{l}{\lambda}\right)}-\frac{k}{j\omega\epsilon_{0}}+1}
\]
Since $\delta\lambda=\sqrt{\frac{D_{\text{eff}}}{\left(k+j\omega\epsilon_{0}\right)}}$
and $\lambda=\sqrt{\frac{D_{\textrm{eff}}}{k}}$ the final expression
for Faradaic current for Li-air batteries under d.c. discharge using
dissolution kinetics can be rewritten as 
\begin{equation}
\boxed{Z_{F}\left(\omega\right)=\frac{Z_{0}\tanh\left(\frac{n}{2V_{T}}\eta\right)}{1-\frac{k}{j\omega\epsilon_{0}}+\frac{k\sqrt{k}}{j\omega\epsilon_{0}\sqrt{k+j\omega\epsilon_{0}}}\frac{\tanh\left(\sqrt{\frac{k+j\omega\epsilon_{0}}{D_{\text{eff}}}}l\right)}{\tanh\left(\sqrt{\frac{k}{D_{\textrm{eff}}}}l\right)}f_{diss}\left(\omega,\eta\right)}}\label{eq:ZF_final_dissolution_kinetics}
\end{equation}
where $Z_{0}$ is 
\begin{equation}
Z_{0}=\frac{-2V_{T}}{ni_{F}}\label{eq:Z0_final_dissolution_kinetics}
\end{equation}
and
\begin{equation}
f_{diss}\left(\omega,\eta\right)=\frac{\sqrt{D_{\text{eff}}k}\tanh\left(\sqrt{\frac{k}{D_{\textrm{eff}}}}l\right)+k_{f}k_{H}}{\sqrt{D_{\text{eff}}\left(k+j\omega\epsilon_{0}\right)}\tanh\left(\sqrt{\frac{k+j\omega\epsilon_{0}}{D_{\text{eff}}}}l\right)+k_{f}k_{H}}\label{eq:f_dissolution_kinetics}
\end{equation}



\section{The case of high dissolution kinetics}

In chapters \ref{chap:EIS_under_dc_discharge}-\ref{chap:Expt_verification}
it was assumed that the rate at which oxygen is dissolving in the
electrolyte is very high (i.e. the consumption of oxygen in the Faradaic
reaction is much slower than the amount of \ce{O2} dissolving in
the electrolyte). In such a case, $k_{f}$ is very high, the dissolution
of oxygen is no longer the limiting factor. The Faradaic impedance
in this case is obtained by setting $k_{f}\rightarrow\infty$ or $1/k_{f}\rightarrow0$
in eqn.~\ref{eq:f_dissolution_kinetics}. 

To take the limit $k_{f}\rightarrow\infty$ it is instrumental to
rewrite eqn.~\ref{eq:f_dissolution_kinetics} in terms of $1/k_{f}$
\begin{equation}
f_{diss}\left(\omega,\eta\right)=\frac{\frac{1}{k_{f}}\sqrt{D_{\text{eff}}k}\tanh\left(\sqrt{\frac{k}{D_{\textrm{eff}}}}l\right)+k_{H}}{\frac{1}{k_{f}}\sqrt{D_{\text{eff}}\left(k+j\omega\epsilon_{0}\right)}\tanh\left(\sqrt{\frac{k+j\omega\epsilon_{0}}{D_{\text{eff}}}}l\right)+k_{H}}
\end{equation}
Under the condition of high dissolution kinetics ($\frac{1}{k_{f}}\rightarrow0$)
$f_{diss}\left(\omega,\eta\right)=1$ and the expression for the Faradaic
impedance (eqn. \ref{eq:ZF_final_dissolution_kinetics}) reduces to
\begin{equation}
Z_{F}=\frac{Z_{0}\tanh\left(\frac{n}{2V_{T}}\eta\right)}{\left[1-\frac{k}{j\omega\epsilon_{0}}+\frac{k\sqrt{k}}{j\omega\epsilon_{0}\sqrt{k+j\omega\epsilon_{0}}}\frac{\tanh\left(\sqrt{\frac{k+j\omega\epsilon_{0}}{D_{\text{eff}}}}l\right)}{\tanh\left(\sqrt{\frac{k}{D_{\textrm{eff}}}}l\right)}\right]}
\end{equation}
which is identical to eqn. \ref{eq:ZF_final_sinh} obtained in chapter
\ref{chap:EIS_under_dc_discharge}.
\end{document}