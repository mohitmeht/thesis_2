\documentclass[../thesis.tex]{subfiles}
\providecommand{\main}{../}
\begin{document}
	
\chapter{Introduction}
\label{chap:Introduction}
\section{Electrochemical impedance spectroscopy}
Electrochemical impedance spectroscopy (EIS) is a versatile measurement tool that measures the dynamic response of a system to a spectrum of sinusoidal a.c. perturbation signals, and the impedance of the system is calculated as the ratio of the small signal voltage over small-signal current. EIS can be performed on a battery during discharge (in-situ) as well as at different stages of discharge (ex-situ). One of the salient features of impedance spectroscopy is that it can measure the impedance response of a system non-destructively. 

Different physical processes, such as ion diffusion, intercalation diffusion, electrochemical reactions, and electrical double layer, have different time constants and can be identified easily using the impedance spectra easily. EIS is also an analytical tool that can provide information such as state-of-charge~\cite{Huet1998,Sikha2007,Slepski2009,Gambhire2013,Seaman2014}, state-of-health~\cite{Huet1998,Slepski2009}, degradation of electrode materials~\cite{Kuriyama1992,Sikha2007,Mellgren2008,Woo2014}, electrode reaction~\cite{Sikha2007,Bandarenka2013}, morphological or geometrical changes inside the cathode~\cite{Bardenhagen2015}, mass-transport processes~\cite{Ivanishchev2008}, and intercalation~\cite{Slepski2014} in a battery. EIS can also be used to extract quantitative information about the electrochemical system such as the values of diffusion coefficient~\cite{Guo2002,Mellgren2008,Mehta2013} and reaction rates~\cite{Dhanda2011,Mehta2013}. 

In order to extract such information one needs to develop a theoretical model that describes the electrochemical system comprehensively by considering the material, geometrical and electrochemical properties, as well as the physical processes in a battery. The common modeling techniques include equivalent circuit modeling~\cite{Ivanishchev2008,Kulikovsky2012,Momma2012,Mn2012,Woo2014,Dilimon2015}, transmission line modeling~\cite{Malevich2012,Makharia2005}, and phenomenological modeling~\cite{Springer1996,Meyers2000,Dhanda2011,Mehta2013}. Among these techniques, phenomenological models can provide important insights about the physical and electrochemical processes that take place during charge and discharge, thus, can help understanding the operation of the system most precisely. The other two techniques are computationally much faster but have a number of drawbacks and limitations. In particular, they are usually based on curve fitting and sometimes have a pure mathematical and non-physical foundation. EIS measurements are usually represented using either Nyquist plot or Bode plot. A Nyquist diagram is a plot of real (abscissa) and imaginary (ordinate) impedance at different measurement frequencies. A typical Nyquist plot for a Li-air battery is shown in figure~\ref{fig:Bode_Nyquist_plot_example}a, where the semicircle and the asymptotic line represent two different physical processes. The X-intercepts show the values of the different resistances of the Li-air battery. A typical Bode plot for the same impedance response is shown in figure~\ref{fig:Bode_Nyquist_plot_example}b. 
\begin{figure}[tb!]
\centering \includegraphics[width=\linewidth]{\main figures/Bode_Nyquist_plot}
\protect\caption{Examples for representing the data of the impedance spectra: (a) A typical Nyquist plot for a Li-air battery with the real value of the impedance on the x-axis and the imaginary value of the impedance on the y-axis. (b) A typical Bode plot with the absolute value and the phase angle of the impedance are plotted for different values of frequency corresponding to the Nyquist plot represented in (a).}
\label{fig:Bode_Nyquist_plot_example} 
\end{figure}

\subsection{Common EIS measuring techniques}
The a.c. response of a system can be measured using a number of techniques such as galvanostatic, potentiostatic, wavelet, Fourier transform, cyclic voltammetry at different frequencies, and demodulation of a noisy signal into its frequency components \cite{Rosvall2000}. The most common measuring methods that are used in literature and deployed by instrument manufacture companies are galvanostatic and potentiostatic. One of the reasons for their widespread acceptance is that they can be applied to a large number of electrochemical systems.

\subsubsection{Potentiostatic}
In a potentiostatic configuration (figure~\ref{fig:Potentiostatic_measurement}), a superposition of a small-signal a.c. voltage ($\delta v$) and a d.c. voltage bias ($v$) is applied to the system under test and it's response is measured. The impedance of the system is given as the small-signal voltage divided by the small-signal current ($\delta v/\delta i$). The applied small-signal voltage is usually limited to less than \SI{10}{\milli\volt} to meet the linearity condition for the impedance response (i.e. to be much smaller than the thermal voltage). 

\subsubsection{Galvanostatic}
In a galvanostatic configuration (figure~\ref{fig:Galvanostatic_measurement}),
a small-signal a.c. current ($\delta i$) is superimposed on a d.c.
current ($i$) and applied to the system being measured. The impedance
of the system is given by the small-signal voltage divided by the
small-signal current ($\delta v/\delta i$). The applied small-signal
current is limited to relatively low magnitudes, which can be estimated
by imposing the small-signal voltage response to be less than \SI{10}{\milli\volt}.
\begin{figure}[t!]
	\begin{center}\includegraphics[width=\linewidth]{\main figures/Potentiostatic}\end{center}
	\caption{Network for measuring the impedance spectra of a battery using the
		potentiostatic measuring technique.}
	\label{fig:Potentiostatic_measurement} 
\end{figure}
\begin{figure}[b!]
\centering \includegraphics[width=\linewidth]{\main figures/Galvanostatic}
\protect\caption{Network for measuring the impedance spectra of a battery using the
galvanostatic measuring technique.}
\label{fig:Galvanostatic_measurement} 
\end{figure}

\section{Modeling of EIS spectra in electrochemical systems}
EIS is widely used in electrochemical measurements because it has high sensitivity and has the ability to separate the effects of different processes. The ability to decouple different electrochemical processes depends upon the accuracy and completeness of the model that is used to describe the system under study. The EIS models can be classified into physical and mathematical models. A physical model can be used to predict the response of the system by taking into consideration the underlying physical phenomena. On the other hand, a mathematical model can be used to fit the experimental data to a phenomenological equation and often has a limited or no physical interpretation.
\subsection{Phenomenological modeling}
\subsubsection{Transport and reaction kinetics in electrochemical systems}
\paragraph{Mass and charge transport equations}
The movement of ions and other molecules in a liquid electrolyte can be described using the mass and charge transport equations. The concentration of the gas in the electrolyte is directly proportional to the partial pressure of the gas just outside the electrolyte, which can be expressed mathematically as (Henry's law)
\begin{equation}
c=p\times k_{H}
\end{equation}
where $c$ is the concentration in the electrolyte, $p$ is the partial pressure of the gas above the electrolyte, and $k_{H}$ is the proportionality constant and is known as Henry's law constant. The Henry's law constant depends on the electrolyte and the temperature of the solution.

The movement of ions or molecules in a porous medium can described by the following Poisson-Nernst-Planck (PNP) equation
\begin{equation}
\mathbf{J_{i}}=-D_{\textrm{eff},i}\nabla c_{i}-\frac{z_{i}}{V_{T}}D_{\textrm{eff},i}c_{i}\nabla\phi+c_{i}\mathbf{v}
\end{equation}
where $\mathbf{J_{i}}$ is the flux vector of species $i$, $\nabla$ is the gradient operator, $D_{\textrm{eff},i}$ is the effective diffusion coefficient, $\nabla c_{i}$ is the concentration gradient, $\nabla\phi$ is the electric field, $V_{T}$ is the thermal voltage, $\mathbf{v}$ is the velocity vector, $z_{i}$ and $c_{i}$ are the charge and concentration of species $i$, respectively. The Poisson-Nernst-Planck equation contains three terms: diffusion, migration, and convection. The diffusion is due to the concentration gradient, the migration is due to the influence of the electric field, and the convection can be forced (laminar or turbulent flow, etc.) or natural (density gradients).

The effective diffusion coefficient in the porous medium can be related to the diffusion coefficient in the bulk by the following expression\cite{Petersen1958,Parada2011,Chung2013} \begin{equation}
D_{\textrm{eff},i}=D_{i}\frac{\epsilon_{0}}{\tau}
\end{equation}
where $\tau$ is the tortuosity.

In the case of neutral species and in the absence of convection the PNP equation reduces to Fick's first law of diffusion
\begin{equation}
\mathbf{J_{i}}=-D_{\textrm{eff},i}\nabla c_{i}
\end{equation}
By applying the continuity equation one obtains (Fick's second law of diffusion)
\begin{equation}
\frac{\partial c_{i}}{\partial t}=D_{\textrm{eff},i}\nabla^{2}c_{i}
\end{equation}
\paragraph{Electrode kinetics}

The charge transfer reaction taking place on the surface of the electrode
governs the electrode kinetics.The charge transfer process can be
modeled using the Butler-Volmer model, which is given by 
\begin{equation}
i_{F}=i_{0}\cdot\left[\frac{c_{o}(0,t)}{c_{o}^{*}}e^{\frac{-\beta n}{V_{T}}\eta}-\frac{c_{r}(0,t)}{c_{r}^{*}}e^{\frac{(1-\beta)n}{V_{T}}\eta}\right]\label{eq:butler_volmer_eq}
\end{equation}
where $i_{0}$ is the exchange current, $\eta$ denotes over-potential, $n$ is the stoichiometric number of electrons consumed, $V_{T}$ is the thermal voltage, and $\beta$ is the charge transfer coefficient (it describes the shape of the energy barrier between the initial and final states of the reaction, for symmetric barrier is $\beta=0.5$).

 The exchange current is given by~\cite{Lasia2002} 
\begin{equation}
i_{0}=nFAk_{0}\label{eq:exchange_current}
\end{equation}
where $F$ is the Faradaic constant, $A$ is the area and $k_{0}$
is the standard rate constant. The over-potential ($\eta$) is given
by 
\[
\eta=E-E_{eq}
\]
$E$ is the actual electrode potential and $E_{eq}$ is the equilibrium
potential.


\subsubsection{Effect of double layer}

In electrochemical systems the current can be divided into two parts:
Faradaic and double layer current (figure~\ref{fig:total_current_schematic}).
The Faradaic current is the current generated due to the electrochemical
reaction taking place at the surface of the electrode and can be described
by the Butler-Volmer equation (\ref{eq:butler_volmer_eq}). The double
layer current is the current consumed to charge the layer formed at
the solid/liquid or electrode/electrolyte interface.

Mathematically, the total current is the algebraic sum of the double
layer current and Faradaic current and is given by
\begin{equation}
i=i_{F}-C_{D}\frac{d\eta}{dt}\label{eq:total_current}
\end{equation}
Here, $i$ is the total current, $i_{F}$ is the Faradaic current,
$C_{D}$ is the double layer capacitance, and $\eta$ is the over-potential.
The total impedance $(Z)$ can be obtained by linearizing eqn.~(\ref{eq:total_current}),
\begin{figure}[tb!]
\begin{center}\includegraphics[width=0.5\textwidth]{\main figures/total_current_schematic}\end{center}
\caption{The total value of the discharge current can be separated into two
parts: charge transfer current and the double layer current.}
\label{fig:total_current_schematic} 
\end{figure}


\begin{equation}
\delta i=\delta i_{F}-\delta i_{dl}=\delta i_{F}-j\omega C_{D}\,\delta\eta
\end{equation}
where, $\delta i$ is the small-signal total current, $\delta i_{F}$
is the small-signal Faradaic current, $i_{dl}$ is the double layer
current, $j$ is the imaginary number ($\sqrt{-1}$), $\omega$ is
the angular frequency, and $\delta\eta$ is the small-signal over-potential.
The above equation leads to the following expression for $Z$ 
\begin{align*}
Z & =-\frac{\delta\eta}{\delta i}\\
\displaybreak[0] & =-\frac{\delta\eta}{\delta i_{F}-j\omega C_{D}\delta\eta}\\
\displaybreak[0] & =-\frac{\delta\eta/\delta i_{F}}{1-j\omega C_{D}\delta\eta/\delta i_{F}}\\
\displaybreak[0] & =\frac{Z_{F}}{1+j\omega C_{D}Z_{F}}\\
\displaybreak[0] & =Z_{F}\|\frac{1}{j\omega C_{D}}
\end{align*}
The final expression for the total impedance is 
\begin{equation}
\boxed{Z=Z_{F}\|\frac{1}{j\omega C_{D}}}\label{eq:Total_impedance}
\end{equation}
where $Z_{F}$ is the Faradaic impedance.

The model of the double layer has evolved over time. In 1885 Hermann
von Helmholtz suggested a double layer model, which was similar to
that of an ideal capacitor model. However, later in 1947, D.C Grahame's
experiments proved that the behavior of EDL was much more complicated
than the ideal capacitor model. The experiments showed that the capacitance
depends on the electrical potential and the concentration of the electrolyte.
Gouy (1909) and Chapman (1913) independently suggested a modified
model to overcome this shortcomings of the Helmholtz model. 

In 1924, Otto Stern proposed combining the Gouy-Chapman model with
the Helmholtz model. Stern's model accounted for the finite size of
the ions and their distance from the electrode surface. D.C. Grahame
(1947) modified the Stern's model to include the variation in hydration.
Grahame's three layer model is shown in figure~\ref{fig:electrical_double_layer_model}.
\begin{figure}[bt]
\centering{}\includegraphics[width=0.45\textwidth]{\main figures/double_layer}
\protect\caption{The structure of a Grahame's double layer with the inner and outer
Helmholtz plane near the electrode surface and the diffuse layer near
the bulk region. Here $\varphi_{m}$, $\varphi_{1}$ and $\varphi_{2}$
are the potentials at the metal surface, inner and outer Helmholtz
plane respectively.}
\label{fig:electrical_double_layer_model} 
\end{figure}


The three layers are: 1) the inner Helmholtz plane (IHP), which is
the locus of centers of all specifically adsorbed ions on the electrode
surface, 2) the outer Helmholtz plane (OHP), which is limited by the
closest distance the solvated ions can come to the electrode surface~\cite{Orazem2008}
and the ions interact with the surface using long range electrostatic
forces, and 3) the diffuse layer, which extends from the OHP to the
bulk region, where the anions and cations are distributed according
to Poisson and Boltzmann distribution~\cite{Orazem2008,Bard2000}.


\subsection{Equivalent circuit modeling}

Equivalent circuit modeling (ECM) is commonly used to describe electrochemical
systems~\cite{Danner2014}. The simplicity of this technique lies
in the fact that it uses elementary circuit elements such as resistors,
inductors, and constant phase elements (CPE) to obtain the impedance
response of the battery. The constant phase element is mathematically
given as $Z_{\textrm{CPE}}=1/\left(j\omega\right)^{\alpha}Q$, where
$\alpha$ is the model parameter related to different time constants
and $Q$ is a capacitance-like term~\cite{Zoltowski1998,Hirschorn2010}.
However, there are many different physical interpretations for introducing
factor $\alpha$. This factor is generally attributed to the spatial
distribution of physical properties such as structure, reactivity,
dielectric constants, and resistivity~\cite{Hirschorn2010} and to
non-homogeneities in the system~\cite{Landa-Medrano2014}. The values
of the components in the circuit are found by fitting the experimental
measurements of the EIS spectra~\cite{Danner2014} to the impedance
network. Different sections of the circuit are attributed to different
physical or electrochemical processes.

An important requirement for building an equivalent circuit model
is to have a \textit{priori} knowledge of the physical processes taking
place in the system. In the absence of this prior knowledge many different
configurations can yield identical impedance response and thus making
the predictions made using this model non-physical and non-practical.

ECM modeling has already been used to provide additional information
about Li-air batteries. For instance, ECM have been used to characterize
the state-of-charge of Li-air batteries~\cite{Wittmaier2014}, analyze
the performance of novel membrane materials~\cite{Yang2013b,Puech2012},
and study Li-air batteries with aqueous electrolyte~\cite{He2011,Shimonishi2011}.

\section{Li-air batteries (LABs)}

The modeling techniques discussed in the previous section can be applied to batteries with different chemistries. Among the metal-air batteries, Li-air batteries (LABs) with organic electrolyte have the highest specific energy density~\cite{Abraham1996,Lim2013,Eswaran2010}. These batteries can store approximately 10 times more energy than conventional Li-ion batteries~\cite{Ogasawara2006,Sahapatsombut2013} and their theoretical energy density is comparable to the theoretical energy density of gasoline~\cite{Girishkumar2010}. The salient features of Li-air batteries are: high open circuit voltage (\SI{2.96} {\volt} for \ce{Li2O2}) and high specific energy (5200 Wh/kg inclusive of oxygen~\cite{Sandhu2007,Lee2011,Wang2011a} and 11140 Wh/kg excluding oxygen~\cite{Sandhu2007,Xu2012b}). These highly desirable features of LABs is the primary driving force for companies, governments, and universities to invest their resources in their research and development. 

\begin{figure}[b!]
\centering{}\includegraphics[width=\textwidth]{\main figures/Li-air}
\protect\caption{A schematic of a typical Li-air battery with oxygen entering the system
from $x=0$. }
\label{fig:Li-air_structure} 
\end{figure}

The main electrochemical reactions in an organic Li-air battery are 
\begin{align}
	\ce{Li <-> Li+ + e-}\textrm{ (anode)}\label{eq:Li-air_anode}\\
	\ce{2Li+ + O2 + 2e- <-> Li2O2}\textrm{ (deposit)}\label{eq:Li-air_cathode_peroxide}\\
	\ce{Li+ + O2 + e- -> LiO2}\textrm{ (deposit)}\label{eq:Li-air_cathode_superoxide} 
\end{align}

A typical Li-air battery consists of a closed anode and a porous cathode that is open to the atmosphere, which allows the oxygen from the atmosphere to enter battery, a \ce{Li+} conducting separator and an electrolyte that transports the Li-ions between the two electrodes (figure~\ref{fig:Li-air_structure}). During discharge, the lithium metal oxidizes to produce \ce{Li+} ions and electrons (eqn.~\ref{eq:Li-air_anode}); the Li-ions diffuse towards the cathode due of electrochemical potential gradient whereas the electrons flow through the external circuit. The oxygen from the atmosphere is reduced at the cathode to form lithium peroxide (eqn.~\ref{eq:Li-air_cathode_peroxide})~\cite{Abraham1996,Ogasawara2006,Andrei2010,Peng2011,Radin2012,Dilimon2015} and lithium superoxide~(eqn.~\ref{eq:Li-air_cathode_superoxide})~\cite{Sahapatsombut2013,Black2012}. These discharge products deposit on the cathode surface until they fill the pores and block the oxygen from reaching the cathode surface~\cite{Read2003} or until the resistance of these batteries increases up to a point after which the electrochemical reaction can no longer be sustained~\cite{Hojberg2015,Wang2013d}.

Despite high specific energy densities and high specific capacities the LABs have a number of limitations that has obstructed their large scale commercialization. Important limitations are low practical specific energy~\cite{Xiao2011a,Christensen2012a}, low power density~\cite{Lu2010,Cui2011,Christensen2012a,Lu2013b,Bhatt2014}, poor cycle life~\cite{Lu2010,Cui2011,Zhang2011e,Bryantsev2011,Christensen2012a,Shao2013,Xu2013a,Lu2013b,Meini2013,Bhatt2014,Safari2014,Kim2014a,Wang2014}, poor charge efficiency~\cite{Lu2010,Xiao2011a,Cui2011,Xu2013a,Safari2014,Kim2014a}, and reduced rate capability~\cite{Kim2014a}. 


\section{Organization of thesis}

In chapter \ref{chap:EIS_under_dc_discharge}, a physics based model is developed to compute the impedance spectra of Li-air batteries with organic electrolyte under high d.c discharge currents. Compact analytical equations are derived for the impedance of these batteries separately for the cases of high discharge current and low discharge currents. In chapter \ref{chap:EIS_under_dc_charge}, a physics based model is developed for the impedance spectra in Li-air batteries during charge. In chapter \ref{chap:Expt_verification}, a model is developed to relate the Nyquist plots of Li-air batteries to the physical and electrochemical parameters of the cathode, and then the model is verified against experimental data published in the literature. In chapter \ref{chap:Dissolution_kinetics}, the initial model developed in chapter \ref{chap:EIS_under_dc_discharge} is extended to include the effects of oxygen dissolution in the electrolyte. In chapter \ref{chap:FEM_model}, finite element simulations are performed to validate the analytical model and to study various effects of electrochemical and physical parameters of the battery on the total impedance of Li-air batteries. Finally, conclusions are drawn in chapter \ref{chap:Conclusions}.
\end{document}