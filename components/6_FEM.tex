\documentclass[../thesis.tex]{subfiles}
\providecommand{\main}{../}
\begin{document}
\chapter{Finite element modeling of EIS}
\label{chap:FEM_model}
\section{Introduction}
The advantage of expressing the EIS spectra into a compact analytical
form is that the final results are computationally simple and, in
the same time, provide a significant amount of phenomenological information
about the system. Compact models can describe a system and predict
its behavior with high accuracy using only few number of parameters.
Unfortunately, analytical solutions are usually restricted to regular
geometries and simple boundary conditions, such as the ones that were
used in the previous chapters. In order to better understand the behavior
of the system one needs to solve the transport equations using more
comprehensive models and computationally expensive techniques such
as finite difference, finite element, or finite volume modeling.

In previous chapters, many factors such the discharge product resistivity,
electrolyte concentration, electron conductivity in the current collector
backbone, diffusion of lithium, consumption of lithium were neglected.
If these effects were included in the analysis, it was much harder
or even impossible to find a closed form solution for the EIS spectra.
In this chapter a novel approach to compute the impedance spectra
using finite element simulations is introduced. 

This chapter is structured as follows. In the next section the finite
element model for the computation of impedance spectra is summarized.
Then, the numerical technique that was developed to compute the impedance
spectra in Li-air batteries under d.c. discharge is presented and
the numerical implementation of the algorithm is discussed. Then,
detailed numerical analysis of the impedance spectra under different
discharge conditions and state of charge is presented.
\section{Finite element model}
The finite element model used to compute the impedance spectra is
based on a transport model that was originally developed by Andrei
et al. \cite{Andrei2010}. The model has been subsequently improved
and calibrated by a number of other research groups in the energy
storage community \cite{Xue2014} and is based on the theory of concentrated solutions originally proposed by Newman
and Thomas-Alyea \cite{Newman2004}. The mass and charge transport
in the electrolyte is described by the following set of partial differential
equation (PDEs):
\begin{itemize}
	\item electrolyte drift-diffusion equations 
	\begin{equation}
	\nabla.\left(\kappa_{\textrm{eff}}\nabla\phi_{\textrm{Li}}+\kappa_{D}\nabla\ln c_{\textrm{Li}}\right)-R_{c}=aC_{D}\frac{\partial\left(\phi-\phi_{\textrm{Li}}\right)}{\partial t},\label{eq:electrolyte_drift_eqn}
	\end{equation}
	\begin{equation}
	\frac{\partial\left(\epsilon c_{\textrm{Li}}\right)}{\partial t}=\nabla.\left(D_{\textrm{Li,eff}}\nabla c_{\textrm{Li}}\right)-\frac{1-t^{+}}{F}R_{c}-\frac{\boldsymbol{I}_{\textrm{Li}}\cdot\nabla t^{+}}{F},\label{eq:electrolyte_diffusion_eqn}
	\end{equation}
	
	\item oxygen diffusion
	\begin{equation}
	\frac{\partial\left(\epsilon c_{o}\right)}{\partial t}=\nabla.\left(D_{o\textrm{,eff}}\nabla c_{o}\right)-\frac{R_{c}}{nF},
	\end{equation}
	
	\item electron conductivity
	\begin{equation}
	\nabla.\left(\sigma_{\textrm{eff}}\nabla\phi\right)+R_{c}=aC_{D}\frac{\partial\left(\phi-\phi_{\textrm{Li}}\right)}{\partial t}\label{eq:electron_conductivity}
	\end{equation}
	
\end{itemize}
where $c_{\textrm{Li}}$ is the concentration of the electrolyte (which
is equal to the concentration of Li ions), $\phi$ is the potential
of electrons, $\phi_{\textrm{Li}}$ is the electrostatic potential
of the electrolyte, $\boldsymbol{I}_{\textrm{Li}}$ is the electrolyte
current density equal to $-\kappa_{\textrm{eff}}\nabla\phi_{\textrm{Li}}-\kappa_{D}\nabla\ln c_{\textrm{Li}}$,
$C_{D}$ is the capacitance of the double layer per unit area, $a$
is the specific area of the electrode, $\epsilon$ is the porosity
(equal to the volume fraction of the electrolyte), $t^{+}$ is the
transference number of the positive ion, and $F$ is the Faraday constant.
The reaction rate at the cathode, $R_{c}$, is given by the Butler-Volmer
equation (see eqn.~\ref{eq:k_BV_w_beta_k})
\begin{equation}
R_{c}=nFc_{o}k_{0}a\left(e^{\frac{\beta n}{V_{T}}\eta}-e^{\frac{(\beta-1)n}{V_{T}}\eta}\right)
\end{equation}
where $k_{0}$ is the reaction rate constant, $n$ is the number of
electrons involved in the cathode reaction, and $V_{T}$ is the thermal
voltage. The over-potential is given by
\begin{equation}
\eta=\phi_{\textrm{Li}}-\phi+E^{0}-V_{\textrm{dis}},
\end{equation}
where $V_{\textrm{dis}}$ is the voltage drop across the discharge
product. The pores in the cathode are assumed to be cylindrical in
shape with single radius (see fig.~\ref{fig:cyl_pores_1_size}),
the voltage drop across the discharge product is \cite{Bevara2014}
\begin{equation}
V_{\textrm{dis}}=\frac{R_{c}\rho_{dis}}{2}\bar{r}\ln\frac{\epsilon_{0}}{\epsilon},
\end{equation}
and the specific area becomes
\begin{equation}
a=\frac{2\epsilon}{\bar{r}},
\end{equation}
where $\rho_{dis}$ is the resistivity of the discharge product and
$\bar{r}$ is the radius of the pores. The effects of the pore radius
distribution and different pore structures on the impedance spectra
are analyzed later in this chapter.
\begin{figure}[tb!]
	\begin{center} \includegraphics[width=0.7\linewidth]{\main figures/Cylinderical_pores_1_size_new}\end{center}
	\protect\caption{Cross-section of a cathode with cylindrical pores with a single radius}
	\label{fig:cyl_pores_1_size} 
\end{figure}

The discharge products in the cathode are: lithium peroxide (\ce{2Li+ + O2 + 2e- -> Li2O2}),
lithium oxide (\ce{4Li+ + O2 + 4e- -> 2Li2O}), and lithium superoxide
(\ce{Li+ + O2 + e- -> LiO2}) \cite{Laoire2009,Yang2013a,Andrei2010,Lu2013b}.
Since all these discharge produces are insoluble in the organic electrolyte,
they deposit on the surface of the cathode, and the porosity changes
according to the following equation
\begin{equation}
\frac{\partial\epsilon}{\partial t}=-R_{c}\frac{M_{\textrm{dis}}}{nF\rho_{\textrm{m,dis}}}\frac{2\epsilon}{\bar{r}},\label{eq:porosity_change}
\end{equation}
where $M_{\textrm{dis}}$ is the molar mass and $\rho_{\textrm{m,dis}}$
is mass density of discharge product.

The effective oxygen and electrolyte diffusion constant and the electrolyte
conductivity constants are given by Bruggeman equations
\begin{align}
D_{o\textrm{,eff}} & =\epsilon^{\text{brugg}}D_{o},\\
D_{\textrm{Li,eff}} & =\epsilon^{\text{brugg}_{\textrm{Li}}}D_{\textrm{Li}}\left(1-\frac{d\ln c_{o}}{d\ln c_{\textrm{Li}}}\right),\\
\kappa_{\textrm{eff}} & =\epsilon^{\text{brugg}_{\kappa}}\kappa,
\end{align}
while the diffusional conductivity of the electrolyte is 
\begin{equation}
\kappa_{D}=\frac{2RT\kappa_{\textrm{eff}}\left(t^{+}-1\right)}{F}\left(1+\frac{\partial f}{\partial\ln c_{\textrm{Li}}}\right),
\end{equation}
The electrolyte drift and diffusion equations {[}i.e. equations~\ref{eq:electrolyte_drift_eqn}
and~\ref{eq:electrolyte_diffusion_eqn}{]} are derived from the transport
equations of the positive and negative ions of the concentrated solution.
A more detailed derivation for equations and can be found in references
\cite{Newman2004} and \cite{Xue2013a}. 

Equations~\ref{eq:electrolyte_drift_eqn}-~\ref{eq:electron_conductivity},
and eqn.~\ref{eq:porosity_change} are subject to boundary and initial
conditions. The modified diffusion equation (\ref{eq:Li2O2_model_1_ficks_second_law_w_k_substitution})
is subject to the following initial and boundary conditions: \begin{subequations} 
	\begin{enumerate}
		\item []\textbf{Initial condition} 
		\[
		c_{o}(x,0)=c_{o}^{*}
		\]
		where $c_{o}^{*}$ is the bulk oxygen concentration
		\item []\textbf{Boundary condition at $x=0$} 
		\[
		c_{o}(0,t)=c_{o}^{*}
		\]
		
		\item []\textbf{Boundary condition at $x=l$} 
		\[
		\left.\frac{dc_{o}(x,t)}{dx}\right|_{x=l}=0
		\]
		
	\end{enumerate}
\end{subequations} and are solved using the Newton technique. 


\section{Computation of impedance spectra}

In this section, the technique to compute the impedance spectra in
electrochemical systems using finite element modeling is presented.
The impedance spectra are computed by applying the small-signal analysis
theory to the transport equations and solving the final linear system
of equations numerically.

The impedance of the battery can be expressed in complex form as 
\[
Z=\frac{\delta v}{\delta i}
\]
In order to compute $\delta v$ any small variation in the discharge
current $\delta i\,e^{j\omega t}$ translates into local variations
of the over-potential$\delta\eta\,e^{j\omega t}$, oxygen concentration
$\delta c_{o}\,e^{j\omega t}$, lithium concentration $\delta c_{\textrm{Li}}\,e^{j\omega t}$,
electron potential $\delta\phi\,e^{j\omega t}$, electrolyte potential
$\delta\phi_{\textrm{Li}}\,e^{j\omega t}$, as well as of the currents,
fluxes, reaction rates at the anode and cathode, and other state variables,
where $\eta$, $c_{o}$, $c_{\textrm{Li}}$, $\phi$, and $\phi_{\textrm{Li}}$,
are complex functions of position that need to be determined. This
fact can be expressed in a compact mathematical form by introducing
the vector of state variables, $\boldsymbol{y}$. In finite element
simulations, the state variables are defined separately at each node
of the mesh, and can be usually written as in vector form as 
\begin{equation}
\boldsymbol{y}=\left[c_{o,1},...,c_{o,N},c_{\textrm{Li},1},...,c_{\textrm{Li},N},\phi_{\textrm{Li},1},...,\phi_{\textrm{Li},N},\phi_{1},...,\phi_{N}\right]^{T}\label{eq:FEM_y_vector_representation}
\end{equation}
where $c_{o,i}$, $c_{\textrm{Li},i}$, $\phi_{\textrm{Li},i}$, and
$\phi_{i}$ denote the values of the oxygen concentration, lithium
concentration, lithium potential and electron potential at each node
$i=1,...,N$, where $N$ is the total number of nodes, and superscript
$T$ denotes the transpose of the vector. Using small-signal perturbations,
the state vector can be written as
\begin{equation}
\boldsymbol{y}=\boldsymbol{Y_{0}}+\delta\boldsymbol{y}\,e^{j\omega t}
\end{equation}
where $\boldsymbol{Y_{0}}$ and $\delta\boldsymbol{y}$ are column
vectors that denote the d.c. and a.c. values of the state variables
at each mesh point. 

The d.c. values of the state vector can be computed numerically by
solving the transport equations for the \textquotedblleft unperturbed\textquotedblright
battery. Indeed, if $i$ is d.c. value of the discharge current, the
transport equations \ref{eq:electrolyte_drift_eqn}-\ref{eq:electron_conductivity}
can be written in discretized form as: 
\begin{equation}
\boldsymbol{F}\left(\boldsymbol{Y_{0}},i\right)=0\label{eq:transport_eqns_in_discretized_form}
\end{equation}
where $\boldsymbol{F}$ is a vector whose components are the discretized
differential equation at each node. If the value of the discharge
current $i$ is given, one can solve transport equations \ref{eq:transport_eqns_in_discretized_form}
numerically to obtain the value of state variable $\boldsymbol{Y_{0}}$. 

The a.c. values of the state vector can be computed by solving
\begin{equation}
\boldsymbol{F}\left(Y_{0}+\delta\boldsymbol{y}\,e^{j\omega t},i+\delta i\,e^{j\omega t}\right)=0
\end{equation}
Using small-signal perturbations and eqn. \ref{eq:transport_eqns_in_discretized_form},
the above equation can be rewritten as 
\begin{equation}
\boldsymbol{F_{Y}}\left(\boldsymbol{Y}_{0},i\right)\delta\boldsymbol{y}\,e^{j\omega t}+\boldsymbol{F}_{\boldsymbol{i}}\left(\boldsymbol{Y}_{0},i\right)\delta i\,e^{j\omega t}=0\label{eq:FEM_IS_linearization}
\end{equation}
where $\boldsymbol{F_{Y}}\left(\boldsymbol{Y}_{0},i\right)$ is the
Jacobian of the transport system and 
\begin{equation}
\boldsymbol{F}_{\boldsymbol{i}}\left(\boldsymbol{Y}_{0},i\right)=\frac{d\boldsymbol{F}_{i}\left(\boldsymbol{y},i\right)}{di}\Biggr|{\scriptstyle _{\begin{array}{c}
		\boldsymbol{y}=\boldsymbol{Y}_{0}\\
		i=i
		\end{array}}}
\end{equation}
are the derivatives of the discretized transport equations with respect
to the discharge current. If the value of the a.c. current perturbation
$\delta i$ is given, Eqn. \ref{eq:FEM_IS_linearization} can be written
as a linear system of equations for the a.c. values of the state variables,
$\delta\boldsymbol{y}$
\begin{equation}
\boldsymbol{F_{Y}}\left(\boldsymbol{Y}_{0},i\right)\delta\boldsymbol{y}=-\boldsymbol{F}_{\boldsymbol{i}}\left(\boldsymbol{Y}_{0},i_{0}\right)\delta i\label{eq:FEM_IS_simplified_linearized_eqn}
\end{equation}
The potential at the air side of the cathode is equal to the value
of the electrostatic potential at the air side of the cathode, $v=\phi|_{x=l}$.
Hence, $\delta v$ is a component of the state vector $\delta\boldsymbol{y}$:$\delta v=\delta\phi|_{x=l}$.
Once the system equations are solved numerically, the impedance spectra
are computed using the impedance equation ($Z=\delta v/\delta i$).

The model described was implemented in our in-house finite element
simulator developed at Florida State University, RandFlux \cite{Randflux}.
The transport equations were discretized on a 1-D non-uniform grid
in the space coordinate and the backward Euler method was applied
to discretize the time coordinate. The Jacobian matrix, $\boldsymbol{F_{Y}}$,
is computed numerically and the transport equations are solved self-consistently
using the Newton technique. 

The simulation time required to compute the discharge curve for a
given discharge current is less than one minute for a grid with 100
mesh points on a 3 GHz single-processor workstation. The computational
overhead to compute the impedance at one frequency is mostly given
by the time required to solve the linear sparse system \ref{eq:FEM_IS_simplified_linearized_eqn},
which is of the order 10-100 milliseconds. The simulation time to
evaluate the impedance spectra at a few hundred frequency points is
less than 10 seconds.

The parameters used in simulations are presented in Table 1. The width
of the air electrode is \SI{200}{\micro\meter}, the separator is
\SI{50}{\micro\meter}, and the metal anode is separated from the
membrane by a \SI{50}{\nano\meter} layer of organic electrolyte (also
known as anode protective layer, APL). 
\begin{table}[tb!]
\caption{List of parameters used in the finite element simulations}
\begin{center}
	\begin{tabular}{|c|c|}
		\hline 
		Parameter & Value\tabularnewline
		\hline 
		Capacitance of the double layer ($C_{D}$) & \SI[per-mode=symbol]{10}{\micro\farad\per\centi\meter\cubed}\tabularnewline
		\hline 
		APL thickness ($L_{A}$) & \SI{50}{\nano\meter}\tabularnewline
		\hline 
		Separator thickness ($L_{s}$) & \SI{50}{\micro\meter}\tabularnewline
		\hline 
		Cathode thickness ($L_{c}$) & \SI{200}{\micro\meter}\tabularnewline
		\hline 
		Bruggeman constants ($\text{brugg}$, $\text{brugg}_{\textrm{Li}}$,
		and $\text{brugg}_{\kappa}$) & 1.5\tabularnewline
		\hline 
		Temperature (T) & \SI{300}{\kelvin}\tabularnewline
		\hline 
		Initial electrolyte concentration ($c_{\textrm{Li}}\left(t=0\right)$) & \SI[per-mode=symbol]{1e-3}{\mol\per\centi\meter\cubed}\tabularnewline
		\hline 
		Open cell voltage ($E_{0}$) & \SI{2.956}{\volt}\tabularnewline
		\hline 
		Initial porosity of the cathode ($\epsilon$) & 0.75\tabularnewline
		\hline 
		Porosity of the separator ($\epsilon$) & 0.75\tabularnewline
		\hline 
		Molecular weight of \ce{Li2O2} & \SI[per-mode=symbol]{45.88}{\gram\per\mol}\tabularnewline
		\hline 
		Mass density of \ce{Li2O2} ($\rho_{m,\ce{Li2O2}}$) & \SI[per-mode=symbol]{2.14}{\gram\per\mol}\tabularnewline
		\hline 
	\end{tabular}
\end{center}	
	\label{Table:List_of_FEM_sim_parameters} 
\end{table}



\section{Simulation results}

To verify the numerical implementation, the numerical results of the
finite element model are compared with analytical results obtained
using the model presented in chapter \ref{sub:low_dc_current}. According
to the analytical model, the impedance of the battery can be written
in compact form as 
\begin{equation}
Z\left(\omega\right)=R_{\Omega}+\frac{Z_{F}\left(\omega\right)}{1+j\omega alAC_{d}Z_{F}\left(\omega\right)}
\end{equation}
where $l$ is the width of the cathode, $a$ is the cross-sectional
area of the cathode, and $R_{\Omega}$ is the combined resistance
of the ohmic contacts, electrolyte, and kinetic processes at the anode.
The Faradaic impedance, $Z_{F}\left(\omega\right)$, depends on the
d.c. value of the discharge current $i_{F}$ according to eqn.~\ref{eq:ZF_final_Tafel}
\[
Z_{F}\left(\omega\right)=\frac{V_{T}}{n\beta i_{F}\left[1-\frac{k}{j\omega\epsilon_{0}}+\frac{k\sqrt{k}\tanh\left(\sqrt{\frac{k+j\omega\epsilon_{0}}{D_{\text{eff}}}}\,l\right)}{j\omega\epsilon_{0}\sqrt{k+j\omega\epsilon_{0}}\tanh\left(\sqrt{\frac{k}{D_{\text{eff}}}}\,l\right)}\right]}
\]
Fig.~\ref{fig:ana_vs_FEM} presents a comparison between the finite
element simulation (symbols) and the analytical model (continuous
line) for different values of the d.c. discharge current density ranging
from \SI[per-mode=symbol]{0.1}{\milli\ampere\per\centi\meter\squared}
to \SI[per-mode=symbol]{1}{\milli\ampere\per\centi\meter\squared}.
A very good agreement between the analytical results and simulations
is observed. The slight deviations at high discharge currents are
due to the fact that the analytical model does not take into account
the high current effects such as voltage losses in the electrolyte,
separator, and the kinetic phenomena at the anode and non-linear deposition
of the deposit products in the cathode (the analytical model assumes
that the porosity in the cathode uniform and that the porosity remains
constant during the time of impedance measurement).

\begin{figure}[tb!]
	\begin{center} \includegraphics[width=0.7\linewidth]{\main figures/Ana_vs_FEM_1}\end{center}
	\protect\caption{Comparison between the finite element simulation (symbols) and the
		analytical model (continuous) for different values of the d.c. discharge
		current density. }
	\label{fig:ana_vs_FEM} 
\end{figure}
\paragraph{Dependence of EIS spectra on the state of discharge}

To analyze the contribution of the discharge product on the impedance
spectra of a Li-air battery, the EIS spectra are simulated at different
state-of-discharge for two values of the discharge current: \SI[per-mode=symbol]{0.1}{\milli\ampere\per\centi\meter\squared}
(fig.~\ref{fig:FEM_0_1mA_SOD}) and \SI[per-mode=symbol]{1}{\milli\ampere\per\centi\meter\squared}
(fig.~\ref{fig:FEM_1mA_SOD}). Fig.~\ref{fig:FEM_0_1mA_SOD} presents
the distribution of the discharge product (light green region), porosity
distribution (red curve), and the concentration of oxygen (blue curve)
in the cathode at (a) 20\%, (b) 50\%, (c) 90\%, and (d) 100\% state-of-discharge.
The discharge product deposits uniformly inside the cathode at low
discharge currents until the pores on the air side of the cathode
are completely blocked and oxygen can no longer enter the battery.
The deposit product is considered to be resistive in nature and adds
to the total charge transfer resistance of the battery. Fig.~\ref{fig:FEM_0_1mA_SOD}e
represents the impedance response computed at different states of
discharge. The contribution of the resistance of the deposit layer
to the total impedance is negligible when the state-of-discharge is
less than 30\%, but the contribution of the deposit layer is significantly
higher and dominate the EIS spectra when the impedance response is
measured near the end of the discharge cycle. It is important to mention
that the impedance contribution due to electrolyte decomposition,
corrosion of the electrode, and separator deformation are not considered
in this analysis.

\begin{figure}[tb!]
	\begin{center}\includegraphics[width=0.7\linewidth]{\main figures/FEM_0_1mA_SOD}\end{center}
	\protect\caption{Distribution of the discharge product (a)-(d) for different states
		of discharge: 20\%, 50\%, 90\%, and 100\% (for a discharge current
		density of \SI[per-mode=symbol]{0.1}{\milli\ampere\per\centi\meter\squared})
		and impedance spectra (e) simulated for different values of the state-of-discharge.}
	\label{fig:FEM_0_1mA_SOD} 
\end{figure}
\begin{figure}[tb!]
	\begin{center} \includegraphics[width=0.7\linewidth]{\main figures/FEM_1mA_SOD}\end{center}
	\protect\caption{Distribution of the discharge product (a)-(d) for different states
		of discharge: 20\%, 50\%, 90\%, and 100\% (for a discharge current
		density of \SI[per-mode=symbol]{1}{\milli\ampere\per\centi\meter\squared})
		and impedance spectra (e) simulated for different values of the state-of-discharge.}
	\label{fig:FEM_1mA_SOD} 
\end{figure}

In fig.~\ref{fig:FEM_1mA_SOD} the distribution of the discharge
product (a-d) and the impedance response (e) for different states
of discharge are presented for a discharge current of \SI[per-mode=symbol]{1}{\milli\ampere\per\centi\meter\squared}.
Under heavy discharge (i.e. large values of the discharge current)
a large amount of the discharge products deposits near the air-side
of the cathode as compared to the separator-side of the cathode. This
non-uniform deposition leads to under-utilization of the air cathode
and is one of the causes for the reduction of the specific capacity
of the battery. The Nyquist plot (fig.~\ref{fig:FEM_1mA_SOD}e) shows
that the total impedance of the cathode increases by more than 300\%
when the EIS is computed at 90\% state-of-discharge as compared on
a fresh battery. It is important to note that the time taken to perform
an actual EIS at 90\% state-of-discharge may exceed the total discharge
time of a Li-air battery for some values of the discharge current,
cathode geometry, cathode materials, and concentration of the ambient
oxygen; these effects are not considered in our analysis.




Figure \ref{fig:FEM_discharge_curves} represents discharge curves
for two discharge currents: \SI[per-mode=symbol]{0.1}{\milli\ampere\per\centi\meter\squared}
and \SI[per-mode=symbol]{1}{\milli\ampere\per\centi\meter\squared}
used in the simulations presented in Fig.~\ref{fig:FEM_0_1mA_SOD}
and Fig.~\ref{fig:FEM_1mA_SOD}. The symbols represent the state-of-discharge
at which the impedance spectra are simulated. 

\begin{figure}[tb!]
	\begin{center}\includegraphics[width=0.7\linewidth]{\main figures/FEM_discharge_curves}\end{center}
	\protect\caption{Cell voltage as a function of specific capacity for the two discharge
		currents used in the simulations presented in Fig.~\ref{fig:FEM_0_1mA_SOD}
		and Fig.~\ref{fig:FEM_1mA_SOD}. The symbols represent the state
		of discharge at which the impedance spectra are simulated. }
	\label{fig:FEM_discharge_curves} 
\end{figure}


Figure ~\ref{fig:FEM_different_resistivities}(a-d) shows the distribution
of the discharge product (light green region), porosity distribution
(red curve), and the concentration of oxygen (blue curve) in the cathode
for four different values of resistivity of the discharge product:
(a) $\rho_{dis}=$\SI{1e10}{\ohm\centi\meter}, (b) $\rho_{dis}=$\SI{1e11}{\ohm\centi\meter},
(c) $\rho_{dis}=$\SI{1e12}{\ohm\centi\meter}, and (d) $\rho_{dis}=$\SI{1e13}{\ohm\centi\meter}.
The resistivity of the deposit layer strongly influences the deposition
profile of the discharge product in the cathode: for large values
of resistivity the discharge products are deposited uniformly throughout
the cathode and , in this way, the battery is slightlybetter utilized
as compared to the case when discharge products have a low resistivity.
In addition to the added electric resistance, the deposition of the
discharge product also causes physical changes in the cathode such
as reduced specific area, reduced porosity, and increased tortuosity.
Figure ~\ref{fig:FEM_different_resistivities}e shows that the impedance
response simulated at 50\% state-of-discharge for a discharge current
of \SI[per-mode=symbol]{1}{\milli\ampere\per\centi\meter\squared}
for four different values of resistivity of the discharge product.
The resistivity of lithium peroxide strongly influences the overall
impedance of the battery, the total impedance increases 1600\% when
the resistivity of lithium peroxide is changed from $\rho_{dis}=$\SI{1e10}{\ohm\centi\meter}
to $\rho_{dis}=$\SI{1e13}{\ohm\centi\meter}. 
\begin{figure}[tb!]
	\begin{center}\includegraphics[width=0.7\linewidth]{\main figures/FEM_different_resistivities}
	\end{center} \protect\caption{(a)-(d) show the distribution profile of the discharge product and
	(e) shows the impedance spectra for different values of \ce{Li2O2}
	resistivity at 50\% state-of-discharge. The simulations are carried
	out for d.c. discharge current density of \SI[per-mode=symbol]{1}{\milli\ampere\per\centi\meter\squared}. }
\label{fig:FEM_different_resistivities} 
\end{figure}


The resistivity of the discharge products plays an important role
in the \textquotedblleft sudden death\textquotedblright of Li-air
batteries (end of the discharge cycle). Figure \ref{fig:FEM_rho_discharge_curve}
shows voltage discharge curves simulated for different values of the
resistivity of the discharge product. The large variation in the specific
capacities for various values of the discharge product resistivity
can be related to the difference in discharge mechanism at low and
high values of the resistivity. For small values of the resistivity,
the sudden death in lithium air batteries may occur because the pores
near the air-side are filled with the product and blocks the oxygen
from entering the battery. For large values of resistivity, the sudden
death in lithium air batteries may occur because the potential drop
due to the resistive layer can no longer sustain the electrochemical
reaction. 
\begin{figure}[tb!]
	\begin{center}\includegraphics[width=0.7\linewidth]{\main figures/FEM_rho_discharge_curves}\end{center}
	\protect\caption{Cell voltage as a function of specific capacity for different values
		of resistivity of the discharge product. The dotted line and the black
		circle symbols denote the 50\% state of discharge.}
	\label{fig:FEM_rho_discharge_curve} 
\end{figure}
\begin{figure}[bt!]
	\begin{center}\includegraphics[width=0.49\linewidth]{\main figures/FEM_conc_dis_curves}\includegraphics[width=0.49\linewidth]{\main figures/FEM_conc_EIS_curves}\end{center}
	\caption{Voltage discharge curves (a) and simulated EIS spectra (b) for different values of oxygen concentration in the atmosphere (relative to 1 atmosphere).}
	\label{fig:FEM_O2_conc} 
\end{figure}

The concentration of dissolved oxygen strongly influence the specific
capacity of practical Li-air batteries. The concentration of oxygen
in the electrolyte can be increased by either using electrolytes with
high oxygen solubility or by increasing the partial pressure of oxygen
that is supplied to the battery. Figure \ref{fig:FEM_O2_conc}a shows
the effect of the external oxygen concentration on the specific capacity
of a Li-air battery. For small values of the partial pressure of oxygen
the redox reaction is restricted to the region near the cathode/air
interface and the major portion of the cathode plays no part in this
reaction, thus reducing the specific capacity of the battery significantly.
However, as the concentration of oxygen in the electrolyte is increased
the oxygen is able to travel throughout the cathode and take part
in the redox reaction, thus increasing the total specific capacity
of the battery Figure \ref{fig:FEM_O2_conc}b represents the impedance
spectra simulated at $50\%$ state-of-discharge for different values
of oxygen concentration in the atmosphere. The two main contributors
of the low frequency resistance are: the low Faradaic reaction rate
and the high oxygen diffusion resistance. For small values of the
oxygen concentrations, the system is starved of oxygen, which causes
a large increase in the diffusion resistance as shown by the high
total impedance at low frequencies. For large values of the oxygen
concentrations, the diffusion impedance reduces drastically and thus
reduces the total impedance of the battery at low frequencies, which
suggests that the system is no longer limited by diffusion. 


\end{document}