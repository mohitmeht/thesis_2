\documentclass[../../thesis.tex]{subfiles}
\providecommand{\main}{../../}
\begin{document}
\section{Introduction}
\todo[author=Mohit]{Improve introduction}
The Butler-Volmer relation can describe charging and discharging mechanism at the electrode/electrolyte interface. Although one can use either eqn~\ref{eq:ZF_final_sinh} or eqn.~\ref{eq:ZF_final_FBV_non_sinh} to describe EIS during charge and discharge, however, these equations implicitly assumes that either electron transfer coefficient or standard reaction rate at the electrode is the same for charging and discharge respectively, which is unlikely in a practical battery. In this chapter, the Butler-Volmer with different forward and reverse reaction rate, non-equal electron transfer factor is considered and is given as
\begin{equation}
k=a\left(k_0^fe^{\frac{-\alpha n}{V_T}\eta} - k_0^re^{\frac{(1-\alpha)n}{V_T}\eta} \right)
\label{eq:k_BV_w_beta}
\end{equation}
where $k_0^f$ is the standard forward reaction rate, $k_0^r$ is the standard reverse reaction rate, $\alpha$ is the electron transfer rate ($0<\alpha<1$), $n$ is the number of electrons transferred in electrochemical reaction, $\eta = E^0 - \Delta V_\textrm{\ce{Li2O2}}$ is the over-potential (the potential drop between the electrode surface and the bulk layer)\todo{Probably is incorrect}.

\section{Modeling}
The same methodology, specifying boundary conditions, obtaining an expression for the spatial distribution of oxygen in the cathode, computing the impedance spectra under steady-state conditions, and finally the small-signal analysis to obtain the final analytical expression, is used to develop the analytical model to compute the EIS under charge and discharge in Li-air batteries with organic electrolyte.
The chemical reaction in the cathode of organic Li-air battery is given by, 
\begin{equation}
\ce{2Li+ + O2 + 2e- <=> Li2O2}
\end{equation}
In order to reduce the length of the equations and to reduce number of variables new variables are introduced and used henceforth in this chapter, the original variable will be reintroduced in the final impedance expression(\todo[author=Mohit]{add the final equation number}).
The forward reaction rate constant with the inclusion of specific capacity of the cathode ($a$) is given by, 
\begin{equation}
k^f =  ak_0^f e^{\frac{-\beta n}{V_T}\eta}
\end{equation}
and the reaction rate constant with the addition of oxygen diffusion coefficient term is given by, 
\begin{equation}
k_{\textrm{wD}_\textrm{eff}}^f = \frac{k^f}{D_\textrm{eff}}\label{eq:kfwDeff_FBV}
\end{equation}
The reverse reaction rate constant with the specific capacity of the cathode given by
\begin{equation}
k^r =  ak_0^r e^{\frac{(1-\beta) n}{V_T}\eta}
\end{equation}
and the reverse reaction rate constant with the addition of oxygen diffusion coefficient term
\begin{equation}
k_{\textrm{wD}_\textrm{eff}}^r = \frac{k^r}{D_\textrm{eff}}
\end{equation}
The complete reaction rate in the cathode is given by
\begin{equation}
R_c = nF\left[k^f c_{o_2}(x) - k^r\right]\label{eqn:chap5_FBV_Rc}
\end{equation}

\subsection{Steady-state analysis}
The mass transport of oxygen inside the cathode is described using Fick's second law of diffusion. For this analysis the effects of convection are neglected, since it is assumed that the setup is not disturbed while the impedance spectrum is measured. The convection term in the Nernst-Plank becomes important while computing the impedance response of Li-air flow type of batteries. The mass transport of oxygen in Li-air batteries under steady state analysis is given by 
\begin{equation}
\frac{\partial^2 c_{o_2}(x)}{\partial x^2} - k_{\textrm{wD}_\textrm{eff}}^f c_{o_2}(x) + k_{\textrm{wD}_\textrm{eff}}^r = 0 \label{eq:Ficks_second_law_FBV_steady_state}
\end{equation}
The solution to the above equation (eqn.~\ref{eq:Ficks_second_law_FBV_steady_state}) is,
\begin{equation}
c_{o_2}(x) = C_1\cosh{\left(\sqrt{k_{\textrm{wD}_\textrm{eff}}^f}x\right)} + C_2\sinh{\left(\sqrt{k_{\textrm{wD}_\textrm{eff}}^f}x\right)} + \frac{k_{\textrm{wD}_\textrm{eff}}^r}{k_{\textrm{wD}_\textrm{eff}}^f} \label{eq:Solution_to_Ficks_second_law_FBV_steady_state}
\end{equation}
where C$_1$ and C$_2$ are the integration constants. The integration constants are computed using the 
boundary conditions:
\begin{subequations}
\begin{enumerate}
\item[(a)] \textbf{Boundary condition at $x=0$}
\begin{align}
c_{o_2}(0) = \frac{p_{o_2}}{k_H}\label{bc:FBV_c_at_0}
\end{align}
\item[(b)] \textbf{Boundary condition at $x=l$}
\begin{align}
\left.\frac{\textrm{d}c_{o_2}(x)}{\textrm{d}x}\right|_{x=l} = 0\label{bc:FBV_c_at_l}
\end{align}
\end{enumerate}
\end{subequations}
Using boundary conditions (eqns.~\ref{bc:FBV_c_at_0} and~\ref{bc:FBV_c_at_l}) and eqn.~\ref{eq:Solution_to_Ficks_second_law_FBV_steady_state}, the expressions for the integration constants become
\begin{align}
C_1 &= \frac{p_{o_2}}{k_H} - \frac{k_{\textrm{wD}_\textrm{eff}}^r} {k_{\textrm{wD}_\textrm{eff}}^f}\\
C_2 &= \left(\frac{k_{\textrm{wD}_\textrm{eff}}^r} {k_{\textrm{wD}_\textrm{eff}}^f} - \frac{p_{o_2}}{k_H}\right)\tanh{\left(\sqrt{k_{\textrm{wD}_\textrm{eff}}^f}l\right)}
\end{align}
The final expression for the oxygen concentration distribution inside the cathode is 
\begin{equation}
\boxed{c_{o_2}(x) = \left(\frac{p_{o_2}}{k_H} - \frac{k_{\textrm{wD}_\textrm{eff}}^r} {k_{\textrm{wD}_\textrm{eff}}^f}\right)\left[\cosh{\left(\sqrt{k_{\textrm{wD}_\textrm{eff}}^f}x\right)} - \tanh{\left(\sqrt{k_{\textrm{wD}_\textrm{eff}}^f}l\right)}\sinh{\left(\sqrt{k_{\textrm{wD}_\textrm{eff}}^f}x\right)}\right] + \frac{k_{\textrm{wD}_\textrm{eff}}^r}{k_{\textrm{wD}_\textrm{eff}}^f}}\label{eqn:chap5_co2_solution}
\end{equation}
The Faradaic component ($I_F$) of the discharge current is computed using
\begin{equation}
I_F = A \int_{0}^{l}R_c\,\textrm{d}x
\end{equation}
Substituting $R_c$ from eqn~\ref{eqn:chap5_FBV_Rc} and separating the integrals
\begin{equation}
I_F  =  nFA\left[k^f \int_{0}^{l}c_{o_2}(x)\,\textrm{d}x - \int_{0}^{l}k^r\,\textrm{d}x\right]\label{eq:FBV_I_F_w_integrals}
\end{equation}
Solving the last integral in the above expression and after applying integration limits we get,
\begin{equation}
I_F  =  nFAk^f \int_{0}^{l}c_{o_2}(x)\,\textrm{d}x - nFAk^rl\label{eqn:chap5_I_integral_partition}
\end{equation}
Solving the first integral in eqn.~\ref{eq:FBV_I_F_w_integrals}, since, the intermediate solutions to the integral are long, it is solved in parts. Substituting the expression for $c_{o_2}(x)$ from eqn~\ref{eqn:chap5_co2_solution} into the first part of the integral
\begin{equation}
nFAk^f\int_{0}^{l}c_{o_2}(x)\,\textrm{d}x = nFAk^f\left(\frac{p_{o_2}}{k_H} - \frac{k_{\textrm{wD}_\textrm{eff}}^r} {k_{\textrm{wD}_\textrm{eff}}^f}\right)\int_{0}^{l}\textrm{int}_1\,\textrm{d}x + nFAk^f\int_{0}^{l}\textrm{int}_2\,\textrm{d}x\label{eqn:chap5_co2_integral_partition} 
\end{equation}
The integral is separated into two parts (int$_1$ and int$_2$). These two parts of the integral are solved separately and substituted back into the above equation
\begin{equation}
\int_{0}^{l}\textrm{int}_1\,\textrm{d}x = \int_{0}^{l}\left[\cosh{\left(\sqrt{k_{\textrm{wD}_\textrm{eff}}^f}x\right)} - \tanh{\left(\sqrt{k_{\textrm{wD}_\textrm{eff}}^f}l\right)}\sinh{\left(\sqrt{k_{\textrm{wD}_\textrm{eff}}^f}x\right)}\right]
\end{equation}
The integration of int$_1$ yields
\begin{equation}
\int_{0}^{l}\textrm{int}_1\,\textrm{d}x = \frac{1}{\sqrt{k_{\textrm{wD}_\textrm{eff}}^f}}\left[\sinh{\left(\sqrt{k_{\textrm{wD}_\textrm{eff}}^f}x\right)} - \tanh{\left(\sqrt{k_{\textrm{wD}_\textrm{eff}}^f}l\right)}\cosh{\left(\sqrt{k_{\textrm{wD}_\textrm{eff}}^f}x\right)}\right]_0^l
\end{equation}
after applying integration limits,
\begin{equation}
\int_{0}^{l}\textrm{int}_1\,\textrm{d}x = \frac{1}{\sqrt{k_{\textrm{wD}_\textrm{eff}}^f}} \tanh{\left(\sqrt{k_{\textrm{wD}_\textrm{eff}}^f}l\right)}\label{eqn:chap5_int1_solution}
\end{equation}
And the integration of int$_2$ yields,
\begin{equation}
\int_{0}^{l}\textrm{int}_2\,\textrm{d}x = \frac{k_{\textrm{wD}_\textrm{eff}}^r}{k_{\textrm{wD}_\textrm{eff}}^f}\,\textrm{d}x = \frac{k_{\textrm{wD}_\textrm{eff}}^r}{k_{\textrm{wD}_\textrm{eff}}^f}\;l\label{eqn:chap5_int2_solution}
\end{equation}
Substituting the eqns.~\ref{eqn:chap5_int1_solution} and~\ref{eqn:chap5_int2_solution} back into eqn.~\ref{eqn:chap5_co2_integral_partition}, results in 
\begin{equation}
\int_{0}^{l}c_{o_2}(x)\,\textrm{d}x = \left(\frac{p_{o_2}}{k_H} - \frac{k_{\textrm{wD}_\textrm{eff}}^r} {k_{\textrm{wD}_\textrm{eff}}^f}\right)\frac{\tanh{\left(\sqrt{k_{\textrm{wD}_\textrm{eff}}^f}l\right)}}{\sqrt{k_{\textrm{wD}_\textrm{eff}}^f}}  + \frac{k_{\textrm{wD}_\textrm{eff}}^r}{k_{\textrm{wD}_\textrm{eff}}^f}\;l\label{eqn:chap5_co2_integral_partition_w_solution}
\end{equation}
Now we substitute eqn.~\ref{eqn:chap5_co2_integral_partition_w_solution} back into eqn.~\ref{eqn:chap5_I_integral_partition}

\begin{equation}
I_F  =  \left(\frac{p_{o_2}}{k_H} - \frac{k^r} {k^f}\right)\frac{nFAk^f\tanh{\left(\sqrt{k_{\textrm{wD}_\textrm{eff}}^f}l\right)}}{\sqrt{k_{\textrm{wD}_\textrm{eff}}^f}}  + nFAk^f \frac{k^r}{k^f}\;l - nFAk^rl
\end{equation}
after rearranging, we get the final equation for the steady-state Faradaic current in Li-air batteries with organic electrolyte during charge and discharge after solving the integrals in the above equation is,
\begin{equation}
I_F  =  \left(\frac{p_{o_2}}{k_H} - \frac{k^r} {k^f}\right)\frac{nFAk^f\tanh{\left(\sqrt{k_{\textrm{wD}_\textrm{eff}}^f}l\right)}}{\sqrt{k_{\textrm{wD}_\textrm{eff}}^f}}
\end{equation}
Now replacing $k_{\textrm{wD}_{\textrm{eff}}}^{f}$ from eqn.\ref{eq:kfwDeff_FBV}
into the above equation. The final expression for steady-state Faradaic current is, 
\begin{equation}
\boxed{I_{F}=nFA\sqrt{k^{f}D_{\textrm{eff}}}\left(\frac{p_{o_{2}}}{k_{H}}-\frac{k^{r}}{k^{f}}\right)\tanh\left(\sqrt{\frac{k^{f}}{D_{\textrm{eff}}}}l\right)}\label{eq:FBV_steady_state_final_equation}
\end{equation}
The Faradaic current contributed due to charging or discharging dc current is a function of forward and backward reaction rate constant, effective diffusion coefficient of oxygen in the electrolyte, length of the cathode, dissolvation of oxygen in the organic electrolyte, partial pressure of oxygen. 

\subsection{Small-signal analysis}
\todo[author=Mohit,noline]{write small-signal analysis}

\end{document}