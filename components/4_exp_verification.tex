\documentclass[../thesis.tex]{subfiles}
\providecommand{\main}{../}
\begin{document}
\chapter{Experimental verification and parameter extraction}
\label{chap:Expt_verification}


\section{Introduction}

Electrochemical impedance spectroscopy can be used to extract the
values of many physical parameters of electrochemical systems. In
this chapter a technique to extract information about the reaction
rate, specific surface area, ohmic dissipation, and effective diffusion
coefficient in the cathode is introduced. Low frequency impedance
spectra in Li-air batteries can be separated into two cases depending
on whether the Faradaic impedance is masked or not by the double layer
capacitance: (1) the case when the effects of the Faradaic impedance
are separated from the effects of the double layer capacitance so
the Nyquist plot presents two different semicircles corresponding
to the cathode diffusion-reaction layer, and (2) the case when the
effects of the Faradaic impedance are hidden by the effects of the
double layer capacitance and the Nyquist plot presents only one large
semicircle corresponding to the cathode diffusion-reaction region.
The first technique is more accurate as it uses more experimental
data from the Nyquist plot, however, if the specific area of the cathode
is larger than \SI[per-mode=symbol]{e4}{\centi\meter\squared\per\centi\meter\cubed}
it might be impossible to identify the two semicircles, in which case
one needs to use the second technique. In the following analysis only
the low-frequency semicircle corresponding to the cathode is considered
and all the other semicircles attributed to the anode or anode-separator
interface are eliminated from the Nyquist plot.


\section{Parameter extraction at low cathode specific areas}

When the specific area of the cathode is relatively low the impedance
spectra present two semicircles: one due to oxygen diffusion and the
other to charge transfer, as shown in Fig.~\ref{fig:parameter_extraction_low_a}.
To find equations for the effective oxygen diffusion coefficient and
reaction rate it is instrumental to rewrite the Faradaic impedance
obtained in chapter \ref{chap:EIS_under_dc_discharge} into an alternate
form 
\begin{equation}
Z_{F}\left(i_{F},\omega\right)=\frac{V_{T}}{n\beta i_{F}}\times F\left[\frac{\omega\epsilon_{0}\lambda\left(i_{F}\right)^{2}}{D_{\textrm{eff}}},\frac{l}{\lambda\left(i_{F}\right)}\right]\label{eq:Z_F using_F_function}
\end{equation}
where $\lambda=\sqrt{\frac{D_{\textrm{eff}}}{k^{f}}}$. The value
of \textbackslash{}lambda depends on the d.c. value of the discharge
current and can be computed by solving the following nonlinear equation
\begin{equation}
i_{F}=\frac{nFAc_{o}^{*}D_{\textrm{eff}}}{\lambda}\tanh\left(\frac{l}{\lambda}\right)\label{eq:i_F_using_Andrei_representation}
\end{equation}
which is obtained from eqn. \ref{eq:dc_i_F_complete_soln}. Function
$F$ is complex and depends on the d.c. value of the discharge current
and cathode length. It is defined as
\begin{equation}
F\left(\Omega,L\right)=\frac{j\Omega}{\frac{\tanh\left(\sqrt{1+j\Omega}L\right)}{\sqrt{1+j\Omega}\tanh\left(L\right)}+j\Omega-1}
\end{equation}
Function $F\left(\Omega,L\right)$ has the following mathematical
properties: 
\begin{equation}
\lim_{l\rightarrow\infty}F\left(\Omega,L\right)=\frac{j\Omega\left(1+j\Omega\right)}{\sqrt{1+j\Omega}-1+\left(j\Omega\right)^{2}}=\frac{j\Omega}{\frac{\left(j\Omega\right)^{2}-1}{1+j\Omega}+\frac{1}{\sqrt{1+j\Omega}}}=\frac{j\Omega}{j\Omega-1+\frac{1}{\sqrt{1+j\Omega}}}\label{eq:param_extract_l_tends_inf}
\end{equation}
\begin{equation}
\lim_{l\rightarrow0}F\left(\Omega,L\right)=\lim_{l\rightarrow0}\frac{j\Omega\left(1+j\Omega\right)}{1+j\Omega-1+\left(j\Omega\right)^{2}}=1
\end{equation}
\begin{equation}
\lim_{l\rightarrow\infty}F\left(\Omega,L\right)=1
\end{equation}
\begin{equation}
\lim_{l\rightarrow0}F\left(\Omega,L\right)=\frac{1}{\frac{1}{2}+\frac{l}{\sinh\left(2l\right)}}\in\left[1,2\right]\label{eq:param_extraction_Omega_tends_0}
\end{equation}
\begin{equation}
\textrm{Im}\left[F\left(\Omega,L\right)\right]\le0\label{eq:our_ana_paper_eqn.C5}
\end{equation}
The total impedance $Z$ expressed in eqn. \ref{eq:Z_R_C_ZF_representation}
can be rewritten into a normalized form by using eqn. \ref{eq:Z_F using_F_function}
\begin{equation}
Z=R_{\Omega}+\frac{RF\left(\omega/\omega_{0},l/\lambda\right)}{1+j\omega RC_{D}F\left(\omega/\omega_{0},l/\lambda\right)}\label{eq:para_extract_small_a_Z_rep}
\end{equation}
where $R_{\Omega}$ is the sum of parasitic resistances such as resistance
of the electrolyte, electrical contacts, separator, anode-separator
interface, solid electrolyte interface layer, etc., and 
\begin{equation}
R=\frac{V_{T}}{n\beta i}\label{eq:para_extract_R}
\end{equation}
\begin{equation}
\omega_{0}=\frac{D_{\textrm{eff}}}{\epsilon_{0}\lambda^{2}},
\end{equation}
Under steady-state conditions, the Faradaic steady-state current is
equal to the total current at steady-state ($i_{F}=i$). Using the
mathematical properties of function $F$, the expressions for the
impedance at low and high frequencies are :
\begin{equation}
\lim_{\Omega\to0}Z=R_{\Omega}+R\frac{2\sinh\left(2L\right)}{\sinh\left(2L\right)+2L}
\end{equation}
\begin{equation}
\lim_{\Omega\to\infty}Z=R_{\Omega}
\end{equation}
\begin{figure}
\begin{center}\includegraphics[width=0.6\textwidth]{\main figures/chapter4_para_extract_small_a}\end{center}\caption{Possible low frequency impedance spectra of Li-air batteries with
low specific area of the cathode. The dashed continuation line at
high frequencies show that the Nyquist spectra might contain other
semicircles, which are usually due to the anode and anode-separator
interface. }


\label{fig:parameter_extraction_low_a}
\end{figure}
Denoting the impedance at low frequency ($\omega\rightarrow0$) by
$Z_{0}$ and at very high frequencies ($\omega\rightarrow\infty$)
by $Z_{\infty}$, equation \ref{eq:para_extract_small_a_Z_rep} becomes
\begin{equation}
Z=Z_{\infty}+\frac{F\left(\omega/\omega_{0},l/\lambda\right)}{\frac{2\sinh\left(2L\right)}{\left(Z_{0}-Z_{\infty}\right)\sinh\left(2L\right)+2L}+j\omega C_{D}F\left(\omega/\omega_{0},l/\lambda\right)}\label{eq:para_extract_small_a_Z}
\end{equation}
The last equation needs to be fitted to the experimental impedance
spectra to compute $Z_{0}$, $Z_{\infty}$, $C_{D}$, $\omega_{0}$,
and $\lambda$. Once these parameters are determined the effective
oxygen diffusion coefficient can be computed using the value of $\lambda$
and solving the following equation
\begin{equation}
D_{\textrm{eff}}=\frac{i\lambda}{2AFc_{o}^{*}}\coth\left(\frac{l}{\lambda}\right)\label{eq:para_extract_small_a_Deff}
\end{equation}
If the value of the over-potential $\eta$ is estimated using the
d.c. measurements then the reaction rate ($k^{f}$ is expressed in
\si{\per\second}) can be computed using:
\begin{equation}
k_{0}^{f}a=\frac{D_{\textrm{eff}}}{\lambda^{2}}e^{\frac{n\beta}{V_{T}}\eta}.\label{eq:para_extract_small_a_k}
\end{equation}
It is important to note that most of the fitting parameters in eqn.
\ref{eq:para_extract_small_a_Z} can be determined easily from the
experimental data. Indeed, $Z_{0}$ and $Z_{\infty}$ are the low
and high frequencies impedances and can be read directly from the
data, while parameter $\lambda$ can be computed using the ratio of
the diameter of the low frequency semicircle to the diameter of the
high frequency semicircle (see notations in Fig.~\ref{fig:parameter_extraction_low_a}),
i.e.:
\begin{equation}
\frac{R_{1}}{R_{2}}=\frac{1}{\frac{1}{2}+\frac{L}{\sinh\left(2L\right)}}-1=\frac{\sinh\left(2L\right)-2L}{\sinh\left(2L\right)+2L}=\frac{\sinh\left(2l/\lambda\right)-2l/\lambda}{\sinh\left(2l/\lambda\right)+2l/\lambda}\label{eq:para_extract_small_a_R1/R2}
\end{equation}
which can be derived from eqn. \ref{eq:param_extraction_Omega_tends_0}. 

The parameter determination technique can be summarized as follows:
one first computes ratio $l/\lambda$ by solving nonlinear eqn. \ref{eq:para_extract_small_a_R1/R2},
then the effective oxygen diffusion coefficient from \ref{eq:para_extract_small_a_Deff}
and the reaction rate from \ref{eq:para_extract_small_a_k}. The remaining
two parameters, $C_{D}$ and $\omega_{0}$, can be identified using
nonlinear least-square estimations or alternative numerical techniques. 


\section{Parameter extraction at high cathode specific areas}

When the specific area of the cathode is relatively high the Faradaic
component of the impedance is masked by the high capacitance of the
double layer and the impedance spectrum of the cathode presents only
one semicircle as shown in Fig. \ref{fig:parameter_extraction_large_a}.
In this case, eqns. \ref{eq:para_extract_small_a_Z}-\ref{eq:para_extract_small_a_k}
still hold, however, radii $R_{1}$ and $R_{2}$ cannot be identified
using eqn. \ref{eq:para_extract_small_a_R1/R2}. In this case it is
more convenient to determine resistance $R_{12}$ from the experimental
spectra and use the following equation
\begin{equation}
R_{12}=\frac{V_{T}}{n\beta i}\frac{2\sinh\left(2L\right)}{\sinh\left(2L\right)+2L}=\frac{V_{T}}{n\beta i}\frac{2\sinh\left(2l/\lambda\right)}{\sinh\left(2l/\lambda\right)+\left(2l/\lambda\right)}\label{eq:para_extract_small_a_R12}
\end{equation}
In this case, the ratio $l/\lambda$ is computed by solving eqn. \ref{eq:para_extract_small_a_R12}
and, then, the effective oxygen diffusion coefficient and the reaction
rate are calculated using eqns. \ref{eq:para_extract_small_a_Deff}
and \ref{eq:para_extract_small_a_Deff} respectively. The value of
charge transfer coefficient ($n$) that is needed to compute $l/\lambda$
from eqn. \ref{eq:para_extract_small_a_R12} can be computed either
using discharge curves or by fitting the experimental and theoretical
values of frequency $\omega_{0}$. 
\begin{figure}
\begin{center}\includegraphics[width=0.6\linewidth]{\main figures/chapter4_para_extract_large_a}\end{center}\protect\caption{Possible low frequency impedance spectra of Li-air batteries with
high specific area of the cathode. The dashed lines show that the
Nyquist spectra might contain other semicircles at high frequencies,
which are usually due to the anode and anode-separator interface. }


\label{fig:parameter_extraction_large_a}
\end{figure}



\section{Experimental verification}

The validity of the analytical model for Li-air batteries under high
d.c. discharge is tested by comparing the theoretical predictions
(obtained by eqns. \ref{eq:dc_i_F_complete_soln} and \ref{eq:ZF_final_Tafel})
to experimental data by Adams et al.\cite{Adams2013}, who measured
the impedance spectra for rechargeable Li-air batteries with cathodes
made using different fabrication techniques: separate cast and dual
cast cathodes with soaked and embedded electrolytes, room temperature
ionic liquid embedded cathodes, and PTFE-calendered cathodes. The
capacitance of the diffusion layer $C_{dl}=\SI[per-mode=symbol]{7}{\micro\farad\per\centi\meter\squared}$
and the specific area of the cathode $a=\SI[per-mode=symbol]{5e4}{\centi\meter\squared\per\centi\meter\cubed}$
were measured experimentally. The large value of the specific area
of the cathode suggests that the Faradaic impedance is hidden by the
double layer capacitance and this effect is indeed observed in the
experimental results (fig. \ref{fig:parameter_extraction_prac_vs_eis_paper})
that show only a single semicircle at the low frequency with the diameter
equal to $R_{12}$. The low and high frequency intercepts on fig.
\ref{fig:parameter_extraction_prac_vs_eis_paper} provide the values
of $Z_{0}=\SI{19}{\ohm}$ and $Z_{\infty}=\SI{570}{\ohm}$ from the
experimental data. Solving eqn. \ref{eq:para_extract_small_a_R12}
the value of $L$ is computed as 
\[
L=l/\lambda=1.96
\]
For a cathode with $l=\SI{0.01}{\milli\meter}$ (such as the one used
in Ref. \cite{Adams2013}), eqn. \ref{eq:para_extract_small_a_Deff}
gives an effective value of the diffusion coefficient of $D_{\textrm{eff}}=\SI[per-mode=symbol]{5.6e-6}{\centi\meter\squared\per\second}$.
For a standard cathode with porosity $\epsilon_{0}=75\%$ and Bruggeman
coefficient of 1.5, the bulk diffusion coefficient is $D_{o}=\SI[per-mode=symbol]{8.6e-6}{\centi\meter\squared\per\second}$,
which is in good agreement with predictions by Read et al. \cite{Read2003}
who obtained $D_{o}=\SI[per-mode=symbol]{7e-6}{\centi\meter\squared\per\second}$.
\begin{figure}
\begin{center}\includegraphics[width=0.6\textwidth]{\main figures/exp_vs_eis_paper}\end{center}\protect\caption{Comparison between the theoretical and experimental impedance spectra.
$R_{12}$ is the diameter of the second semicircle along the real
axis and can be used to determine the effective value of the oxygen
diffusion coefficient and the reaction rate in the cathode using equations
\ref{eq:para_extract_small_a_Deff}, \ref{eq:para_extract_small_a_k},
and \ref{eq:para_extract_small_a_R12}. }


\label{fig:parameter_extraction_prac_vs_eis_paper}
\end{figure}
 The theoretical predictions are compared to experimental data in
Fig. \ref{fig:parameter_extraction_prac_vs_eis_paper}. The frequency
was varied between 1 mHz and 1 MHz in agreement with the experimental
data. The low frequency points were not used in the parameter determination
because, as mentioned by the authors, those points were subject to
high experimental errors due to the long measurement time. Notice
that the experimental spectra does not start with a slope of $45^{\circ}$
at high frequencies like in the case of traditional Gerischer type
impedance spectra (see appendix for more information) but at a much
higher slope as predicted by the analytical model for high d.c. discharge
current. The high frequency semicircle of the cell simulated in Fig.
\ref{fig:parameter_extraction_prac_vs_eis_paper} is due to the anode-separator
interface and is modeled with a constant phase element represented
using $1/\left(j\omega\right)^{\alpha}Q$, where parameters $\alpha=0.8$
and $Q=8.2\times10^{-3}$ are measured experimentally \cite{Adams2013}. 

The analytical model predicts the experimental values of resistance
$R_{12}$ (which is denoted as $R_{2}$ in Ref. \cite{Adams2013})
remarkably well. Indeed, at relatively large discharge currents, eqn.
\ref{eq:i_F_using_Andrei_representation} predicts large values for
$L$, so $R_{12}=\frac{2V_{T}}{n\beta i}$. Hence, for a d.c. discharge
current of $\SI[per-mode=symbol]{0.1}{\milli\ampere\per\centi\meter\squared}$
and $n=2$, the predicted value of $R_{12}$ is the \SI{1}{\kilo\ohm},
which agrees very well with the value of $1\pm0.02$ \SI{}{\kilo\ohm}
presented in Fig. 14 from Ref. \cite{Adams2013}. At a discharge current
of \SI{0.6}{\milli\ampere\per\centi\meter\squared}, by keeping the
same value of the charge transfer coefficient $n=2$, the predicted
value of $R_{12}$ comes out to be \SI{166}{\ohm}, which agrees relatively
well with the value of ($145\pm25$ \SI{}{\ohm}) data from the same
reference. The slight overestimate in the value of $R_{12}$ is due
to the fact that in reality $n$ increases slightly with the value
of the discharge current. In addition, it is shown experimentally
that at relatively large discharge currents the values of this resistance
is independent of the cathode fabrication technique and oxygen concentration,
in agreement with the analytical model (eqn. \ref{eq:para_extract_small_a_R12},
which is independent of oxygen concentration, specific area, and tortuosity). 

At low discharge currents, the value of $L$ become sensitive to the
oxygen concentration and can vary from $L=\infty$ when the oxygen
concentration is large to $L=0$ when the oxygen concentration is
very small. Hence, by decreasing the oxygen concentration one can
double the value of $R_{12}$ from $R$ to $2R$ (as implied by \ref{eq:para_extract_small_a_R12}
and \ref{eq:param_extraction_Omega_tends_0}). This effect is clearly
observed in the experimental data presented in Ref. \cite{Adams2013},
where $R_{12}$ increases from \SI{860}{\ohm} to \SI{1570}{\ohm}
when the oxygen concentration is decreased from $100\%$ to $5\%$. 


\section{Equivalent circuit model (ECM)}

In this section, a technique to extract electrochemical parameters
of a Li-air battery using a small-signal equivalent circuit is developed.
Using the ECM technique, the total impedance ($Z$) of the circuit
can be represented as a resistor $R_{\Omega}$ in series with a parallel
combination of the double-layer capacitance ($C_{D}$) with impedance
the Faradaic impedance ($Z_{F}$) (fig. \ref{fig:ECM_our_model_using_ZF}
represents the equivalent circuit model) 
\begin{equation}
Z=R_{\Omega}+\frac{1}{j\omega C_{D}}\parallel Z_{F}\label{eq:Total_impedance of the circuit}
\end{equation}
\begin{figure}
\begin{center}\includegraphics[width=0.5\textwidth]{\main figures/ECM_our_model_w_ZF}\end{center}\protect\caption{Small-signal equivalent circuit of Li-air batteries. $Z_{F}$ denotes
the Faradaic impedance, $C_{D}$ is the capacitance of the double
layer, $R_{\Omega}$ is the combined resistance of the electrolyte,
Li-ions, and electrons in the cathode matrix.}
\label{fig:ECM_our_model_using_ZF}
\end{figure}
where $R_{\Omega}$ represents the combined resistance of the electrolyte,
anode, and anode-separator interface, which might play a significant
role in Li-air batteries as suggested by Adams et al. \cite{Adams2013}
In such cases $R_{\Omega}$ might be modeled with resistors in series
with one or more parallel resistor-capacitor (or resistor-constant
phase element) pairs connected in series. The circuit corresponding
to model \ref{eq:Total_impedance of the circuit} is represented in
Fig. \ref{fig:ECM_our_model_using_ZF}. In this circuit, the Faradaic
impedance $Z_{F}$ is given in terms of the geometric and material
properties of the battery by using an alternate equation \ref{eq:Z_F using_F_function}.
This circuit can be compared to the standard Randles equivalent circuit
\cite{Randles1947}.

Depending on the value of $\lambda$ it is worthwhile investigating
two special cases: one for large currents and cathode widths ($l\gg\lambda$)
and the other one for small currents and cathode widths ($l\ll\lambda$).
The condition $l\gg\lambda$ implies that
\begin{equation}
\frac{il}{A}\gg nFc_{o}^{*}D_{\textrm{eff}}\label{eq:ECM_large_cathode_widths_condition}
\end{equation}
which can be obtained directly from~\ref{eq:i_F_using_Andrei_representation}.
Using typical values for a Li-air battery with organic electrolyte,
$n=2$ \cite{Adams2013}, $c_{o}^{*}=\SI[per-mode=symbol]{3.26e-6}{mol\per\centi\meter\cubed}$
\cite{Andrei2010}, $D_{o}=\SI[per-mode=symbol]{7e-6}{centi\meter\per\second}$,
and using the Bruggeman relation $D_{\textrm{eff}}=\epsilon^{brugg}D_{o}$,
the condition~ \ref{eq:ECM_large_cathode_widths_condition} becomes
\begin{equation}
\frac{il}{A}\gg4.4\times10^{-6}\,\frac{\textrm{A}}{\textrm{cm}}
\end{equation}
This condition is often satisfied during the normal operation of Li-air
batteries particularly when the discharge current and cathode widths
are large enough. 
\begin{figure}
\begin{center}\includegraphics[width=0.6\textwidth]{\main figures/chapter4_semicircle_approx}\end{center}\protect\caption{Nyquist plot of the Faradaic impedance at large discharge currents
and cathode widths (i.e. $l\gg\lambda$). }


\label{fig:ECM_semi_circle_approx}
\end{figure}
If both the cathode width and the discharge current are large enough
so that condition \ref{eq:ECM_large_cathode_widths_condition} is
satisfied, $L\gg1$ and the total Faradaic impedance can be approximated
to 
\begin{equation}
Z_{F}\simeq\frac{V_{T}}{n\beta i}\,\frac{j\Omega}{j\Omega-1+\frac{1}{\sqrt{1+j\Omega}}}
\end{equation}
The last equation was derived using approximations \ref{eq:param_extract_l_tends_inf}-\ref{eq:our_ana_paper_eqn.C5}.
The Nyquist diagram of the Faradaic impedance is represented in Fig.
\ref{fig:ECM_semi_circle_approx} with continuous line. Notice that
the real part of the impedance extends from $R=\frac{V_{T}}{n\beta i}$
at high frequencies to $\frac{2V_{T}}{n\beta i}$ at low frequencies
and the absolute value of the imaginary part has a maximum at $\Omega_{\textrm{max}}=0.647$,
for which $\textrm{Re}\left[Z_{F}\left(\Omega_{\textrm{max}}\right)\right]=\frac{1.521\times V_{T}}{n\beta i}$
and $\textrm{Im}\left[Z_{F}\left(\Omega_{\textrm{max}}\right)\right]=0.471\times V_{T}$
. At this $\Omega$ can be written as 
\begin{equation}
\frac{\omega_{\textrm{max}}\epsilon_{0}\lambda^{2}}{D_{\textrm{eff}}}=\Omega_{\textrm{max}}=0.647\label{eq:ECM_Omega_max}
\end{equation}
The impedance spectra represented in Fig. \ref{fig:ECM_semi_circle_approx}
can be approximated with a semicircle with radius equal to $R$ and
centered at $1.5\times R$, where $R$ is given by eqn. \ref{eq:para_extract_R}
(see the dashed line in Fig. \ref{fig:ECM_semi_circle_approx}). It
is known that such a semicircle can be modeled by a resistor $R$
in series with another resistor of the same value $R$ in parallel
with a capacitor $C$ (see the network encircled with dash line in
Fig. \ref{fig:ECM_our_model_figure_representation}). 
\begin{figure}
\begin{center}\includegraphics[width=0.6\textwidth]{\main figures/ECM_our_model_wo_ZF}\end{center}\protect\caption{Approximate small-signal equivalent circuit of Li-air batteries operating
at large discharge currents and with large cathode width ($l\gg\lambda$).
The values of $R_{\Omega}$, $C_{D}$, $R$, and $C$ can be expressed
in terms of physical parameters using eqns. \ref{eq:small_signal_ohmic_resistance},
\ref{eq:double_layer_capacitance_defn}, \ref{eq:para_extract_R},
and \ref{eq:ECM_C}, respectively. This circuit should be used with
care in practical applications and, instead, one should use the more
general circuit from Fig. \ref{fig:ECM_our_model_using_ZF}. }


\label{fig:ECM_our_model_figure_representation}
\end{figure}
The impedance of this network is 
\begin{equation}
Z_{F0}=R\left(1+\frac{1}{1+j\omega RC}\right)=R\left(1+\frac{1}{1+j\omega/\omega_{\textrm{max}}}\right)\label{eq:ECM_ZF0}
\end{equation}
where
\begin{equation}
\omega_{\textrm{max}}=\frac{1}{RC}
\end{equation}
is the frequency at which the absolute value of the imaginary component
of $Z_{F0}$ is maximum. Eqns. \ref{eq:para_extract_R}, \ref{eq:ECM_Omega_max},
and \ref{eq:ECM_ZF0} can be solved to compute $C$ as a function
of the parameters of the battery
\begin{equation}
C=\frac{\epsilon_{0}\lambda^{2}}{RD_{\textrm{eff}}\Omega_{\textrm{max}}}\label{eq:ECM_C_interm}
\end{equation}
The diffusion length under the condition of large cathode widths ($l/\lambda\rightarrow\infty$)
can be written as 
\begin{equation}
\lambda=\frac{nFAD_{\textrm{eff}}c_{o}^{*}}{i}\label{eq:lambda_large_cathode_widths}
\end{equation}
Using the eqns. \ref{eq:ECM_C_interm} and \ref{eq:lambda_large_cathode_widths}
the capacitance $C$ can be written as
\begin{equation}
C=\frac{n^{3}F^{2}A^{2}\beta\epsilon_{0}D_{\textrm{eff}}\left(c_{o}^{*}\right)^{2}}{V_{T}\Omega_{\textrm{max}}i}\label{eq:ECM_C}
\end{equation}
Hence, the small-signal equivalent circuit of Li-air batteries can
be approximated with the one shown in Fig. \ref{fig:ECM_our_model_figure_representation}
with $R$ and $C$ given by equations \ref{eq:para_extract_R} and
\ref{eq:ECM_C}, where $\Omega_{\textrm{max}}=0.647$. The circuit
represented in Fig. \ref{fig:ECM_our_model_figure_representation}
and eqn. \ref{eq:ECM_C} should be used with care in practical applications
because it requires large discharge currents under which the porosity
of the cathode can change significantly in time over the duration
of the impedance measurements. 

One can show that $\parallel Z_{F0}(\Omega)-Z(\Omega)\parallel<4.3\%$
for all values of $\Omega$. Hence, in the case of Li-air batteries
with wide cathode width, the error in using the small-signal equivalent
circuit shown in Fig. \ref{fig:ECM_our_model_figure_representation}
instead of the complex circuit elements shown in Fig. \ref{fig:ECM_our_model_using_ZF}
is $4.3\%$. Equations \ref{eq:para_extract_R} and \ref{fig:ECM_our_model_using_ZF}
are instrumental because they relate the small-signal circuit elements
to the geometrical and electrochemical properties of the battery.
By properly fitting the experimental data to the small-signal equivalent
circuit shown in Fig. \ref{fig:ECM_our_model_figure_representation}
one can extract information about the effective oxygen diffusion coefficient
and the reaction rate constant.
\end{document}