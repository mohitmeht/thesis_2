\documentclass[../../thesis.tex]{subfiles}
\providecommand{\main}{../../}
\begin{document}
\section{Impedance calculation}
In this section we derive the Faradaic impedance for porous electrode under dc discharge. The steps followed are very similar to those in the previous chapter. Those steps were, first we define Faradaic current for a particular case (also Fick's first law of diffusion) then state Fick's second law of diffusion original or modified (here we use an additional term for the products formed during discharge, additional term was used in the case of Gerischer impedance in sec.~\ref{sec:Gerischer_impedance}), after that we define the boundary and initial conditions. We then solve the second order differential equation and apply the boundary conditions to obtain a solution for oxidant concentration. We substitute this oxidant concentration in Faradaic current to obtain dc current contribution to the Faradaic impedance. Next we find small signal Faradaic current by linearizing the second order differential equation and then solving for small signal oxidant concentration. Then we combine the dc and small signal Faradaic currents. Finally we compute the Faradaic impedance.
\paragraph{Analysis under dc discharge:}The Faradaic current for porous electrodes is sum of all reaction sites and is given by
\begin{equation}
i_F=A\int\limits_{0}^{l}R_c\,dx\label{eq:Faradaic_current_wo_rc_integral}
\end{equation}
where, $A$ is the cross-sectional area of the cathode and $Rc$ is the specific reaction rate at the cathode~\cite{Andrei2010}. The specific reaction rate is obtained by simplifying the Butler Volmer equation,
\begin{equation}
R_c=nFk\,c_o(x,t)\label{eq:Li2O2_model_1_reaction_rate}
\end{equation}
where, $F$ is the Faraday's constant, $k$ is the reaction rate. When $\eta<0$ the simplified reaction rate is given as,
\begin{equation}
k=k_0\,a(\epsilon)\,e^{-\frac{n\beta}{V_T}\eta}\label{eq:dc_reaction_rate_contant}
\end{equation} 
where, $k_0$ is the standard reaction rate constant, $a(\epsilon)$ is the specific surface area of the cathode, $\eta$ is the over-potential and $\beta$ is the symmetry factor of the electrochemical reaction.
The specific area is given using the following relation,
\begin{align}
a(\epsilon)&=\epsilon/\overline{r}_{p}\label{eq:specific_area}\\
\overline{r}_{p}&=\overline{r}_{p,0}\left[\frac{\epsilon}{\epsilon_0}\right]^{\frac{1}{d}}
\end{align}
where, $\epsilon$ is the porosity of the cathode, $\epsilon_0$ is the initial porosity, $\overline{r}_{p}$ is the average size of the pores in the cathode, $\overline{r}_{p,0}$ is the initial average pore radius. $d$ is the coefficient (2 for cylindrical pores and 3 for spherical pores). We assume the pores to be cylindrical, hence we use $d=2$ throughout the derivation of Faradaic impedance for Li-air batteries.
The over-potential is related to cell voltage using,
\begin{equation}
V=V_0+\eta-V_\Omega
\end{equation}
where, $V$ is the cell voltage, $V_0$ is the open cell voltage, $V_\Omega$ represents voltage drop across separator, anode, deposit layers, interfaces, electrolytes and contacts.
Substituting $R_c$ from eqn.~(\ref{eq:Li2O2_model_1_reaction_rate}) into eqn.~(\ref{eq:Faradaic_current_wo_rc_integral})
\begin{equation}
i_F=nAFk\int\limits_{0}^{l}c_o(x,t)\,dx\label{eq:dc_faradiac_current}
\end{equation}
The oxidant concentration for the porous electrode can be obtained by using modified fick's second law of diffusion. Here we assume the Faradaic impedance is derived for Li-Air batteries with organic electrolyte. In Li-Air batteries, the reaction products such as lithium peroxide, lithium superoxide, etc. deposit within the cathode pores and reduce the effective area. This reduction is taken into account by adding an additional term to Fick's second law of diffusion. The modified Fick's second law is,
\begin{align}
\epsilon_0\frac{\partial c_o(x,t)}{\partial t}&=
D_{\text{eff}}\frac{d^2 c_o(x,t)}{dx^2}-
kc_o(x,t)\label{eq:dc_ficks_second_law}
\end{align}
replacing the value of $k$ from eqn.~(\ref{eq:dc_reaction_rate_contant}), we get,
\begin{align}
\epsilon_0\frac{\partial c_o(x,t)}{\partial t}&=
D_{\text{eff}}\frac{d^2 c_o(x,t)}{dx^2}-
k_0a(\epsilon)e^{-\frac{n\beta}{V_T}\eta}\,c_o(x,t)\label{eq:Li2O2_model_1_ficks_second_law_w_k_substitution}
\end{align}
where, $D_{\text{eff}}$ is the effective diffusion constant and is given by the Bruggeman relation,
\begin{align}
D_{\text{eff}}=\epsilon^{\text{brugg}}D_o
\end{align}
where, brugg is the Bruggeman constant.
We assume the porosity of the cathode does not change significantly during EIS measurement, therefore, $\epsilon(x)=\epsilon_0$. 
\paragraph{Initial and boundary conditions} The modified diffusion equation (\ref{eq:Li2O2_model_1_ficks_second_law_w_k_substitution}) is subjected to following initial and boundary conditions,
\begin{subequations}
\begin{enumerate}
\item[] \textbf{Initial condition}
\begin{align}
c_o(x,0)&=c_o^*\nonumber
\end{align}
\item[(a)] \textbf{Boundary condition at $x=0$}
\begin{align}
c_o(0,t)=&c_o^*\label{bc:Li2O2_model_1_x_at_0}
\end{align}
\item[(b)] \textbf{Boundary condition at $x=l$}
\begin{align}
\left.\frac{dc_o(x,t)}{dx}\right|_{x=l}=&0\label{bc:Li2O2_model_1_x_at_l}
\end{align}
\end{enumerate}
\end{subequations}
\paragraph{Steady State Analysis:}
Let us consider steady state condition, i.e. when time derivatives in eqn.~(\ref{eq:Li2O2_model_1_ficks_second_law_w_k_substitution}) can be neglected. Although the perfect steady state can never be reached, this approximation is valid if the discharge currents are not high. 
The oxygen diffusion equation at the steady state is,
\begin{equation}
\frac{d^2c_o(x)}{dx^2}-\frac{kc_o(x)}{D_{\text{eff}}}=0
\end{equation}
The solution to the above equation is,
\begin{equation}
c_o(x)=C_1\cosh{\left(\sqrt{\frac{k}{D_{\text{eff}}}}x\right)}+C_2\sinh{\left(\sqrt{\frac{k}{D_{\text{eff}}}}x\right)}\label{eq:Li2O2_model_1_dc_ficks_second_law_solution}
\end{equation}
Subjecting the eqn.~(\ref{eq:Li2O2_model_1_dc_ficks_second_law_solution}) to the boundary condition~(\ref{bc:Li2O2_model_1_x_at_0}), we get,
\begin{align}
c(0)&=C_1+C_2\times 0=c_o^*\nonumber\\
%\therefore\qquad 
C_1&=c_o^*\label{eq:Li2O2_model_1_dc_C1}
\end{align}
and subjecting the eqn.~(\ref{eq:Li2O2_model_1_dc_ficks_second_law_solution}) to the boundary condition~(\ref{bc:Li2O2_model_1_x_at_l}), we get,
\begin{align}
\left.\frac{dc_o(x)}{dx}\right|_{x=l}=&\sqrt{\frac{k}{D_{\text{eff}}}}\left[c_o^*\sinh{\left(\sqrt{\frac{k}{D_{\text{eff}}}}l\right)}+C_2\cosh{\left(\sqrt{\frac{k}{D_{\text{eff}}}}l\right)}\right]=0\nonumber\\
%=&0\nonumber\\
%\therefore
C_2=&-c_o^*\tanh{\left(\sqrt{\frac{k}{D_{\text{eff}}}}l\right)}\label{eq:Li2O2_model_1_dc_C2}
\end{align}
Now, we substitute the values of $C_1$ (\ref{eq:Li2O2_model_1_dc_C1}) and $C_2$ (\ref{eq:Li2O2_model_1_dc_C2}) into~(\ref{eq:Li2O2_model_1_dc_ficks_second_law_solution}), we get,
\begin{equation}
c_o(x)=c_o^*\left[\cosh{\left(\sqrt{\frac{k}{D_{\text{eff}}}}x\right)}-\tanh{\left(\sqrt{\frac{k}{D_{\text{eff}}}}l\right)}\sinh{\left(\sqrt{\frac{k}{D_{\text{eff}}}}x\right)}\right]\label{eq:dc_co_complete_solution}
\end{equation}
[Revise] The oxygen concentration at the two boundaries (air side and separator) from the above equation are,
\begin{subequations}
\begin{align}
c_o(0) &= c_o^*\\
c_o(l) &= c_o^*\;\textrm{sech}{\left(\sqrt{\frac{k}{D_{\textrm{eff}}}}l\right)}
\end{align}
\end{subequations}
The steady state Faradaic current is obtained by substituting $c_o(x)$ into eq.~(\ref{eq:dc_faradiac_current}) and integrating it,
\begin{equation}
i_F=nAFkc_o^*\sqrt{\frac{D_{\text{eff}}}{k}}\tanh{\left(\sqrt{\frac{k}{D_{\text{eff}}}}l\right)}\label{eq:dc_i_F_complete_soln}
\end{equation}  
\paragraph{Small Signal Analysis:} The small-signal representation of over-potential, the Faradaic current and the oxidant concentration are $\delta\eta\,e^{j\omega t}$, $\delta i_F\,e^{j\omega t}$ and $\delta c_o\,e^{j\omega t}$ respectively.
%The small-signal Faradaic current is obtained by taking the Taylor series expansion of eqn.~\ref{eq:butler_volmer_eq_one_specie_transmissive_gerischer} and keeping only the linear terms of the expansion .In EIS very small current or voltage is applied and its response is measured. Small perturbations keep the system operating in the pseudo-linear region. We, a sinusoidal current $(\delta i_F\,e^{j\omega t})$ is applied to the battery and the perturbation in voltage is measured. Small variation in voltage $(\delta\eta\,e^{j\omega t})$ also causes perturbation in reaction rate $(\delta k\,e^{j\omega t})$ and oxidant concentration $(\delta c_o(x)\,e^{j\omega t})$. 
Under d.c. discharge the total change in the parameters is the summation of d.c. and the small-signal component. The total Faradaic current for a porous electrode is the superposition of large-signal and the small-signal Faradaic current,
\begin{align}
i_F+\delta i_F&=2AF(k+\delta k)\int\limits_{0}^{l}\left(c_o(x)+\delta c_o(x)\right)\,dx\nonumber\\
&=2AF\int\limits_{0}^{l}\left[kc_o(x)+k\delta c_o(x)+\delta kc_o(x)+\delta k\delta c_o(x)\right]\,dx\nonumber
\end{align}
where, $\delta k$ is,
\begin{align}
\delta k=\frac{\partial k}{\partial\eta}\delta\eta\nonumber
\end{align}
therefore,
\begin{equation}
\delta k=\frac{-n\beta k_0a(\epsilon)e^{-\frac{n\beta}{V_T}\eta}}{V_T}\delta\eta\label{eq:delta_k}
\end{equation}
using eqn.~(\ref{eq:dc_faradiac_current}), we get,
\begin{align}
\delta i_F&=nAF\int\limits_{0}^{l}\left[k\delta c_o(x)+\delta kc_o(x)\right]\,dx\nonumber\\
\therefore\quad\delta i_F&=nAFk\int\limits_{0}^{l}\delta c_o(x)\,dx+nAF\delta k\int\limits_{0}^{l}c_o(x)\,dx\label{eq:dc_small_faradiac_current_template}
\end{align}
[Revise] The small-signal Faradaic current causes a small change in the oxygen concentration, this variation can be computed by linearizing the eqn.~(\ref{eq:dc_ficks_second_law}), 
\begin{align}
%\epsilon_0\frac{\partial \left[c_o(x)+\delta c_oe^{j\omega t}\right]}{\partial t}=
%D_{\text{eff}}\frac{d^2 \left[c_o(x)+\delta c_oe^{j\omega t}\right]}{dx^2}-\left(
%k_c+\delta k_ce^{j\omega t}\right)\,\left(c_o(x)+\delta c_oe^{j\omega t}\right)\label{eq:dc_ficks_second_law_small_signal}\\
%\epsilon_0\frac{\partial \left[c_o(x)\right]}{\partial t}+\epsilon_0\frac{\partial \left[\delta c_oe^{j\omega t}\right]}{\partial t}=
%D_{\text{eff}}\frac{d^2 \left[c_o(x)\right]}{dx^2}+D_{\text{eff}}\frac{d^2 \left[\delta c_oe^{j\omega t}\right]}{dx^2}\\-
%\left(k_cc_o(x)+k_c\delta c_oe^{j\omega t}+\delta k_ce^{j\omega t}c_o(x)+\delta k_c\delta c_o(x)e^{2j\omega t}\right)\\
j\omega\epsilon_0\,\delta c_o(x)=D_{\text{eff}}\frac{d^2\delta c_o(x)}{dx^2}-\left[\delta k\,c_o(x)+k\,\delta c_o(x)\right]\nonumber\\
\frac{d^2\delta c_o(x)}{dx^2} - \frac{\left(k+j\omega\epsilon_0\right)}{D_{\text{eff}}}\delta c_o(x) - \frac{\delta k\,c_o(x)}{D_{\text{eff}}}=0
\end{align}
Substituting $c_o(x)$ from the eq.~(\ref{eq:dc_co_complete_solution}),
\begin{align}
\frac{d^2\delta c_o(x)}{dx^2} - \frac{\left(k+j\omega\epsilon_0\right)}{D_{\text{eff}}}\delta c_o(x) &= 
\frac{\delta kc_o^*}{D_{\text{eff}}} \left[\cosh{\left(\sqrt{\frac{k}{D_{\text{eff}}}}x\right)}\right.\nonumber\\ &\left.-\tanh{\left(\sqrt{\frac{k}{D_{\text{eff}}}}l\right)}\sinh{\left(\sqrt{\frac{k}{D_{\text{eff}}}}x\right)}\right] \label{eq:dc_non_homo_differ}
\end{align}
The solution to the above non-homogenous differential equation is sum of the complimentary and the particular solution.
\begin{align}
\delta c_o(x)=\delta c_{o_c}(x)+\delta c_{o_p}(x)\label{eq:dc_small_signal_complete_soln_sum}
\end{align}
The complementary solution is given by,
\begin{align}
%\frac{d^2\delta c_o(x)}{dx^2} - \frac{\left(k+j\omega\epsilon_0\right)}{D_{\text{eff}}}\delta c_o(x)=0\nonumber\\
\delta c_{o_c}(x)=C_1\cosh{\sqrt{\frac{k+j\omega \epsilon_0}{D_{\text{eff}}}}x}+C_2\sinh{\sqrt{\frac{k+j\omega \epsilon_0}{D_{\text{eff}}}}x}\label{eq:dc_small_signal_complementary_soln}
\end{align}
Assuming the particular solution,
\begin{align}
\delta c_{o_p}(x)=&
C_3\cosh{\left(\sqrt{\frac{k}{D_{\text{eff}}}}x\right)} + C_4\sinh{\left(\sqrt{\frac{k}{D_{\text{eff}}}}x\right)}\label{eq:dc_small_signal_particular_soln}
\end{align}
where, $C_1$, $C_2$, $C_3$ and $C_4$ are the integration constants.
The integration constants $C_3$ and $C_4$ are obtained by replacing $\delta c_o(x)$ in eqn.~(\ref{eq:dc_non_homo_differ}) by the above equation, we obtain,
%eqn.~\ref{eq:dc_small_signal_particular_soln}. The Left Hand Side (L.H.S) of the equation~\ref{eq:dc_non_homo_differ} becomes,
\begin{align}
L.H.S=&
\frac{d^2}{dx^2}\left[{ 
C_3\cosh{\left(\sqrt{\frac{k}{D_{\text{eff}}}}x\right)} + C_4\sinh{\left(\sqrt{\frac{k}{D_{\text{eff}}}}x\right)}}\right]\nonumber\\ -&\frac{\left(k+j\omega\epsilon_0\right)}{D_{\text{eff}}}\left[{ 
C_3\cosh{\left(\sqrt{\frac{k}{D_{\text{eff}}}}x\right)} + C_4\sinh{\left(\sqrt{\frac{k}{D_{\text{eff}}}}x\right)}}\right]\nonumber
%=&\frac{k}{D_{\text{eff}}}\left[C_3\cosh{\left(\sqrt{\frac{k}{D_{\text{eff}}}}x\right)}+C_4\sinh{\left(\sqrt{\frac{k}{D_{\text{eff}}}}x\right)}\right]\nonumber\\
%-&\frac{\left(k+j\omega\epsilon_0\right)}{D_{\text{eff}}}\left[C_3\cosh{\left(\sqrt{\frac{k}{D_{\text{eff}}}}x\right)} + C_4\sinh{\left(\sqrt{\frac{k}{D_{\text{eff}}}}x\right)}\right]\nonumber
\end{align}
\begin{subequations}
After taking derivatives and rearranging the above equation, we get,
\begin{align}
L.H.S=&\frac{-j\omega\epsilon_0}{D_{\text{eff}}}\left[C_3\cosh{\left(\sqrt{\frac{k}{D_{\text{eff}}}}x\right)} + C_4\sinh{\left(\sqrt{\frac{k}{D_{\text{eff}}}}x\right)}\right]\label{eq:Li2O2_model_1_LHS}
\end{align}
The Right Hand Side (R.H.S) of equation~(\ref{eq:dc_non_homo_differ}) is,
\begin{align}
R.H.S=&\frac{\delta kc_o^*}{D_{\text{eff}}} \left[\cosh{\left(\sqrt{\frac{k}{D_{\text{eff}}}}x\right)} -\tanh{\left(\sqrt{\frac{k}{D_{\text{eff}}}}l\right)}\sinh{\left(\sqrt{\frac{k}{D_{\text{eff}}}}x\right)}\right]\label{eq:Li2O2_model_1_RHS}
\end{align}
\end{subequations}
Equating both sides~(\ref{eq:Li2O2_model_1_LHS}) and~(\ref{eq:Li2O2_model_1_RHS}), we obtain equations for $C_3$ and $C_4$,
\begin{subequations}
\begin{align}
%-j\omega\epsilon_0C_3=&\delta k_cc_o^*\nonumber\\
C_3=&-\frac{\delta kc_o^*}{j\omega\epsilon_0}\label{eq:dc_small_signal_C3}\\
%-j\omega\epsilon_0C_4=-\delta k_cc_o^*\tanh{\left(\sqrt{\frac{k_c}{D_\text{eff}}}l\right)}
C_4=&\frac{\delta kc_o^*}{j\omega\epsilon_0}\tanh{\left(\sqrt{\frac{k}{D_\text{eff}}}l\right)}\label{eq:dc_small_signal_C4}
\end{align}
\end{subequations}
Now, we substitute the values of $C_3$ (\ref{eq:dc_small_signal_C3}) and $C_4$ (\ref{eq:dc_small_signal_C4}) into~(\ref{eq:dc_small_signal_particular_soln}), we obtain,
\begin{equation}
\delta c_{o_p}(x)=\frac{\delta kc_o^*}{j\omega\epsilon_0}\left[
-\cosh{\left(\sqrt{\frac{k}{D_{\text{eff}}}}x\right)} + \tanh{\left(\sqrt{\frac{k}{D_\text{eff}}}l\right)}\sinh{\left(\sqrt{\frac{k}{D_{\text{eff}}}}x\right)}\right]\label{eq:dc_small_signal_particular_soln_complete}
\end{equation}
This represents the particular solution of eqn.~(\ref{eq:dc_non_homo_differ}). The complete solution of after substituting the complementary solution  eqn.~(\ref{eq:dc_small_signal_complementary_soln}) and the particular solution (\ref{eq:dc_small_signal_particular_soln_complete}) into  eq.~(\ref{eq:dc_small_signal_complete_soln_sum}), 
\begin{align}
\delta c_o(x)&=C_1\cosh{\sqrt{\frac{k+j\omega \epsilon_0}{D_{\text{eff}}}}x}+C_2\sinh{\sqrt{\frac{k+j\omega \epsilon_0}{D_{\text{eff}}}}x}\nonumber\\
&+\frac{\delta kc_o^*}{j\omega\epsilon_0}\left[
-\cosh{\left(\sqrt{\frac{k}{D_{\text{eff}}}}x\right)} + \tanh{\left(\sqrt{\frac{k}{D_\text{eff}}}l\right)}\sinh{\left(\sqrt{\frac{k}{D_{\text{eff}}}}x\right)}\right]\label{eq:dc_small_signal_complete_soln_partial}
\end{align}
The small-signal boundary conditions needed to find the integration constants $C_1$ and $C_2$ are obtained by linearizing the eqns.~(\ref{bc:Li2O2_model_1_x_at_0}) and~(\ref{bc:Li2O2_model_1_x_at_l}),
\begin{subequations}
\begin{align}
\left.\delta c_o(x)\right|_{x=0}=0\label{bc:dc_small_signal_x_0}\\
\left.\frac{d\delta c_o(x)}{dx}\right|_{x=l}=0\label{bc:dc_small_signal_x_l}
\end{align}
\end{subequations}
By applying boundary conditions~(\ref{bc:dc_small_signal_x_0}) and~(\ref{bc:dc_small_signal_x_l}) to  eqn.~(\ref{eq:dc_small_signal_complete_soln_partial}), we get,
\begin{subequations}	
\begin{align}
C_1=&\frac{\delta kc_o^*}{j\omega\epsilon_0}\label{eq:dc_small_signal_C1}
\end{align}
\begin{align}
C_2=&-\frac{\delta kc_o^*}{j\omega\epsilon_0}\tanh{\left(\sqrt{\frac{k+j\omega\epsilon_0}{D_{\text{eff}}}}l\right)}\label{eq:dc_small_signal_C2}
\end{align}
\end{subequations}
Now, we replace the values of $C_1$ (\ref{eq:dc_small_signal_C1}) and $C_2$ (\ref{eq:dc_small_signal_C2}) in eq.~(\ref{eq:dc_small_signal_complete_soln_partial}),
\begin{align}
\delta c_o(x)&=\frac{\delta kc_o^*}{j\omega\epsilon_0}
\left[\cosh{\sqrt{\frac{k+j\omega \epsilon_0}{D_{\text{eff}}}}x}\right.\nonumber\\
&-\tanh{\left(\sqrt{\frac{k+j\omega\epsilon_0}{D_{\text{eff}}}}l\right)}\sinh{\sqrt{\frac{k+j\omega \epsilon_0}{D_{\text{eff}}}}x}\nonumber\\
&
\left.-\cosh{\left(\sqrt{\frac{k}{D_{\text{eff}}}}x\right)} + \tanh{\left(\sqrt{\frac{k}{D_\text{eff}}}l\right)}\sinh{\left(\sqrt{\frac{k}{D_{\text{eff}}}}x\right)}\right]\label{eq:dc_small_signal_oxidant_concentration}
\end{align}
%{\color{red}Not important..
%On expanding eq. \ref{eq:dc_reaction_rate_contant} using power series expansion. We get,
%\begin{align}
%k_c+\delta k_c\,e^{j\omega t}=&\frac{k_0\epsilon_0}{\overline{r}_{p,0}}\times \left(e^{-\frac{\beta}{V_T}\eta+\delta\eta\,e^{j\omega t}}-e^{\frac{1-\beta}{V_T}\eta+\delta\eta\,e^{j\omega t}}\right)\\
%=&\frac{k_0\epsilon_0}{\overline{r}_{p,0}}\times \left(e^{-\frac{\beta}{V_T}\eta}e^{-\frac{\beta}{V_T}\delta\eta\,e^{j\omega t}}-
%e^{\frac{1-\beta}{V_T}\eta}e^{\frac{1-\beta}{V_T}\delta\eta\,e^{j\omega t}}\right)
%\end{align}}
This is the small-signal concentration of oxygen in the cathode during impedance spectroscopy.
The small-signal Faradaic current is obtained by substituting the eqns.~(\ref{eq:dc_co_complete_solution}) and (\ref{eq:dc_small_signal_oxidant_concentration}) into the eq.~(\ref{eq:dc_small_faradiac_current_template}),
\begin{align}
%\delta i_F&=2AFk\int\limits_{0}^{l}\frac{\delta kc_o^*}{j\omega\epsilon_0}
%\left[\cosh{\left(\sqrt{\frac{k+j\omega \epsilon_0}{D_{\text{eff}}}}x\right)}\right.\nonumber\\
%&-\tanh{\left(\sqrt{\frac{k+j\omega\epsilon_0}{D_{\text{eff}}}}l\right)}\sinh{\left(\sqrt{\frac{k+j\omega \epsilon_0}{D_{\text{eff}}}}x\right)}\nonumber\\
%&
%\left.-\cosh{\left(\sqrt{\frac{k}{D_{\text{eff}}}}x\right)} + \tanh{\left(\sqrt{\frac{k}{D_\text{eff}}}l\right)}\sinh{\left(\sqrt{\frac{k}{D_{\text{eff}}}}x\right)}\right]\nonumber\\
%&+2AF\delta k\int\limits_{0}^{l}c_o^*\left[\cosh{\left(\sqrt{\frac{k}{D_{\text{eff}}}}x\right)}\right.\nonumber\\
%&-\left.\tanh{\left(\sqrt{\frac{k}{D_{\text{eff}}}}l\right)}\sinh{\left(\sqrt{\frac{k}{D_{\text{eff}}}}x\right)}\right]\,dx\\ 
%Next Equation
\delta i_F=&\frac{nAFk\delta kc_o^*}{j\omega\epsilon_0}\int\limits_{0}^{l}
\cosh{\left(\sqrt{\frac{k+j\omega \epsilon_0}{D_{\text{eff}}}}x\right)dx}\nonumber\\
&-\frac{nAFk\delta kc_o^*\tanh{\left(\sqrt{\frac{k+j\omega\epsilon_0}{D_{\text{eff}}}}l\right)}}{j\omega\epsilon_0}\int\limits_{0}^{l}\sinh{\left(\sqrt{\frac{k+j\omega \epsilon_0}{D_{\text{eff}}}}x\right)}dx\nonumber\\
&-\frac{nAFk\delta kc_o^*}{j\omega\epsilon_0}\int\limits_{0}^{l}\cosh{\left(\sqrt{\frac{k}{D_{\text{eff}}}}x\right)}dx\nonumber\\
&+\frac{nAFk\delta kc_o^*\tanh{\left(\sqrt{\frac{k}{D_\text{eff}}}l\right)}} {j\omega\epsilon_0} \int\limits_{0}^{l}\sinh{\left(\sqrt{\frac{k}{D_{\text{eff}}}}x\right)}dx\nonumber\\
% Space for separation
&+nAF\delta kc_o^*\int\limits_{0}^{l}\cosh{\left(\sqrt{\frac{k}{D_{\text{eff}}}}x\right)}dx\nonumber\\
&-nAF\delta kc_o^*\tanh{\left(\sqrt{\frac{k}{D_{\text{eff}}}}l\right)}\int\limits_{0}^{l} \sinh{\left(\sqrt{\frac{k}{D_{\text{eff}}}}x\right)}dx
\end{align}
% Spacing 
% More simplification
Integrating the above equation, we get,
\begin{align}
%\therefore
\delta i_F=&\frac{nAFk\delta kc_o^*}{j\omega\epsilon_0}
\sqrt{\frac{D_{\text{eff}}}{k+j\omega\epsilon_0}} 
\left[\sinh{\left(\sqrt{\frac{k+j\omega \epsilon_0}{D_{\text{eff}}}}x\right)}\right]_0^l\nonumber\\
&-\frac{nAFk\delta kc_o^*\tanh{\left(\sqrt{\frac{k+j\omega\epsilon_0}{D_{\text{eff}}}}l\right)}}{j\omega\epsilon_0}\sqrt{\frac{D_{\text{eff}}}{k+j\omega\epsilon_0}}\left[\cosh{\left(\sqrt{\frac{k+j\omega \epsilon_0}{D_{\text{eff}}}}x\right)}\right]_0^l\nonumber\\
&
-\frac{nAFk\delta kc_o^*}{j\omega\epsilon_0}\sqrt{\frac{D_{\text{eff}}}{k}}\left[\sinh{\left(\sqrt{\frac{k}{D_{\text{eff}}}}x\right)}\right]_0^l\nonumber\\
&+\frac{nAFk\delta kc_o^*\tanh{\left(\sqrt{\frac{k}{D_\text{eff}}}l\right)}} {j\omega\epsilon_0}\sqrt{\frac{D_{\text{eff}}}{k}}
\left[\cosh{\left(\sqrt{\frac{k}{D_{\text{eff}}}}x\right)}\right]_0^l\nonumber\\
% Space for separation
&+nAF\delta kc_o^*\sqrt{\frac{D_{\text{eff}}}{k}}\left[\sinh{\left(\sqrt{\frac{k}{D_{\text{eff}}}}x\right)}\right]_0^l\nonumber\\
% Current
&-nAF\delta kc_o^*\tanh{\left(\sqrt{\frac{k}{D_{\text{eff}}}}l\right)}
\sqrt{\frac{D_{\text{eff}}}{k}}\left[\cosh{\left(\sqrt{\frac{k}{D_{\text{eff}}}}x\right)}\right]_0^l\nonumber
\end{align}
%another simplification step
After applying the integration limits, we get,
\begin{align}
%\therefore
\delta i_F&=\frac{nAFk\delta kc_o^*}{j\omega\epsilon_0}
\sqrt{\frac{D_{\text{eff}}}{k+j\omega\epsilon_0}} 
\sinh{\left(\sqrt{\frac{k+j\omega \epsilon_0}{D_{\text{eff}}}}l\right)}\nonumber\\
&-\frac{nAFk\delta kc_o^*\tanh{\left(\sqrt{\frac{k+j\omega\epsilon_0}{D_{\text{eff}}}}l\right)}}{j\omega\epsilon_0}\sqrt{\frac{D_{\text{eff}}}{k+j\omega\epsilon_0}}\left[\cosh{\left(\sqrt{\frac{k+j\omega \epsilon_0}{D_{\text{eff}}}}l\right)}-1\right]\nonumber\\
&
-\frac{nAFk\delta kc_o^*}{j\omega\epsilon_0}\sqrt{\frac{D_{\text{eff}}}{k}}\sinh{\left(\sqrt{\frac{k}{D_{\text{eff}}}}l\right)}\nonumber\\
&+\frac{nAFk\delta kc_o^*\tanh{\left(\sqrt{\frac{k}{D_\text{eff}}}l\right)}} {j\omega\epsilon_0}\sqrt{\frac{D_{\text{eff}}}{k}}
\left[\cosh{\left(\sqrt{\frac{k}{D_{\text{eff}}}}l\right)}-1\right]\nonumber\\
% Space for separation
&+nAF\delta kc_o^*\sqrt{\frac{D_{\text{eff}}}{k}}\sinh{\left(\sqrt{\frac{k}{D_{\text{eff}}}}l\right)}\nonumber\\
&-nAF\delta kc_o^*\tanh{\left(\sqrt{\frac{k}{D_{\text{eff}}}}l\right)}
\sqrt{\frac{D_{\text{eff}}}{k}}\left[\cosh{\left(\sqrt{\frac{k}{D_{\text{eff}}}}l\right)}-1\right]\nonumber
\end{align}
Rearranging the above equation,
\begin{align}
%\therefore
\frac{\delta i_F}{\delta k}=&\frac{nAFkc_o^*\sqrt{D_{\text{eff}}}}{j\omega\epsilon_0\sqrt{{k+j\omega\epsilon_0}}}\tanh{\left(\sqrt{\frac{k+j\omega\epsilon_0}{D_{\text{eff}}}}\,l\right)}\nonumber\\
&-\frac{nAFkc_o^*\sqrt{D_{\text{eff}}}}{j\omega\epsilon_0\sqrt{k}}
\tanh{\left(\sqrt{\frac{k}{D_\text{eff}}}\,l\right)}\nonumber\\
% Space for separation
&+\frac{nAFc_o^*\sqrt{D_{\text{eff}}}}{\sqrt{k}}\tanh{\left(\sqrt{\frac{k}{D_{\text{eff}}}}\,l\right)}\label{eq:dc_small_signal_iF_final_solution}
\end{align}
Collecting common terms,
\begin{align}
\frac{\delta i_{F}}{\delta k}=\frac{nAFkc_{o}^{*}\sqrt{D_{\text{eff}}}}{\sqrt{k}}&\left[\frac{\sqrt{k}}{j\omega\epsilon_{0}\sqrt{k+j\omega\epsilon_{0}}}\tanh\left(\sqrt{\frac{k+j\omega\epsilon_{0}}{D_{\text{eff}}}}\,l\right)\right.\\
	&\left.-\frac{1}{j\omega\epsilon_{0}}\tanh\left(\sqrt{\frac{k}{D_{\text{eff}}}}\,l\right)+\frac{1}{k}\tanh\left(\sqrt{\frac{k}{D_{\text{eff}}}}\,l\right)\right]
\end{align}
Taking $\tanh\left(\sqrt{\frac{k}{D_{\text{eff}}}}\,l\right)$ common (this allows us to substitute $i_F$ in the equation),
\begin{align}
\frac{\delta i_{F}}{\delta k}=\frac{nAFkc_{o}^{*}\sqrt{D_{\text{eff}}}}{\sqrt{k}}\tanh\left(\sqrt{\frac{k}{D_{\text{eff}}}}\,l\right)&\left[\frac{\sqrt{k}}{j\omega\epsilon_{0}\sqrt{k+j\omega\epsilon_{0}}}\frac{\tanh\left(\sqrt{\frac{k+j\omega\epsilon_{0}}{D_{\text{eff}}}}\,l\right)}{\tanh\left(\sqrt{\frac{k}{D_{\text{eff}}}}\,l\right)}\right.\\&\left.-\frac{1}{j\omega\epsilon_{0}}+\frac{1}{k}\right]
\end{align}
Finally, the expression reduces to
\begin{equation}
\frac{\delta i_{F}}{\delta k}=\frac{i_{F}}{k}\left[\frac{k\sqrt{k}}{j\omega\epsilon_{0}\sqrt{k+j\omega\epsilon_{0}}}\frac{\tanh\left(\sqrt{\frac{k+j\omega\epsilon_{0}}{D_{\text{eff}}}}\,l\right)}{\tanh\left(\sqrt{\frac{k}{D_{\text{eff}}}}\,l\right)}-\frac{k}{j\omega\epsilon_{0}}+1\right]
\end{equation}
The Faradaic impedance is given by, 
\begin{align}
Z_F&=-\frac{\delta\eta}{\delta i_F}\nonumber
\end{align}
after rearranging, we get,
\begin{align}
Z_F&=-\frac{\delta\eta}{\delta k}\frac{\delta k}{\delta i_F}\label{eq:dc_Z_F_eqn}
\end{align}
where, the negative sign indicates that when voltage increases the current decreases.
The first term in the eq.~(\ref{eq:dc_Z_F_eqn}) is obtained by linearizing the eq.~(\ref{eq:dc_reaction_rate_contant}),
\begin{align}
\delta k&=-k_0a(\epsilon)\frac{n\beta}{V_T} e^{-\frac{n\beta}{V_T}\eta}\delta\eta\nonumber\\
\frac{\delta k}{\delta\eta}&=-\frac{n\beta k}{V_T}\label{eq:dc_linearized k}
\end{align} 
By substituting the eqns.~(\ref{eq:dc_small_signal_iF_final_solution}) and~(\ref{eq:dc_linearized k}) into eqn.~(\ref{eq:dc_Z_F_eqn}) and using~(\ref{eq:dc_i_F_complete_soln}), we obtain the Faradaic impedance for a Li-air battery with a porous cathode,
\begin{align}
\boxed{Z_F=\frac{Z_0}{\left[1-\frac{k}{j\omega\epsilon_0} +\frac{k\sqrt{k}\tanh{\left(\sqrt{\frac{k+j\omega\epsilon_0}{D_{\text{eff}}}}\,l\right)}}{j\omega\epsilon_0\sqrt{k+j\omega\epsilon_0}\tanh\left(\sqrt{\frac{k}{D_{\text{eff}}}}\,l\right)}\right]}}\label{eq:dc_Faradaic_final_solution}
\end{align}
where, 
\begin{equation}
Z_0=\frac{V_T}{n\beta i_F}
\end{equation}
The total impedance (\ref{eq:Total_impedance}) is the parallel combination of the Faradaic impedance (\ref{eq:dc_Faradaic_final_solution}) and the double layer impedance.
\section{Simulation Results}
The simulation results showing the effect of each parameter on the overall impedance spectra.
\begin{figure}[b!]
\begin{center}
\includegraphics[width=0.49\textwidth]{\main figures/one_specie_andrei_c_int_1} 
\includegraphics[width=0.49\textwidth]{\main figures/one_specie_andrei_c_int_2}
\caption{The effect of the initial oxygen concentration on the impedance plot for two values of the system length: $l=100\,\textrm{nm}$ (left) and $l=10\,\mu\textrm{m}$ (right). The other simulation parameters are, $\textrm{C}_\textrm{d}= 4\,\mu\textrm{F}/\textrm{cm}^2$, $\textrm{D}_\textrm{o}=7\times10^{-6}\,\textrm{cm}^2/\textrm{s}$, $\textrm{I}_\textrm{dis}= 200\,\mu\textrm{A}/\textrm{cm}^2$, $\textrm{k}=1.3\times10^{-8}\,\textrm{cm}/\textrm{s}$ and $n=3$}
\label{plots:our_1_model_c_int}
\end{center}
\end{figure}
\begin{figure}[h!]
\begin{center}
\includegraphics[width=0.49\textwidth]{\main figures/one_specie_andrei_I_dis_2} 
\includegraphics[width=0.49\textwidth]{\main figures/one_specie_andrei_I_dis_1}
\includegraphics[width=0.49\textwidth]{\main figures/one_specie_andrei_Do_3}
\includegraphics[width=0.49\textwidth]{\main figures/one_specie_andrei_Do_2}
\caption{(Top) The effect of the d.c. discharge current on the impedance plot for two values of the oxygen diffusion coefficient: $\textrm{D}_\textrm{o}=3.5\times10^{-6}\,\textrm{cm}^2/\textrm{s}$ (left) and $\textrm{D}_\textrm{o}=7\times10^{-5}\,\textrm{cm}^2/\textrm{s}$ (right). The other simulation parameters are, $\textrm{c}_\textrm{o}= 3.26\times10^{-7}\,\textrm{mol}/\textrm{cm}^2$, $\textrm{C}_\textrm{d}= 40\,\mu\textrm{F}/\textrm{cm}^2$, $\textrm{k}=1.3\times10^{-9}\,\textrm{cm}/\textrm{s}$, $l=10\,\mu\textrm{m}$ and $n=2$.
(Bottom) The effect of the oxidant diffusion coefficient on the impedance plot for two values of the double layer capacitance: $\textrm{C}_\textrm{d}= 4\,\mu\textrm{F}/\textrm{cm}^2$ (left) and $\textrm{C}_\textrm{d}= 40\,\mu\textrm{F}/\textrm{cm}^2$ (right). The other simulation parameters are, $\textrm{c}_\textrm{o}= 3.26\times10^{-7}\,\textrm{mol}/\textrm{cm}^2$, $\textrm{I}_\textrm{dis}= 200\,\mu\textrm{A}/\textrm{cm}^2$, $\textrm{k}=1.3\times10^{-7}\,\textrm{cm}/\textrm{s}$, $l=10\,\mu\textrm{m}$ and $n=3$.}
\label{plots:our_1_model_I_dis}
\end{center}
\end{figure}
\begin{figure}
\begin{center}
\includegraphics[width=0.49\textwidth]{\main figures/one_specie_andrei_k0_4} 
\includegraphics[width=0.49\textwidth]{\main figures/one_specie_andrei_k0_3}
\includegraphics[width=0.49\textwidth]{\main figures/one_specie_andrei_n_1}
\includegraphics[width=0.49\textwidth]{\main figures/one_specie_andrei_n_2}
\caption{(Top) The effect of the reaction rate on the impedance plot for two values of the stoichiometric number of electrons: $n=2$ (left) and $n=3$ (right). The other simulation parameters are, $\textrm{c}_\textrm{o}= 3.26\times10^{-6}\,\textrm{mol}/\textrm{cm}^2$, $\textrm{C}_\textrm{d}= 40\,\mu\textrm{F}/\textrm{cm}^2$, $\textrm{D}_\textrm{o}=7\times10^{-6}\,\textrm{cm}^2/\textrm{s}$, $\textrm{I}_\textrm{dis}= 200\,\mu\textrm{A}/\textrm{cm}^2$ and $l=1\,\mu\textrm{m}$.
(Bottom) The effect of the stoichiometric number of electrons on the impedance plot for two values of the oxidant diffusion coefficient: $\textrm{D}_\textrm{o}=7\times10^{-7}\,\textrm{cm}^2/\textrm{s}$ (left) and $\textrm{D}_\textrm{o}=1.4\times10^{-5}\,\textrm{cm}^2/\textrm{s}$ (right). The other simulation parameters are, $\textrm{c}_\textrm{o}= 3.26\times10^{-7}\,\textrm{mol}/\textrm{cm}^2$, $\textrm{C}_\textrm{d}= 4\,\mu\textrm{F}/\textrm{cm}^2$, $\textrm{I}_\textrm{dis}= 500\,\mu\textrm{A}/\textrm{cm}^2$, $\textrm{k}=1.3\times10^{-7}\,\textrm{cm}/\textrm{s}$ and $l=10\,\mu\textrm{m}$.}
\label{plots:our_1_model_k0}
\end{center}
\end{figure}
\begin{figure}
\begin{center}
\includegraphics[width=0.49\textwidth]{\main figures/one_specie_andrei_l_1} 
\includegraphics[width=0.49\textwidth]{\main figures/one_specie_andrei_l_2}
\includegraphics[width=0.49\textwidth]{\main figures/one_specie_andrei_Cd_1}
\includegraphics[width=0.49\textwidth]{\main figures/one_specie_andrei_Cd_2}
\caption{(Top) The effect of the system length on the impedance plot for two values of the stoichiometric number of electrons: $n=2$ (left) and $n=3$ (right). The other simulation parameters are, $\textrm{c}_\textrm{o}= 3.26\times10^{-7}\,\textrm{mol}/\textrm{cm}^2$, $\textrm{C}_\textrm{d}= 4\,\mu\textrm{F}/\textrm{cm}^2$, $\textrm{D}_\textrm{o}=7\times10^{-6}\,\textrm{cm}^2/\textrm{s}$, $\textrm{I}_\textrm{dis}= 200\,\mu\textrm{A}/\textrm{cm}^2$ and $\textrm{k}=1.3\times10^{-8}\,\textrm{cm}/\textrm{s}$.
(Bottom) The effect of the double layer capacitance on the impedance plot for two values of the stoichiometric number of electrons: $n=2$ (left) and $n=3$ (right). The other simulation parameters are, $\textrm{c}_\textrm{o}= 3.26\times10^{-6}\,\textrm{mol}/\textrm{cm}^2$, $\textrm{C}_\textrm{d}= 4\,\mu\textrm{F}/\textrm{cm}^2$, $\textrm{D}_\textrm{o}=7\times10^{-7}\,\textrm{cm}^2/\textrm{s}$, $\textrm{I}_\textrm{dis}= 200\,\mu\textrm{A}/\textrm{cm}^2$,  $\textrm{k}=1.3\times10^{-9}\,\textrm{cm}/\textrm{s}$ and $l=10\,\mu\textrm{m}$.}
\label{plots:our_1_model_l}
\end{center}
\end{figure}
%\begin{figure}[t!]
%\begin{center}
%\includegraphics[width=0.49\textwidth]{figures/one_specie_andrei_c_int_1} 
%\includegraphics[width=0.49\textwidth]{figures/one_specie_andrei_c_int_2}
%\caption{The effect of the initial oxygen concentration on the impedance plot for two values of the system length: $l=100\,\textrm{nm}$ (left) and $l=10\,\mu\textrm{m}$ (right). The other simulation parameters are, $\textrm{C}_\textrm{d}= 4\,\mu\textrm{F}/\textrm{cm}^2$, $\textrm{D}_\textrm{o}=7\times10^{-6}\,\textrm{cm}^2/\textrm{s}$, $\textrm{I}_\textrm{dis}= 200\,\mu\textrm{A}/\textrm{cm}^2$, $\textrm{k}=1.3\times10^{-8}\,\textrm{cm}/\textrm{s}$ and $n=3$}
%\label{plots:our_1_model_c_int}
%\end{center}
%\end{figure}
\end{document}



