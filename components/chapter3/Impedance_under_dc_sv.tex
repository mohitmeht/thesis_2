\documentclass[../../thesis.tex]{subfiles}
\providecommand{\main}{../../}
\begin{document}
\section{Modeling}


\subsection{Under high dc discharge currents}
\todo[noline,author=Mohit]{Rewrite the section with introduction to high discharge currents and then the model}

The Faradaic current for porous electrodes is sum of all reaction sites and is given by
\begin{equation}
i_F=A\int\limits_{0}^{l}R_c\,dx\label{eq:Faradaic_current_wo_rc_integral}
\end{equation}
where, $A$ is the cross-sectional area of the cathode and $Rc$ is the specific reaction rate at the cathode~\cite{Andrei2010}. The specific reaction rate is obtained by simplifying the Butler Volmer equation,
\begin{equation}
R_c=nFk\,c_o(x,t)\label{eq:Li2O2_model_1_reaction_rate}
\end{equation}
where, $F$ is the Faraday's constant, $k$ is the reaction rate. When $\eta<0$ the simplified reaction rate is given as,
\begin{equation}
k=k_0\,a(\epsilon)\,e^{-\frac{n\beta}{V_T}\eta}\label{eq:dc_reaction_rate_contant}
\end{equation} 
where, $k_0$ is the standard reaction rate constant, $a(\epsilon)$ is the specific surface area of the cathode, $\eta$ is the over-potential and $\beta$ is the symmetry factor of the electrochemical reaction.
The specific area is given using the following relation,
\begin{align}
a(\epsilon)&=\epsilon/\overline{r}_{p}\label{eq:specific_area}\\
\overline{r}_{p}&=\overline{r}_{p,0}\left[\frac{\epsilon}{\epsilon_0}\right]^{\frac{1}{d}}
\end{align}
where, $\epsilon$ is the porosity of the cathode, $\epsilon_0$ is the initial porosity, $\overline{r}_{p}$ is the average size of the pores in the cathode, $\overline{r}_{p,0}$ \missingfigure{Add a figure showing what we mean by porosity}is the initial average pore radius. $d$ is the coefficient (2 for cylindrical pores and 3 for spherical pores). We assume the pores to be cylindrical, hence we use $d=2$ throughout the derivation of Faradaic impedance for Li-air batteries.
The over-potential is related to cell voltage using,
\begin{equation}
V=V_0+\eta-V_\Omega
\end{equation}
where, $V$ is the cell voltage, $V_0$ is the open cell voltage, $V_\Omega$ represents voltage drop across separator, anode, deposit layers, interfaces, electrolytes and contacts.
Substituting $R_c$ from eqn.~(\ref{eq:Li2O2_model_1_reaction_rate}) into eqn.~(\ref{eq:Faradaic_current_wo_rc_integral})
\begin{equation}
i_F=nAFk\int\limits_{0}^{l}c_o(x,t)\,dx\label{eq:dc_faradiac_current}
\end{equation}
The oxidant concentration for the porous electrode can be obtained by using modified fick's second law of diffusion. Here we assume the Faradaic impedance is derived for Li-air batteries with organic electrolyte. In Li-air batteries, the reaction products such as lithium peroxide, lithium superoxide, etc. deposit within the cathode pores and reduce the effective area. This reduction is taken into account by adding an additional term to Fick's second law of diffusion. The modified Fick's second law is,
\begin{equation}
\epsilon_0\frac{\partial c_o(x,t)}{\partial t}=
D_{\text{eff}}\frac{d^2 c_o(x,t)}{dx^2}-
kc_o(x,t)\label{eq:dc_ficks_second_law}
\end{equation}
replacing the value of $k$ from eqn.~(\ref{eq:dc_reaction_rate_contant}), we get,
\begin{equation}
\epsilon_0\frac{\partial c_o(x,t)}{\partial t}=
D_{\text{eff}}\frac{d^2 c_o(x,t)}{dx^2}-
k_0a(\epsilon)e^{-\frac{n\beta}{V_T}\eta}\,c_o(x,t)\label{eq:Li2O2_model_1_ficks_second_law_w_k_substitution}
\end{equation}
where, $D_{\text{eff}}$ is the effective diffusion constant and is given by the Bruggeman relation,
\begin{equation}
D_{\text{eff}}=\epsilon^{\text{brugg}}D_o
\end{equation}
where, brugg is the Bruggeman constant.
We assume the porosity of the cathode does not change significantly during EIS measurement, therefore, $\epsilon(x)=\epsilon_0$. 

The modified diffusion equation (\ref{eq:Li2O2_model_1_ficks_second_law_w_k_substitution}) is subjected to following initial and boundary conditions:
\begin{subequations}
\begin{enumerate}
\item[] \textbf{Initial condition}
\begin{equation}
c_o(x,0)=c_o^*\nonumber
\end{equation}
\item[(a)] \textbf{Boundary condition at $x=0$}
\begin{equation}
c_o(0,t)=c_o^*\label{bc:Li2O2_model_1_x_at_0}
\end{equation}
\item[(b)] \textbf{Boundary condition at $x=l$}
\begin{equation}
\left.\frac{dc_o(x,t)}{dx}\right|_{x=l}=0\label{bc:Li2O2_model_1_x_at_l}
\end{equation}
\end{enumerate}
\end{subequations}
\subsubsection{Steady State Analysis}
Let us consider steady state condition, i.e. when time derivatives in eqn.~(\ref{eq:Li2O2_model_1_ficks_second_law_w_k_substitution}) can be neglected. Although the perfect steady state can never be reached, this approximation is valid if the discharge currents are not high. 
The oxygen diffusion equation at the steady state is,
\begin{equation}
\frac{d^2c_o(x)}{dx^2}-\frac{kc_o(x)}{D_{\text{eff}}}=0
\end{equation}
The solution to the above equation is,
\begin{equation}
c_o(x)=C_1\cosh{\left(\sqrt{\frac{k}{D_{\text{eff}}}}x\right)}+C_2\sinh{\left(\sqrt{\frac{k}{D_{\text{eff}}}}x\right)}\label{eq:Li2O2_model_1_dc_ficks_second_law_solution}
\end{equation}
The integration constants are obtained by subjecting the eqn.~(\ref{eq:Li2O2_model_1_dc_ficks_second_law_solution}) to the boundary condition~(\ref{bc:Li2O2_model_1_x_at_0})
\begin{equation}
C_1=c_o^*\label{eq:Li2O2_model_1_dc_C1}
\end{equation}
and subjecting the eqn.~(\ref{eq:Li2O2_model_1_dc_ficks_second_law_solution}) to the boundary condition~(\ref{bc:Li2O2_model_1_x_at_l})
\begin{equation}
C_2=-c_o^*\tanh{\left(\sqrt{\frac{k}{D_{\text{eff}}}}l\right)}\label{eq:Li2O2_model_1_dc_C2}
\end{equation}
The final expression for the oxygen concentration inside the cathode under steady-state condition is obtained by using eqns.~\ref{eq:Li2O2_model_1_dc_C1},~\ref{eq:Li2O2_model_1_dc_C2}, and~\ref{eq:Li2O2_model_1_dc_ficks_second_law_solution}
\begin{equation}
c_o(x)=c_o^*\left[\cosh{\left(\sqrt{\frac{k}{D_{\text{eff}}}}x\right)}-\tanh{\left(\sqrt{\frac{k}{D_{\text{eff}}}}l\right)}\sinh{\left(\sqrt{\frac{k}{D_{\text{eff}}}}x\right)}\right]\label{eq:dc_co_complete_solution}
\end{equation}
Using the above equation one can calculate the oxygen concentration at the two boundaries (air side and separator)
\begin{subequations}
\begin{equation}
c_o(0) = c_o^*
\end{equation}
\begin{equation}
c_o(l) = c_o^*\;\textrm{sech}{\left(\sqrt{\frac{k}{D_{\textrm{eff}}}}l\right)}
\end{equation}
\end{subequations}
The final expression for the steady state Faradaic current is obtained by substituting eqn.~\ref{eq:dc_co_complete_solution} into eq.~(\ref{eq:dc_faradiac_current}) and integrating,
\begin{equation}
i_F=nAFkc_o^*\sqrt{\frac{D_{\text{eff}}}{k}}\tanh{\left(\sqrt{\frac{k}{D_{\text{eff}}}}l\right)}\label{eq:dc_i_F_complete_soln}
\end{equation}  


\subsection{Small Signal Analysis} 
The small-signal representation of over-potential, the Faradaic current and the oxidant concentration are $\delta\eta\,e^{j\omega t}$, $\delta i_F\,e^{j\omega t}$ and $\delta c_o\,e^{j\omega t}$ respectively.
Under d.c. discharge the total change in the parameters is the summation of d.c. and the small-signal component. The total Faradaic current for a porous electrode is the superposition of large-signal and the small-signal Faradaic current,
\begin{align}
i_F+\delta i_F&=2AF(k+\delta k)\int\limits_{0}^{l}\left(c_o(x)+\delta c_o(x)\right)\,dx\nonumber\\
&=2AF\int\limits_{0}^{l}\left[kc_o(x)+k\delta c_o(x)+\delta kc_o(x)+\delta k\delta c_o(x)\right]\,dx\nonumber
\end{align}
where $\delta k$ is obtained by linearizing eqn.~\ref{eq:dc_reaction_rate_contant},
\begin{equation}
\delta k=\frac{\partial k}{\partial\eta}\delta\eta\nonumber
\end{equation}
therefore,
\begin{equation}
\delta k=\frac{-n\beta k_0a(\epsilon)e^{-\frac{n\beta}{V_T}\eta}}{V_T}\delta\eta\label{eq:delta_k}
\end{equation}
using eqn.~(\ref{eq:dc_faradiac_current}), we get,
\begin{equation}
\delta i_F=nAFk\int\limits_{0}^{l}\delta c_o(x)+nAF\delta k\int\limits_{0}^{l}c_o(x)\,dx\label{eq:dc_small_faradiac_current_template}
\end{equation}
Applying small perturbation in the discharge current causes infinitesimal variation in the oxygen concentration, this variation is computed by linearizing the eqn.~(\ref{eq:dc_ficks_second_law}), 
\begin{equation}
\frac{d^2\delta c_o(x)}{dx^2} - \frac{\left(k+j\omega\epsilon_0\right)}{D_{\text{eff}}}\delta c_o(x) - \frac{\delta k\,c_o(x)}{D_{\text{eff}}}=0
\end{equation}
Substituting $c_o(x)$ from the eq.~(\ref{eq:dc_co_complete_solution}),
\begin{align}
\frac{d^2\delta c_o(x)}{dx^2} - \frac{\left(k+j\omega\epsilon_0\right)}{D_{\text{eff}}}\delta c_o(x) &= 
\frac{\delta kc_o^*}{D_{\text{eff}}} \left[\cosh{\left(\sqrt{\frac{k}{D_{\text{eff}}}}x\right)}\right.\nonumber\\ &\left.-\tanh{\left(\sqrt{\frac{k}{D_{\text{eff}}}}l\right)}\sinh{\left(\sqrt{\frac{k}{D_{\text{eff}}}}x\right)}\right] \label{eq:dc_non_homo_differ}
\end{align}
The solution to the non-homogenous differential equation (eqn.~\ref{eq:dc_non_homo_differ}) after applying linearized boundary conditions of eqns.~\ref{bc:Li2O2_model_1_x_at_0} and \ref{bc:Li2O2_model_1_x_at_l} is,
\begin{align}
\delta c_o(x)&=\frac{\delta kc_o^*}{j\omega\epsilon_0}
\left[ \cosh{\left(\sqrt{\frac{k+j \omega \epsilon_0}{D_{\text{eff}}}}x\right)} -\cosh{\left(\sqrt{\frac{k}{D_{\text{eff}}}}x\right)}\right.\nonumber\\
&-\tanh{\left(\sqrt{\frac{k+j\omega\epsilon_0}{D_{\text{eff}}}}l\right)} \sinh{\left(\sqrt{\frac{k+j\omega \epsilon_0}{D_{\text{eff}}}}x\right)}\nonumber\\
&
\left. + \tanh{\left(\sqrt{\frac{k}{D_\text{eff}}}l\right)}\sinh{\left(\sqrt{\frac{k}{D_{\text{eff}}}}x\right)}\right]\label{eq:dc_small_signal_oxidant_concentration}
\end{align}
This equation gives the variation of the oxygen concentration in the electrolyte during the impedance measurement. This variation is as a result of the applied ac signal at various frequencies in addition to normal discharge current.

The small-signal Faradaic current is obtained by substituting the eqns.~(\ref{eq:dc_co_complete_solution}) and (\ref{eq:dc_small_signal_oxidant_concentration}) into the eq.~(\ref{eq:dc_small_faradiac_current_template}),
\begin{align}
\delta i_F=&\frac{nAFk\delta kc_o^*}{j\omega\epsilon_0}\int\limits_{0}^{l}
\cosh{\left(\sqrt{\frac{k+j\omega \epsilon_0}{D_{\text{eff}}}}x\right)dx}\nonumber\\
&-\frac{nAFk\delta kc_o^*\tanh{\left(\sqrt{\frac{k+j\omega\epsilon_0}{D_{\text{eff}}}}l\right)}}{j\omega\epsilon_0}\int\limits_{0}^{l}\sinh{\left(\sqrt{\frac{k+j\omega \epsilon_0}{D_{\text{eff}}}}x\right)}dx\nonumber\\
&-\frac{nAFk\delta kc_o^*}{j\omega\epsilon_0}\int\limits_{0}^{l}\cosh{\left(\sqrt{\frac{k}{D_{\text{eff}}}}x\right)}dx\nonumber\\
&+\frac{nAFk\delta kc_o^*\tanh{\left(\sqrt{\frac{k}{D_\text{eff}}}l\right)}} {j\omega\epsilon_0} \int\limits_{0}^{l}\sinh{\left(\sqrt{\frac{k}{D_{\text{eff}}}}x\right)}dx\nonumber\\
% Space for separation
&+nAF\delta kc_o^*\int\limits_{0}^{l}\cosh{\left(\sqrt{\frac{k}{D_{\text{eff}}}}x\right)}dx\nonumber\\
&-nAF\delta kc_o^*\tanh{\left(\sqrt{\frac{k}{D_{\text{eff}}}}l\right)}\int\limits_{0}^{l} \sinh{\left(\sqrt{\frac{k}{D_{\text{eff}}}}x\right)}dx
\end{align}
Integrating the above equation, we get,
\begin{align}
\delta i_F=&\frac{nAFk\delta kc_o^*}{j\omega\epsilon_0}
\sqrt{\frac{D_{\text{eff}}}{k+j\omega\epsilon_0}} 
\left[\sinh{\left(\sqrt{\frac{k+j\omega \epsilon_0}{D_{\text{eff}}}}x\right)}\right]_0^l\nonumber\\
\displaybreak[0]
&-\frac{nAFk\delta kc_o^*\tanh{\left(\sqrt{\frac{k+j\omega\epsilon_0}{D_{\text{eff}}}}l\right)}}{j\omega\epsilon_0}\sqrt{\frac{D_{\text{eff}}}{k+j\omega\epsilon_0}}\left[\cosh{\left(\sqrt{\frac{k+j\omega \epsilon_0}{D_{\text{eff}}}}x\right)}\right]_0^l\nonumber\\
\displaybreak[0]
&-\frac{nAFk\delta kc_o^*}{j\omega\epsilon_0} \sqrt{\frac{D_{\text{eff}}}{k}}\left[\sinh{\left(\sqrt{\frac{k}{D_{\text{eff}}}}x\right)}\right]_0^l + nAF\delta kc_o^*\sqrt{\frac{D_{\text{eff}}}{k}}\left[\sinh{\left(\sqrt{\frac{k}{D_{\text{eff}}}}x\right)}\right]_0^l\nonumber\displaybreak[0]\\
& +\frac{nAFk\delta kc_o^*\tanh{\left(\sqrt{\frac{k}{D_\text{eff}}}l\right)}} {j\omega\epsilon_0}\sqrt{\frac{D_{\text{eff}}}{k}} \left[\cosh{\left(\sqrt{\frac{k}{D_{\text{eff}}}}x\right)}\right]_0^l\nonumber\displaybreak[0]\\
% Current
&-nAF\delta kc_o^*\tanh{\left(\sqrt{\frac{k}{D_{\text{eff}}}}l\right)} \sqrt{\frac{D_{\text{eff}}}{k}}\left[\cosh{\left(\sqrt{\frac{k}{D_{\text{eff}}}}x\right)}\right]_0^l\nonumber
\end{align}
After applying the integration limits, we get,
\begin{align}
%\therefore
\delta i_F&=\frac{nAFk\delta kc_o^*}{j\omega\epsilon_0}
\sqrt{\frac{D_{\text{eff}}}{k+j\omega\epsilon_0}} 
\sinh{\left(\sqrt{\frac{k+j\omega \epsilon_0}{D_{\text{eff}}}}l\right)}\nonumber\\
&-\frac{nAFk\delta kc_o^*\tanh{\left(\sqrt{\frac{k+j\omega\epsilon_0}{D_{\text{eff}}}}l\right)}}{j\omega\epsilon_0}\sqrt{\frac{D_{\text{eff}}}{k+j\omega\epsilon_0}}\left[\cosh{\left(\sqrt{\frac{k+j\omega \epsilon_0}{D_{\text{eff}}}}l\right)}-1\right]\nonumber\\
& -\frac{nAFk\delta kc_o^*}{j\omega\epsilon_0}\sqrt{\frac{D_{\text{eff}}}{k}}\sinh{\left(\sqrt{\frac{k}{D_{\text{eff}}}}l\right)} + nAF\delta kc_o^*\sqrt{\frac{D_{\text{eff}}}{k}}\sinh{\left(\sqrt{\frac{k}{D_{\text{eff}}}}l\right)} \nonumber\\
&+ \frac{nAFk\delta kc_o^*\tanh{\left(\sqrt{\frac{k}{D_\text{eff}}}l\right)}} {j\omega\epsilon_0}\sqrt{\frac{D_{\text{eff}}}{k}} \left[\cosh{\left(\sqrt{\frac{k}{D_{\text{eff}}}}l\right)}-1\right]\nonumber\\
% Space for separation
&-nAF\delta kc_o^*\tanh{\left(\sqrt{\frac{k}{D_{\text{eff}}}}l\right)} \sqrt{\frac{D_{\text{eff}}}{k}}\left[\cosh{\left(\sqrt{\frac{k}{D_{\text{eff}}}}l\right)}-1\right]\nonumber
\end{align}
after simplification,
\begin{align}
%\therefore
&\frac{\delta i_F}{\delta k} = \frac{nAFkc_o^*\sqrt{D_{\text{eff}}}}{j\omega\epsilon_0\sqrt{{k+j\omega\epsilon_0}}} \tanh{\left(\sqrt{\frac{k+j\omega\epsilon_0}{D_{\text{eff}}}}\,l\right)} + \frac{nAFc_o^*\sqrt{D_{\text{eff}}}}{\sqrt{k}}\tanh{\left(\sqrt{\frac{k}{D_{\text{eff}}}}\,l\right)}\nonumber\\
&\qquad\qquad\qquad\qquad\qquad\qquad\qquad\qquad\qquad\qquad\quad -\frac{nAFkc_o^*\sqrt{D_{\text{eff}}}}{j\omega\epsilon_0\sqrt{k}} \tanh{\left(\sqrt{\frac{k}{D_\text{eff}}}\,l\right)}\nonumber
\end{align}
After reordering the above equation,
\begin{equation}
%\therefore
\frac{\delta i_F}{\delta k} = \frac{i_F}{k}\left[\frac{k\sqrt{k}}{j\omega\epsilon_0\sqrt{{k+j\omega\epsilon_0}}} \frac{\tanh{\left(\sqrt{\frac{k+j\omega\epsilon_0}{D_{\text{eff}}}}\,l\right)}}{\tanh{\left(\sqrt{\frac{k}{D_\text{eff}}}\,l\right)}} - \frac{k}{j\omega\epsilon_0} + 1\right] \label{eq:dc_small_signal_iF_final_solution}
\end{equation}
The Faradaic impedance is given by, 
\begin{equation}
Z_F=-\frac{\delta\eta}{\delta i_F}\nonumber
\end{equation}
after rearranging, we get,
\begin{equation}
Z_F=-\frac{\delta\eta}{\delta k}\frac{\delta k}{\delta i_F}\label{eq:dc_Z_F_eqn}
\end{equation}
where, the negative sign indicates that when voltage increases the current decreases.
The first term in the eq.~(\ref{eq:dc_Z_F_eqn}) is obtained by linearizing the eq.~(\ref{eq:dc_reaction_rate_contant}),
\begin{equation}
\delta k=-k_0a(\epsilon)\frac{n\beta}{V_T} e^{-\frac{n\beta}{V_T}\eta}\delta\eta\label{eq:dc_linearized k}
\end{equation}
after rearranging,
\begin{equation}
-\frac{\delta\eta}{\delta k}=\frac{V_T}{n\beta k}\label{eq:tafel_dk/dn}
\end{equation} 
By substituting the eqns.~(\ref{eq:dc_small_signal_iF_final_solution}) and~(\ref{eq:tafel_dk/dn}) into eqn.~(\ref{eq:dc_Z_F_eqn}) and using~(\ref{eq:dc_i_F_complete_soln}), we obtain the Faradaic impedance for a Li-air battery with a porous cathode,
\begin{align}
\boxed{Z_F=\frac{Z_0}{\left[1-\frac{k}{j\omega\epsilon_0} +\frac{k\sqrt{k}\tanh{\left(\sqrt{\frac{k+j\omega\epsilon_0}{D_{\text{eff}}}}\,l\right)}}{j\omega\epsilon_0\sqrt{k+j\omega\epsilon_0}\tanh\left(\sqrt{\frac{k}{D_{\text{eff}}}}\,l\right)}\right]}}\label{eq:dc_Faradaic_final_solution}
\end{align}
This is the final equation for the Faradaic current where $Z_0$ is
\begin{equation}
Z_0=\frac{V_T}{n\beta i_F}
\end{equation}
and $k$ is the solution of the nonlinear equation of eqn.(~\ref{eq:dc_i_F_complete_soln})
\begin{equation}
i_F=nAFkc_o^*\sqrt{\frac{D_{\text{eff}}}{k}}\tanh{\left(\sqrt{\frac{k}{D_{\text{eff}}}}l\right)}\nonumber
\end{equation}
The total impedance (\ref{eq:Total_impedance}) is the parallel combination of the Faradaic impedance (\ref{eq:dc_Faradaic_final_solution}) and the double layer impedance.
\begin{equation}
Z = Z_F + Z_{dl}\nonumber
\end{equation}


\section{Simulation Results}
\todo[noline]{Redraw all the figures}
The simulation results of the electrochemical impedance spectroscopy are presented in this section. The impedance spectrum in Li-air batteries with organic electrolyte is computed using the following steps:  
\begin{enumerate}
\item Solve the non-linear eqn.\ref{eq:dc_i_F_complete_soln} to obtain
the value of the reaction rate ($k$) 
\item Compute the Faradaic impedance for a range of frequencies using eqn.
\ref{eq:dc_Faradaic_final_solution} and $k$ from step 1 
\item Compute the total impedance using $Z=Z_{F}+Z_{dl}$ 
\end{enumerate}
Figure~\ref{plots:our_1_model_c_int} presents the effect of the oxygen concentration on the impedance spectra for two different values of the diffusion coefficient. From this figure, we can see that as the concentration of oxygen decreases, the oxygen diffusion peak grows until it combines with the double layer peak.
\begin{figure}[bt]
\begin{center}
\includegraphics[width=0.49\textwidth]{\main figures/one_specie_andrei_c_int_1} 
\includegraphics[width=0.49\textwidth]{\main figures/one_specie_andrei_c_int_2}
\caption{The effect of the initial oxygen concentration on the impedance plot for two values of the system length: $l=100\,\textrm{nm}$ (left) and $l=10\,\mu\textrm{m}$ (right). The other simulation parameters are, $\textrm{C}_\textrm{d}= 4\,\mu\textrm{F}/\textrm{cm}^2$, $\textrm{D}_\textrm{o}=7\times10^{-6}\,\textrm{cm}^2/\textrm{s}$, $\textrm{I}_\textrm{dis}= 200\,\mu\textrm{A}/\textrm{cm}^2$, $\textrm{k}=1.3\times10^{-8}\,\textrm{cm}/\textrm{s}$ and $n=3$}
\label{plots:our_1_model_c_int}
\end{center}
\end{figure}

The figure~\ref{plots:our_1_model_k0} shows the effect of the reaction rate constant (top) and the number of electrons involved (bottom) on the Nyquist plots. When only Li$_{2}$O$_{2}$ is formed ($n=2$), the reaction rate has no effect on the Nyquist plot (top left), but, if other products are also formed ($n\neq2$), the change in reaction rate can be observed on the Nyquist plot. The number of electrons consumed in the cathode reaction has a large effect on of number of semicircles and total resistance for a system (bottom). The total resistance and the radius of the semicircles depend significantly on the oxygen diffusion coefficient of the electrolyte. The simulation parameters are listed in table~\ref{Table:plots_our_1_model_k0}
\begin{table}[tb]
\centering
\begin{tabular}{|c|c|c|c|}
\hline 
\multirow{2}{*}{Parameter}  & Top Figure  & Bottom Figure  & \multirow{2}{*}{Units}\tabularnewline
\cline{2-3}
 & Value  & Value  & \tabularnewline
\hline 
\hline 
$c_{o}^{*}$  & $3.26\times10^{-6}$  & $3.26\times10^{-7}$  & $\textrm{mol/c\ensuremath{m^{3}}}$\tabularnewline
\hline 
{$C_{d}$}  & {$40\times10^{-6}$}  & {$4\times10^{-6}$}  &{$\textrm{F/c\ensuremath{m^{2}}}$}\tabularnewline
\hline 
$k$  & variable plotted  & $1.3\times10^{-7}$  & cm/s\tabularnewline
\hline 
$l$  & $1\times10^{-4}$  & $10\times10^{-4}$  & cm\tabularnewline
\hline 
\multirow{2}{*}{$n$}  & $2$ (left)  & \multirow{2}{*}{variable plotted}  & \multirow{2}{*}{}\tabularnewline
\cline{2-2}
 & 3 (right)  &  & \tabularnewline
\hline 
$i_{F}$  & $200\times10^{-6}$  & $500\times10^{-6}$  & $\textrm{A/c\ensuremath{m^{2}}}$\tabularnewline
\hline 
\multirow{2}{*}{$D_{o}$}  & \multirow{2}{*}{$7\times10^{-6}$}  & $7\times10^{-7}$ (left)  & \multirow{2}{*}{$\textrm{c\ensuremath{m^{2}}/s}$}\tabularnewline
\cline{3-3}
 &  & $1.4\times10^{-5}$ (right)  & \tabularnewline
\hline 
\end{tabular}
\caption{List of simulation parameters for fig.~\ref{plots:our_1_model_k0}}
\label{Table:plots_our_1_model_k0}
\end{table}

\begin{figure}[bt]
\begin{center}
\includegraphics[width=0.49\textwidth]{\main figures/one_specie_andrei_k0_4} 
\includegraphics[width=0.49\textwidth]{\main figures/one_specie_andrei_k0_3}
\includegraphics[width=0.49\textwidth]{\main figures/one_specie_andrei_n_1}
\includegraphics[width=0.49\textwidth]{\main figures/one_specie_andrei_n_2}
\caption{(Top) The effect of the reaction rate on the impedance plot for two values of the stoichiometric number of electrons: $n=2$ (left) and $n=3$ (right).
(Bottom) The effect of the stoichiometric number of electrons on the impedance plot for two values of the oxidant diffusion coefficient: $\textrm{D}_\textrm{o}=7\times10^{-7}\,\textrm{cm}^2/\textrm{s}$ (left) and $\textrm{D}_\textrm{o}=1.4\times10^{-5}\,\textrm{cm}^2/\textrm{s}$ (right).}
\label{plots:our_1_model_k0}
\end{center}
\end{figure}

The figure~\ref{plots:our_1_model_I_dis} shows the effect of different discharge currents for two values of the oxygen diffusion coefficient (top) and oxygen diffusion coefficient for two values of the double layer capacitance (bottom). The shape of the Nyquist curves and the total resistance of the battery depends on how easily oxygen molecules can diffuse through the electrolyte; for low diffusivity (top left) we see two distinct semicircles (one corresponding to double layer and charge transfer, and the other one to oxygen diffusion). At higher values of the diffusion coefficient (top right) we observe much smaller values of the total resistance and for currents smaller than 1 mA/cm$^{2}$ the diffusion semicircle vanishes, which suggests that the oxygen diffusion is no longer a limiting factor for the system. The bottom figure shows the effect of the oxygen diffusion for two different values of the double layer capacitance on the impedance response. For cathodes with low capacitance per unit area (bottom left) we observe three different effects: first, for large values of oxygen diffusivity, the system is not limited by oxygen diffusion; second, as the oxygen diffusivity is reduced a second semicircle corresponding to diffusion starts to appear; third, as the oxygen diffusion coefficient is reduced further the two effects (double layer and oxygen diffusion) combine and result into a single semicircle on the Nyquist plot. The simulation parameters are listed in table~\ref{Table:plots_our_1_model_I_dis} (unless specified otherwise). 
\begin{table}[tb]
\centering
\begin{tabular}{|c|c|c|c|}
\hline 
\multirow{2}{*}{Parameter}  & Top Figure  & Bottom Figure  & \multirow{2}{*}{Units}\tabularnewline\cline{2-3}
 & Value  & Value  & \tabularnewline
\hline 
\hline 
$c_{o}^{*}$  & $3.26\times10^{-6}$  & $3.26\times10^{-7}$  & $\textrm{mol/c\ensuremath{m^{3}}}$\tabularnewline 
\hline 
\multirow{2}{*}{$C_{d}$}  & \multirow{2}{*}{$40\times10^{-6}$}  & $4\times10^{-6}$ (left)  & \multirow{2}{*}{$\textrm{F/c\ensuremath{m^{2}}}$}\tabularnewline
\cline{3-3}
 &  & $40\times10^{-6}$ (right)  & \tabularnewline
\hline 
$k$  & $1.3\times10^{-9}$  & $1.3\times10^{-7}$  & cm/s\tabularnewline
\hline 
$l$  & $10^{-4}$  & $10^{-4}$  & cm\tabularnewline
\hline 
$n$  & $2$  & $3$  & \tabularnewline
\hline 
$i_{F}$  & variable plotted  & $200\times10^{-6}$  & $\textrm{A/c\ensuremath{m^{2}}}$\tabularnewline
\hline 
\multirow{2}{*}{$D_{o}$}  & $3.5\times10^{-6}$ (left)  & \multirow{2}{*}{variable plotted}  & \multirow{2}{*}{$\textrm{c\ensuremath{m^{2}}/s}$}\tabularnewline
\cline{2-2}
 & $7\times10^{-5}$ (right)  &  & \tabularnewline
\hline 
\end{tabular}
\caption{List of simulation parameters for fig.~\ref{plots:our_1_model_I_dis}}
\label{Table:plots_our_1_model_I_dis}
\end{table}
\begin{figure}[bt]
\begin{center}
\includegraphics[width=0.49\textwidth]{\main figures/one_specie_andrei_I_dis_2} 
\includegraphics[width=0.49\textwidth]{\main figures/one_specie_andrei_I_dis_1}
\includegraphics[width=0.49\textwidth]{\main figures/one_specie_andrei_Do_3}
\includegraphics[width=0.49\textwidth]{\main figures/one_specie_andrei_Do_2}
\caption{(Top) The effect of the d.c. discharge current on the impedance plot for two values of the oxygen diffusion coefficient: $\textrm{D}_\textrm{o}=3.5\times10^{-6}\,\textrm{cm}^2/\textrm{s}$ (left) and $\textrm{D}_\textrm{o}=7\times10^{-5}\,\textrm{cm}^2/\textrm{s}$ (right).
(Bottom) The effect of the oxidant diffusion coefficient on the impedance plot for two values of the double layer capacitance: $\textrm{C}_\textrm{d}= 4\,\mu\textrm{F}/\textrm{cm}^2$ (left) and $\textrm{C}_\textrm{d}= 40\,\mu\textrm{F}/\textrm{cm}^2$ (right).}
\label{plots:our_1_model_I_dis}
\end{center}
\end{figure}

Figure~\ref{plots:our_1_model_l} shows the effect of the cathode length (top) and cathode capacitance (bottom) on the Nyquist plots. For a system with a small stoichiometric number (top left) there is a direct relationship between the length of the cathode and the total resistance. But for higher number of electrons there is no difference in the shape and size of the curve but only a slight change in the values at each frequency point (top right). The double layer capacitance affects the number of peaks, while the total resistance remains constant. Small values of the cathode capacitance could be used to distinguish a system with low oxygen diffusivity (bottom left) from a system with higher oxygen diffusion (bottom right). The simulation parameters are listed in table~\ref{Table:} (unless specified otherwise).
\begin{table}[tb]
\centering
\begin{tabular}{|c|c|c|c|}
\hline 
\multirow{2}{*}{Parameter}  & Top Figure  & Bottom Figure  & \multirow{2}{*}{Units}\tabularnewline
\cline{2-3}
 & Value  & Value  & \tabularnewline
\hline 
\hline 
$c_{o}^{*}$  & $3.26\times10^{-7}$  & $3.26\times10^{-6}$  & $\textrm{mol/c\ensuremath{m^{3}}}$\tabularnewline
\hline 
{$C_{d}$}  & {$4\times10^{-6}$}  &{variable plotted}  & {$\textrm{F/c\ensuremath{m^{2}}}$}\tabularnewline
\hline 
$k$  & $1.3\times10^{-8}$  & $1.3\times10^{-9}$  & cm/s\tabularnewline
\hline 
$l$  & variable plotted  & $10\times10^{-4}$  & cm\tabularnewline
\hline 
\multirow{2}{*}{$n$}  & $2.2$ (left)  & \multirow{2}{*}{$2.2$}  & \multirow{2}{*}{}\tabularnewline
\cline{2-2}
 & $3$ (right)  &  & \tabularnewline
\hline 
$i_{F}$  & $200\times10^{-6}$  & $200\times10^{-6}$  & $\textrm{A/c\ensuremath{m^{2}}}$\tabularnewline
\hline 
\multirow{2}{*}{$D_{o}$}  & \multirow{2}{*}{$7\times10^{-6}$}  & $7\times10^{-7}$ (left)  & \multirow{2}{*}{$\textrm{c\ensuremath{m^{2}}/s}$}\tabularnewline
\cline{3-3}
 &  & $3.5\times10^{-6}$ (right)  & \tabularnewline
\hline 
\end{tabular}
\caption{List of simulation parameters for fig.~\ref{plots:our_1_model_l}}
\label{Table:plots_our_1_model_l}
\end{table}

\begin{figure}[bt]
\begin{center}
\includegraphics[width=0.49\textwidth]{\main figures/one_specie_andrei_l_1} 
\includegraphics[width=0.49\textwidth]{\main figures/one_specie_andrei_l_2}
\includegraphics[width=0.49\textwidth]{\main figures/one_specie_andrei_Cd_1}
\includegraphics[width=0.49\textwidth]{\main figures/one_specie_andrei_Cd_2}
\caption{(Top) The effect of the system length on the impedance plot for two values of the stoichiometric number of electrons: $n=2$ (left) and $n=3$ (right).
(Bottom) The effect of the double layer capacitance on the impedance plot for two values of the stoichiometric number of electrons: $n=2$ (left) and $n=3$ (right).}
\label{plots:our_1_model_l}
\end{center}
\end{figure}
\end{document}



