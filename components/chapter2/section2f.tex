\documentclass[../../thesis.tex]{subfiles}
\providecommand{\main}{../../}
\begin{document}
\section{Finite-length Gerischer Impedance}
In this section, we derive the Faradaic impedance for a single diffusing specie in an electrochemical system with a transmissive boundary located at a distance $l$ from the electrode.
% much shorter than characteristic diffusion length of the ion ($\Delta x=\sqrt{2D_ot}$). 
The half cell electrochemical reaction followed by an irreversible chemical reaction considered is,
\begin{align}
\ce{O + ne <=>[k_f][k_b] R}\nonumber\\
\ce{R ->[k] T}\nonumber
\end{align}
\begin{figure}[h!]
\begin{center}
\includegraphics[width=0.5\textwidth]{\main figures/one_specie_transmissive_concentration_profile}
\end{center}
\caption{The spatial variation of oxidant concentration of a finite-length electrochemical system with transmissive boundary at different discharge currents is shown: 0 d.c. discharge (dotted line) and finite d.c. discharge (continuous line).}
\label{fig:one_specie_transmissive_gerischer_diffusion}
\end{figure}
The mass transport in the absence of convection and electronmigration is given by modified Fick's second law of diffusion,
\begin{align}
\frac{\partial c_o}{\partial t}=& D_o\frac{\partial^2  c_o}{\partial x^2}-k c_o\label{eq:ficks_2nd_law_finite_gerischer_with_deposition}
\end{align}
\paragraph{Initial and Boundary conditions} The initial and boundary conditions required to solve the partial differential equation (\ref{eq:ficks_2nd_law_finite_gerischer_with_deposition}) are,
\begin{subequations}
\begin{enumerate}
\item[] \textbf{Initial condition}
\begin{align}
c_o(x,0)&=c_o^*\nonumber
\end{align}
\item[(a)] \textbf{Boundary conditions at $x=l$}
\begin{align}
\delta c_o(l)&=0\label{bc:one_specie_transmissive_gerischer_change_0}
\end{align}
\begin{align}
\frac{d\,\delta c_o(l)}{dx}&\neq0\label{bc:one_specie_transmissive_gerischer_change_concentration}
\end{align}
where, $l$ is the distance between the electrode and the transmissive boundary (see fig.~\ref{fig:one_specie_transmissive_gerischer_diffusion})
\item[(b)] \textbf{Boundary condition at the electrode/electrolyte interface}
\begin{align}
D_o\left.\frac{\partial c_{o}(x,t)}{\partial x}\right|_{x=0}=\frac{i_F(t)}{nFA}\label{bc:one_specie_transmissive_gerischer_ficks_1st_law}
\end{align}
\end{enumerate}
\end{subequations}
The Butler-Volmer for a single diffusing specie is given by,
\begin{equation}
i_F=i_0\cdot{\left[\frac{c_o(0,t)}{c_o^*}e^{\frac{-\beta n}{V_T}\eta}-e^{\frac{(1-\beta) n}{V_T}\eta}\right]}\label{eq:butler_volmer_eq_one_specie_transmissive_gerischer}
\end{equation}
\paragraph{Calculation of Impedance Spectra} The small-signal representation of over-potential, the Faradaic current, and the oxidant concentration are $\delta\eta\,e^{j\omega t}$, $\delta i_F\,e^{j\omega t}$ and $\delta c_o\,e^{j\omega t}$ respectively.
The small-signal Faradaic current is obtained again by using linearizion,
\begin{equation}
\delta i_F=\left(\frac{\partial i_F}{\partial\eta}\right)\delta\eta+
\left(\frac{\partial i_F}{\partial c_o}\right)\delta c_o(0)\label{eq:linealized_ficks_one_specie_transmissive_gerischer}
\end{equation}
The partial derivatives in the above equation are,
\begin{subequations}
\begin{align}
\left(\frac{\partial i_F}{\partial \eta}\right)=&-i_0\left[\frac{n\beta }{V_T}\frac{c_o(0,t)}{c_o^*}e^{\frac{-\beta n}{V_T} \eta}+\frac{(1-\beta)n}{V_T}e^{\frac{(1-\beta)n}{V_T} \eta}\right]\label{eq:one_specie_transmissive_gerischer_taylor_series_derivative_overpotential}\\
\left(\frac{\partial i_F}{\partial c_o}\right)=&\frac{i_0}{c_o^*}e^{\frac{-\beta n}{V_T} \eta} \label{eq:one_specie_transmissive_gerischer_taylor_series_derivative_oxidant_concentration}
\end{align}
\end{subequations}
Now, we substitute the partial derivatives (\ref{eq:one_specie_transmissive_gerischer_taylor_series_derivative_overpotential}) and~(\ref{eq:one_specie_transmissive_gerischer_taylor_series_derivative_oxidant_concentration}) into the small-signal Faradaic impedance (\ref{eq:linealized_ficks_one_specie_transmissive_gerischer}),
\begin{equation}
\delta i_F=-i_0\left[\frac{n\beta }{V_T}\frac{c_o(0,t)}{c_o^*}e^{\frac{-\beta n}{V_T} \eta}+\frac{(1-\beta)n}{V_T}e^{\frac{(1-\beta)n}{V_T} \eta}\right]\delta\eta + \frac{i_0}{c_o^*}e^{\frac{-\beta n}{V_T} \eta}\delta c_o(0)\label{eq:small_signal_substituted_one_specie_transmissive_gerischer}
\end{equation}
Linearizing eqn.~(\ref{eq:ficks_2nd_law_finite_gerischer_with_deposition}) to find $\delta c_o(0)$,
\begin{equation}
j\omega \delta c_o(x)=D_o \frac{d^2\left[\delta c_o(x)\right]}{dx^2}-k\delta c_o(x)\label{eq:ficks_small_signal_one_specie_gerischer_transmissive}
\end{equation}
The solution of the above equation is,
\begin{equation}
\delta c_o(x)=C_1e^{-\sqrt{\frac{k+j\omega}{D_o}}x}+C_2e^{\sqrt{\frac{k+j\omega}{D_o}}x}\label{eq:linealized_ficks_transmissive_gerischer}
\end{equation}
The linearize boundary conditions are, 
\begin{subequations}
\begin{align}
D_o\left.\frac{d\delta c_o(x)}{d x}\right|_{x=0}&=\frac{\delta i_F}{nFA}
\label{bc:linearized_boundary_condition_one_specie_transmissive_gerischer_ficks_1st_law}\\
\delta c_o(l)&=0\label{bc:linearized_boundary_condition_one_specie_transmissive_gerischer}
\end{align}
\end{subequations}
\begin{subequations}
Integration constants $C_1$ and $C_2$ can be found by introducing~(\ref{eq:linealized_ficks_transmissive_gerischer}) into~(\ref{bc:linearized_boundary_condition_one_specie_transmissive_gerischer_ficks_1st_law}) and~(\ref{bc:linearized_boundary_condition_one_specie_transmissive_gerischer})
\begin{align}
C_2-C_1=\frac{\delta i_F}{nFA\sqrt{(k+j\omega) D_o}}\label{eq:constant_relation_one_specie_transmissive_gerischer_subject_1st_diffusion}\\
-C_1=C_2e^{2\sqrt{\frac{k+j\omega}{D_o}}l}\label{eq:constant_relation_one_specie_transmissive_gerischer_subject_boundary}
\end{align}
\end{subequations}
Solving for $C_1$ amd $C_2$, we obtain,
\begin{subequations}
\begin{align}
C_1=&-\frac{\delta i_F}{nFA\sqrt{(k+j\omega) D_o}}\left[\frac{e^{2\sqrt{\frac{k+j\omega}{D_o}}l}}{1+e^{2\sqrt{\frac{k+j\omega}{D_o}}l}}\right]\label{eq:one_specie_transmissive_gerischer_c1}\\
C_2=&\frac{\delta i_F}{nFA\sqrt{(k+j\omega)D_o}}\left[\frac{1}{1+e^{2\sqrt{\frac{k+j\omega}{D_o}}l}}\right]\label{eq:one_specie_transmissive_gerischer_c2}
\end{align}
\end{subequations}
Now, we replace integration constants $C_1$ and $C_2$ into eqn.~(\ref{eq:linealized_ficks_transmissive_gerischer})
% with  the ones in eqns.~\ref{eq:one_specie_transmissive_gerischer_c1} and~\ref{eq:one_specie_transmissive_gerischer_c2}, 
\begin{align}
\delta c_o(x)&=-\frac{\delta i_F}{nFA\sqrt{(k+j\omega)D_o}}\left[\frac{e^{\sqrt{\frac{k+j\omega}{D_o}}(l-x)}-e^{-\sqrt{\frac{k+j\omega}{D_o}}(l-x)}}{e^{\sqrt{\frac{k+j\omega}{D_o}}l}+e^{-\sqrt{\frac{k+j\omega}{D_o}}l}}\right]\nonumber\\
\therefore\qquad\delta c_o(x)&=-\frac{\delta i_F}{nFA\sqrt{(k+j\omega)D_o}}\left[\frac{\sinh{\sqrt{\frac{k+j\omega}{D_o}}(l-x)}}{\cosh{\sqrt{\frac{k+j\omega}{D_o}}l}}\right]
\end{align}
The small-signal oxidant concentration at the electrode surface ($x=0$) is,
\begin{align}
\delta c_o(0)&=-\frac{\delta i_F}{nFA\sqrt{(k+j\omega)D_o}}\tanh\left({\sqrt{\frac{k+j\omega}{D_o}}l}\right)\label{eq:one_specie_transmission_gerischer_delta_co}
\end{align}
Now, we substitute eqn.~(\ref{eq:one_specie_transmission_gerischer_delta_co}) into the equation for small-signal faradaic current (\ref{eq:small_signal_substituted_one_specie_transmissive_gerischer}),
\begin{align}
\delta i_F=&-i_0\left[\frac{n\beta }{V_T}\frac{c_o(0,t)}{c_o^*}e^{\frac{-\beta n}{V_T} \eta}+\frac{(1-\beta)n}{V_T}e^{\frac{(1-\beta)n}{V_T} \eta}\right]\delta\eta\nonumber\\ &- \frac{i_0\delta i_F}{nFAc_o^*\sqrt{(k+j\omega)D_o}}\tanh\left({\sqrt{\frac{k+j\omega}{D_o}}l}\right)e^{\frac{-\beta n}{V_T} \eta}\label{eq:small_signal_faradaic_current_one_specie_transmissive_gerischer}
\end{align}
The faradaic impedance ($Z_F$) is given by $Z_F=-\frac{\delta\eta}{\delta i_F}$, which gives,
\begin{align}
Z_F(c_o(0,t),\eta)=\frac{1+\frac{i_0\tanh\left({\sqrt{\frac{k+j\omega}{D_o}}l}\right)}{nFAc_o^*\sqrt{(k+j\omega)D_o}}e^{\frac{-\beta n}{V_T} \eta}}
{\frac{i_0n}{V_T} \left[\frac{\beta c_o(0,t)}{c_o^*}e^{-\frac{\beta n}{V_T}\eta} + \left(1-\beta\right)e^{\frac{\left(1-\beta\right)n}{V_T}\eta}\right]}
\end{align}
Under 0 d.c. discharge;~$\eta=0$ and~$c_o(0,t)=c_o^*$ and the Faradaic impedance becomes,
\begin{equation}
\boxed{Z_F=R_\eta+\frac{V_T}{n^2FAc_o^*\sqrt{D_o}}\left[\frac{\tanh{\left(\sqrt{\frac{k+j\omega}{D_o}}l\right)}}{\sqrt{k+j\omega}}\right]}\label{eq:one_specie_transmissive_gerischer_Zf}
\end{equation} 
The total impedance (\ref{eq:Total_impedance}) is the parallel combination of the Faradaic impedance (\ref{eq:one_specie_transmissive_gerischer_Zf}) and the double layer impedance.
\paragraph{Simulation results}
\begin{figure}[b!]
\begin{center}
\includegraphics[width=0.49\textwidth]{\main figures/one_specie_gerischer_transmissive_Do_2}
\includegraphics[width=0.49\textwidth]{\main figures/one_specie_gerischer_transmissive_Do_1}
\caption{The Nyquist plot of the open boundary finite-length Gerischer impedance for a single electrochemical diffusing specie. The figures show the effect of the oxidant diffusion coefficient on the impedance plot for two values of the double layer capacitance: $C_d= 400\,\textrm{nF}/\textrm{cm}^2$ (left) and $C_d= 4\,\textrm{mF}/\textrm{cm}^2$ (right). The other simulation parameters are, $c_o= 3.26\times10^{-7}\,\textrm{mol}/\textrm{cm}^2$, $l=500\,\textrm{nm}$ and $k_0=1.3\times10^{-8}\,\textrm{cm}/\textrm{s}$.}
\label{plots:1S_Ger_Do_comp}
\end{center}
\end{figure}
\begin{figure}[t!]
\begin{center}
\includegraphics[width=0.49\textwidth]{\main figures/one_specie_gerischer_transmissive_Cd_1} 
\includegraphics[width=0.49\textwidth]{\main figures/one_specie_gerischer_transmissive_Cd_2}
\includegraphics[width=0.49\textwidth]{\main figures/one_specie_gerischer_transmissive_ko_1}
\includegraphics[width=0.49\textwidth]{\main figures/one_specie_gerischer_transmissive_ko_2}
\caption{The Nyquist plot of the open boundary finite-length Gerischer impedance for a single electrochemical diffusing specie. The top two figures show the effect of the double layer capacitance on the impedance plot for two values of the oxidant diffusion coefficient: $D_o=7\times10^{-7}\,\textrm{cm}^2/\textrm{s}$ (top left) and $D_o=7\times10^{-5}\,\textrm{cm}^2/\textrm{s}$ (top right). The other simulation parameters are, $c_o= 1.63\times10^{-6}\,\textrm{mol}/\textrm{cm}^2$, $k_0=6.5\times10^{-9}\,\textrm{cm}/\textrm{s}$ and $l=1\,\mu\textrm{m}$. The bottom two figures show the effect of the reaction constant on the impedance plot for two values of the double layer capacitance: $C_d= 400\,\textrm{nF}/\textrm{cm}^2$ (bottom left) and $C_d= 400\,\mu\textrm{F}/\textrm{cm}^2$ (bottom right). The other simulation parameters are, $c_o= 3.26\times10^{-7}\,\textrm{mol}/\textrm{cm}^2$, $D_o=3.5\times10^{-6}\,\textrm{cm}^2/\textrm{s}$ and $l=500\,\textrm{nm}$.}
\label{plots:1S_Ger_Cd_comp}
\end{center}
\end{figure}
\begin{figure}[h!]
\begin{center}
\includegraphics[width=0.49\textwidth]{\main figures/one_specie_gerischer_transmissive_l_2} 
\includegraphics[width=0.49\textwidth]{\main figures/one_specie_gerischer_transmissive_l_1}
\includegraphics[width=0.49\textwidth]{\main figures/one_specie_gerischer_transmissive_co_1}
\includegraphics[width=0.49\textwidth]{\main figures/one_specie_gerischer_transmissive_co_2}
\caption{The Nyquist plot of the open boundary finite-length Gerischer impedance for a single electrochemical diffusing specie. The top two figures show the effect of the system length on the impedance plot for two values of the double layer capacitance: $C_d= 80\,\mu\textrm{F}/\textrm{cm}^2$ (top left) and $C_d= 4\,\textrm{mF}/\textrm{cm}^2$ (top right). The other simulation parameters are, $c_o= 3.26\times10^{-7}\,\textrm{mol}/\textrm{cm}^2$, $D_o=7\times10^{-6}\,\textrm{cm}^2/\textrm{s}$ and $k_0=1.3\times10^{-7}\,\textrm{cm}/\textrm{s}$. 
The bottom two figures show the effect of the oxidant concentration on the impedance plot for two values of the system length: $l=500\,\textrm{nm}$ (bottom left) and $l=10\,\mu\textrm{m}$ (bottom right). The other simulation parameters are, $C_d= 4\,\textrm{mF}/\textrm{cm}^2$, $D_o=7\times10^{-6}\,\textrm{cm}^2/\textrm{s}$ and $k_0=2.6\times10^{-8}\,\textrm{cm}/\textrm{s}$}
\label{plots:2S_RF_l_comp}
\end{center}
\end{figure}
\end{document}