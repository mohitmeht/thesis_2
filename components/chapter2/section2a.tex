\documentclass[../../thesis.tex]{subfiles}
\providecommand{\main}{../../}
\begin{document}
\section{Semi-infinite linear diffusion of one electrochemical specie}\label{sec:one_specie_semi_infinite}
We start with a simple case of one diffusing electroactive specie that is subjected to the semi-infinite boundary condition, which means the diffusing specie would never hit a wall, a membrane or any other boundary during the measurement. This condition is usually met if the distance between the electrode and the boundary is much larger than the diffusion length of the electroactive specie in the solution. 
Consider the case of a generalized electrochemical half cell reaction
\begin{equation}
\ce{O + \textit{n}e <=>[k_f][k_b] R}\label{general_reaction}
\end{equation}
where, O is the oxidant specie and R is the reductant, $n$ is the number of electrons, $k_f$ is the forward reaction rate and $k_b$ is the backward reaction rate.
The concentration profile for a single diffusing specie at various d.c. discharge currents is shown in figure~\ref{one_specie_diffusion}. The solid lines in the figure represent oxidant concentrations at different d.c. discharge currents and the dotted line represents initial concentration throughout the system.
\begin{figure}[t!]
\begin{center}
\includegraphics[width=0.55\textwidth]{\main figures/one_specie_semi_infinite_concentration_profile}
\end{center}
\caption{Concentration profiles of the oxidant for an electrochemical system described by eqns.~(\ref{general_reaction}-\ref{bc:ficks_first_law_at_surface}).} 
\label{one_specie_diffusion}
\end{figure}
The Fick's second law for a single diffusing specie is given by,
\begin{equation}
\frac{\partial c_{o}(x,t)}{\partial t}=D_o \frac{\partial^2 c_o(x,t)}{\partial^2 x} 
\label{eq:ficks_second_law_of_diff}
\end{equation}
where, $c_o$ is the oxidant concentration and $D_o$ is the diffusion constant of the oxidant.
\paragraph{Initial and boundary conditions} The initial and boundary conditions for the above homogeneous partial differential equation are
\begin{subequations}
\begin{enumerate}
\item[(a)] \textbf{Initial condition}
\begin{align}
c_o(x,0)=c_o^*\label{bc:smple1e_1}
\end{align}
\item[(b)] \textbf{Boundary condition at x=$\infty$}
\begin{align}
c_o(\infty,t)&=c_o^*\label{bc:smple1e_2}
\end{align}
\item[(c)] \textbf{Boundary condition at Electrode/Electrolyte interface}
\begin{align}
D_o \left.\frac{\partial c_{o}(x,t)}{\partial x}\right|_{x=0}=\frac{i_F(t)}{nFA}
\label{bc:ficks_first_law_at_surface}
\end{align}
\end{enumerate}
\end{subequations}
Let us assume the reaction rate is given by the following Butler-Volmer equation,
\begin{equation}
i_F=i_0\cdot{\left[\frac{c_o(0,t)}{c_o^*}e^{\frac{-\beta n}{V_T}\eta}-e^{\frac{(1-\beta) n}{V_T}\eta}\right]}
\label{eq:reduced_Butler_Volmer_eqn}
\end{equation}
%(During discharge at high currents or high negative overpotential $\eta>>V_T$) the Butler-Volmer equation reduces to
%\begin{equation}
%i_F(t)=i_0\frac{c_o(0,t)}{c_o^*}e^{\frac{-\beta n}{V_T}\eta}
%\label{eq:reduced_Butler_Volmer_eqn}
%\end{equation}
\paragraph{Calculation of Impedance Spectra} In EIS a small sinusoidal perturbation of voltage (potentiostatic) or current (galvanostatic) is applied to a system and its response is measured. In this prospectus, we only consider galvanostatic EIS, hence a small-signal current is applied and its corresponding voltage is measured. The small variation in current ($\delta i_F\, e^{j\omega t}$) causes perturbation in voltage and hence in the over-potential ($\delta\eta\,e^{j\omega t}$), the perturbation also affects the oxidant concentration, $\delta c_o(x)\,e^{j\omega t}$. The current is a function of the over-potential and the concentration of electrochemical species. We consider only the oxidant in our analysis and  assume the reductant's concentration ($c_r^*$) to remain constant throughout the system and be unaffected by mass transport during the measurements. We can expand eqn.~(\ref{eq:reduced_Butler_Volmer_eqn}) as an infinite Taylor series for small-signal variations.
\begin{align}
\delta i_F =&\left(\frac{\partial i_F}{\partial \eta}\right)\delta \eta + \left(\frac{\partial i_F}{\partial c_o}\right)\delta c_o(0)\nonumber \\
+& \frac{1}{2}\left(\frac{\partial^2 i_F}{\partial \eta^2}\right)\left(\delta \eta \right)^2+\frac{1}{2}\left(\frac{\partial^2 i_F}{\partial c_o^2}\right)\left(\delta c_o(0) \right)^2\nonumber \\
+&\left(\frac{\partial^2 i_F}{\partial \eta\,\partial c_o}\right)\left(\delta \eta\,\delta c_o(0) \right)+\dots
\label{eq:inf_taylor_series}
\end{align}
Since we assume the perturbations are infinitesimally small, we can neglect the high order terms in the equation~(\ref{eq:inf_taylor_series}), leaving only the linear terms
\begin{equation}
\delta i_F=\left(\frac{\partial i_F}{\partial \eta}\right)\delta \eta + \left(\frac{\partial i_F}{\partial c_o}\right)\delta c_o(0)
\label{eq:linear_inf_taylor_series}
\end{equation}
The partial derivatives of faradiac current w.r.t the over-potential and the oxidant concentration are,
\begin{align}
\left(\frac{\partial i_F}{\partial \eta}\right)=&-i_0\left[\frac{n\beta }{V_T}\frac{c_o(0,t)}{c_o^*}e^{\frac{-\beta n}{V_T} \eta}+\frac{(1-\beta)n}{V_T}e^{\frac{(1-\beta)n}{V_T} \eta}\right]\label{eq:one_specie_taylor_series_derivative_overpotential}\\
\left(\frac{\partial i_F}{\partial c_o}\right)=&\frac{i_0}{c_o^*}e^{\frac{-\beta n}{V_T} \eta} \label{eq:one_specie_taylor_series_derivative_oxidant_concentration}
\end{align}
Using the relations~(\ref{eq:one_specie_taylor_series_derivative_overpotential}) and~(\ref{eq:one_specie_taylor_series_derivative_oxidant_concentration}), eqn.~(\ref{eq:linear_inf_taylor_series}) becomes
\begin{equation}
\delta i_F=-i_0\left[\frac{n\beta }{V_T}\frac{c_o(0,t)}{c_o^*}e^{\frac{-\beta n}{V_T} \eta}+\frac{(1-\beta)n}{V_T}e^{\frac{(1-\beta)n}{V_T} \eta}\right]\delta\eta + \frac{i_0}{c_o^*}e^{\frac{-\beta n}{V_T} \eta}\delta c_o(0)
\label{eq:small_signal_faradaic_current}
\end{equation}
Now, we shift our attention to computing $\delta c_o(x)$ in the above equation. The total oxidant concentration is the sum of dc and ac components, $c_o(x,t)+\delta c_o(x)e^{j\omega t}$. Eqn.~(\ref{eq:ficks_second_law_of_diff}) becomes
\begin{align}
\frac{\partial\left[c_o(x,t)+\delta c_o(x)e^{j\omega t}\right]}{\partial t} &=D_o\frac{d^2\left[c_o(x,t)+\delta c_o(x)e^{j\omega t}\right]}{dx^2}\\
\intertext{By using eqn.~(\ref{eq:ficks_second_law_of_diff}), only the ac component of the total concentration remains,}
\frac{\partial \left[\delta c_o(x)e^{j\omega t}\right]}{\partial t}&=D_o \frac{d^2\left[\delta c_o(x)e^{j\omega t}\right]}{dx^2}\\
\intertext{Taking time derivative on the L.H.S. we get,}
j\omega \delta c_o(x) &=D_o \frac{d^2\left[\delta c_o(x)\right]}{dx^2}
\label{eq:small_signal__one_specie_ficks_second_law}\\
\intertext{The solution for the above homogeneous differential equation (\ref{eq:small_signal__one_specie_ficks_second_law}) is}
\delta c_o(x)&=C_1e^{-\sqrt{\frac{j\omega}{D_o}}x}+C_2e^{\sqrt{\frac{j\omega}{D_o}}x}
\label{eq:solution_of_one_specie_small_variation_ficks_second_law}\\
\intertext{where, $C_1$ and $C_2$ are the integration constants.
These constants can be obtained by using small-signal initial and boundary conditions. 
The small-signal equation can be found by linearizing eqns.~(\ref{bc:smple1e_2}) and (\ref{bc:ficks_first_law_at_surface}), which leads to:}
D_o\left.\frac{d\delta c_o(x)}{d x}\right|_{x=0}&=\frac{\delta i_F}{nFA}
\label{eq:boundary_condition_one_specie_xto0_ficks_first_law}\\
\left.\delta c_o(x)\right|_{x=\infty}&=0\label{eq:boundary_condition_one_specie_xtoinf}\\
\intertext{Applying the boundary condition~(\ref{eq:boundary_condition_one_specie_xtoinf}) to the  eqn~(\ref{eq:solution_of_one_specie_small_variation_ficks_second_law}), we get,}
C_2&=0 \\
\therefore\quad\delta c_o(x)&=C_1e^{-\sqrt{\frac{j\omega}{D_o}x}}\label{eq:solution_of_one_specie_small_variation_ficks_second_law_after_x_to_inf_condition}\\
\intertext{Now, we differentiate the eqn.~(\ref{eq:solution_of_one_specie_small_variation_ficks_second_law_after_x_to_inf_condition}) w.r.t $x$ and subject it to the boundary condition (\ref{eq:boundary_condition_one_specie_xto0_ficks_first_law}), we obtain,}
\frac{\partial \delta c_o(x)}{\partial x}&=-\sqrt{\frac{j\omega}{D_o}}C_1e^{-\sqrt{\frac{j\omega}{D_o}}x}\nonumber\\
\therefore\quad\frac{\partial \delta c_o(0)}{\partial x}&=-\sqrt{\frac{j\omega}{D_o}}C_1\nonumber\\
-D_o\sqrt{\frac{j\omega}{D_o}}C_1&=\frac{\delta i_F}{nFA}\nonumber\\
\therefore\quad C_1&=-\frac{\delta i_F}{nFA\sqrt{j\omega D_o}}\label{eq:boundary_condition_one_specie_x_to_0}
\intertext{By introducing eqn.~(\ref{eq:boundary_condition_one_specie_x_to_0}) into eqn.~(\ref{eq:solution_of_one_specie_small_variation_ficks_second_law_after_x_to_inf_condition}).
we get,}
\delta c_o(x)&=-\frac{\delta i_F}{nFA\sqrt{j\omega D_o}}e^{-\sqrt{\frac{j\omega}{D_o}}x}\\
\intertext{Now, we put this relation of $\delta c_o(0)$ into the small-signal Faradaic current given by eqn.(~\ref{eq:small_signal_faradaic_current}),}
\delta i_F
=-\frac{i_0n}{V_T}\left[\frac{c_o(0,t)\beta}{c_o^*}e^{\frac{-\beta n}{V_T} \eta}\right.&\left.+(1-\beta)e^{\frac{(1-\beta)n}{V_T} \eta}\right]\delta\eta - \frac{i_0}{c_o^*}\frac{\delta i_F}{nFA\sqrt{j\omega D_o}}e^{\frac{-\beta n}{V_T} \eta}\nonumber\\
\delta i_F\left(1+\frac{i_0}{nFAc_o^*\sqrt{j\omega D_o}}e^{\frac{-\beta n}{V_T} \eta}\right)
&=-\frac{i_0n}{V_T}\left[\frac{c_o(0,t)\beta}{c_o^*}e^{\frac{-\beta n}{V_T} \eta}+ (1-\beta)e^{\frac{(1-\beta)n}{V_T}\eta}\right]\delta\eta\nonumber\\
-\frac{\delta\eta}{\delta i_F}&= \frac{V_T}{i_0n}\frac{\left(1+\frac{i_0}{nFAc_o^*\sqrt{j\omega D_o}}e^{\frac{-\beta n}{V_T}\eta}\right)}{\beta n\frac{c_o(0,t)}{c_o^*}e^{\frac{-\beta n}{V_T}\eta}+(1-\beta)e^{\frac{(1-\beta)n}{V_T}\eta}}
\end{align}
The Faradaic impedance becomes,
\begin{equation}
Z_F(c_o(0,t),\eta)=-\frac{\delta\eta}{\delta i_F}=\frac{V_T}{ni_0}\left[\frac{\left(1+\frac{i_0}{nFAc_o^*\sqrt{j\omega D_o}}e^{\frac{-\beta n}{V_T}\eta}\right)}{\beta n\frac{c_o(0,t)}{c_o^*}e^{\frac{-\beta n}{V_T}\eta}+(1-\beta)e^{\frac{(1-\beta)n}{V_T}\eta}}\right]
\end{equation}
Under 0 d.c. current discharge $c_o(0,t)=c_o^*$ and $\eta=0$. Therefore the Faradaic impedance becomes,
\begin{align}
Z_F&=\frac{V_T}{ni_0}\left[1+\frac{i_0}{nFA}\left(\frac{1}{\sqrt{j\omega D_o}c_o^*}\right)\right]\nonumber\\
\intertext{The final Faradaic impedance is,}
&\boxed{Z_F=R_\eta+\frac{V_T}{n^2FAc_o^*\sqrt{D_o}} \frac{1}{\sqrt{j\omega}}}\label{eq:ZF_SI_Warburg}
\end{align}
where, $R_\eta=\frac{V_T}{ni_0}$ is the polarization resistance. The second term in the above equation is commonly referred to as the infinite Warburg impedance.
$\left(Z(j\omega)\propto (j\omega)^{-\frac{1}{2}}\right)$. The Faradiac impedance in parallel with the double layer capacitance gives the total impedance of an electrochemical system.
The total impedance is obtained by substituting the eqn.~(\ref{eq:ZF_SI_Warburg}) into eqn.~(\ref{eq:Total_impedance}).
The simulation results for varying diffusion coefficient (fig.~\ref{plots:1S_SI_Do_comp} (top)), oxidant concentration (fig.~\ref{plots:1S_SI_co_comp}) and the double layer capacitance (fig.~\ref{plots:1S_SI_Do_comp} (bottom)) shows how these parameters can influence the impedance spectra of a system of single diffusion specie with a semi-infinite boundary condition.   
\begin{figure}[h!]
\begin{center}
\includegraphics[width=0.49\textwidth]{\main figures/one_specie_SM_Do_3} 
\includegraphics[width=0.49\textwidth]{\main figures/one_specie_SM_Do_4} 
\includegraphics[width=0.49\textwidth]{\main figures/one_specie_SM_Cd_3_1} 
\includegraphics[width=0.49\textwidth]{\main figures/one_specie_SM_Cd_2} 
\caption{The simulation results for a single specie semi-infinite linear diffusion, showing the effect of the oxidant diffusion coefficient (Top) for two values of the oxidant concentration: $c_o= 3.26\times10^{-8}\,\textrm{mol}/\textrm{cm}^2$ (left) and $c_o= 3.26\times10^{-5}\,\textrm{mol}/\textrm{cm}^2$ (right) and the double layer capacitance (bottom) for two values of the diffusion oxidant concentration: $\textrm{D}_\textrm{o}=7\times10^{-8}\,\textrm{cm}^2/\textrm{s}$ (left) to $\textrm{D}_\textrm{o}=7\times10^{-4}\,\textrm{cm}^2/\textrm{s}$ (right). The constant simulation parameters are $\textrm{C}_\textrm{d}=4\mu\textrm{F}/\textrm{cm}^2$ (Top) and $c_o= 3.26\times10^{-4}\,\textrm{mol}/\textrm{cm}^2$ (Bottom).}
\label{plots:1S_SI_Do_comp}
\end{center}
\end{figure}
%\begin{figure}[t!]
%\begin{center}
%\includegraphics[width=0.49\textwidth]{figures/one_specie_SM_Do_3} 
%\includegraphics[width=0.49\textwidth]{figures/one_specie_SM_Do_4} 
%\caption{The single specie semi-infinite linear diffusion model predicts a large change in the impedance curves of increasing diffusion coefficients at small oxidant concentration  $c_o= 3.26\times10^{-8}\,\textrm{mol}/\textrm{cm}^2$ (left) and predicts no change at higher oxidant concentration $c_o= 3.26\times10^{-5}\,\textrm{mol}/\textrm{cm}^2$ (right) in the electrolyte while the double layer capacitance kept constant at $\textrm{C}_\textrm{d}=4\mu\textrm{F}/\textrm{cm}^2$.}
%\label{plots:1S_SI_Do_comp}
%\end{center}
%\end{figure}
\begin{figure}[t!]
\begin{center}
\includegraphics[width=0.49\textwidth]{\main figures/one_specie_SM_Co_4_1} 
\includegraphics[width=0.49\textwidth]{\main figures/one_specie_SM_Co_5_2} 
\caption{The single specie semi-infinite linear diffusion model predicts a small change in the impedance curves of increasing oxidant concentrations when $\textrm{D}_\textrm{o}=7\times10^{-5}\,\textrm{cm}^2/\textrm{s}$ and $\textrm{C}_\textrm{d}=4\mu\textrm{F}/\textrm{cm}^2$ (left), but predicts a different behavior at low $\textrm{c}_\textrm{o}$ (right) when $\textrm{D}_\textrm{o}=7\times10^{-6}\,\textrm{cm}^2/\textrm{s}$ and $\textrm{C}_\textrm{d}=400\mu\textrm{F}/\textrm{cm}^2$.}
\label{plots:1S_SI_co_comp}
\end{center}
\end{figure}
\end{document}