\documentclass[../../../thesis.tex]{subfiles}
\providecommand{\main}{../../../}
\begin{document}
\subsection{Open boundary finite-length Warburg impedance}
\label{sec:one_specie_transmissive_boundary}
Open boundary (also known as transmissive boundary) means the concentration of the diffusing ions is kept constant (see figure~\ref{fig:one_specie_transmissive_diffusion}, which shows the concentration profile of diffusing ions for a finite-length open boundary system at different discharge currents).
\begin{figure}[b!]
\begin{center}
\includegraphics[width=0.5\textwidth]{\main figures/one_specie_transmissive_concentration_profile}
\end{center}
\caption{The concentration profiles of the oxidant in a finite-length electrochemical system with transmissive boundary. The spatial variation of the concentration at different discharge currents is shown: 0 d.c. discharge (dotted) and finite d.c. discharge (continuous).}
\label{fig:one_specie_transmissive_diffusion}
\end{figure} 
The system consists of a working electrode (left), the absorbing boundary (right) and the electrolyte in-between. The half cell reaction being considered is,
\begin{equation}
\ce{O + ne <=>[k_f][k_b] R}\nonumber
\end{equation}
Here, we assume that the diffusion is the only mass transport process by which the ions travel to the surface of the electrode and undergoes the electrochemical reaction. This diffusion for a single specie can be described by Fick's second law,
\begin{align}
\frac{\partial c_{o}(x,t)}{\partial t}=D_o \frac{\partial^2 c_o(x,t)}{\partial^2 x}\label{eq:one_specie_transmissive_ficks_2nd_law}
\end{align}
\paragraph{Initial and boundary conditions} The initial and boundary conditions required to solve the eqn.~(\ref{eq:one_specie_transmissive_ficks_2nd_law}) are,
\begin{subequations}
\begin{enumerate}
\item[(a)] \textbf{Initial condition}
\begin{align}
c_o(x,0)&=c_o^*\nonumber
\end{align}
\item[(b)] \textbf{Boundary conditions at x=\textit{l} (transmissive boundary)}
\begin{align}
\delta c_o(l)&=0\label{bc:one_specie_transmissive_change_0}
%\\\frac{d\,\delta c_o(l)}{dx}&\neq0\label{bc:one_specie_transmissive_change_concentration}
\end{align}
where, $l$ is distance between the electrode and the boundary.
\item[(c)] \textbf{Boundary condition at Electrode/Electrolyte interface}
\begin{align}
D_o\left.\frac{\partial c_{o}(x,t)}{\partial x}\right|_{x=0}=\frac{i_F(t)}{nFA}\label{bc:one_specie_transmissive_ficks_1st_law}
\end{align}
\end{enumerate}
\end{subequations}
The mass transport equations are used to describe the movement of the ions towards and away from the electrode surface but electrode kinetics are used to describe the electrochemical reaction. The electrode kinetics can be described using Butler-Volmer equation. 
\begin{equation}
i_F=i_0\cdot{\left[\frac{c_o(0,t)}{c_o^*}e^{\frac{-\beta n}{V_T}\eta}-e^{\frac{(1-\beta) n}{V_T}\eta}\right]}\label{eq:butler_volmer_eq_one_specie_transmissive}
\end{equation}
\paragraph{Calculation of Impedance Spectra} The small-signal theory is used to compute the impedance spectra. The small-signal potential, current and concentration are given by, $\delta\eta\,e^{j\omega t}$, $\delta i_F\,e^{j\omega t}$ and $\delta c_o\,e^{j\omega t}$ respectively. The Faradaic impedance is given by, $Z_F=-\delta\eta/\delta i_F$. 
The small-signal Faradaic current can be expressed using the Taylor series expansion. Keeping only the linear terms in the Taylor expansion of~(\ref{eq:butler_volmer_eq_one_specie_transmissive}) and neglecting the high order terms, we obtain,
\begin{equation}
\delta i_F=\left(\frac{\partial i_F}{\partial\eta}\right)\delta\eta+
\left(\frac{\partial i_F}{\partial c_o}\right)\delta c_o(0)\label{eq:one_specie_transmissive_small_signal}
\end{equation}
The partial derivatives in the above equation can be obtained by taking derivative of the faradaic current (\ref{eq:butler_volmer_eq_one_specie_transmissive}) w.r.t over-potential and the oxidant concentration. The partial derivatives are,
\begin{subequations}
\begin{align}
\left(\frac{\partial i_F}{\partial \eta}\right)=&-i_0\left[\frac{n\beta }{V_T}\frac{c_o(0,t)}{c_o^*}e^{\frac{-\beta n}{V_T} \eta}+\frac{(1-\beta)n}{V_T}e^{\frac{(1-\beta)n}{V_T} \eta}\right]\label{eq:one_specie_transmissive_taylor_series_derivative_overpotential}\\
\left(\frac{\partial i_F}{\partial c_o}\right)=&\frac{i_0}{c_o^*}e^{\frac{-\beta n}{V_T} \eta} \label{eq:one_specie_transmissive_taylor_series_derivative_oxidant_concentration}
\end{align}
\end{subequations}
Now, we substitute the eqns.~(\ref{eq:one_specie_transmissive_taylor_series_derivative_overpotential}) and~(\ref{eq:one_specie_transmissive_taylor_series_derivative_oxidant_concentration}) into eqn.~(\ref{eq:one_specie_transmissive_small_signal}),
\begin{equation}
\delta i_F=-i_0\left[\frac{n\beta }{V_T}\frac{c_o(0,t)}{c_o^*}e^{\frac{-\beta n}{V_T} \eta}+\frac{(1-\beta)n}{V_T}e^{\frac{(1-\beta)n}{V_T} \eta}\right]\delta\eta + \frac{i_0}{c_o^*}e^{\frac{-\beta n}{V_T} \eta}\delta c_o(0)\label{eq:small_signal_substituted_one_specie_transmissive}
\end{equation}
This equation is similar to the one we obtained for one specie semi-infinite case (\ref{eq:small_signal_faradaic_current}). 
Now, we linearize eqn.~(\ref{eq:one_specie_transmissive_ficks_2nd_law}),
\begin{equation}
j\omega \delta c_o(x)=D_o \frac{d^2\left[\delta c_o(x)\right]}{dx^2}\label{eq:one_specie_transmissive_linearized_2nd_law}
\end{equation}
The solution of the above equation is,
\begin{equation}
\delta c_o(x)=C_1e^{-\sqrt{\frac{j\omega}{D_o}}x}+C_2e^{\sqrt{\frac{j\omega}{D_o}}x}\label{eq:one_specie_transmissive_linealized_ficks_2nd_law}
\end{equation}
The integration constants $C_1$ and $C_2$ can be obtained using the linearized boundary conditions of eqns.~(\ref{bc:one_specie_transmissive_change_0}) and~(\ref{bc:one_specie_transmissive_ficks_1st_law}), which are,
\begin{subequations}
\begin{align}
\delta c_o(l)&=0\label{bc:linearized_boundary_condition_one_specie_transmissive_boundary}
\end{align}
\begin{align}
D_o\left.\frac{d\delta c_o(x)}{d x}\right|_{x=0}&=\frac{\delta i_F}{nFA}
\label{bc:linearized_boundary_condition_one_specie_transmissive_ficks_1st_law}
\end{align}
\end{subequations}
Applying boundary condition~(\ref{bc:linearized_boundary_condition_one_specie_transmissive_boundary}) to eqn.~(\ref{eq:one_specie_transmissive_linealized_ficks_2nd_law}), we get
\begin{align}
-C_1=C_2e^{2\sqrt{\frac{j\omega}{D_o}}l}\label{eq:constant_relation_one_specie_transmissive_subject_boundary}
\end{align}
Similarly, if we apply boundary condition~(\ref{bc:linearized_boundary_condition_one_specie_transmissive_ficks_1st_law}) to~(\ref{eq:one_specie_transmissive_linealized_ficks_2nd_law}), we obtain,
\begin{align}
C_2-C_1=\frac{\delta i_F}{nFA\sqrt{j\omega D_o}}\label{eq:constant_relation_one_specie_transmissive_subject_1st_diffusion}
\end{align}
Solving eqns.~(\ref{eq:constant_relation_one_specie_transmissive_subject_boundary}) and~(\ref{eq:constant_relation_one_specie_transmissive_subject_1st_diffusion}), we get,
\begin{subequations}
\begin{align}
C_1=&-\frac{\delta i_F}{nFA\sqrt{j\omega D_o}}\frac{e^{2\sqrt{\frac{j\omega}{D_o}}l}}{1+e^{2\sqrt{\frac{j\omega}{D_o}}l}}\label{eq:one_specie_transmission_c1}\\
C_2=&\frac{\delta i_F}{nFA\sqrt{j\omega D_o}}\frac{1}{1+e^{2\sqrt{\frac{j\omega}{D_o}}l}}\label{eq:one_specie_transmission_c2}
\end{align}
\end{subequations}
Next, we substitute integration constants $C_1$ (\ref{eq:one_specie_transmission_c1}) and $C_2$ (\ref{eq:one_specie_transmission_c2}) into eqn.~(\ref{eq:one_specie_transmissive_linealized_ficks_2nd_law}),
\begin{align}
\delta c_o(x)=-\frac{\delta i_F}{nFA\sqrt{j\omega D_o}}\frac{1}{1+e^{2\sqrt{\frac{j\omega}{D_o}}l}} \left[e^{\sqrt{\frac{j\omega}{D_o}}(2l-x)}-e^{\sqrt{\frac{j\omega}{D_o}}x}\right]
\end{align}
The concentration at the cathode surface ($x=0$) is given by,
\begin{align}
%\delta c_o(0)&=-\frac{\delta i_F}{nFA\sqrt{j\omega D_o}}\frac{1}{1+e^{2\sqrt{\frac{j\omega}{D_o}}l}} \left[e^{2\sqrt{\frac{j\omega}{D_o}}l}-1\right]\nonumber\\
%&=-\frac{\delta i_F}{nFA\sqrt{j\omega D_o}}\frac{e^{\sqrt{\frac{j\omega}{D_o}}l}-e^{-\sqrt{\frac{j\omega}{D_o}}l}}{e^{-\sqrt{\frac{j\omega}{D_o}}l}+e^{\sqrt{\frac{j\omega}{D_o}}l}}\nonumber\\
\delta c_o(0)&=-\frac{\delta i_F}{nFA\sqrt{j\omega D_o}}\tanh\left({\sqrt{\frac{j\omega}{D_o}}l}\right)\label{eq:one_specie_transmission_delta_co}
\end{align}
Now, we replace $\delta c_o(0)$ into eqn.~(\ref{eq:small_signal_substituted_one_specie_transmissive}),
\begin{align}
\delta i_F=&-i_0\left[\frac{n\beta }{V_T}\frac{c_o(0,t)}{c_o^*}e^{\frac{-\beta n}{V_T} \eta}+\frac{(1-\beta)n}{V_T}e^{\frac{(1-\beta)n}{V_T} \eta}\right]\delta\eta- \frac{i_0\delta i_F}{nFAc_o^*\sqrt{j\omega D_o}}\tanh\left({\sqrt{\frac{j\omega}{D_o}}l}\right)e^{\frac{-\beta n}{V_T} \eta}\label{eq:small_signal_faradaic_current_one_specie_transmissive}
\end{align}
The faradaic impedance ($Z_F$) is given by,
\begin{align}
Z_F=-\frac{\delta\eta}{\delta i_F}\nonumber
\end{align}
Rearranging eqn.~(\ref{eq:small_signal_faradaic_current_one_specie_transmissive}), we get,
\begin{align}
Z_F(c_o(0,t),\eta)=\frac{1+\frac{i_0\tanh\left({\sqrt{\frac{j\omega}{D_o}}l}\right)}{nFAc_o^*\sqrt{j\omega D_o}}e^{\frac{-\beta n}{V_T} \eta}}
{\frac{i_0n}{V_T} \left[\frac{\beta c_o(0,t)}{c_o^*}e^{-\frac{\beta n}{V_T}\eta} + \left(1-\beta\right)e^{\frac{\left(1-\beta\right)n}{V_T}\eta}\right]}
\end{align}
Under 0 d.c. discharge; $\eta=0$, $c_o(0,t)=c_o^*$ and $c_r(0,t)=c_r^*$, and the Faradaic impedance is given by,
\begin{equation}
\boxed{Z_F=R_\eta+\frac{V_T}{n^2FAc_o^*\sqrt{D_o}}\frac{\tanh{\left(\sqrt{\frac{j\omega}{D_o}}l\right)}}{\sqrt{j\omega}}} \label{eq:Transmissive_Zf_one_specie}
\end{equation}
The total impedance (\ref{eq:Total_impedance}) is a parallel combination of Faradaic impedance (\ref{eq:Transmissive_Zf_one_specie}) and the double layer impedance.
\begin{figure}[b!]
\begin{center}
\includegraphics[width=0.49\textwidth]{\main figures/one_specie_transmissive_l_new_4}
\includegraphics[width=0.49\textwidth]{\main figures/one_specie_transmissive_l_new_5} 
\caption{Nyquist diagram for a finite-length diffusion system with transmissive boundary of one electrochemical specie. The figures show the effect of the system length on the impedance plot for two values of diffusion coefficient:  $D_o=7\times10^{-4}\,\textrm{cm}^2/\textrm{s}$ (left) and $D_o=7\times10^{-8}\,\textrm{cm}^2/\textrm{s}$ (right). The other simulation parameters are  $C_d= 4\,\mu\textrm{F}/\textrm{cm}^2$ and $c_o= 3.26\times10^{-6}\,\textrm{mol}/\textrm{cm}^2$.}
\label{plots:1S_TR_l_comp}
\end{center}
\end{figure}
\begin{figure}
\begin{center}
\includegraphics[width=0.48\textwidth]{\main figures/one_specie_transmissive_Cd_1} 
\includegraphics[width=0.48\textwidth]{\main figures/one_specie_transmissive_Cd_2} 
\includegraphics[width=0.48\textwidth]{\main figures/one_specie_transmissive_Cd_3} 
\includegraphics[width=0.48\textwidth]{\main figures/one_specie_transmissive_Cd_4}  
\caption{Nyquist diagram for a finite-length diffusion system with transmissive boundary of one electrochemical specie. The top two figures show the effect of the double layer capacitance on the impedance plot for two values of the oxidant concentration: $c_o= 3.26\times10^{-7}\,\textrm{mol}/\textrm{cm}^2$ (top left) and $c_o= 3.26\times10^{-5}\,\textrm{mol}/\textrm{cm}^2$ (top right). The other simulation parameters are $D_o=7\times10^{-7}\,\textrm{cm}^2/\textrm{s}$ and $l=500\,\textrm{nm}$. The bottom two figures show the effect of the double layer capacitance on the impedance plot for two values of the oxidant concentration: $c_o= 3.26\times10^{-7}\,\textrm{mol}/\textrm{cm}^2$ (bottom left) and $c_o= 3.26\times10^{-5}\,\textrm{mol}/\textrm{cm}^2$ (bottom right). The other simulation parameters are $D_o=7\times10^{-8}\,\textrm{cm}^2/\textrm{s}$ and $l=100\,\mu\textrm{m}$.}
\label{plots:1S_TR_Cd_comp}
\end{center}
\end{figure}
\begin{figure}
\begin{center}
\includegraphics[width=0.49\textwidth]{\main figures/one_specie_transmissive_Do_1} 
\includegraphics[width=0.49\textwidth]{\main figures/one_specie_transmissive_Do_2}
\includegraphics[width=0.49\textwidth]{\main figures/one_specie_transmissive_Co_3}
\includegraphics[width=0.49\textwidth]{\main figures/one_specie_transmissive_Co_4}
\caption{Nyquist diagram for a finite-length diffusion system with transmissive boundary of one electrochemical specie. The top two figures show the effect of the diffusion coefficient on the impedance plot for two values of double layer capacitance: $C_d= 4 \,\textrm{mF}/\textrm{cm}^2$ (top left) and $C_d= 400\, \textrm{nF}/\textrm{cm}^2$ (top right). The other simulation parameters are $c_o= 3.26\times10^{-6}\,\textrm{mol}/\textrm{cm}^2$ and $l=10\,\mu\textrm{m}$. The bottom two figures show the effect of the oxidant concentration on the impedance plot for two values of system length: $l=100\,\textrm{nm}$ (left) and $l=10\,\mu\textrm{m}$ (right). The other simulation parameters are $D_o=3.5\times10^{-6}\,\textrm{cm}^2/\textrm{s}$ and $C_d= 400\,\textrm{nF}/\textrm{cm}^2$.}
\label{plots:1S_TR_Do_comp}
\end{center}
\end{figure}
\end{document}