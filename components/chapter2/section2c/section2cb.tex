\documentclass[../../../thesis.tex]{subfiles}
\providecommand{\main}{../../../}
\begin{document}
\subsection{Blocked boundary finite-length Warburg impedance}
\label{sec:one_specie_reflective_boundary}
The blocked boundary (also called as reflective boundary) condition physically means the ions are cannot pass through the boundary of the wall but are reflected away. Figure~\ref{fig:one_specie_reflective_diffusion} shows the concentration profile of the diffusing ions at different d.c. discharge currents. It also important to highlight that the ion concentration near the reflective boundary is much higher than a transmissive boundary (dash-dot lines). This increase in concentration is due to the ions reflecting from the boundary surface. The general half cell reaction for single reacting specie is,
\begin{equation}
\ce{O + ne <=>[k_f][k_b] R}\nonumber
\end{equation}
Diffusion of ions from the bulk region to the surface of an electrode can be described using Fick's second law of diffusion,
\begin{align}
\frac{\partial c_{o}(x,t)}{\partial t}=D_o \frac{\partial^2 c_o(x,t)}{\partial^2 x}\label{eq:ficks_second_law_of_diff_one_specie_bounded_reflective}
\end{align}
\paragraph{Initial and boundary conditions} The initial and boundary conditions required to solve the eqn.~(\ref{eq:ficks_second_law_of_diff_one_specie_bounded_reflective}) are,
\begin{subequations}
\begin{enumerate}
\item[] \textbf{Initial condition}
\begin{align}
c_o(x,0)&=c_o^*\nonumber
\end{align}
\item[(a)] \textbf{Boundary condition at $x=l$ (reflective boundary)}
\begin{align}
\frac{d\,c_o(l)}{dx}=0\label{bc:one_specie_reflective_change_concentration}
\end{align}
where, $l$ is the system length (as shown in fig.~\ref{fig:one_specie_reflective_diffusion}).
\item[(b)] \textbf{Boundary condition at the Electrode/Electrolyte interface}
\begin{align}
D_o\left.\frac{\partial c_{o}(x,t)}{\partial x}\right|_{x=0}=\frac{i_F(t)}{nFA}\label{bc:one_specie_reflective_ficks_1st_law}
\end{align}
\end{enumerate}
\end{subequations}
%The Nernst-Plank equation describes the ion movement from the bulk region to the surface of the electrode in an electrochemical system. 
The following Butler-Volmer (BV) equation is used to describe the reaction at the electrode/electrolyte interface.
\begin{equation}
i_F=i_0\cdot{\left[\frac{c_o(0,t)}{c_o^*}e^{\frac{-\beta n}{V_T}\eta}-e^{\frac{(1-\beta) n}{V_T}\eta}\right]}\label{eq:butler_volmer_eq_one_specie_reflective}
\end{equation}
\begin{figure}
\begin{center}
\includegraphics[width=0.5\textwidth]{\main figures/one_specie_reflective_w_semiinfinite_concentration_profile}
\end{center}
\caption{The concentration profiles of the cations in a finite-length system with reflective boundary at 0 (broken continuous), small and high (continuous) d.c. discharge currents. The dotted line shows the concentration of cations for a finite-length system with transmissive or semi-infinite system. This highlights the concentration on the boundary surface is higher in presence of reflective boundary.}
\label{fig:one_specie_reflective_diffusion}
\end{figure}
\paragraph{Calculation of Impedance Spectra} Expanding the small-signal Faradaic current in Taylor series and keeping only the linear terms of the expansion, we can write
\begin{equation}
\delta i_F=\left(\frac{\partial i_F}{\partial\eta}\right)\delta\eta+
\left(\frac{\partial i_F}{\partial c_o}\right)\delta c_o(0)\label{eq:linealized_ficks_semi_infinite_reflective}
\end{equation}
The partial derivatives in the above relation are (same as eqn.~((\ref{eq:one_specie_taylor_series_derivative_overpotential}) and eqn.~(\ref{eq:one_specie_taylor_series_derivative_oxidant_concentration})),
\begin{subequations}
\begin{align}
\left(\frac{\partial i_F}{\partial \eta}\right)=&-i_0\left[\frac{n\beta }{V_T}\frac{c_o(0,t)}{c_o^*}e^{\frac{-\beta n}{V_T} \eta}+\frac{(1-\beta)n}{V_T}e^{\frac{(1-\beta)n}{V_T} \eta}\right]\label{eq:one_specie_reflective_partial_derivarive_wrt_eta}\\
\left(\frac{\partial i_F}{\partial c_o}\right)=&\frac{i_0}{c_o^*}e^{\frac{-\beta n}{V_T} \eta}\label{eq:one_specie_reflective_partial_derivarive_wrt_co}
\end{align}
\end{subequations}
Now, we replace the partial derivatives in eqn.~(\ref{eq:linealized_ficks_semi_infinite_reflective}) by partial derivatives~(\ref{eq:one_specie_reflective_partial_derivarive_wrt_eta}) and~(\ref{eq:one_specie_reflective_partial_derivarive_wrt_co}) to get,
\begin{equation}
\delta i_F=-i_0\left[\frac{n\beta }{V_T}\frac{c_o(0,t)}{c_o^*}e^{\frac{-\beta n}{V_T} \eta}+\frac{(1-\beta)n}{V_T}e^{\frac{(1-\beta)n}{V_T} \eta}\right]\delta\eta + \frac{i_0}{c_o^*}e^{\frac{-\beta n}{V_T} \eta}\delta c_o(0)\label{eq:small_signal_substituted_one_specie_reflective}
\end{equation}
$\delta c_o(0)$ can be computed by linearizing the  eqn.~(\ref{eq:ficks_second_law_of_diff_one_specie_bounded_reflective}), 
\begin{equation}
j\omega \delta c_o(x)=D_o \frac{d^2\left[\delta c_o(x)\right]}{dx^2}\nonumber
\end{equation}
and solving the resulting second order differential equation, we get,
\begin{equation}
\delta c_o(x)=C_1e^{-\sqrt{\frac{j\omega}{D_o}}x}+C_2e^{\sqrt{\frac{j\omega}{D_o}}x}\label{eq:solution_ficks_second_law_one_specie_reflective}
\end{equation}
The integration constants ($C_1$ and $C_2$) can be found by linearizing boundary conditions (\ref{bc:one_specie_reflective_change_concentration}) and~(\ref{bc:one_specie_reflective_ficks_1st_law}),
\begin{subequations}
\begin{align}
\frac{d\delta c_o(l)}{dx}&=0\label{bc:linearized_boundary_condition_one_specie_reflective_boundary}\\
D_o\left.\frac{d\delta c_o(x)}{d x}\right|_{x=0}&=\frac{\delta i_F}{nFA}\label{bc:linearized_boundary_condition_one_specie_reflective_ficks_1st_law}
\end{align}
\end{subequations}
We obtain,
\begin{align}
C_1&=C_2e^{2\sqrt{\frac{j\omega}{D_o}}l}\label{eq:constant_relation_one_specie_reflective_subject_boundary}\\
C_2-C_1&=\frac{\delta i_F}{nFA\sqrt{j\omega D_o}}\label{eq:constant_relation_one_specie_reflective_subject_1st_diffusion}
\end{align}
Solving eqns.~\ref{eq:constant_relation_one_specie_reflective_subject_boundary} and~\ref{eq:constant_relation_one_specie_reflective_subject_1st_diffusion}, we get,
\begin{subequations}
\begin{align}
C_1&=\frac{\delta i_F}{nFA\sqrt{j\omega D_0}}\frac{e^{\sqrt{\frac{j\omega}{D_o}}l}}{e^{-\sqrt{\frac{j\omega}{D_o}}l}-e^{\sqrt{\frac{j\omega}{D_o}}l}}\label{eq:one_specie_reflective_c1}\\
C_2&=\frac{\delta i_F}{nFA\sqrt{j\omega D_0}}\frac{e^{-\sqrt{\frac{j\omega}{D_o}}l}}{e^{-\sqrt{\frac{j\omega}{D_o}}l}-e^{\sqrt{\frac{j\omega}{D_o}}l}}\label{eq:one_specie_reflective_c2}
\end{align}
\end{subequations}
Now, we substitute eqns.~(\ref{eq:one_specie_reflective_c1}) and~(\ref{eq:one_specie_reflective_c2}) into eqn.~(\ref{eq:solution_ficks_second_law_one_specie_reflective}) and get,
\begin{align}
%\delta c_o(x)&=\frac{\delta i_F}{nFA\sqrt{j\omega D_0}}\left[\frac{e^{\sqrt{\frac{j\omega}{D_o}}(l-x)}+e^{-\sqrt{\frac{j\omega}{D_o}}(l-x)}}{e^{-\sqrt{\frac{j\omega}{D_o}}l}-e^{\sqrt{\frac{j\omega}{D_o}}l}}\right]\nonumber\\\therefore\qquad
\delta c_o(x)&=\frac{-\delta i_F}{nFA\sqrt{j\omega D_0}}\frac{\cosh{\left(\sqrt{\frac{j\omega}{D_o}}(l-x)\right)}}{\sinh{\left(\sqrt{\frac{j\omega}{D_o}}l\right)}}\label{eq:one_specie_reflective_delta_co(x)}
\end{align}
Equation~(\ref{eq:one_specie_reflective_delta_co(x)}) gives the oxidant concentration as a function of position ($x$). To obtain the electrode surface concentration ($\delta c_o(0)$).
We substitute $x=0$ into eqn.~(\ref{eq:one_specie_reflective_delta_co(x)}) and obtain,
\begin{align}
\delta c_o(0)&=\frac{-\delta i_F}{nFA\sqrt{j\omega D_0}}\coth{\left(\sqrt{\frac{j\omega}{D_o}}l\right)}\label{eq:one_specie_reflective_delta_co}
\end{align}
Now, we replace $\delta c_o(0)$ into eqn.~(\ref{eq:small_signal_substituted_one_specie_reflective}) and get,
\begin{align}
\delta i_F=&-i_0\left[\frac{n\beta }{V_T}\frac{c_o(0,t)}{c_o^*}e^{\frac{-\beta n}{V_T} \eta}+\frac{(1-\beta)n}{V_T}e^{\frac{(1-\beta)n}{V_T} \eta}\right]\delta\eta\nonumber\\
&-\frac{i_0\delta i_F}{nFAc_o^*\sqrt{j\omega D_0}}\coth{\left(\sqrt{\frac{j\omega}{D_o}}l\right)}e^{\frac{-\beta n}{V_T} \eta} \label{eq:small_signal_faradaic_current_one_specie_reflective}
\end{align}
The Faradaic impedance ($Z_F$) is given by,
\begin{align}
Z_F&=-\frac{\delta\eta}{\delta i_F}\nonumber\\
Z_F(c_o(0,t),\eta)&=\frac{1+\frac{i_0\coth\left({\sqrt{\frac{j\omega}{D_o}}l}\right)}{nFAc_o^*\sqrt{j\omega D_o}}e^{\frac{-\beta n}{V_T}\eta}}
{\frac{i_0n}{V_T}\left[\frac{\beta c_o(0,t)}{c_o^*}e^{-\frac{\beta n}{V_T}\eta} + \left(1-\beta\right)e^{\frac{\left(1-\beta\right)n}{V_T}\eta}\right]}
\end{align}
Under 0 d.c. discharge;~$\eta=0$ and~$c_o(0,t)=c_o^*$ and the Faradaic impedance becomes,
\begin{equation}
\boxed{Z_F=R_\eta+\frac{V_T}{n^2FAc_o^*\sqrt{D_o}}\frac{\coth{\left(\sqrt{\frac{j\omega}{D_o}}l\right)}}{\sqrt{j\omega}}} \label{eq:Reflective_Zf_one_specie}
\end{equation}
This is the finite-length diffusion impedance when a single specie electrochemical system has a reflective boundary. In the above equation
\begin{equation}
R_\eta=\frac{V_T}{i_0n}
\end{equation}
is called the polarization resistance.
The total impedance (\ref{eq:Total_impedance}) is a parallel combination of Faradaic~(\ref{eq:Reflective_Zf_one_specie}) and double layer impedance.
%Fig.~\ref{plots:1S_RF_Cd_comp} presents the effect of the capacitance of the double layer and the initial oxidant concentration on the Nyquist plots. When the initial oxidant conce 
%The simulation results for varying diffusion coefficient (fig.~\ref{plots:1S_RF_Do_comp} (top)), the double layer capacitance (fig.~\ref{plots:1S_RF_Cd_comp} (top)), the oxidant concentration (fig.~\ref{plots:1S_RF_Cd_comp} (bottom)) and system length (fig.~\ref{plots:1S_RF_Do_comp} (bottom)) shows the influence of these parameters on the impedance spectra of a system of one specie finite diffusion with a reflective boundary condition.
\begin{figure}[b!]
\begin{center}
\includegraphics[width=0.49\textwidth]{\main figures/one_specie_reflective_Cd_4} 
\includegraphics[width=0.49\textwidth]{\main figures/one_specie_reflective_Cd_3}
\caption{Nyquist diagram for a finite-length diffusion system with reflective boundary of one electrochemical specie. The figures show the effect of the double layer capacitance on the impedance plot for two values of the oxidant concentration:  $c_o= 3.26\times10^{-8}\,\textrm{mol}/\textrm{cm}^2$ (left) and $c_o= 3.26\times10^{-7}\,\textrm{mol}/\textrm{cm}^2$ (right). The other simulation parameters are $D_o=3.5\times10^{-5}\,\textrm{cm}^2/\textrm{s}$ and $l=100\,\mu\textrm{m}$.}
\label{plots:1S_RF_Cd_comp}
\end{center}
\end{figure}
\begin{figure}[h!]
\begin{center}
\includegraphics[width=0.49\textwidth]{\main figures/one_specie_reflective_Do_2}
\includegraphics[width=0.49\textwidth]{\main figures/one_specie_reflective_Do_6}
\includegraphics[width=0.49\textwidth]{\main figures/one_specie_reflective_l_6}
\includegraphics[width=0.49\textwidth]{\main figures/one_specie_reflective_l_5} 
\caption{Nyquist diagram for a finite-length diffusion system with reflective boundary of one electrochemical specie. The top two figures show the effect of the diffusion coefficient on the impedance plot for two values of the oxidant concentration: $c_o= 3.26\times10^{-8}\,\textrm{mol}/\textrm{cm}^2$ (top left) and $c_o= 3.26\times10^{-5}\,\textrm{mol}/\textrm{cm}^2$ (top right). The other simulation parameters are $C_d= 4\,\mu\textrm{F}/\textrm{cm}^2$ and $l=2\,\mu\textrm{m}$. The bottom two figures show the effect of the system length on the impedance plot for two values of the double layer capacitance:  $C_d= 400\,\textrm{nF}/\textrm{cm}^2$ (left) and $C_d= 400\,\mu\textrm{F}/\textrm{cm}^2$ (right). The other simulation parameters are $D_o=3.5\times10^{-5}\,\textrm{cm}^2/\textrm{s}$ and $l=2\,\mu\textrm{m}$.}
\label{plots:1S_RF_Do_comp}
\end{center}
\end{figure}
\begin{figure}[t!]
\begin{center}
\includegraphics[width=0.49\textwidth]{\main figures/one_specie_reflective_co_1}
\includegraphics[width=0.49\textwidth]{\main figures/one_specie_reflective_co_9}
\caption{Nyquist diagram for a finite-length diffusion system with reflective boundary of one electrochemical specie. The figures show the effect of the oxidant concentration on the impedance plot for two values of the double layer capacitance: $C_d= 400\,\textrm{nF}/\textrm{cm}^2$ (left) and $C_d= 4\,\textrm{mF}/\textrm{cm}^2$ (right). The other simulation parameters are  $D_o=7\times10^{-7}\,\textrm{cm}^2/\textrm{s}$ and $c_o= 3.26\times10^{-5}\,\textrm{mol}/\textrm{cm}^2$.}
\label{plots:1S_RF_Co_comp}
\end{center}
\end{figure}
%\begin{figure}[t!]
%\begin{center}
%\includegraphics[width=0.49\textwidth]{figures/one_specie_reflective_l_6}
%\includegraphics[width=0.49\textwidth]{figures/one_specie_reflective_l_5} 
%\caption{Nyquist diagram for a finite-length diffusion system with transmissive boundary of one electrochemical specie. The top two figures show the effect of the diffusion coefficient on the impedance plot for two values of double layer capacitance: $C_d= 4 \,\textrm{mF}/\textrm{cm}^2$ (top left) and $C_d= 400\, \textrm{nF}/\textrm{cm}^2$ (top right). The other simulation parameters are $c_o= 3.26\times10^{-6}\,\textrm{mol}/\textrm{cm}^2$ and $l=10\,\mu\textrm{m}$. The bottom two figures show the effect of the oxidant concentration on the impedance plot for two values of system length: $l=100\,\textrm{nm}$ (left) and $l=10\,\mu\textrm{m}$ (right). The other simulation parameters are $D_o=3.5\times10^{-6}\,\textrm{cm}^2/\textrm{s}$ and $C_d= 400\,\textrm{nF}/\textrm{cm}^2$.}
%\label{plots:1S_RF_l_comp}
%\end{center}
%\end{figure}
\end{document}