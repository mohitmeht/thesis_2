\documentclass[../../thesis.tex]{subfiles}
\providecommand{\main}{../../}
\begin{document}
\section{Gerischer Impedance}
\label{sec:Gerischer_impedance}
In the previous sections, we treated the cases of one specie infinite Warburg (sec.~\ref{sec:one_specie_semi_infinite}), two species infinite Warburg impedance (sec.~\ref{sec:two_species_semi_infinite}), one specie bounded diffusion (sec.~\ref{sec:one_specie_bounded_diffusion}) and two species bounded diffusion (sec.~\ref{sec:Two_species_bounded}). Heinz Gerischer was the first to study the DC and AC response for CE-type (Chemical reaction followed by electrochemical reaction) of reaction in 1951. Other studies on CE and CEC type of reactions were done by Sluyters-Rehbach and Sluyters in 1970 and 1984 respectively~\cite{Boukamp2003}. The irreversible reactions are incorporated into Fick's second law of diffusion by adding an additional reaction term ($-k\delta c$, where $k$ is the reaction constant and $\delta c$ is the excess concentration)~\cite{Bisquert2002}.
An electrochemical reaction followed by and a chemical reactions is represented chemically by,
\begin{align}
\ce{O + ne <=>[k_f][k_b] R}\nonumber\\
\ce{R ->[k] T}
\end{align}
Here, the product ($R$) formed at the end of the electrochemical reaction decomposes chemically into ($T$) and $k$ is the rate at which the $T$ is formed.
%Considering diffusion to be the only mass transport process by which the ion from the bulk reach the surface to consume an electron and undergo a electrochemical reaction.
Fick's second law for a single diffusing specie with an additional term for irreversible reaction is given by,
\begin{align}
\frac{\partial c_o}{\partial t}=& D_o\frac{\partial^2  c_o}{\partial x^2}-k c_o\label{eq:ficks_2nd_law_with_deposition}
\end{align}
\paragraph{Initial and boundary conditions} The initial and boundary conditions required to solve the partial differential equation are,
\begin{subequations}
\begin{enumerate}
\item[] \textbf{Initial condition}
\begin{align}
c_o(x,0)&=c_o^*\nonumber
\end{align}
\item[(a)] \textbf{Boundary condition at $x=\infty$}
\begin{align}
c_o(\infty,t)&=c_o^*\label{bc:one_specie_gerischer_semi_infinite}
\end{align}
\item[(b)] \textbf{Boundary condition at the electrode/electrolyte interface}
\begin{align}
D_o\left.\frac{\partial c_{o}(x,t)}{\partial x}\right|_{x=0}=\frac{i_F(t)}{nFA}\label{bc:one_specie_gericher_semi_infinite_ficks_1st_law}
\end{align}
\end{enumerate}
\end{subequations}
In this section we will also assume that the electrode kinetics are described by the following Butler-Volmer equation,
\begin{equation}
i_F=i_0\cdot{\left[\frac{c_o(0,t)}{c_o^*}e^{\frac{-\beta n}{V_T}\eta}-e^{\frac{(1-\beta) n}{V_T}\eta}\right]}\label{eq:butler_volmer_eq_gerischer_semi_infinite}
\end{equation}
\paragraph{Calculation of Impedance Spectra} The perturbation in the over-potential, Faradaic current, and the oxidant concentration are denoted by $\delta\eta\,e^{j\omega t}$, $\delta i_F\,e^{j\omega t}$ and $\delta c_o\,e^{j\omega t}$ respectively.
The small-signal Faradaic current is obtained by linearizing eqn.~(\ref{eq:butler_volmer_eq_gerischer_semi_infinite}) and keeping only the linear terms, 
\begin{equation}
\delta i_F=\left(\frac{\partial i_F}{\partial\eta}\right)\delta\eta+
\left(\frac{\partial i_F}{\partial c_o}\right)\delta c_o(0)\label{eq:one_specie_gerischer_tayor}
\end{equation}
Taking the partial derivatives of the Faradaic current (\ref{eq:butler_volmer_eq_gerischer_semi_infinite}) w.r.t the over-potential and the oxidant concentration, we obtain,
\begin{subequations}
\begin{align}
\left(\frac{\partial i_F}{\partial \eta}\right)=&-i_0\left[\frac{n\beta }{V_T}\frac{c_o(0,t)}{c_o^*}e^{\frac{-\beta n}{V_T} \eta}+\frac{(1-\beta)n}{V_T}e^{\frac{(1-\beta)n}{V_T} \eta}\right]\label{eq:one_specie_gerischer_taylor_series_derivative_overpotential}\\
\left(\frac{\partial i_F}{\partial c_o}\right)=&\frac{i_0}{c_o^*}e^{\frac{-\beta n}{V_T} \eta} \label{eq:one_specie_gerischer_taylor_series_derivative_oxidant_concentration}
\end{align}
\end{subequations}
Now, we substitute eqns.~(\ref{eq:one_specie_gerischer_taylor_series_derivative_overpotential}) and~(\ref{eq:one_specie_gerischer_taylor_series_derivative_oxidant_concentration}) into eqn.~(\ref{eq:one_specie_gerischer_tayor}) and we get,
\begin{equation}
\delta i_F=-i_0\left[\frac{n\beta }{V_T}\frac{c_o(0,t)}{c_o^*}e^{\frac{-\beta n}{V_T} \eta}+\frac{(1-\beta)n}{V_T}e^{\frac{(1-\beta)n}{V_T} \eta}\right]\delta\eta + \frac{i_0}{c_o^*}e^{\frac{-\beta n}{V_T} \eta}\delta c_o(0)\label{eq:small_signal_substituted_one_specie_gerischer}
\end{equation}
Linearizing the modified Fick's second law of diffusion for a single diffusing specie (\ref{eq:ficks_2nd_law_with_deposition}), we obtain,
\begin{equation}
j\omega \delta c_o(x)=D_o \frac{d^2\left[\delta c_o(x)\right]}{dx^2}-k\delta c_o(x)\label{eq:ficks_small_signal_one_specie_gerischer}
\end{equation}
with the following solution,
\begin{equation}
\delta c_o(x)=C_1e^{-\sqrt{\frac{k+j\omega}{D_o}}x}+C_2e^{\sqrt{\frac{k+j\omega}{D_o}}x}\label{eq:linealized_ficks_semi_infinite_gerischer}
\end{equation}
The small-signal boundary conditions required to find the integration constants $C_1$ and $C_2$ are obtained by linearizing boundary conditions~(\ref{bc:one_specie_gerischer_semi_infinite}) and~(\ref{bc:one_specie_gericher_semi_infinite_ficks_1st_law}),
\begin{align}
\left.\delta c_o(x)\right|_{x=\infty}&=0\label{bc:linearized_one_specie_gerischer_xtoinf}\\
D_o\left.\frac{d\delta c_o(x)}{d x}\right|_{x=0}&=\frac{\delta i_F}{nFA}
\label{bc:linearized_one_specie_gerischer_xto0_ficks_first_law}
\end{align}
By applying boundary conditions~(\ref{bc:linearized_one_specie_gerischer_xtoinf}) and~(\ref{bc:linearized_one_specie_gerischer_xto0_ficks_first_law}) to eqn.~(\ref{eq:linealized_ficks_semi_infinite_gerischer}), we obtain,
\begin{align}
C_2&=0\label{eq:one_specie_gerischer_C2}\\
C_1&=-\frac{\delta i_F}{nFA\sqrt{(k+j\omega) D_o}}\label{eq:one_specie_gerischer_C1}
\end{align}
If we replace the above integration constants into~(\ref{eq:linealized_ficks_semi_infinite_gerischer}), we obtain,
\begin{align}
\delta c_o(x)=-\frac{\delta i_F}{nFA\sqrt{(k+j\omega) D_o}}e^{-\sqrt{\frac{k+j\omega}{D_o}}x}
\end{align}
The small-signal oxidant concentration at the electrode surface ($x=0$) is,
\begin{align}
\delta c_o(0)=-\frac{\delta i_F}{nFA\sqrt{(k+j\omega) D_o}}\label{eq:one_specie_gerischer_delta_co(0)}
\end{align}
and if we substitute eqn.~(\ref{eq:one_specie_gerischer_delta_co(0)}) into eqn.~(\ref{eq:small_signal_substituted_one_specie_gerischer}), we obtain,
\begin{align}
\delta i_F=&-i_0\left[\frac{n\beta }{V_T}\frac{c_o(0,t)}{c_o^*}e^{\frac{-\beta n}{V_T} \eta}+\frac{(1-\beta)n}{V_T}e^{\frac{(1-\beta)n}{V_T} \eta}\right]\delta\eta\nonumber\\
&- \left[\frac{i_0\,e^{\frac{-\beta n}{V_T} \eta}}{nFAc_o^*\sqrt{(k+j\omega) D_o}}\right]\delta i_F\label{eq:small_signal_faradaic_current_one_specie_gerishcer}
\end{align}
The general expression for Faradaic impedance ($Z_F=-\frac{\delta\eta}{\delta i_F}$) becomes,
%\begin{align}
%Z_F=-\frac{\delta\eta}{\delta i_F}\nonumber
%\end{align}
%Rearranging the eqn.~\ref{eq:small_signal_faradaic_current_one_specie_gerishcer},
\begin{align}
Z_F(c_o(0,t),\eta)=\frac{1+\frac{i_0}{nFAc_o^*\sqrt{(k+j\omega) D_o}}e^{\frac{-\beta n}{V_T} \eta}}
{\frac{i_0n}{V_T} \left[\frac{\beta c_o(0,t)}{c_o^*}e^{-\frac{\beta n}{V_T}\eta} + \left(1-\beta\right)e^{\frac{\left(1-\beta\right)n}{V_T}\eta}\right]}
\end{align}
Under 0 d.c. discharge;~$\eta=0$ and~$c_o(0,t)=c_o^*$ and the Faradaic impedance becomes, 
\begin{equation}
\boxed{Z_F=R_\eta+\frac{V_T}{n^2FAc_o^*}\frac{1}{\sqrt{(k+j\omega)D_o}}} \label{eq:Gerischer_Zf_one_specie}
\end{equation}
%This expansion condition is valid since we assume the EIS is performed under 0 dc discharge, the concentration near the electrode is equal to the bulk concentration ($c_o^*$) and the over-potential ($\eta$) is zero. 
The total impedance is the parallel combination of the Faradaic impedance (\ref{eq:Gerischer_Zf_one_specie}) and the double layer impedance.
\paragraph{Simulation results}
\begin{figure}[b!]
\begin{center}
\includegraphics[width=0.48\textwidth,height=0.44\textwidth]{\main figures/one_specie_gerischer_Do_1} 
\includegraphics[width=0.48\textwidth,height=0.44\textwidth]{\main figures/one_specie_gerischer_Do_2}
\includegraphics[width=0.48\textwidth,height=0.44\textwidth]{\main figures/one_specie_gerischer_Cd_1}
\includegraphics[width=0.48\textwidth,height=0.44\textwidth]{\main figures/one_specie_gerischer_Cd_2}
\caption{Nyquist plot for a semi-infinite diffusion system with side reaction(s) for a single electrochemical specie. The top two figures show the effect of the oxidant diffusion coefficient on the impedance plot for two values of the double layer capacitance: $C_d= 40\,\mu\textrm{F}/\textrm{cm}^2$ (top left) and $C_d= 4\,\textrm{mF}/\textrm{cm}^2$ (right). The other simulation parameters are, $c_o= 3.26\times10^{-7}\,\textrm{mol}/\textrm{cm}^2$ and $k=2.6\times10^{-8}\,\textrm{cm}/\textrm{s}$. The bottom two figures show the effect of the double layer capacitance on the impedance plot for two values of the oxidant concentration: $c_o= 1.63\times10^{-6}\,\textrm{mol}/\textrm{cm}^2$ (bottom left) and $c_o= 3.26\times10^{-4}\,\textrm{mol}/\textrm{cm}^2$ (bottom right). The other simulation parameters are, $D_o=7\times10^{-5}\,\textrm{cm}^2/\textrm{s}$ and $k=6.5\times10^{-9}\,\textrm{cm}/\textrm{s}$.}
\label{plots:1S_Gr_Do_comp}
\end{center}
\end{figure}
\begin{figure}[h!]
\begin{center}
\includegraphics[width=0.49\textwidth,height=0.47\textwidth]{\main figures/one_specie_gerischer_co_2} 
\includegraphics[width=0.49\textwidth,height=0.47\textwidth]{\main figures/one_specie_gerischer_co_1}
\includegraphics[width=0.49\textwidth,height=0.47\textwidth]{\main figures/one_specie_gerischer_ko_1}
\includegraphics[width=0.49\textwidth,height=0.47\textwidth]{\main figures/one_specie_gerischer_ko_2}
\caption{Nyquist plot for a semi-infinite diffusion system with side reaction(s) for a single electrochemical specie. The top two figures show the effect of the oxidant concentration on the impedance plot for two values of the oxidant diffusion coefficient: $D_o=7\times10^{-7}\,\textrm{cm}^2/\textrm{s}$ (top left) and $D_o=7\times10^{-5}\,\textrm{cm}^2/\textrm{s}$ (top right). The other simulation parameters are, $C_d= 40\,\mu\textrm{F}/\textrm{cm}^2$ and $k=1.3\times10^{-8}\,\textrm{cm}/\textrm{s}$. 
The bottom two figures show the effect of the reaction constant on the impedance plot for two values of the double layer capacitance: $C_d= 80\,\mu\textrm{F}/\textrm{cm}^2$ (bottom left) and $C_d= 4\,\textrm{mF}/\textrm{cm}^2$ (bottom right). The other simulation parameters are, $c_o= 3.26\times10^{-6}\,\textrm{mol}/\textrm{cm}^2$ and $D_o=7\times10^{-6}\,\textrm{cm}^2/\textrm{s}$.}
\label{plots:2S_RF_cr_comp}
\end{center}
\end{figure}
%\begin{figure}[h!]
%\begin{center}
%\includegraphics[width=0.49\textwidth]{figures/one_specie_gerischer_Cd_1}
%\includegraphics[width=0.49\textwidth]{figures/one_specie_gerischer_Cd_2}
%\caption{The simulation results of the Gerischer impedance for the semi-infinite boundary of single electrochemical diffusing specie are shown. The effect of the double layer capacitance on the impedance plot for two values of the oxidant concentration: $\textrm{c}_\textrm{o}= 1.63\times10^{-6	}\,\textrm{mol}/\textrm{cm}^2$ (left) and $\textrm{c}_\textrm{o}= 3.26\times10^{-4}\,\textrm{mol}/\textrm{cm}^2$ (right). The other simulation parameters are, $\textrm{D}_\textrm{o}=7\times10^{-5}\,\textrm{cm}^2/\textrm{s}$ and $\textrm{k}=6.5\times10^{-9}\,\textrm{cm}/\textrm{s}$.}
%\label{plots:1S_Gr_Cd_comp}
%\end{center}
%\end{figure}
\end{document}