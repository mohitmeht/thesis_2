\documentclass[../../../thesis.tex]{subfiles}
\providecommand{\main}{../../../}
\begin{document}
\subsection{Open boundary finite-length Warburg impedance}
\label{sec:Two_species_transmissive}
Figure~\ref{fig:two_species_transmissive_concentration_profile} shows the concentration profile of the diffusing ions at different d.c. discharge currents. The system consists of a working electrode (left), the absorbing boundary (right) and the electrolyte in-between. 
\begin{figure}[t!]
\begin{center}
\includegraphics[keepaspectratio,height=0.4\textheight]{\main figures/two_species_transmissive_concentration_profile}
\end{center}
\caption{Concentration profiles of the oxidant and reductant in a finite-length electrochemical system with transmissive boundary. The spatial variation of their concentrations at different discharge currents is shown: 0 d.c. discharge (dotted) and finite d.c. discharge (continuous for the oxidant and dash-dotted for the reductant).}
\label{fig:two_species_transmissive_concentration_profile}
\end{figure}
The half cell reaction being considered is,
\begin{equation}
\ce{O + ne <=>[k_f][k_b] R}\nonumber
\end{equation}
The mass transport for two diffusing species in absence of convection and electromigration is, 
\begin{align}
\frac{\partial c_{o}(x,t)}{\partial t}=D_o \frac{\partial^2 c_o(x,t)}{\partial^2 x}\label{eq:ficks_second_law_of_diff_two_species_bounded_transmissive_O}\\
\frac{\partial c_{r}(x,t)}{\partial t}=D_r \frac{\partial^2 c_r(x,t)}{\partial^2 x}
\label{eq:ficks_second_law_of_diff_two_species_bounded_transmissive_R}
\end{align}
\paragraph{Initial and boundary conditions} The initial and boundary conditions required to solve the above equations are,
\begin{subequations}
\begin{enumerate}
\item[] \textbf{Initial conditions}
\begin{align}
c_o(x,0)&=c_o^*\nonumber
\end{align}
\begin{align}
c_r(x,0)&=c_r^*\nonumber
\end{align}
\item[(a)] \textbf{Boundary conditions at $x=l$}
\begin{align}
\delta c_o(l)&=0\label{bc:two_species_transmissive_concentration_change_O}\\
%\frac{d\,\delta c_o(l)}{dx}&\neq 0\label{bc:two_species_transmissive_change_concentration_O}\\
\delta c_r(l)&=0\label{bc:two_species_transmissive_concentration_change_R}
%\\\frac{d\,\delta c_r(l)}{dx}&\neq 0\label{bc:two_species_transmissive_change_concentration_R}
\end{align}
where, $l$ is distance between the electrode and the boundary (see fig.~\ref{fig:two_species_transmissive_concentration_profile}).
\item[(b)] \textbf{Boundary conditions at the electrode/electrolyte interface}
\begin{align}
D_o\left.\frac{\partial c_{o}(x,t)}{\partial x}\right|_{x=0}=\frac{i_F(t)}{nFA}\label{bc:two_species_transmissive_ficks_1st_law_O}\\
D_r\left.\frac{\partial c_{r}(x,t)}{\partial x}\right|_{x=0}=-\frac{i_F(t)}{nFA}\label{bc:two_species_transmissive_ficks_1st_law_R}
\end{align}
\end{enumerate}
The solution of the diffusion equation should satisfy the condition of conservation of matter in an electrode reaction.
\begin{align}
D_o\left[\frac{\partial c_o(x,t)}{\partial x}\right]_{x=0}+D_r\left[\frac{\partial c_r(x,t)}{\partial x}\right]_{x=0}=0\label{bc:two_species_transmissive_matter_conservation_at_electrode}
\end{align}
\end{subequations}
The kinetics at the electrode surface is described using the Butler-Volmer equation,
\begin{equation}
i_F=i_0\cdot{\left[\frac{c_o(0,t)}{c_o^*}e^{\frac{-\beta n}{V_T}\eta}-\frac{c_r(0,t)}{c_r^*}e^{\frac{(1-\beta) n}{V_T}\eta}\right]}\label{eq:butler_volmer_eq_two_species_transmissive}
\end{equation}
\paragraph{Calculation of Impedance Spectra} The small-signal Faradaic current, over-potential, and oxidant and reductant concentrations are represented using $\delta i_F\,e^{j\omega t}$, $\delta\eta\,e^{j\omega t}$, $\delta c_o\,e^{j\omega t}$ and $\delta c_r\,e^{j\omega t}$ respectively. 
The small-signal Faradaic current is obtained by expanding eqn.~(\ref{eq:butler_volmer_eq_two_species_transmissive}) using the Taylor series and keeping only the linear terms, 
\begin{equation}
\delta i_F=\left(\frac{\partial i_F}{\partial\eta}\right)\delta\eta+
\left(\frac{\partial i_F}{\partial c_o}\right)\delta c_o(0)+
\left(\frac{\partial i_F}{\partial c_r}\right)\delta c_r(0) \label{eq:two_specie_transmissive_tayor}
\end{equation}
The partial derivatives in the above equation are,
\begin{subequations}
\begin{align}
\left(\frac{\partial i_F}{\partial\eta}\right)&=
-\frac{i_0n}{V_T}\left[\frac{\beta c_o(0,t)}{c_o^*}e^{-\frac{\beta n}{V_T}\eta}+
\frac{\left(1-\beta\right)c_r(0,t)}{c_r^*}e^{\frac{\left(1-\beta\right)n}{V_T}\eta}\right]\label{eq:two_species_transmissive_partial_derivarive_wrt_eta}\\
\left(\frac{\partial i_F}{\partial c_o}\right)&=
\frac{i_0}{c_o^*}e^{-\frac{\beta n}{V_T} \eta}\label{eq:two_species_transmissive_partial_derivarive_wrt_co}\\
\left(\frac{\partial i_F}{\partial c_r}\right)&=
-\frac{i_0}{c_r^*}e^{ \frac{\left (1-\beta \right)n}{V_T}\eta}\label{eq:two_species_transmissive_partial_derivarive_wrt_cr}
\end{align}
\end{subequations}
Substuting eqns.~(\ref{eq:two_species_transmissive_partial_derivarive_wrt_eta}),~(\ref{eq:two_species_transmissive_partial_derivarive_wrt_co}) and~(\ref{eq:two_species_transmissive_partial_derivarive_wrt_cr}) into eqn.~(\ref{eq:two_specie_transmissive_tayor}), we obtain,
\begin{align}
\delta i_F=
-&\frac{i_0n}{V_T}\left[\frac{\beta c_o(0,t)}{c_o^*}e^{-\frac{\beta n}{V_T}\eta}+
\frac{\left(1-\beta\right)c_r(0,t)}{c_r^*}e^{\frac{\left(1-\beta\right)n}{V_T}\eta}\right]\delta\eta\nonumber\\
+&\frac{i_0}{c_o^*}e^{-\frac{\beta n}{V_T} \eta}\delta c_o(0)
-\frac{i_0}{c_r^*}e^{ \frac{\left (1-\beta \right)n}{V_T}\eta}\delta c_r(0)\label{eq:two_species_transmissive_tayor_with_substitution}
\end{align}
Small-signal concentrations $\delta c_o$ and $\delta c_r$ are found by solving the linearized diffusion equations,
\begin{align}
j\omega \delta c_o&=D_o\frac{d^2\delta c_o}{dx^2}\label{eq:ficks_small_signal_two_species_transmissive_O}\\
j\omega \delta c_r&=D_r\frac{d^2\delta c_r}{dx^2}\label{eq:ficks_small_signal_two_species_transmissive_R}
\end{align}
The solution to the homogeneous equations are,
\begin{align}
\delta c_o(x)&=C_1\,e^{-\sqrt{\frac{j\omega}{D_o}}x}+C_2\,e^{\sqrt{\frac{j\omega}{D_o}}x}\label{eq:two_specie_oxidant_tranmissive_solution_to_ficks_diffusion_law}\\
\delta c_r(x)&=C'_1\,e^{-\sqrt{\frac{j\omega}{D_r}}x}+C'_2\,e^{\sqrt{\frac{j\omega}{D_r}}x}\label{eq:two_specie_reductant_tranmissive_solution_to_ficks_diffusion_law}
\end{align}
The linearized boundary conditions are,
%The integration constants $C_1$ $C_2$, $C'_1$ and $C'_2$ are obtained by subjecting eqns.~\ref{eq:two_specie_oxidant_tranmissive_solution_to_ficks_diffusion_law} and~\ref{eq:two_specie_reductant_tranmissive_solution_to_ficks_diffusion_law} to the linearized boundary conditions of ~\ref{bc:two_species_transmissive_concentration_change_O} ,~\ref{bc:two_species_transmissive_concentration_change_R},~\ref{bc:two_species_transmissive_ficks_1st_law_O} and~\ref{bc:two_species_transmissive_ficks_1st_law_R},
\begin{align}
D_o\left.\frac{d\delta c_o(x)}{d x}\right|_{x=0}&=\frac{\delta i_F}{nFA}
\label{bc:linearized_boundary_condition_two_species_oxidant_transmissive_ficks_1st_law}\\
\delta c_o(l)&=0\label{bc:linearized_boundary_condition_two_species_oxidant_transmissive_boundary}\\
D_r\left.\frac{d\delta c_r(x)}{d x}\right|_{x=0}&=-\frac{\delta i_F}{nFA}
\label{bc:linearized_boundary_condition_two_species_reductant_transmissive_ficks_1st_law}\\
\delta c_r(l)&=0\label{bc:linearized_boundary_condition_two_species_reductant_transmissive_boundary}
\end{align}
Constants $C_1$ $C_2$, $C'_1$ and $C'_2$ can be obtained by applying boundary conditions to (\ref{eq:two_specie_oxidant_tranmissive_solution_to_ficks_diffusion_law}) and 
(\ref{eq:two_specie_reductant_tranmissive_solution_to_ficks_diffusion_law}),
%to boundary conditions \ref{bc:linearized_boundary_condition_two_species_oxidant_transmissive_ficks_1st_law} and \ref{bc:linearized_boundary_condition_two_species_oxidant_transmissive_boundary},
\begin{subequations}
\begin{align}
C_2-C_1&=\frac{\delta i_F}{nFA\sqrt{j\omega D_o}}\label{eq:constant_relation_two_species_oxidant_transmissive_subject_1st_diffusion}\\
C_1&=-C_2\,e^{2\sqrt{\frac{j\omega}{D_o}}l}\label{eq:constant_relation_two_species_oxidant_transmissive_subject_boundary}
\end{align}
\end{subequations}
and,
\begin{subequations}
\begin{align}
C'_2-C'_1&=-\frac{\delta i_F}{nFA\sqrt{j\omega D_r}}\label{eq:constant_relation_two_species_reductant_transmissive_subject_1st_diffusion}\\
C'_1&=-C'_2\,e^{2\sqrt{\frac{j\omega}{D_r}}l}\label{eq:constant_relation_two_species_reductant_transmissive_subject_boundary}
\end{align}
\end{subequations}
Solving eqns.~(\ref{eq:constant_relation_two_species_oxidant_transmissive_subject_1st_diffusion}) and~(\ref{eq:constant_relation_two_species_oxidant_transmissive_subject_boundary}), we get, 
\begin{subequations}
\begin{align}
C_1=&-\frac{\delta i_F}{nFA\sqrt{j\omega D_o}}\frac{e^{2\sqrt{\frac{j\omega}{D_o}}l}}{1+e^{2\sqrt{\frac{j\omega}{D_o}}l}}\label{eq:two_species_oxidant_transmission_c1}\\
C_2=&\frac{\delta i_F}{nFA\sqrt{j\omega D_o}}\frac{1}{1+e^{2\sqrt{\frac{j\omega}{D_o}}l}}\label{eq:two_species_oxidant_transmission_c2}
\end{align}
\end{subequations}
and solving equations~(\ref{eq:constant_relation_two_species_reductant_transmissive_subject_1st_diffusion}) and~(\ref{eq:constant_relation_two_species_reductant_transmissive_subject_boundary}), we get,
\begin{subequations}
\begin{align}
C'_1&=\frac{\delta i_F}{nFA\sqrt{j\omega D_r}}\frac{e^{2\sqrt{\frac{j\omega}{D_r}}l}}{1+e^{2\sqrt{\frac{j\omega}{D_r}}l}}\label{eq:two_species_reductant_transmission_c1}\\
C'_2&=-\frac{\delta i_F}{nFA\sqrt{j\omega D_r}}\frac{1}{1+e^{2\sqrt{\frac{j\omega}{D_r}}l}}\label{eq:two_species_reductantr_transmission_c2}
\end{align}
\end{subequations}
Now, we substitute integration constants $C_1$ (\ref{eq:two_species_oxidant_transmission_c1}) and $C_2$ (\ref{eq:two_species_oxidant_transmission_c2}) into eqn.~(\ref{eq:two_specie_oxidant_tranmissive_solution_to_ficks_diffusion_law}),
\begin{align}
\delta c_o(x)&=\frac{\delta i_F}{nFA\sqrt{j\omega D_o}} \left[\frac{e^{\sqrt{\frac{j\omega}{D_o}}x}-e^{\sqrt{\frac{j\omega}{D_o}}(2l-x)}}{1+e^{2\sqrt{\frac{j\omega}{D_o}}l}}\right]\nonumber\\
%\therefore\qquad 
\delta c_o(x)&=\frac{-\delta i_F}{nFA\sqrt{j\omega D_o}}\left[\frac{\sinh{\left(\sqrt{\frac{j\omega}{D_o}}\left(l-x\right)\right)}}{\cosh{\left(\sqrt{\frac{j\omega}{D_o}}l\right)}}\right]\label{eq:two_species_transmissive_oxidant_delta_co(x)}
\end{align}
and constants $C_1'$ (\ref{eq:two_species_reductant_transmission_c1}) and $C_2'$ (\ref{eq:two_species_reductantr_transmission_c2}) into eqn. (\ref{eq:two_specie_reductant_tranmissive_solution_to_ficks_diffusion_law}),
\begin{align}
%\delta c_r(x)&=\frac{\delta i_F}{nFA\sqrt{j\omega D_r}} \left[\frac{e^{\sqrt{\frac{j\omega}{D_r}}(2l-x)}-e^{\sqrt{\frac{j\omega}{D_r}}x}}{1+e^{2\sqrt{\frac{j\omega}{D_r}}l}}\right]\nonumber\\
%\therefore\qquad
\delta c_r(x)&=\frac{\delta i_F}{nFA\sqrt{j\omega D_r}}\left[\frac{\sinh{\left(\sqrt{\frac{j\omega}{D_r}}\left(l-x\right)\right)}}{\cosh{\left(\sqrt{\frac{j\omega}{D_r}}l\right)}}\right]\label{eq:two_species_transmissive_reductant_delta_co(x)}
\end{align}
The concentrations at the cathode surface ($x=0$) are,
\begin{align}
\delta c_o(0)&=\frac{-\delta i_F}{nFA\sqrt{j\omega D_o}}\tanh{\left(\sqrt{\frac{j\omega}{D_o}}l\right)}\label{eq:two_species_transmissive_oxidant_delta_co(0)}\\
\delta c_r(0)&=\frac{\delta i_F}{nFA\sqrt{j\omega D_r}}\tanh{\left(\sqrt{\frac{j\omega}{D_r}}l\right)}\label{eq:two_species_transmissive_reductant_delta_cr(0)}
\end{align}
If we substitute the relations for $\delta c_o(0)$ and $\delta c_r(0)$
%from eqns.~\ref{eq:two_species_transmissive_oxidant_delta_co(0)} and~\ref{eq:two_species_transmissive_reductant_delta_cr(0)} respectively, 
into the small-signal Faradaic current (\ref{eq:two_species_transmissive_tayor_with_substitution}), we obtain,
\begin{align}
\delta i_F=
-&\frac{i_0n}{V_T}\left[\frac{\beta c_o(0,t)}{c_o^*}e^{-\frac{\beta n}{V_T}\eta}+
\frac{\left(1-\beta\right)c_r(0,t)}{c_r^*}e^{\frac{\left(1-\beta\right)n}{V_T}\eta}\right]\delta\eta\nonumber\\
-&\frac{i_0\delta i_F}{nFA\sqrt{j\omega}}\left[\frac{e^{-\frac{\beta n}{V_T} \eta}}{c_o^*\sqrt{D_o}}\tanh{\left(\sqrt{\frac{j\omega}{D_o}}l\right)}+\frac{e^{ \frac{\left (1-\beta \right)n}{V_T}\eta}}{c_r^*\sqrt{D_r}}\tanh{\left(\sqrt{\frac{j\omega}{D_r}}l\right)}\right]\label{eq:small_signal_faradaic_current_two_species_transmissive}
\end{align}
The Faradaic impedance ($Z_F=-\frac{\delta\eta}{\delta i_F}$) becomes,
%\begin{align}
%Z_F=-\frac{\delta\eta}{\delta i_F}\nonumber
%\end{align}
%Rearranging the eqn.~\ref{eq:small_signal_faradaic_current_two_species_transmissive},
\begin{align}
Z_F(c_o(0,t),c_r(0,t),\eta)=\frac{1+\frac{i_0}{nFA\sqrt{j\omega}}\left[\frac{e^{-\frac{\beta n}{V_T} \eta}}{c_o^*\sqrt{D_o}}\tanh{\left(\sqrt{\frac{j\omega}{D_o}}l\right)}+\frac{e^{ \frac{\left (1-\beta \right)n}{V_T}\eta}}{c_r^*\sqrt{D_r}}\tanh{\left(\sqrt{\frac{j\omega}{D_r}}l\right)}\right]}
{\frac{i_0n}{V_T} \left[\frac{\beta c_o(0,t)}{c_o^*}e^{-\frac{\beta n}{V_T}\eta} + \frac{\left(1-\beta\right)c_r(0,t)}{c_r^*}e^{\frac{\left(1-\beta\right)n}{V_T}\eta}\right]}
\end{align}
Under 0 d.c. discharge;~$\eta=0$,~$c_o(0,t)=c_o^*$ and~$c_r(0,t)=c_r^*$. The Faradaic impedance is, 
\begin{equation}
\boxed{Z_F=R_\eta+
\frac{V_T}{n^2FA\sqrt{j\omega}}
\left[\frac{\tanh{\left(\sqrt{\frac{j\omega}{D_o}}l\right)}}{c_o^*\sqrt{D_o}}+\frac{\tanh{\left(\sqrt{\frac{j\omega}{D_r}}l\right)}}{c_r^*\sqrt{D_r}}\right]}\label{eq:Transmissive_Zf_two_species}
\end{equation}
%This is the finite-length diffusion impedance when two species electrochemical system has a transmissive boundary. 
The total impedance (\ref{eq:Total_impedance}) is a parallel combination of the Faradaic impedance (\ref{eq:Transmissive_Zf_two_species}) and the double layer impedance. 
%The simulation results for varying reductant diffusion coefficient (fig.~\ref{plots:2S_TR_Dr_comp} (top)), the double layer capacitance (fig.~\ref{plots:2S_TR_Dr_comp} (bottom)), the reductant concentration (fig.~\ref{plots:2S_TR_cr_comp} (top)) and system length (fig.~\ref{plots:2S_TR_cr_comp} (bottom)) shows the influence of these parameters on the impedance spectra. 
\paragraph{Simulation Results}
\begin{figure}[b!]
\begin{center}
\includegraphics[width=0.49\textwidth,height=0.44\textwidth]{\main figures/two_species_transmissive_Do_2}
\includegraphics[width=0.49\textwidth,height=0.44\textwidth]{\main figures/two_species_transmissive_Do_3}
\includegraphics[width=0.49\textwidth,height=0.44\textwidth]{\main figures/two_species_transmissive_Cd_2}
\includegraphics[width=0.49\textwidth,height=0.44\textwidth]{\main figures/two_species_transmissive_Cd_1}  
\caption{Nyquist diagram for a finite-length diffusion system with transmissive boundary for two electrochemical species. The top two figures show the effect of the reductant diffusion coefficient on the impedance plot for two values of the reductant concentration:  $c_r= 3.26\times10^{-7}\,\textrm{mol}/\textrm{cm}^2$ (left) and $c_r= 3.26\times10^{-4}\,\textrm{mol}/\textrm{cm}^2$ (right). The other simulation parameters are $c_o= 3.26\times10^{-6}\,\textrm{mol}/\textrm{cm}^2$, $C_d= 4\,\mu\textrm{F}/\textrm{cm}^2$, $D_o=7\times10^{-6}\,\textrm{cm}^2/\textrm{s}$ and $l=1\,\mu\textrm{m}$.
The bottom two figures show the effect of the double layer capacitance on the impedance plot for two values of the system length:  $l=10\,\textrm{nm}$ (left) and $l=100\,\textrm{nm}$ (right). The other simulation parameters are $c_o= 3.26\times10^{-6}\,\textrm{mol}/\textrm{cm}^2$, $c_r= 1.63\times10^{-6}\,\textrm{mol}/\textrm{cm}^2$,  $D_o=7\times10^{-6}\,\textrm{cm}^2/\textrm{s}$ and
$D_r=7\times10^{-8}\,\textrm{cm}^2/\textrm{s}$.}
\label{plots:2S_TR_Dr_comp}
\end{center}
\end{figure}
\begin{figure}[h!]
\begin{center}
\includegraphics[width=0.49\textwidth,height=0.47\textwidth]{\main figures/two_species_transmissive_cr_2}
\includegraphics[width=0.49\textwidth,height=0.47\textwidth]{\main figures/two_species_transmissive_cr_1}
\includegraphics[width=0.49\textwidth,height=0.47\textwidth]{\main figures/two_species_transmissive_l_3}
\includegraphics[width=0.49\textwidth,height=0.47\textwidth]{\main figures/two_species_transmissive_l_2} 
\caption{Nyquist diagram for a finite-length diffusion system with transmissive boundary for two electrochemical species. The top two figures show the effect of the reductant concentration on the impedance plot for two values of the double layer capacitance: $C_d= 400\,\textrm{nF}/\textrm{cm}^2$ (left) and $C_d= 4\,\textrm{mF}/\textrm{cm}^2$ (right). The other simulation parameters are $c_o= 3.26\times10^{-6}\,\textrm{mol}/\textrm{cm}^2$, $D_o=7\times10^{-6}\,\textrm{cm}^2/\textrm{s}$, $D_r=7\times10^{-7}\,\textrm{cm}^2/\textrm{s}$ and $l=2\,\mu\textrm{m}$.
The bottom two figures show the effect of the system length on the impedance plot for two values of the reductant concentration: $c_r= 1.63\times10^{-6}\,\textrm{mol}/\textrm{cm}^2$ (left) and $c_r= 3.26\times10^{-4}\,\textrm{mol}/\textrm{cm}^2$ (right). The other simulation parameters are $c_o= 3.26\times10^{-6}\,\textrm{mol}/\textrm{cm}^2$, $D_o=7\times10^{-6}\,\textrm{cm}^2/\textrm{s}$, 
$D_r=7\times10^{-7}\,\textrm{cm}^2/\textrm{s}$ and $C_d= 400\,\mu\textrm{F}/\textrm{cm}^2$.}
\label{plots:2S_TR_cr_comp}
\end{center}
\end{figure}
\end{document}