\documentclass[../../../thesis.tex]{subfiles}
\providecommand{\main}{../../../}
\begin{document}
\subsection{Blocked boundary finite-length Warburg impedance}
\label{sec:Two_species_reflective}
Figure~\ref{fig:two_species_reflective_concentration_profile} shows the concentration profile for two diffusing species (oxidant and reductant) at different d.c. discharge currents. We assume that the value of the initial oxidant concentration and the reductant concentration is equal. The general half cell reaction for single reacting specie is,
\begin{equation}
\ce{O + ne <=>[k_f][k_b] R}\nonumber
\end{equation}
The ion movement in the absence of convection and electro-migration is described using the Fick's second law of diffusion, 
\begin{align}
\frac{\partial c_{o}(x,t)}{\partial t}=D_o \frac{\partial^2 c_o(x,t)}{\partial^2 x}\label{eq:ficks_second_law_of_diff_two_species_bounded_reflective_O}\\
\frac{\partial c_{r}(x,t)}{\partial t}=D_r \frac{\partial^2 c_r(x,t)}{\partial^2 x}\label{eq:ficks_second_law_of_diff_two_species_bounded_reflective_R}
\end{align}
\paragraph{Initial and boundary conditions} The initial and boundary conditions required to solve these differential equations are,
\begin{subequations}
\begin{enumerate}
\item[] \textbf{Initial conditions}
\begin{align}
c_o(x,0)&=c_o^*\nonumber\\
c_r(x,0)&=c_r^*\nonumber
\end{align}
\item[(a)] \textbf{Boundary conditions at $x=l$}
\begin{align}
\frac{d\,\delta c_o(l)}{dx}= 0\label{bc:two_species_reflective_change_concentration_oxidant}\\
\frac{d\,\delta c_r(l)}{dx}= 0\label{bc:two_species_reflective_change_concentration_reductant}
\end{align}
where, $l$ is distance between the electrode and the reflective boundary (see fig.~\ref{fig:two_species_reflective_concentration_profile}).
\item[(b)] \textbf{Boundary conditions at the electrode/electrolyte interface}
\begin{align}
D_o\left.\frac{\partial c_{o}(x,t)}{\partial x}\right|_{x=0}=\frac{i_F(t)}{nFA}\label{bc:two_species_reflective_oxidant_ficks_1st_law}\\
D_r\left.\frac{\partial c_{r}(x,t)}{\partial x}\right|_{x=0}=-\frac{i_F(t)}{nFA}\label{bc:two_species_reflective_reductant_ficks_1st_law}
\end{align}
\end{enumerate}
\begin{figure}[t!]
\begin{center}
\includegraphics[width=0.48\textwidth,height=0.47\textwidth]{\main figures/two_species_reflective_concentration_profile} 
\caption{The spatial variation of oxidant and reductant concentrations of a finite-length electrochemical system with reflective boundary at different discharge currents is shown: 0 d.c. discharge (dotted) and finite d.c. discharge (continuous for the oxidant and dash-dotted for the reductant).} 
\label{fig:two_species_reflective_concentration_profile}
\end{center}
\end{figure}
The above diffusion equations should also satisfy the following condition of conservation of matter in an electrode reaction.
\begin{align}
D_o\left[\frac{\partial c_o(x,t)}{\partial x}\right]_{x=0}+D_r\left[\frac{\partial c_r(x,t)}{\partial x}\right]_{x=0}=0\label{bc:two_species_reflective_matter_conservation_at_electrode}
\end{align}
\end{subequations}
We also assume that the following Butler-Volmer equation describes the electron transfer reaction on the electrode surface,
\begin{equation}
i_F=i_0\cdot{\left[\frac{c_o(0,t)}{c_o^*}e^{\frac{-\beta n}{V_T}\eta}-\frac{c_r(0,t)}{c_r^*}e^{\frac{(1-\beta) n}{V_T}\eta}\right]}\label{eq:butler_volmer_eq_two_species_reflective}
\end{equation}
\paragraph{Calculation of Impedance Spectra} The small-signal theory is used to compute the impedance spectra. The small-signal over-potential, Faradaic current, and concentrations of the oxidant and the reductant are given by, $\delta\eta\,e^{j\omega t}$, $\delta i_F\,e^{j\omega t}$, $\delta c_o\,e^{j\omega t}$ and $\delta c_r\,e^{j\omega t}$ respectively. The small-signal Faradaic current can be expressed again as,
\begin{equation}
\delta i_F=\left(\frac{\partial i_F}{\partial\eta}\right)\delta\eta+
\left(\frac{\partial i_F}{\partial c_o}\right)\delta c_o(0)+
\left(\frac{\partial i_F}{\partial c_r}\right)\delta c_r(0) \label{eq:two_specie_reflective_tayor}
\end{equation}
The partial derivatives in the above equations are,
\begin{subequations}
\begin{align}
\left(\frac{\partial i_F}{\partial\eta}\right)&=
-\frac{i_0n}{V_T}\left[\frac{\beta c_o(0,t)}{c_o^*}e^{-\frac{\beta n}{V_T}\eta}+
\frac{\left(1-\beta\right)c_r(0,t)}{c_r^*}e^{\frac{\left(1-\beta\right)n}{V_T}\eta}\right]\label{eq:two_species_reflective_partial_derivarive_wrt_eta}\\
\left(\frac{\partial i_F}{\partial c_o}\right)&=
\frac{i_0}{c_o^*}e^{-\frac{\beta n}{V_T} \eta}\label{eq:two_species_reflective_partial_derivarive_wrt_co}\\
\left(\frac{\partial i_F}{\partial c_r}\right)&=
-\frac{i_0}{c_r^*}e^{ \frac{\left (1-\beta \right)n}{V_T}\eta}\label{eq:two_species_reflective_partial_derivarive_wrt_cr}
\end{align}
\end{subequations}
If we replace these partial derivatives into the eqn.~(\ref{eq:two_specie_reflective_tayor}), we get,
\begin{align}
\delta i_F=
-&\frac{i_0n}{V_T}\left[\frac{\beta c_o(0,t)}{c_o^*}e^{-\frac{\beta n}{V_T}\eta}+
\frac{\left(1-\beta\right)c_r(0,t)}{c_r^*}e^{\frac{\left(1-\beta\right)n}{V_T}\eta}\right]\delta\eta\nonumber\\
+&\frac{i_0}{c_o^*}e^{-\frac{\beta n}{V_T} \eta}\delta c_o(0)
-\frac{i_0}{c_r^*}e^{ \frac{\left (1-\beta \right)n}{V_T}\eta}\delta c_r(0)\label{eq:two_species_reflective_tayor_with_substitution}
\end{align}
The small-signal diffusion equations are,
\begin{align}
j\omega \delta c_o&=D_o\frac{d^2\delta c_o}{dx^2}\label{eq:ficks_small_signal_two_specie_reflective_O}\\
j\omega \delta c_r&=D_r\frac{d^2\delta c_r}{dx^2}\label{eq:ficks_small_signal_two_specie_reflective_R}
\end{align}
with the following solutions,,
\begin{align}
\delta c_o(x)&=C_1\,e^{-\sqrt{\frac{j\omega}{D_o}}x}+C_2\,e^{\sqrt{\frac{j\omega}{D_o}}x}\label{eq:two_specie_oxidant_reflective_solution_to_ficks_diffusion_law}\\
\delta c_r(x)&=C'_1\,e^{-\sqrt{\frac{j\omega}{D_r}}x}+C'_2\,e^{\sqrt{\frac{j\omega}{D_r}}x}\label{eq:two_specie_reductant_reflective_solution_to_ficks_diffusion_law}
\end{align}
Integration constants $C_1$ $C_2$, $C'_1$ and $C'_2$ are obtained by applying the following linearized boundary conditions to eqns.~(\ref{eq:two_specie_oxidant_reflective_solution_to_ficks_diffusion_law}) and~(\ref{eq:two_specie_reductant_reflective_solution_to_ficks_diffusion_law}) 
%to the linearized boundary conditions~\ref{bc:linearized_boundary_condition_two_species_oxidant_reflective_ficks_1st_law}$-$\ref{bc:linearized_boundary_condition_two_species_reductant_reflective_boundary},
\begin{subequations}
\begin{align}
D_o\left.\frac{d\delta c_o(x)}{d x}\right|_{x=0}&=\frac{\delta i_F}{nFA}
\label{bc:linearized_boundary_condition_two_species_oxidant_reflective_ficks_1st_law}\\
\frac{d\delta c_o(l)}{dx}&=0\label{bc:linearized_boundary_condition_two_species_oxidant_reflective_boundary}\\
D_r\left.\frac{d\delta c_r(x)}{d x}\right|_{x=0}&=-\frac{\delta i_F}{nFA}
\label{bc:linearized_boundary_condition_two_species_reductant_reflective_ficks_1st_law}\\
\frac{d\delta c_r(l)}{dx}&=0 \label{bc:linearized_boundary_condition_two_species_reductant_reflective_boundary}
\end{align}
\end{subequations}
We obtain,
%Subjecting the eqn.~\ref{eq:two_specie_oxidant_reflective_solution_to_ficks_diffusion_law} to boundary conditions \ref{bc:linearized_boundary_condition_two_species_oxidant_reflective_ficks_1st_law} and \ref{bc:linearized_boundary_condition_two_species_oxidant_reflective_boundary}, we get,
\begin{subequations}
\begin{align}
C_2-C_1&=\frac{\delta i_F}{nFA\sqrt{j\omega D_o}}\label{eq:constant_relation_two_species_oxidant_reflective_subject_1st_diffusion}\\
C_1&=C_2\,e^{2\sqrt{\frac{j\omega}{D_o}}l}\label{eq:constant_relation_two_species_oxidant_reflective_subject_boundary}
\end{align}
\end{subequations}
Solving eqns.(\ref{eq:constant_relation_two_species_oxidant_reflective_subject_1st_diffusion}) and~(\ref{eq:constant_relation_two_species_oxidant_reflective_subject_boundary}) for $C_1$ and $C_2$, we obtain, 
%\begin{subequations}
\begin{align}
C_1&=-\frac{\delta i_F}{nFA\sqrt{j\omega D_o}}\left[\frac{e^{2\sqrt{\frac{j\omega}{D_o}}l}}{e^{2\sqrt{\frac{j\omega}{D_o}}l}-1}\right]\label{eq:two_species_oxidant_reflective_c1}\\
C_2&=-\frac{\delta i_F}{nFA\sqrt{j\omega D_o}}\left[\frac{1}{e^{2\sqrt{\frac{j\omega}{D_o}}l}-1}\right]\label{eq:two_species_oxidant_reflective_c2}
\end{align}
%\end{subequations}
Now, substitute these equations of $C_1$ and  $C_2$ into eqn.~(\ref{eq:two_specie_oxidant_reflective_solution_to_ficks_diffusion_law}),
\begin{align}
%\delta c_o(x)&=-\frac{\delta i_F}{nFA\sqrt{j\omega D_o}} \left[\frac{e^{\sqrt{\frac{j\omega}{D_o}}(l-x)}+e^{-\sqrt{\frac{j\omega}{D_o}}(l-x)}}{e^{\sqrt{\frac{j\omega}{D_o}}l}-e^{-\sqrt{\frac{j\omega}{D_o}}l}}\right]\nonumber\\\therefore\qquad 
\delta c_o(x)&=-\frac{\delta i_F}{nFA\sqrt{j\omega D_o}}\left[\frac{\cosh{\left(\sqrt{\frac{j\omega}{D_o}}\left(l-x\right)\right)}}{\sinh{\left(\sqrt{\frac{j\omega}{D_o}}l\right)}}\right]\label{eq:two_species_reflective_oxidant_delta_co(x)}
\end{align}
Using boundary conditions~(\ref{bc:linearized_boundary_condition_two_species_reductant_reflective_ficks_1st_law}) and~(\ref{bc:linearized_boundary_condition_two_species_reductant_reflective_boundary}), we obtain,
\begin{subequations}
\begin{align}
C'_2-C'_1&=-\frac{\delta i_F}{nFA\sqrt{j\omega D_r}}\label{eq:constant_relation_two_species_reductant_reflective_subject_1st_diffusion}
%\\
\end{align}
\begin{align}
C'_1&=C'_2\,e^{2\sqrt{\frac{j\omega}{D_r}}l}\label{eq:constant_relation_two_species_reductant_reflective_subject_boundary}
\end{align}
\end{subequations}
Solving equations~(\ref{eq:constant_relation_two_species_reductant_reflective_subject_1st_diffusion}) and~(\ref{eq:constant_relation_two_species_reductant_reflective_subject_boundary}), we get,
\begin{subequations}
\begin{align}
C'_1&=\frac{\delta i_F}{nFA\sqrt{j\omega D_r}}\left[\frac{e^{2\sqrt{\frac{j\omega}{D_r}}l}}{e^{2\sqrt{\frac{j\omega}{D_r}}l}-1}\right]\label{eq:two_species_reductant_reflective_c1}\\
C'_2&=\frac{\delta i_F}{nFA\sqrt{j\omega D_r}}\left[\frac{1}{e^{2\sqrt{\frac{j\omega}{D_r}}l}-1}\right]\label{eq:two_species_reductantr_reflective_c2}
\end{align}
\end{subequations}
and, then, substituting eqns.~(\ref{eq:two_species_reductant_reflective_c1}) and~(\ref{eq:two_species_reductantr_reflective_c2}) into eqn.~(\ref{eq:two_specie_reductant_reflective_solution_to_ficks_diffusion_law}),
\begin{align}
%\delta c_r(x)&=\frac{\delta i_F}{nFA\sqrt{j\omega D_r}} \left[\frac{e^{\sqrt{\frac{j\omega}{D_r}}(l-x)}+e^{-\sqrt{\frac{j\omega}{D_r}}(l-x)}}{e^{\sqrt{\frac{j\omega}{D_r}}l}-e^{-\sqrt{\frac{j\omega}{D_r}}l}}\right]\nonumber\\\therefore\qquad
\delta c_r(x)&=\frac{\delta i_F}{nFA\sqrt{j\omega D_r}}\left[\frac{\cosh{\left(\sqrt{\frac{j\omega}{D_r}}\left(l-x\right)\right)}}{\sinh{\left(\sqrt{\frac{j\omega}{D_r}}l\right)}}\right]\label{eq:two_species_reflective_reductant_delta_co(x)}
\end{align}
The small-signal concentrations at the electrode surfaces ($x=0$) are,
\begin{align}
\delta c_o(0)&=\frac{-\delta i_F}{nFA\sqrt{j\omega D_o}}\coth{\left(\sqrt{\frac{j\omega}{D_o}}l\right)}\label{eq:two_species_reflective_oxidant_delta_co(0)}\\
\delta c_r(0)&=\frac{\delta i_F}{nFA\sqrt{j\omega D_r}}\coth{\left(\sqrt{\frac{j\omega}{D_r}}l\right)}\label{eq:two_species_reflective_reductant_delta_cr(0)}
\end{align}
Substitute eqns.~(\ref{eq:two_species_reflective_oxidant_delta_co(0)}) and~(\ref{eq:two_species_reflective_reductant_delta_cr(0)}) into eqn.~(\ref{eq:two_species_reflective_tayor_with_substitution}),
\begin{align}
\delta i_F=
-&\frac{i_0n}{V_T}\left[\frac{\beta c_o(0,t)}{c_o^*}e^{-\frac{\beta n}{V_T}\eta}+
\frac{\left(1-\beta\right)c_r(0,t)}{c_r^*}e^{\frac{\left(1-\beta\right)n}{V_T}\eta}\right]\delta\eta\nonumber\\
-&\frac{i_0\delta i_F}{nFA\sqrt{j\omega}}\left[\frac{e^{-\frac{\beta n}{V_T} \eta}}{c_o^*\sqrt{D_o}}\coth{\left(\sqrt{\frac{j\omega}{D_o}}l\right)}+\frac{e^{ \frac{\left (1-\beta \right)n}{V_T}\eta}}{c_r^*\sqrt{D_r}}\coth{\left(\sqrt{\frac{j\omega}{D_r}}l\right)}\right]\label{eq:small_signal_faradaic_current_two_species_reflective}
\end{align}
The Faradaic impedance is given by,
\begin{equation}
Z_F=-\frac{\delta\eta}{\delta i_F}\nonumber
\end{equation}
%Rearranging eqn.~(\ref{eq:small_signal_faradaic_current_two_species_reflective}), we obtain,
\begin{align}
Z_F(c_o(0,t),c_r(0,t),\eta)=\frac{1+\frac{i_0}{nFA\sqrt{j\omega}}\left[\frac{e^{-\frac{\beta n}{V_T} \eta}}{c_o^*\sqrt{D_o}}\coth{\left(\sqrt{\frac{j\omega}{D_o}}l\right)}+\frac{e^{ \frac{\left (1-\beta \right)n}{V_T}\eta}}{c_r^*\sqrt{D_r}}\coth{\left(\sqrt{\frac{j\omega}{D_r}}l\right)}\right]}
{\frac{i_0n}{V_T} \left[\frac{\beta c_o(0,t)}{c_o^*}e^{-\frac{\beta n}{V_T}\eta} + \frac{\left(1-\beta\right)c_r(0,t)}{c_r^*}e^{\frac{\left(1-\beta\right)n}{V_T}\eta}\right]}
\end{align}
Under 0 d.c. discharge;~$\eta=0$,~$c_o(0,t)=c_o^*$ and~$c_r(0,t)=c_r^*$. The Faradaic impedance is, 
\begin{equation}
\boxed{Z_F=R_\eta+
\frac{V_T}{n^2FA\sqrt{j\omega}}
\left[\frac{\coth{\left(\sqrt{\frac{j\omega}{D_o}}l\right)}}{c_o^*\sqrt{D_o}}+\frac{\coth{\left(\sqrt{\frac{j\omega}{D_r}}l\right)}}{c_r^*\sqrt{D_r}}\right]}\label{eq:Reflective_Zf_two_species}
\end{equation}
The total impedance (\ref{eq:Total_impedance}) is a parallel combination of the Faradaic impedance (\ref{eq:Reflective_Zf_two_species}) and the double layer impedance.
\paragraph{Simulation results}
\begin{figure}[b!]
\begin{center}
\includegraphics[width=0.49\textwidth]{\main figures/two_species_reflective_Dr_2} 
\includegraphics[width=0.49\textwidth]{\main figures/two_species_reflective_Dr_3}
\caption{Nyquist diagram for a finite-length diffusion system with reflective boundary for two electrochemical species. The figures show the effect of the reductant diffusion coefficient on the impedance plot for two values of the reductant concentration: $c_r= 3.26\times10^{-8}\,\textrm{mol}/\textrm{cm}^2$ (left) and $c_r= 6.52\times10^{-6}\,\textrm{mol}/\textrm{cm}^2$ (right). The other simulation parameters are $c_o= 3.26\times10^{-6}\,\textrm{mol}/\textrm{cm}^2$, $C_d= 4\,\mu\textrm{F}/\textrm{cm}^2$, $D_o=7\times10^{-6}\,\textrm{cm}^2/\textrm{s}$ and $l=1\,\mu\textrm{m}$. }
\label{plots:2S_RF_Dr_comp}
\end{center}
\end{figure}
\begin{figure}
\begin{center}
\includegraphics[width=0.49\textwidth,height=0.47\textwidth]{\main figures/two_species_reflective_cr_2} 
\includegraphics[width=0.49\textwidth,height=0.47\textwidth]{\main figures/two_species_reflective_cr_1}
\includegraphics[width=0.49\textwidth,height=0.47\textwidth]{\main figures/two_species_reflective_l_1}
\includegraphics[width=0.49\textwidth,height=0.47\textwidth]{\main figures/two_species_reflective_l_2}
\caption{Nyquist diagram for a finite-length diffusion system with reflective boundary for two electrochemical species. The top two figures show the effect of the reductant concentration on the impedance plot for two values of the double layer capacitance: $C_d= 400\,\textrm{nF}/\textrm{cm}^2$ (left) and $C_d= 400\,\mu\textrm{F}/\textrm{cm}^2$ (right). The other simulation parameters are $c_o= 3.26\times10^{-6}\,\textrm{mol}/\textrm{cm}^2$, $D_o=7\times10^{-6}\,\textrm{cm}^2/\textrm{s}$, $D_r=7\times10^{-5}\,\textrm{cm}^2/\textrm{s}$ and $l=2\,\mu\textrm{m}$. 
The bottom two figures show the effect of the system length on the impedance plot for two values of the reductant diffusion coefficient: $D_r=7\times10^{-8}\,\textrm{cm}^2/\textrm{s}$ (left) and $D_r=7\times10^{-5}\,\textrm{cm}^2/\textrm{s}$ (right). The other simulation parameters are $c_o= 3.26\times10^{-6}\,\textrm{mol}/\textrm{cm}^2$, $c_r= 1.63\times10^{-6}\,\textrm{mol}/\textrm{cm}^2$, $C_d= 40\,\mu\textrm{F}/\textrm{cm}^2$ and $D_o=7\times10^{-6}\,\textrm{cm}^2/\textrm{s}$.}
\label{plots:2S_RF_cr_comp}
\end{center}
\end{figure}
\begin{figure}[t!]
\begin{center}
\includegraphics[width=0.49\textwidth]{\main figures/two_species_reflective_Cd_3}
\includegraphics[width=0.49\textwidth]{\main figures/two_species_reflective_Cd_2}
\caption{Nyquist diagram for a finite-length diffusion system with reflective boundary for two electrochemical species. The figures show the effect of the double layer capacitance on the impedance plot for two values of the system length: $l=1\,\mu\textrm{m}$ (left) and $l=100\,\mu\textrm{m}$ (right). The other simulation parameters are $c_o= 3.26\times10^{-6}\,\textrm{mol}/\textrm{cm}^2$, $c_r= 3.26\times10^{-6}\,\textrm{mol}/\textrm{cm}^2$, $D_o=7\times10^{-6}\,\textrm{cm}^2/\textrm{s}$ and $D_r=7\times10^{-5}\,\textrm{cm}^2/\textrm{s}$.}
\label{plots:2S_RF_Cd_comp}
\end{center}
\end{figure}
\end{document}