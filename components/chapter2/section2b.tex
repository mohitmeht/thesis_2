\documentclass[../../thesis.tex]{subfiles}
\providecommand{\main}{../../}
\begin{document}
\section{Semi-infinite linear diffusion of two electrochemical species}
\label{sec:two_species_semi_infinite}
In the previous section, we considered the diffusion of only one electrochemical specie (oxidant). In this section, we allow both species (oxidant and reductant) to be governed by mass transport. We would like to point out once again that in this manuscript we neglect the contribution of convection and migration to mass transport. For a detailed discussion on this section of two electrochemical diffusing species for semi-infinite boundary conditions we recommend~\cite{Lasia2002}.
Here, the two electroactive species the oxidant ($O$) and the reductant ($R$) are allowed to diffuse towards and away from the electrode.
\begin{equation}
\ce{O + \textit{n}e <=>[k_f][k_b] R}\nonumber
\end{equation}
\begin{figure}[h!]
\begin{center}
\includegraphics[keepaspectratio,height=0.47\textheight]{\main figures/two_species_semi_infinite_concentration_profile}
\end{center}
\caption{Concentration profiles of the oxidant and reductant with a transmissive boundary at 0 d.c. and increasing d.c. currents.}
\label{two_specie_diffusion}
\end{figure}
Mass transport of two diffusing species is given by Fick's second law of diffusion,
\begin{align}
\frac{\partial c_{o}(x,t)}{\partial t}=D_o \frac{\partial^2 c_o(x,t)}{\partial^2 x}\label{eq:ficks_second_law_of_diff_two_species_O}\\
\frac{\partial c_{r}(x,t)}{\partial t}=D_r \frac{\partial^2 c_r(x,t)}{\partial^2 x}\label{eq:ficks_second_law_of_diff_two_species_R}
\end{align}
where, $c_r(x,t)$ is the reductant concentration and $D_r$ is the reductant diffusion constant. The initial and boundary conditions are,
\begin{enumerate}
\item[(a)] \textbf{Initial conditions}
\begin{align}
c_o(x,0)&=c_o^*\label{bc:semi_infinite_2_species_initial_O}\\
c_r(x,0)&=c_r^*\label{bc:semi_infinite_2_species_initial_R}
\end{align}
\item[(b)] \textbf{Boundary conditions at x=$\infty$}
\begin{align}
c_o(\infty,t)&=c_o^*\label{bc:semi_infinite_2_species_boundary_O}\\
c_r(\infty,t)&=c_r^*\label{bc:semi_infinite_2_species_boundary_R}
\end{align}
\item[(c)] \textbf{Boundary conditions at the electrode/electrolyte interface}
\begin{align}
D_o\left.\frac{\partial c_{o}(x,t)}{\partial x}\right|_{x=0}=\frac{i_F(t)}{nFA}\label{bc:semi_infinite_2_species_interface_O}\\
D_r\left.\frac{\partial c_{r}(x,t)}{\partial x}\right|_{x=0}=-\frac{i_F(t)}{nFA} \label{bc:semi_infinite_2_species_interface_R}
\end{align}
\end{enumerate}
The solution of the diffusion equation should also meet the condition of conservation of matter in an electrode reaction.
\begin{align}
D_o\left[\frac{\partial c_o(x,t)}{\partial x}\right]_{x=0}+D_r\left[\frac{\partial c_r(x,t)}{\partial x}\right]_{x=0}=0\label{bc:matter_conservation_at_electrode}
\end{align}
The Butler-Volmer equation for reaction kinetics of the two diffusing species is given by eqn.~(\ref{eq:butler_volmer_eq}), which is,
\begin{equation}
i_F=i_0\cdot{\left[\frac{c_o(0,t)}{c_o^*}e^{\frac{-\beta n}{V_T}\eta}-\frac{c_r(0,t)}{c_r^*}e^{\frac{(1-\beta) n}{V_T}\eta}\right]}\label{eq:butler_volmer_eq_two_species_semi_infinite}
\end{equation}
\paragraph{Calculation of impedance spectra} In order to calculate the impedance spectra we apply the perturbation theory to the faradaic current ($\delta i_F\,e^{j\omega t}$), over-potential ($\delta\eta\,e^{j\omega t}$), oxidant concentration ($\delta c_o\,e^{j\omega t}$), and reductant concentration ($\delta c_r\,e^{j\omega t}$). Small-signal faradaic current can be expressed using the Taylor series expansion. Keeping only the linear terms in the Taylor expansion of eqn.~(\ref{eq:butler_volmer_eq_two_species_semi_infinite}) and neglecting the high order terms, 
\begin{equation}
\delta i_F=\left(\frac{\partial i_F}{\partial\eta}\right)\delta\eta+
\left(\frac{\partial i_F}{\partial c_o}\right)\delta c_o(0)+
\left(\frac{\partial i_F}{\partial c_r}\right)\delta c_r(0) \label{eq:two_specie_infinite_tayor}
\end{equation}
The partial derivatives in the above equation is obtained by taking partial derivatives of the Faradaic current (\ref{eq:butler_volmer_eq_two_species_semi_infinite}) w.r.t over-potential, oxidant and reductant concentration respectively.
\begin{subequations}
\begin{align}
\left(\frac{\partial i_F}{\partial\eta}\right)&=
-\frac{i_0n}{V_T}\left[\frac{\beta c_o(0,t)}{c_o^*}e^{-\frac{\beta n}{V_T}\eta}+
\frac{\left(1-\beta\right)c_r(0,t)}{c_r^*}e^{\frac{\left(1-\beta\right)n}{V_T}\eta}\right]\label{eq:two_specie_di/dn}\\
\left(\frac{\partial i_F}{\partial c_o}\right)&=
\frac{i_0}{c_o^*}e^{-\frac{\beta n}{V_T} \eta}\label{eq:two_specie_di/dco}\\
\left(\frac{\partial i_F}{\partial c_r}\right)&=
-\frac{i_0}{c_r^*}e^{ \frac{\left (1-\beta \right)n}{V_T}\eta}\label{eq:two_specie_di/dcr}
\end{align}
\end{subequations}
Now, we substitute these partial derivatives into eqn~(\ref{eq:two_specie_infinite_tayor}),
\begin{align}
\delta i_F=
-&\frac{i_0n}{V_T}\left[\frac{\beta c_o(0,t)}{c_o^*}e^{-\frac{\beta n}{V_T}\eta}+
\frac{\left(1-\beta\right)c_r(0,t)}{c_r^*}e^{\frac{\left(1-\beta\right)n}{V_T}\eta}\right]\delta\eta\nonumber\\
+&\frac{i_0}{c_o^*}e^{-\frac{\beta n}{V_T} \eta}\delta c_o(0)
-\frac{i_0}{c_r^*}e^{ \frac{\left (1-\beta \right)n}{V_T}\eta}\delta c_r(0)\label{eq:two_specie_infinite_tayor_with_substitution}
\end{align}
The equations for $\delta c_o$ and $\delta c_r$ are obtained by solving the small-signal diffusion equations, which are,
\begin{align}
\frac{\partial\left[\delta c_{o}(x)\,e^{j\omega t}\right]}{\partial t}&=D_o \frac{\partial^2\left[\delta c_o(x)\,e^{j\omega t}\right]}{\partial^2 x}\label{eq:ficks_second_law_of_diff_two_species_ac_O}\\
\frac{\partial\left[\delta c_{r}(x)\,e^{j\omega t}\right]}{\partial t}&=D_r \frac{\partial^2\left[\delta c_r(x)\,e^{j\omega t}\right]}{\partial^2 x}
\label{eq:ficks_second_law_of_diff_two_species_ac_R}
\end{align}
after taking the time derivatives we get,
\begin{align}
j\omega \delta c_o&=D_o\frac{d^2\delta c_o}{dx^2}\label{eq:ficks_small_signal_two_specie_semi_infinite_O}\\
j\omega \delta c_r&=D_r\frac{d^2\delta c_r}{dx^2}\label{eq:ficks_small_signal_two_specie_semi_infinite_R}
\end{align}
The above equations are subject to the small-signal semi-infinite boundary conditions which are obtained by linearizing eqns.~(\ref{bc:semi_infinite_2_species_boundary_O}) and~(\ref{bc:semi_infinite_2_species_boundary_R}). 
\begin{align}
\left.\delta c_o(x)\right|_{x=\infty}&=0\label{bc:two_specie_x_to_inf_O}
\end{align}
\begin{align}
\left.\delta c_r(x)\right|_{x=\infty}&=0\label{bc:two_specie_x_to_inf_R}
\end{align}
and the two small-signal boundary condition at the electrode/electrolyte interface are obtained by linearizing eqns.~(\ref{bc:semi_infinite_2_species_interface_O}) and~(\ref{bc:semi_infinite_2_species_interface_R}):
\begin{align}
D_o\left.\frac{d\,\delta c_o(x)}{dx}\right|_{x=0}&=\frac{\delta i_F}{nFA}
\label{bc:two_species_x_to_0_ficks_first_law_O}\\
D_r\left.\frac{d\,\delta c_r(x)}{dx}\right|_{x=0}&=-\frac{\delta i_F}{nFA} \label{bc:two_species_x_to_0_ficks_first_law_R}
\end{align}
and eqn.~(\ref{bc:matter_conservation_at_electrode}),
\begin{align}
D_o\frac{d\,\delta c_o}{dx}+D_r\frac{d\,\delta c_r}{dx}=0\label{bc:matter_conservation_at_electrode_linearized}
\end{align}
The small-signal concentration of the oxidant and the reductant is obtained by solving the eqns~(\ref{eq:ficks_small_signal_two_specie_semi_infinite_O}) and~(\ref{eq:ficks_small_signal_two_specie_semi_infinite_R}), respectively.
The solution of the homogeneous equations are,
\begin{align}
\delta c_o(x)&=C_1\,e^{-\sqrt{\frac{j\omega}{D_o}}x}+C_2\,e^{\sqrt{\frac{j\omega}{D_o}}x}\label{eq:two_specie_solution_to_ficks_diffusion_law_O}\\
\delta c_r(x)&=C'_1\,e^{-\sqrt{\frac{j\omega}{D_r}}x}+C'_2\,e^{\sqrt{\frac{j\omega}{D_r}}x}\label{eq:two_specie_solution_to_ficks_diffusion_law_R}
\end{align}
Integration constants $C_2$ and $C_1$ are obtained by applying the boundary conditions~(\ref{bc:two_specie_x_to_inf_O}) and~(\ref{bc:two_species_x_to_0_ficks_first_law_O}) to the  eqn.~(\ref{eq:two_specie_solution_to_ficks_diffusion_law_O}),
\begin{align}
C_1&=-\frac{\delta i_F}{nFA\sqrt{j\omega D_o}}\nonumber\\
C_2&=0\label{eq:two_species_integration_constants_C2_C'2_O}
\end{align}
The integration constants $C'_2$ and $C'_1$ are obtained by applying boundary conditions~(\ref{bc:two_specie_x_to_inf_R}) and~(\ref{bc:two_species_x_to_0_ficks_first_law_R}) to the eqn.~(\ref{eq:two_specie_solution_to_ficks_diffusion_law_O}),
\begin{align}
C'_1&=\frac{\delta i_F}{nFA\sqrt{j\omega D_r}}\nonumber
\end{align}
\begin{align}
C'_2&=0\label{eq:two_species_integration_constants_C2_C'2_R}
\end{align}
Now, we introduce the eqns.~(\ref{eq:two_species_integration_constants_C2_C'2_O}) and~(\ref{eq:two_species_integration_constants_C2_C'2_R}) into eqn.~(\ref{eq:two_specie_solution_to_ficks_diffusion_law_O}) and~(\ref{eq:two_specie_solution_to_ficks_diffusion_law_R}) respectively.
\begin{align}
\delta c_o(x)&=-\frac{\delta i_F}{nFA\sqrt{j\omega D_o}}\,e^{-\sqrt{\frac{j\omega}{D_o}}x}\label{eq:two_specie_complete_solution_to_ficks_diffusion_law_O}\\
\delta c_r(x)&=\frac{\delta i_F}{nFA\sqrt{j\omega D_r}}\,e^{-\sqrt{\frac{j\omega}{D_r}}x}\label{eq:two_specie_complete_solution_to_ficks_diffusion_law_R}
\end{align}
and substitute the equations for $\delta c_o(0)$ and $\delta c_r(0)$ into the eqn~(\ref{eq:two_specie_infinite_tayor_with_substitution}),
\begin{align}
\delta i_F=
-&\frac{i_0n}{V_T}\left[\frac{\beta c_o(0,t)}{c_o^*}e^{-\frac{\beta n}{V_T}\eta}+
\frac{\left(1-\beta\right)c_r(0,t)}{c_r^*}e^{\frac{\left(1-\beta\right)n}{V_T}\eta}\right]\delta\eta\nonumber\\
-&\frac{i_0}{c_o^*}e^{-\frac{\beta n}{V_T} \eta}\frac{\delta i_F}{nFA\sqrt{j\omega D_o}}
-\frac{i_0}{c_r^*}e^{ \frac{\left (1-\beta \right)n}{V_T}\eta}\frac{\delta i_F}{nFA\sqrt{j\omega D_r}}\label{eq:small_signal_faradaic_current_two_species_semi_infinite_condition}
\end{align}
The faradaic impedance ($Z_F$) is given by,
\begin{align}
Z_F=-\frac{\delta\eta}{\delta i_F}\nonumber
\end{align}
which results into the following equation:
\begin{align}
Z_F(c_o(0,t),c_r(0,t),\eta)=\frac{1+\frac{i_0}{nFA\sqrt{j\omega}} \left[\frac{1}{c_o^*\sqrt{D_o}}e^{-\frac{\beta n}{V_T}\eta}+
\frac{1}{c_r^*\sqrt{D_r}}e^{\frac{\left(1-\beta\right)n}{V_T}\eta}
\right]}
{\frac{i_0n}{V_T} \left[\frac{\beta c_o(0,t)}{c_o^*}e^{-\frac{\beta n}{V_T}\eta} + \frac{\left(1-\beta\right)c_r(0,t)}{c_r^*}e^{\frac{\left(1-\beta\right)n}{V_T}\eta}\right]
}\label{eq:two_specie_taylor_expansion_result}
\end{align}
This gives us the Faradaic impedance under d.c. discharge and also for different surface concentrations.
However, under 0 d.c. discharge;~$\eta=0$,~$c_o(0,t)=c_o^*$ and~$c_r(0,t)=c_r^*$ and the Faradaic impedance is given by,
\begin{align}
\boxed{Z_F=
R_\eta+
\frac{V_T}{n^2FA\sqrt{j\omega}}
\left[
\frac{1}{c_o^*\sqrt{D_o}}+
\frac{1}{c_r^*\sqrt{D_r}}
\right]}\label{eq:Zf_two_species}
\end{align}
%The second term of the above equation represents the Warburg impedance for two diffusing species. The difference between Warburg impedance for one and two diffusing specie(s) is an additional term $\left(\frac{1}{c_r^*\sqrt{(D_r)}}\right)$ for additional specie. 
The total impedance (\ref{eq:Total_impedance}) is a parallel combination of the Faradaic (\ref{eq:Zf_two_species}) and the double layer impedance.
% The simulation results for varying diffusion coefficient (fig.~\ref{plots:2S_SI_Dr_comp}), the double layer capacitance (fig.~\ref{plots:2S_SI_Cd_comp} (top)) and the reductant concentration (fig.~\ref{plots:2S_SI_Cd_comp} (bottom)) andshows how these parameters can influence the impedance spectra of a system of single diffusion specie with a semi-infinite boundary condition.
\begin{figure}[t!]
\begin{center}
\includegraphics[width=0.47\textwidth]{\main figures/two_species_SM_Cd_1} 
\includegraphics[width=0.47\textwidth]{\main figures/two_species_SM_Cd_2} 
\includegraphics[width=0.47\textwidth]{\main figures/two_species_SM_cr_1} 
\includegraphics[width=0.47\textwidth]{\main figures/two_species_SM_cr_2} 
\caption{Nyquist diagram for a semi-infinite linear diffusion system with two electrochemical species. The top two figures show the effect of the double layer capacitance on the impedance plot for two values of the reductant concentration: $c_r= 3.26\times10^{-7}\,\textrm{mol}/\textrm{cm}^2$ (top left) and $c_r= 3.26\times10^{-6}\,\textrm{mol}/\textrm{cm}^2$ (top right). The other simulation parameters are $D_o=7\times10^{-6}\,\textrm{cm}^2/\textrm{s}$, $D_r=7\times10^{-7}\,\textrm{cm}^2/\textrm{s}$ and $c_o= 3.26\times10^{-6}\,\textrm{mol}/\textrm{cm}^2$. The bottom two figures show the effect of the reductant concentration on the impedance plot for two values of the double layer capacitance: $C_d= 4\textrm{m} \textrm{F}/\textrm{cm}^2$ (bottom left) and $C_d= 4\mu \textrm{F}/\textrm{cm}^2$ (bottom right). The other simulation parameters are $D_o=7\times10^{-6}\,\textrm{cm}^2/\textrm{s}$, $D_r=7\times10^{-6}\,\textrm{cm}^2/\textrm{s}$ 
and $c_o=3.26\times10^{-6}\,\textrm{mol}/\textrm{cm}^2$.}
\label{plots:2S_SI_Cd_comp}
\end{center}
\end{figure}
\begin{figure}[htp!]
\begin{center}
\includegraphics[width=0.49\textwidth]{\main figures/two_species_SM_Dr_1} 
\includegraphics[width=0.49\textwidth]{\main figures/two_species_SM_Dr_2} 
\caption{Nyquist diagram for a semi-infinite linear diffusion system of two electrochemical species. The figures show the effect of the diffusion coefficient on the impedance plot for two values of reductant concentration: $c_r= 3.26\times10^{-8}\,\textrm{mol}/\textrm{cm}^2$ (left) and $c_r= 3.26\times10^{-6}\,\textrm{mol}/\textrm{cm}^2$ (right). The other simulation parameters are $D_o=7\times10^{-6}\,\textrm{cm}^2/\textrm{s}$, $c_o= 3.26\times10^{-6}\,\textrm{mol}/\textrm{cm}^2$ and $C_d= 4\mu \textrm{F}/\textrm{cm}^2$.}
\label{plots:2S_SI_Dr_comp}
\end{center}
\end{figure}
\end{document}