\documentclass[../thesis.tex]{subfiles}
\providecommand{\main}{../}
\begin{document}

\chapter{EIS of Li-air batteries under d.c. charge and discharge}

\label{chap:EIS_under_dc_charge}The model developed in the previous
chapter for the EIS spectra can be applied only for the d.c. discharge
of Li-air batteries because the Butler-Volmer (BV) equation that is
used to describe the kinetics at the cathode was valid only under
d.c. discharge. In this chapter a slightly more elaborate expression
for the reaction kinetics, which is valid under both d.c. charge and
d.c. discharge is presented. First, we present the reaction model,
then we proceed with the derivation of the oxygen concentration and
Faradaic current at steady-state and, then, we derive the expression
for the complex impedance. As a particular case we will recover the
results obtained in the previous chapter under high d.c. discharge. 


\section{Introduction}

The Butler-Volmer relation can describe reaction kinetics at the electrode/electrolyte
interface for both charging and discharging. While charging, the discharge
products in Li-air batteries such as \ce{Li2O2} decomposes into lithium
ions and oxygen; the lithium ions migrate through the electrolyte
and the separator, and deposit on the lithium anode; the oxygen diffuses
through the electrolyte and is returned to the atmosphere. \begin{equation} \ce{2Li+ + O2 + 2e- <=> Li2O2} \end{equation}
If the rate of the electrochemical reaction at the cathode can be
descried by BV eq. (\ref{eq:k_BV_w_beta_k}), eqns.~\ref{eq:ZF_final_sinh}
and \ref{eq:ZF_final_FBV_non_sinh} can be used to describe the EIS
during both charge and discharge. However, if the rate of the electrochemical
reaction at the cathode deviates from this functional form, eqns.~\ref{eq:ZF_final_sinh}
and \ref{eq:ZF_final_FBV_non_sinh} do not correctly predict the EIS
of LABs. In practical batteries, the electrochemical reactions are
often irreversible and the forward and reverse reactions proceed at
different rates. For this reason, in this chapter, we consider the
case of Butler-Volmer kinetics with different forward and reverse
reaction rates and non-equal electron transfer factors. Specifically,
we assume that the specific reaction rate in the cathode is given
as
\begin{equation}
R_{c}=nF\left[k^{r}-k^{f}c_{o}\left(x\right)\right]\label{eqn:Charging_FBV_Rc}
\end{equation}
where the reaction rate constant for the forward reaction is given
by 
\begin{equation}
k^{f}=ak_{0}^{f}e^{\frac{-\beta n}{V_{T}}\eta}.\label{eq:kf_def_FBV}
\end{equation}
where $a$ is the specific area of the cathode, $k_{0}^{f}$ (measured
in \SI[per-mode=symbol]{}{\centi\meter\per\second}) is the standard
reaction rate constant for the forward reaction, $\beta$ is the electron
transfer coefficient ($0<\beta<1$), $n$ is the number of electrons
transferred in electrochemical reaction, and $\eta$ is the over-potential
(it is the potential drop between the electrode surface and the bulk
layer). The reaction rate constant for the reverse reaction is given
by 
\begin{equation}
k^{r}=ak_{0}^{r}e^{\frac{(1-\beta)n}{V_{T}}\eta}\label{eq:kr_def_FBV}
\end{equation}
where $k_{0}^{r}$ is the standard reverse reaction rate (measured
in \SI[per-mode=symbol]{}{\mol\per\centi\meter\squared\per\second}). 

Under equilibrium conditions, $R_{c}=0$, $\eta=0$ and $c_{o}\left(x\right)=c_{o}^{*}$,
the reverse reaction rate is related to the forward reaction rate
by (using eqn. \ref{eqn:Charging_FBV_Rc}) 
\begin{equation}
k^{r}=k^{f}c_{o}^{*}\label{eq:relation_k0f_and_k0r}
\end{equation}
During charge $k^{f}>0$ and $k^{r}>0$. During discharge $k^{f}<0$ and
$k^{r}<0$.


\section{Steady-state analysis}

The mass transport of oxygen inside the cathode is described using
Fick's second law of diffusion. It is important to note that in this
analysis the effects of convection are neglected. For this reason
the model presented in this section cannot be applied to compute the
impedance response of Li-air flow batteries, where the electrolyte
is continuously flowing.


\subsection{Oxygen concentration at steady-state}

Since the oxygen in the electrolyte carries no charge the migration
term in the Nernst-Plank equation can be neglected and the mass-transport
of oxygen in Li-air batteries under steady-state conditions can be
described by
\[
\frac{\partial^{2}c_{o}(x)}{\partial x^{2}}-\frac{R_{c}}{nFD_{\textrm{eff}}}=0
\]
Using $R_{c}$ from eqn.~\ref{eqn:Charging_FBV_Rc}, the mass transport
equation becomes 
\begin{equation}
\frac{\partial^{2}c_{o}\left(x\right)}{\partial x^{2}}+\frac{k^{f}}{D_{\textrm{eff}}}c_{o}\left(x\right)-\frac{k^{r}}{D_{\textrm{eff}}}=0\label{eq:Ficks_second_law_FBV_steady_state}
\end{equation}
The homogenous solution to the above differential equation \ref{eq:Ficks_second_law_FBV_steady_state}
is 
\[
c_{o_{c}}\left(x\right)=C_{1}\cos\left(\sqrt{\frac{k^{f}}{D_{\textrm{eff}}}}x\right)+C_{2}\sin\left(\sqrt{\frac{k^{f}}{D_{\textrm{eff}}}}x\right)
\]
where $C_{1}$ and $C_{2}$ are the integration constants. Assuming
the particular solution to be a constant 
\[
c_{o_{p}}\left(x\right)=C
\]
eqn. \ref{eq:Ficks_second_law_FBV_steady_state} gives
\[
\frac{\partial^{2}C}{\partial x^{2}}+\frac{k^{f}}{D_{\textrm{eff}}}C-\frac{k^{r}}{D_{\textrm{eff}}}=0
\]
The particular solution becomes 
\[
c_{o_{p}}\left(x\right)=C=\frac{k^{r}}{k^{f}}
\]
and the complete solution for eqn. \ref{eq:Ficks_second_law_FBV_steady_state}
is 
\begin{equation}
c_{o}\left(x\right)=C_{1}\cos\left(\sqrt{\frac{k^{f}}{D_{\textrm{eff}}}}x\right)+C_{2}\sin\left(\sqrt{\frac{k^{f}}{D_{\textrm{eff}}}}x\right)+\frac{k^{r}}{k^{f}}\label{eq:Partial_solution_to_Ficks_second_law_charging_steady_state}
\end{equation}
The integration constants are computed using the following boundary
conditions: \begin{subequations} 
\begin{enumerate}
\item []\textbf{Boundary condition at $x=0$} 
\begin{equation}
c_{o}(0)=c_{o}^{*}\label{bc:charging_c_at_0}
\end{equation}

\item []\textbf{Boundary condition at $x=l$} 
\begin{equation}
\left.\frac{\textrm{d}c_{o}(x)}{\textrm{d}x}\right|_{x=l}=0\label{bc:charging_c_at_l}
\end{equation}

\end{enumerate}
\end{subequations} The expressions for the integration constants
($C_{1}$ and $C_{2}$) are obtained by using the boundary conditions
(eqns.~\ref{bc:charging_c_at_0} and~\ref{bc:charging_c_at_l})
and eqn.~\ref{eq:Partial_solution_to_Ficks_second_law_charging_steady_state},
\begin{align}
C_{1} & =\left(c_{o}^{*}-\frac{k^{r}}{k^{f}}\right)\label{eq:charging_C1}\\
C_{2} & =\left(c_{o}^{*}-\frac{k^{r}}{k^{f}}\right)\tan\left(\sqrt{\frac{k^{f}}{D_{\textrm{eff}}}}l\right)\label{eq:charging_C2}
\end{align}
The final expression for the oxygen concentration distribution inside
the cathode while charging is obtained by using eqns. \ref{eq:Partial_solution_to_Ficks_second_law_charging_steady_state},
\ref{eq:charging_C1}, and \ref{eq:charging_C1}. 
\begin{equation}
\boxed{c_{o}(x)=c_{chg}\left[\cos\left(\sqrt{\frac{k^{f}}{D_{\textrm{eff}}}}x\right)+\tan\left(\sqrt{\frac{k^{f}}{D_{\textrm{eff}}}}l\right)\sin\left(\sqrt{\frac{k^{f}}{D_{\textrm{eff}}}}x\right)\right]+\frac{k^{r}}{k^{f}}}\label{eqn:charging_co_steady_state_solution}
\end{equation}
where 
\begin{equation}
c_{chg}=\left(c_{o}^{*}-\frac{k^{r}}{k^{f}}\right)\label{eq:charging_c_chg_def}
\end{equation}



\subsection{Faradaic current at steady-state}

The Faradaic component ($i_{F}$) of the discharge current can be
computed using 
\begin{equation}
i_{F}=A\int_{0}^{l}R_{c}\,\textrm{d}x
\end{equation}
Substituting $R_{c}$ from eqn~\ref{eqn:Charging_FBV_Rc} into the
above equation, we obtain 
\begin{equation}
i_{F}=-A\int_{0}^{l}nF\left[k^{f}c_{o}\left(x\right)-k^{r}\right]\,\textrm{d}x\label{eq:FBV_I_F_w_integrals}
\end{equation}
Substituting $c_{o}\left(x\right)$ from eqn.~\ref{eqn:charging_co_steady_state_solution}
and separating the integrals
\begin{equation}
i_{F}=-nFAk^{f}c_{chg}\int_{0}^{l}\left[\cos\left(\sqrt{\frac{k^{f}}{D_{\textrm{eff}}}}x\right)+\tan\left(\sqrt{\frac{k^{f}}{D_{\textrm{eff}}}}l\right)\sin\left(\sqrt{\frac{k^{f}}{D_{\textrm{eff}}}}x\right)\right]\,\textrm{d}x
\end{equation}
Splitting the integral into two terms and computing the first term
gives
\[
i_{F}=-nFAk^{f}c_{chg}\left[\sqrt{\frac{D_{\textrm{eff}}}{k^{f}}}\left[\sin\left(\sqrt{\frac{k^{f}}{D_{\textrm{eff}}}}x\right)\right]_{0}^{l}+\tan\left(\sqrt{\frac{k^{f}}{D_{\textrm{eff}}}}l\right)\int_{0}^{l}\sin\left(\sqrt{\frac{k^{f}}{D_{\textrm{eff}}}}x\right)\,\textrm{d}x\right]
\]
Similarly, computing the second integral and applying the limits,
yields
\[
i_{F}=-nFAk^{f}c_{chg}\sqrt{\frac{D_{\textrm{eff}}}{k}}\left\{ \sin\left(\sqrt{\frac{k}{D_{\textrm{eff}}}}l\right)+\tan\left(\sqrt{\frac{k^{f}}{D_{\textrm{eff}}}}l\right)\left[1-\cos\left(\sqrt{\frac{k^{f}}{D_{\textrm{eff}}}}l\right)\right]\right\} 
\]
After simplifying the above equation and using eqn. \ref{eq:charging_c_chg_def},
the final expression for the steady-state Faradaic current is 
\begin{equation}
\boxed{i_{F}=-nFAk^{f}\left(c_{o}^{*}-\frac{k^{r}}{k^{f}}\right)\sqrt{\frac{D_{\textrm{eff}}}{k^{f}}}\tan\left(\sqrt{\frac{k^{f}}{D_{\textrm{eff}}}}l\right)}\label{eq:charging_steady_state_final_equation}
\end{equation}
This equations expresses the Faradaic current as a function of the
forward and backward reaction rate constants, effective diffusion
coefficient of oxygen in the electrolyte, length of the cathode, and
oxygen concentration in the organic electrolyte.


\subsection{Steady-state Faradaic current during discharge}

The model presented in eqn. \ref{eq:charging_steady_state_final_equation}
can describe the steady-state Faradaic current during both charge
and discharge. However, during d.c. discharge, both $k^{r}$ and $k^{r}$
are negative, and eq. \ref{eq:charging_steady_state_final_equation}
can be rewritten into a slightly different from more appropriate for
the numerical implementation. Indeed, using the relation $\tanh\left(jx\right)=j\tan\left(x\right)$,
where where $j=\sqrt{-1}$, eq. \ref{eq:charging_steady_state_final_equation}
becomes
\begin{equation}
i_{F}=nFAk^{f}\left(c_{o}^{*}-\frac{\left|k^{r}\right|}{\left|k^{f}\right|}\right)\sqrt{\frac{D_{\textrm{eff}}}{\left|k^{f}\right|}}\tanh\left(\sqrt{\frac{\left|k^{f}\right|}{D_{\textrm{eff}}}}l\right)\label{eq:charging_vs_discharge_steady_state_final_equation}
\end{equation}


During high discharge one can set $k^{r}=0$ and $k^{f}=-k$ in eqn.
\ref{eq:charging_steady_state_final_equation} and obtain 
\[
i_{F}=nFAkc_{o}^{*}\sqrt{\frac{D_{\textrm{eff}}}{k}}\tanh\left(\sqrt{\frac{k}{D_{\textrm{eff}}}}l\right)
\]
In this way, we have have recovered the expression for the Faradaic
current obtained by using Tafel relation (eqn. \ref{eq:dc_i_F_complete_soln}).


\section{Small-signal analysis}

Now we assume that an external sinusoidal signal of small amplitude
with angular frequency $\omega$ is superimposed on d.c. value of
the d.c. current. In the limit of small perturbations the oxygen concentration,
over-potential, forward reaction rate constant, reverse reaction rate
constant, and the Faradaic current can be written as
\begin{align}
V & +\delta v\,e^{j\omega t}\textrm{, }\nonumber \\
i & +\delta i\,e^{j\omega t}\textrm{, }\nonumber \\
i_{F} & +\delta i_{F}\,e^{j\omega t}\textrm{, }\nonumber \\
i_{dl} & +\delta i_{dl}\,e^{j\omega t}\textrm{, and }\nonumber \\
\eta & +\delta\eta\,e^{j\omega t}\textrm{ respectively.}\label{eq:ss_rep_FBV}
\end{align}
where $j=\sqrt{-1}$ and $\delta c_{o}\left(x\right)$, $\delta\eta$,
and $\delta i_{F}$ are the amplitudes of the a.c. signal. The oxygen
concentration in the cathode can be described using Fick's second
law of diffusion,
\[
\epsilon_{0}\frac{\partial c_{o}(x)}{\partial t}=D_{\textrm{eff}}\frac{\partial^{2}c_{o}(x)}{\partial x^{2}}-\frac{R_{c}}{nF}
\]
Using eqn. \ref{eq:ss_rep_FBV}, the above equation can be rewritten
as
\begin{multline}
\epsilon_{0}\frac{\partial\left[c_{o}\left(x\right)+\delta c_{o}\left(x\right)\,e^{j\omega t}\right]}{\partial t}=  D_{\textrm{eff}}\frac{\partial^{2}\left[c_{o}\left(x\right)+\delta c_{o}\left(x\right)\,e^{j\omega t}\right]}{\partial x^{2}} \\
+\left(k^{f}+\delta k^{f}\,e^{j\omega t}\right)\left(c_{o}\left(x\right)+\delta c_{o}\left(x\right)\,e^{j\omega t}\right)-\left(k^{r}+\delta k^{r}\,e^{j\omega t}\right)
\end{multline}
Using eqn.~\ref{eq:Ficks_second_law_FBV_steady_state} and neglecting
the term $\delta k_{1}\delta c_{o}(x)$ (since their product is negligible),
the above equation reduces to
\[
\frac{\partial^{2}\delta c_{o}\left(x\right)}{\partial x^{2}}+\frac{\left(k^{f}-j\omega\epsilon_{0}\right)}{D_{\textrm{eff}}}\delta c_{o}\left(x\right)+\frac{c_{o}\left(x\right)}{D_{\textrm{eff}}}\delta k^{f}-\frac{\delta k^{r}}{D_{\textrm{eff}}}=0
\]
Rearranging the above equation yields 
\begin{equation}
\frac{\partial^{2}\delta c_{o}\left(x\right)}{\partial x^{2}}+\frac{\Delta k^{f}}{D_{\textrm{eff}}}\delta c_{o}\left(x\right)=\frac{\delta k^{r}}{D_{\textrm{eff}}}-\frac{c_{o}\left(x\right)}{D_{\textrm{eff}}}\delta k^{f}\label{eq:charging_Ficks_second_law_small_signal}
\end{equation}
where$\Delta k^{f}=k^{f}-j\omega\epsilon_{0}$. Substituting the expression
for oxygen concentration $c_{o}\left(x\right)$ from eqn.~\ref{eqn:charging_co_steady_state_solution}
into eqn.~\ref{eq:charging_Ficks_second_law_small_signal}
\[
\frac{\partial^{2}\delta c_{o}\left(x\right)}{\partial x^{2}}+\frac{\Delta k^{f}}{D_{\textrm{eff}}}\delta c_{o}\left(x\right)+\frac{c_{chg}\left[\cos\left(\sqrt{\frac{k^{f}}{D_{\textrm{eff}}}}x\right)+\tan\left(\sqrt{\frac{k^{f}}{D_{\textrm{eff}}}}l\right)\sin\left(\sqrt{\frac{k^{f}}{D_{\textrm{eff}}}}x\right)\right]+\frac{k^{r}}{k^{f}}}{D_{\textrm{eff}}}\delta k^{f}-\frac{\delta k^{r}}{D_{\textrm{eff}}}=0
\]
where $c_{chg}=\left(c_{o}^{*}-\frac{k^{r}}{k^{f}}\right)$. Rearranging
the above equation leads to 
\begin{equation}
\frac{\partial^{2}\delta c_{o}\left(x\right)}{\partial x^{2}}+\frac{\Delta k^{f}}{D_{\textrm{eff}}}\delta c_{o}\left(x\right)=\frac{\delta k^{r}}{D_{\textrm{eff}}}-\frac{c_{chg}\left[\cos\left(\sqrt{\frac{k^{f}}{D_{\textrm{eff}}}}x\right)+\tan\left(\sqrt{\frac{k^{f}}{D_{\textrm{eff}}}}l\right)\sin\left(\sqrt{\frac{k^{f}}{D_{\textrm{eff}}}}x\right)\right]\delta k^{f}}{D_{\textrm{eff}}}-\frac{k^{r}\delta k^{f}}{k^{f}D_{\textrm{eff}}}\label{eq:charging_small_signal_differential}
\end{equation}
The homogeneous solution of the above equation is
\begin{equation}
\delta c_{o_{c}}\left(x\right)=C_{1}\cos\left(\sqrt{\frac{\Delta k^{f}}{D_{\text{eff}}}}x\right)+C_{2}\sin\left(\sqrt{\frac{\Delta k^{f}}{D_{\text{eff}}}}x\right)
\end{equation}
where $C_{1}$ and $C_{2}$ are the integration constants and $\delta c_{o_{c}}\left(x\right)$
denotes the complementary solution for the small-signal oxygen concentration.
Assuming the particular solution for the differential eqn.~\ref{eq:charging_small_signal_differential}
to be
\begin{equation}
\delta c_{o_{p}}(x)=C_{3}\cos\left(\sqrt{\frac{k^{f}}{D_{\textrm{eff}}}}x\right)+C_{4}\sin\left(\sqrt{\frac{k^{f}}{D_{\textrm{eff}}}}x\right)+C_{5}\label{eq:charging_particular_solution}
\end{equation}
where $C_{3}$, $C_{4}$, and $C_{5}$ are the integration constants
. The complete solution for eqn. \ref{eq:charging_small_signal_differential}
is
\begin{equation}
\delta c_{o}\left(x\right)=C_{1}\cos\left(\sqrt{\frac{\Delta k^{f}}{D_{\text{eff}}}}x\right)+C_{2}\sin\left(\sqrt{\frac{\Delta k^{f}}{D_{\text{eff}}}}x\right)+C_{3}\cos\left(\sqrt{\frac{k^{f}}{D_{\textrm{eff}}}}x\right)+C_{4}\sin\left(\sqrt{\frac{k^{f}}{D_{\textrm{eff}}}}x\right)+C_{5}\label{eq:charging_complete_solution_relation}
\end{equation}
The integration constants $C_{3}$, $C_{4}$, and $C_{5}$ are obtained
by replacing $\delta c_{o}(x)$ by $\delta c_{o_{p}}(x)$ in eqn.\ref{eq:charging_small_signal_differential}
where $\delta c_{o_{p}}(x)$ is given by (eqn.~\ref{eq:charging_particular_solution}).
The left hand side (L.H.S) of equation \ref{eq:charging_small_signal_differential}
is
\begin{multline*}
L.H.S=\frac{\partial^{2}\left[C_{3}\cos\left(\sqrt{\frac{k^{f}}{D_{\textrm{eff}}}}x\right)+C_{4}\sin\left(\sqrt{\frac{k^{f}}{D_{\textrm{eff}}}}x\right)+C_{5}\right]}{\partial x^{2}}\\
+\frac{\Delta k^{f}}{D_{\textrm{eff}}}\left[C_{3}\cos\left(\sqrt{\frac{k^{f}}{D_{\textrm{eff}}}}x\right)+C_{4}\sin\left(\sqrt{\frac{k^{f}}{D_{\textrm{eff}}}}x\right)+C_{5}\right]
\end{multline*}
After taking derivatives and rearranging the above equation, one obtains
\begin{equation}
L.H.S=\frac{\Delta k^{f}}{D_{\textrm{eff}}}C_{5}-\frac{j\omega\epsilon_{0}C_{3}}{D_{\textrm{eff}}}\cos\left(\sqrt{\frac{k^{f}}{D_{\textrm{eff}}}}x\right)-\frac{j\omega\epsilon_{0}C_{4}}{D_{\textrm{eff}}}\sin\left(\sqrt{\frac{k^{f}}{D_{\textrm{eff}}}}x\right)
\end{equation}
 The right hand side (R.H.S) of eq. \ref{eq:charging_small_signal_differential}
becomes
\[
R.H.S=\frac{\delta k^{r}}{D_{\textrm{eff}}}-\frac{c_{chg}\left[\cos\left(\sqrt{\frac{k^{f}}{D_{\textrm{eff}}}}x\right)+\tan\left(\sqrt{\frac{k^{f}}{D_{\textrm{eff}}}}l\right)\sin\left(\sqrt{\frac{k^{f}}{D_{\textrm{eff}}}}x\right)\right]\delta k^{f}}{D_{\textrm{eff}}}-\frac{k^{r}\delta k^{f}}{k^{f}D_{\textrm{eff}}}
\]
Rearranging the above equation one obtains 
\[
R.H.S=\frac{k^{f}\delta k^{r}-k^{r}\delta k^{f}}{k^{f}D_{\textrm{eff}}}-\frac{c_{chg}\delta k^{f}}{D_{\textrm{eff}}}\cos\left(\sqrt{\frac{k^{f}}{D_{\textrm{eff}}}}x\right)-\frac{c_{chg}\delta k^{f}\tan\left(\sqrt{\frac{k^{f}}{D_{\textrm{eff}}}}l\right)}{D_{\textrm{eff}}}\sin\left(\sqrt{\frac{k^{f}}{D_{\textrm{eff}}}}x\right)
\]
Since L.H.S. should be equal to R.H.S. for all the values of variable
$x$, one can write
\begin{align*}
-\frac{j\omega\epsilon_{0}C_{3}}{D_{\textrm{eff}}} & =-\frac{c_{chg}\delta k^{f}}{D_{\textrm{eff}}}\\
-\frac{j\omega\epsilon_{0}C_{4}}{D_{\textrm{eff}}} & =-\frac{c_{chg}\delta k^{f}\tan\left(\sqrt{\frac{k^{f}}{D_{\textrm{eff}}}}l\right)}{D_{\textrm{eff}}}\\
\frac{\Delta k^{f}}{D_{\textrm{eff}}}C_{5} & =\frac{k^{f}\delta k^{r}-k^{r}\delta k^{f}}{k^{f}D_{\textrm{eff}}}
\end{align*}
which gives the following values for the integration constants
\begin{align}
C_{3} & =\frac{c_{chg}\delta k^{f}}{j\omega\epsilon_{0}}\\
C_{4} & =\frac{c_{chg}\delta k^{f}}{j\omega\epsilon_{0}}\tan\left(\sqrt{\frac{k^{f}}{D_{\textrm{eff}}}}l\right)\\
C_{5} & =\frac{k^{f}\delta k^{r}-k^{r}\delta k^{f}}{\Delta k^{f}k^{f}}
\end{align}
Substituting the integration constants ($C_{3}$, $C_{4}$, and $C_{5}$)
back into the particular equation (eqn. \ref{eq:charging_particular_solution})
\begin{equation}
\delta c_{o_{p}}(x)=\frac{c_{chg}\delta k^{f}}{j\omega\epsilon_{0}}\left[\tan\left(\sqrt{\frac{k^{f}}{D_{\textrm{eff}}}}l\right)\right. \left.\sin\left(\sqrt{\frac{k^{f}}{D_{\textrm{eff}}}}x\right)+\cos\left(\sqrt{\frac{k^{f}}{D_{\textrm{eff}}}}x\right)\right] +\frac{k^{f}\delta k^{r}-k^{r}\delta k^{f}}{\Delta k^{f}k^{f}}\label{eq:charging_ss_partial_co_final}
\end{equation}
Substituting the particular solution given in eqn. \ref{eq:charging_ss_partial_co_final}
into eqn. \ref{eq:charging_complete_solution_relation} the small-signal
oxygen concentration becomes
\begin{multline}
\delta c_{o}(x)  =C_{1}\cos\left(\sqrt{\frac{\Delta k^{f}}{D_{\text{eff}}}}x\right)+C_{2}\sin\left(\sqrt{\frac{\Delta k^{f}}{D_{\text{eff}}}}x\right)+\frac{c_{chg}\delta k^{f}}{j\omega\epsilon_{0}}\cos\left(\sqrt{\frac{k^{f}}{D_{\textrm{eff}}}}x\right)\\
+\frac{k^{f}\delta k^{r}-k^{r}\delta k^{f}}{\Delta k^{f}k^{f}}+\frac{c_{chg}\delta k^{f}}{j\omega\epsilon_{0}}\tan\left(\sqrt{\frac{k^{f}}{D_{\textrm{eff}}}}l\right)\sin\left(\sqrt{\frac{k^{f}}{D_{\textrm{eff}}}}x\right)\label{eq:charging_dc_complete_solution_partial}
\end{multline}
Integration constants $C_{1}$ and $C_{2}$ are obtained by applying
the following boundaries conditions\begin{subequations} 
\begin{enumerate}
\item []\textbf{Boundary condition at $x=0$} 
\begin{equation}
\left.\delta c_{o}\left(x\right)\right|_{x=0}=0\label{bc:charging_ss_at_0}
\end{equation}

\item []\textbf{Boundary condition at $x=l$} 
\begin{equation}
\left.\frac{\textrm{d}\delta c_{o}\left(x\right)}{\textrm{d}x}\right|_{x=l}=0\label{bc:charging_ss_at_l}
\end{equation}

\end{enumerate}
\end{subequations} which can be obtained by linearizing eqns. \ref{bc:charging_c_at_0}
and \ref{bc:charging_c_at_l} respectively.

Integration constant $C_{1}$ is obtained by applying boundary condition
\ref{bc:charging_ss_at_0} to eqn.\ref{eq:charging_dc_complete_solution_partial}
\[
\delta c_{o}(0)=C_{1}+\frac{c_{chg}\delta k^{f}}{j\omega\epsilon_{0}}+\frac{k^{f}\delta k^{r}-k^{r}\delta k^{f}}{\Delta k^{f}k^{f}}
\]
The expression for integration constant $C_{1}$ is 
\begin{equation}
C_{1}=-\left(\frac{c_{chg}\delta k^{f}}{j\omega\epsilon_{0}}+\frac{k^{f}\delta k^{r}-k^{r}\delta k^{f}}{\Delta k^{f}k^{f}}\right)\label{eq:charging_ss_C1}
\end{equation}
Integration constant $C_{2}$ is obtained by applying boundary condition
\ref{bc:charging_ss_at_l} to eqn. \ref{eq:charging_dc_complete_solution_partial}
\begin{multline}
\frac{\textrm{d}}{\textrm{d}x} \left\{ C_{1}\cos\left(\sqrt{\frac{\Delta k^{f}}{D_{\text{eff}}}}x\right)+C_{2}\sin\left(\sqrt{\frac{\Delta k^{f}}{D_{\text{eff}}}}x\right)+\frac{k^{f}\delta k^{r}-k^{r}\delta k^{f}}{\Delta k^{f}k^{f}}+\frac{c_{chg}\delta k^{f}}{j\omega\epsilon_{0}}\cos\left(\sqrt{\frac{k^{f}}{D_{\textrm{eff}}}}x\right)\right. \\
\left.\left.+\frac{c_{chg}\delta k^{f}}{j\omega\epsilon_{0}}\tan\left(\sqrt{\frac{k^{f}}{D_{\textrm{eff}}}}l\right)\sin\left(\sqrt{\frac{k^{f}}{D_{\textrm{eff}}}}x\right)\right\} \right|_{x=l}=0
\end{multline}
After taking derivatives, the above equation becomes
\begin{multline}
\sqrt{\frac{k^{f}}{D_{\textrm{eff}}}} \frac{c_{chg}\delta k^{f}}{j\omega\epsilon_{0}}\tan\left(\sqrt{\frac{k^{f}}{D_{\textrm{eff}}}}l\right)\cos\left(\sqrt{\frac{k^{f}}{D_{\textrm{eff}}}}x\right)-\sqrt{\frac{\Delta k^{f}}{D_{\text{eff}}}}C_{1}\sin\left(\sqrt{\frac{\Delta k^{f}}{D_{\text{eff}}}}x\right) \\
\left.-\sqrt{\frac{k^{f}}{D_{\textrm{eff}}}}\frac{c_{chg}\delta k^{f}}{j\omega\epsilon_{0}}\sin\left(\sqrt{\frac{k^{f}}{D_{\textrm{eff}}}}x\right)+\sqrt{\frac{\Delta k^{f}}{D_{\text{eff}}}}C_{2}\cos\left(\sqrt{\frac{\Delta k^{f}}{D_{\text{eff}}}}x\right)\right|_{x=l}=0
\end{multline}
After simplifications, the final expression for the integration constant
$C_{2}$ is 
\begin{equation}
C_{2}=C_{1}\tan\left(\sqrt{\frac{\Delta k^{f}}{D_{\text{eff}}}}l\right)
\end{equation}
Substituting $C_{1}$from eqn.~\ref{eq:charging_ss_C1}, constant
$C_{2}$ can be computed as
\begin{equation}
C_{2}=-\left(\frac{c_{chg}\delta k^{f}}{j\omega\epsilon_{0}}+\frac{k^{f}\delta k^{r}-k^{r}\delta k^{f}}{\Delta k^{f}k^{f}}\right)\tan\left(\sqrt{\frac{\Delta k^{f}}{D_{\text{eff}}}}l\right)\label{eq:charging_ss_C2}
\end{equation}
Substituting $C_{1}$ and $C_{2}$ from eqns.~\ref{eq:charging_ss_C1}
and \ref{eq:charging_ss_C2} into equation \ref{eq:charging_dc_complete_solution_partial},
the final expression for the small-signal oxygen concentration while
becomes
\begin{multline}
\delta c_{o}(x)=  \frac{c_{chg}\delta k^{f}}{j\omega\epsilon_{0}}\cos\left(\sqrt{\frac{k^{f}}{D_{\textrm{eff}}}}x\right)+\frac{c_{chg}\delta k^{f}}{j\omega\epsilon_{0}}\tan\left(\sqrt{\frac{k^{f}}{D_{\textrm{eff}}}}l\right)\sin\left(\sqrt{\frac{k^{f}}{D_{\textrm{eff}}}}x\right) \\
-\left(\frac{c_{chg}\delta k^{f}}{j\omega\epsilon_{0}}+\frac{k^{f}\delta k^{r}-k^{r}\delta k^{f}}{\Delta k^{f}k^{f}}\right)\cos\left(\sqrt{\frac{\Delta k^{f}}{D_{\text{eff}}}}x\right)+\frac{k^{f}\delta k^{r}-k^{r}\delta k^{f}}{\Delta k^{f}k^{f}} \\
-\left(\frac{c_{chg}\delta k^{f}}{j\omega\epsilon_{0}}+\frac{k^{f}\delta k^{r}-k^{r}\delta k^{f}}{\Delta k^{f}k^{f}}\right)\tan\left(\sqrt{\frac{\Delta k^{f}}{D_{\text{eff}}}}l\right)\sin\left(\sqrt{\frac{\Delta k^{f}}{D_{\text{eff}}}}x\right)\label{eq:charging_dc_complete_solution}
\end{multline}
The small-signal Faradaic current is calculated by applying perturbation
theory to eqn.~\ref{eq:FBV_I_F_w_integrals}
\begin{multline}
i_{F}+\delta i_{F}\,e^{j\omega t}=-nFA \left[\int_{0}^{l}\left(k^{f}+\delta k^{f}\,e^{j\omega t}\right)\left(c_{o}(x)+\delta c_{o}\left(x\right)\,e^{j\omega t}\right)\,\textrm{d}x\right.\\
\left.-\int_{0}^{l}\left(k^{r}+\delta k^{r}\,e^{j\omega t}\right)\,\textrm{d}x\right]
\end{multline}
Using eqn.~\ref{eq:FBV_I_F_w_integrals} and neglecting $\delta k^{f}\times\delta c_{o}\left(x\right)$
(since their product is negligible), the small-signal Faradaic current
becomes
\begin{equation}
-\delta i_{F}=nFA\int_{0}^{l}k^{f}\delta c_{o}\left(x\right)\,\textrm{d}x+nFA\int_{0}^{l}\delta k^{f}c_{o}\left(x\right)\,\textrm{d}x-nFA\int_{0}^{l}\delta k^{r}\,\textrm{d}x
\end{equation}
Computing the last integral the above equation can be represented
as 
\begin{equation}
-\delta i_{F}=\textrm{int}_{1}+\textrm{int}_{2}-nFA\delta k^{r}l\label{eq:charging_di_F_expression_w_int}
\end{equation}
where 
\begin{equation}
\textrm{int}_{1}=nFAk^{f}\int_{0}^{l}\delta c_{o}\left(x\right)\,\textrm{d}x\label{eq:charging_int1_rep}
\end{equation}
and 
\begin{equation}
\textrm{int}_{2}=nFA\delta k^{f}\int_{0}^{l}c_{o}\left(x\right)\,\textrm{d}x\label{eq:charging_int2_rep}
\end{equation}
Next, integrals $\textrm{int}_{1}$ and $\textrm{int}_{2}$ will be evaluated
separately and reintroduced into \ref{eq:charging_di_F_expression_w_int}.
Substituting $\delta c_{o}\left(x\right)$ (from eqn. \ref{eq:charging_dc_complete_solution})
into eqn. \ref{eq:charging_int1_rep}
\begin{multline}
\frac{\textrm{int}_{1}}{nFAk^{f}}=\frac{c_{chg}\delta k^{f}}{j\omega\epsilon_{0}}\int_{0}^{l}\cos\left(\sqrt{\frac{k^{f}}{D_{\textrm{eff}}}}x\right)\,\textrm{d}x+\frac{k^{f}\delta k^{r}-k^{r}\delta k^{f}}{\Delta k^{f}k^{f}}\int_{0}^{l}\,\textrm{d}x \\
-\left(\frac{c_{chg}\delta k^{f}}{j\omega\epsilon_{0}}+\frac{k^{f}\delta k^{r}-k^{r}\delta k^{f}}{\Delta k^{f}k^{f}}\right)\left[\int_{0}^{l}\cos\left(\sqrt{\frac{\Delta k^{f}}{D_{\text{eff}}}}x\right)\,\textrm{d}x+\tan\left(\sqrt{\frac{\Delta k^{f}}{D_{\text{eff}}}}l\right)\int_{0}^{l}\sin\left(\sqrt{\frac{\Delta k^{f}}{D_{\text{eff}}}}x\right)\,\textrm{d}x\right]\\
+\frac{c_{chg}\delta k^{f}}{j\omega\epsilon_{0}}\tan\left(\sqrt{\frac{k^{f}}{D_{\textrm{eff}}}}l\right)\int_{0}^{l}\sin\left(\sqrt{\frac{k^{f}}{D_{\textrm{eff}}}}x\right)\,\textrm{d}x
\end{multline}
After taking the integrals and applying the limits the expression
for $\textrm{int}_{1}$ becomes 
\begin{multline}
\textrm{int}_{1}=nFAk^{f} \left[\frac{c_{chg}\delta k^{f}}{j\omega\epsilon_{0}}\sqrt{\frac{D_{\text{eff}}}{k^{f}}}\tan\left(\sqrt{\frac{k^{f}}{D_{\textrm{eff}}}}l\right)+\frac{k^{f}\delta k^{r}-k^{r}\delta k^{f}}{\Delta k^{f}k^{f}}l\right.\\
\left.-\left(\frac{c_{chg}\delta k^{f}}{j\omega\epsilon_{0}}+\frac{k^{f}\delta k^{r}-k^{r}\delta k^{f}}{\Delta k^{f}k^{f}}\right)\sqrt{\frac{D_{\text{eff}}}{\Delta k^{f}}}\tan\left(\sqrt{\frac{\Delta k^{f}}{D_{\text{eff}}}}l\right)\right]\label{eq:charging_int_1_solved}
\end{multline}
Integral $\textrm{int}_{2}$ can be evaluated by substituting eqn.
\ref{eqn:charging_co_steady_state_solution} into eqn. \ref{eq:charging_int2_rep}
\begin{multline}
\frac{\textrm{int}_{2}}{nFA\delta k^{f}}=  c_{chg}\int_{0}^{l}\cos\left(\sqrt{\frac{k^{f}}{D_{\textrm{eff}}}}x\right)\,\textrm{d}x+\frac{k^{r}}{k^{f}}\int_{0}^{l}\,\textrm{d}x \\
+c_{chg}\tan\left(\sqrt{\frac{k^{f}}{D_{\textrm{eff}}}}l\right)\int_{0}^{l}\sin\left(\sqrt{\frac{k^{f}}{D_{\textrm{eff}}}}x\right)\,\textrm{d}x
\end{multline}
The final expression for $\textrm{int}_{2}$ after integrating and
applying boundary conditions is
\begin{equation}
\textrm{int}_{2}=nFA\delta k^{f}c_{chg}\sqrt{\frac{D_{\text{eff}}}{k^{f}}}\tan\left(\sqrt{\frac{k^{f}}{D_{\textrm{eff}}}}l\right)+\frac{nFA\delta k^{f}k^{r}}{k^{f}}l\label{eq:charging_int_2_solved}
\end{equation}
Substituting $\textrm{int}_{1}$ and $\textrm{int}_{2}$ from eqns.~\ref{eq:charging_int_1_solved}
and~\ref{eq:charging_int_2_solved} into eqn.\ref{eq:charging_di_F_expression_w_int}
leads to the following expression for $\delta i_{F}$ 
\begin{multline}
-\delta i_{F}=nFAk^{f} \left[\frac{c_{chg}\delta k^{f}}{j\omega\epsilon_{0}}\sqrt{\frac{D_{\text{eff}}}{k^{f}}}\tan\left(\sqrt{\frac{k^{f}}{D_{\textrm{eff}}}}l\right)+\frac{k^{f}\delta k^{r}-k^{r}\delta k^{f}}{\Delta k^{f}k^{f}}l-nFA\delta k^{r}l\right. \\
\left.-\left(\frac{c_{chg}\delta k^{f}}{j\omega\epsilon_{0}}+\frac{k^{f}\delta k^{r}-k^{r}\delta k^{f}}{\Delta k^{f}k^{f}}\right)\sqrt{\frac{D_{\text{eff}}}{\Delta k^{f}}}\tan\left(\sqrt{\frac{\Delta k^{f}}{D_{\text{eff}}}}l\right)\right] \\
+\frac{nFA\delta k^{f}k^{r}}{k^{f}}l+nFA\delta k^{f}c_{chg}\sqrt{\frac{D_{\text{eff}}}{k^{f}}}\tan\left(\sqrt{\frac{k^{f}}{D_{\textrm{eff}}}}l\right)\label{eq:charging_di_F_before_dkf_dkr_subs}
\end{multline}
The terms $\delta k^{f}$ and $\delta k^{r}$ can be computed by linearizing
eqs. \ref{eq:kf_def_FBV} and \ref{eq:kr_def_FBV} 
\begin{equation}
\delta k^{f}=\frac{-\beta nk^{f}}{V_{T}}\delta\eta\label{eq:charging_dkf}
\end{equation}
\begin{equation}
\delta k^{r}=\frac{(1-\beta)nk^{r}}{V_{T}}\delta\eta\label{eq:charging_dkr}
\end{equation}
Substituting eqns. \ref{eq:charging_dkf} and \ref{eq:charging_dkr}
into eqn. \ref{eq:charging_di_F_before_dkf_dkr_subs} and rearranging
\begin{multline}
-\frac{\delta i_{F}}{\delta\eta}=  nFAk^{f}\sqrt{\frac{D_{\text{eff}}}{k^{f}}}\tan\left(\sqrt{\frac{k^{f}}{D_{\textrm{eff}}}}l\right)\times \\
\left[\frac{l}{V_{T}}\left(\frac{nk^{r}}{\Delta k^{f}}\right)\sqrt{\frac{k^{f}}{D_{\textrm{eff}}}}\cot\left(\sqrt{\frac{k^{f}}{D_{\textrm{eff}}}}l\right)-\frac{\beta nk^{f}c_{chg}}{j\omega\epsilon_{0}V_{T}}-\frac{\beta nc_{chg}}{V_{T}}-\sqrt{\frac{k^{f}}{D_{\textrm{eff}}}}\cot\left(\sqrt{\frac{k^{f}}{D_{\textrm{eff}}}}l\right)\frac{nk^{r}l}{k^{f}V_{T}}\right. \\
\left.-\frac{1}{V_{T}}\left(\frac{nk^{r}}{\Delta k^{f}}-\frac{\beta nk^{f}c_{chg}}{j\omega\epsilon_{0}}\right)\sqrt{\frac{k^{f}}{\Delta k^{f}}}\frac{\tan\left(\sqrt{\frac{\Delta k^{f}}{D_{\text{eff}}}}l\right)}{\tan\left(\sqrt{\frac{k^{f}}{D_{\textrm{eff}}}}l\right)}\right]
\end{multline}
Since $i_{F}=-nFAk^{f}c_{chg}\sqrt{\frac{D_{\textrm{eff}}}{k^{f}}}\tan\left(\sqrt{\frac{k^{f}}{D_{\textrm{eff}}}}l\right)$
the last equation becomes
\begin{multline}
\frac{\delta i_{F}}{\delta\eta}= \frac{i_{F}}{c_{chg}}\left[\frac{l}{V_{T}}\left(\frac{nk^{r}}{\Delta k^{f}}\right)\sqrt{\frac{k^{f}}{D_{\textrm{eff}}}}\cot\left(\sqrt{\frac{k^{f}}{D_{\textrm{eff}}}}l\right)-\frac{\beta nk^{f}c_{chg}}{j\omega\epsilon_{0}V_{T}}-\frac{\beta nc_{chg}}{V_{T}}\right. \\
\left.-\frac{1}{V_{T}}\left(\frac{nk^{r}}{\Delta k^{f}}-\frac{\beta nk^{f}c_{chg}}{j\omega\epsilon_{0}}\right)\sqrt{\frac{k^{f}}{\Delta k^{f}}}\frac{\tan\left(\sqrt{\frac{\Delta k^{f}}{D_{\text{eff}}}}l\right)}{\tan\left(\sqrt{\frac{k^{f}}{D_{\textrm{eff}}}}l\right)} -\sqrt{\frac{k^{f}}{D_{\textrm{eff}}}}\cot\left(\sqrt{\frac{k^{f}}{D_{\textrm{eff}}}}l\right)\frac{nk^{r}l}{k^{f}V_{T}}\right]
\end{multline}
Rearranging the above equation and using $Z_{F}\left(\omega\right)=-\frac{\delta\eta}{\delta i_{F}}$ and $\Delta k^{f}=k^{f}-j\omega\epsilon_{0}$, the final expression for the Faradaic impedance becomes
\begin{equation}
Z_{F}\left(\omega\right)=\frac{Z_{0}}{f\left(\beta,\omega,\eta\right)}\label{eq:charging_ZF_final_expression}
\end{equation}
where
\begin{equation}
Z_{0}=\frac{V_{T}}{n\beta i_{F}}\label{eq:charging_Z0}
\end{equation}
and 
\begin{multline}
f\left(\beta,\omega,\eta\right)=1 + \left[\frac{k^{r}}{\left(c_{o}^{*}-\frac{k^{r}}{k^{f}}\right)\left(k^{f}-j\omega\epsilon_{0}\right)\beta}-\frac{k^{f}}{j\omega\epsilon_{0}}\right]\sqrt{\frac{k^{f}}{k^{f}-j\omega\epsilon_{0}}}\frac{\tan\left(\sqrt{\frac{k^{f}-j\omega\epsilon_{0}}{D_{\text{eff}}}}l\right)}{\tan\left(\sqrt{\frac{k^{f}}{D_{\textrm{eff}}}}l\right)} \\
+\frac{k^{f}}{j\omega\epsilon_{0}}-\left[\frac{j\omega\epsilon_{0}k^{r}l}{\beta k^{f}\left(k^{f}-j\omega\epsilon_{0}\right)\left(c_{o}^{*}-\frac{k^{r}}{k^{f}}\right)}\right]\sqrt{\frac{k^{f}}{D_{\text{eff}}}}\cot\left(\sqrt{\frac{k^{f}}{D_{\textrm{eff}}}}l\right) \label{eq:charging_fn_comp_chg}
\end{multline}
The eqn.~\ref{eq:charging_ZF_final_expression} describes the Faradaic impedance of Li-air batteries under d.c. charge current.
%The eqn.~\ref{eq:charging_ZF_final_expression} describes the Faradaic impedance of Li-air batteries under d.c. charge current; the final equation depends on the forward and backward reaction rate constants, effective diffusion coefficient of oxygen in the electrolyte, length of the cathode, and oxygen concentration in the organic electrolyte.


\subsection{The Faradaic impedance during discharge}

Eqns. \ref{eq:charging_ZF_final_expression}-\ref{eq:charging_fn_comp_chg}
can compute the Faradaic impedance during both charge and discharge.
Since $k^{f}$ and $k^{r}$ are negative during d.c. discharge eq.
\ref{eq:charging_steady_state_final_equation} can be rewritten into
a slightly different from which is sometimes more appropriate for
the numerical implementation. Using relations $\cot\left(jx\right)=-j\coth\left(x\right)$
and $\tan\left(jx\right)=j\tanh\left(x\right)$, eq.~\ref{eq:charging_fn_comp_chg}
becomes
\begin{multline}
f\left(\beta,\omega,\eta\right)=1+\left[\frac{k^{r}}{\beta\left(c_{o}^{*}-\frac{k^{r}}{k^{f}}\right)\left(k^{f}-j\omega\epsilon_{0}\right)}-\frac{k^{f}}{j\omega\epsilon_{0}}\right]\sqrt{\frac{k^{f}}{k^{f}-j\omega\epsilon_{0}}}\frac{\tanh\left(\sqrt{\frac{\left|k^{f}\right|+j\omega\epsilon_{0}}{D_{\text{eff}}}}l\right)}{\tanh\left(\sqrt{\frac{\left|k^{f}\right|}{D_{\textrm{eff}}}}l\right)} \\
+\frac{k^{f}}{j\omega\epsilon_{0}}-\left[\frac{j\omega\epsilon_{0}k^{r}l}{\beta k^{f}\left(k^{f}-j\omega\epsilon_{0}\right)\left(c_{o}^{*}-\frac{k^{r}}{k^{f}}\right)}\right]\sqrt{\frac{\left|k^{f}\right|}{D_{\text{eff}}}}\coth\left(\sqrt{\frac{\left|k^{f}\right|}{D_{\textrm{eff}}}}l\right)\label{eq:charging_fn_comp_dis}
\end{multline}
The Faradaic impedance during discharge can be calculated using eqns.
\ref{eq:charging_ZF_final_expression}, \ref{eq:charging_fn_comp_dis},
\ref{eq:charging_Z0}, and \ref{eq:charging_vs_discharge_steady_state_final_equation}. 

By putting $k^{r}=0$ and $k^{f}=-k$ into eqn. \ref{eq:charging_ZF_final_expression}
the expression for the Faradaic impedance under discharge given in
eqn. \ref{eq:ZF_final_Tafel} ($Z_{F}^{\textrm{Tafel}}\left(\omega\right)$)
is easily recovered
\[
Z_{F}=\frac{Z_{0}}{\left[1-\frac{k}{j\omega\epsilon_{0}}+\frac{k\sqrt{k}\tanh\left(\sqrt{\frac{k+j\omega\epsilon_{0}}{D_{\text{eff}}}}l\right)}{j\omega\epsilon_{0}\sqrt{k+j\omega\epsilon_{0}}\tanh\left(\sqrt{\frac{k}{D_{\textrm{eff}}}}l\right)}\right]}
\]
Similarly, the Faradaic impedances developed for small-values of the
d.c. discharge current (eqns. \ref{eq:ZF_final_FBV_non_sinh}and \ref{eq:ZF_final_sinh})
can also be recovered easily.
\end{document}