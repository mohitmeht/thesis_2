\documentclass[11pt,expanded,copyright]{fsuthesis}
\usepackage{amsmath,amssymb,empheq,nicefrac}
\usepackage{siunitx}
\usepackage[version=4]{mhchem}
\usepackage{printlen}
\usepackage{caption,enumitem,subfig,setspace,longtable,cite,subfiles,multirow}
\usepackage{subfiles}
% Do NOT use 'geometry' or 'setspace' packages! They will
% mess up the spacing provided by 'fsuthesis'.
\providecommand{\tabularnewline}{\\}
%\usepackage[round]{natbib}  % an alternative bibliography package
\ifpdf   % We will execute this part if we are in PDF mode
  \usepackage[pdftex]{graphicx}
  \usepackage{color}
  \definecolor{mygreen}{rgb}{0,0.75,0}
  \definecolor{myblue}{rgb}{0.1,0,0.8}
  \definecolor{myred}{rgb}{0.9,0.2,0.2}
  \usepackage[colorlinks=false,bookmarks=true,pdfborder={0 0 0},
    linkcolor=myblue,urlcolor=myred,citecolor=mygreen,
    breaklinks=true,bookmarksnumbered=true]{hyperref}
\else    % otherwise we'll run this part if we are not in PDF mode	
  \usepackage[dvips]{graphicx}
\fi
\widowpenalty=9999
\clubpenalty=9999

\title{Modeling, simulation, and experimental verification\protect\\of impedance spectra in Li-air batteries}	%% Manuscript title
\author{Mohit Rakesh Mehta}
\college{College of Engineering}
\department{Department of Electrical and Computer Engineering} % Delete if no department
\manuscripttype{Dissertation}              % [Thesis, Dissertation, Treatise]
\degree{Doctor of Philosophy}                 
\degreeyear{2015}
\defensedate{August 26, 2015}

%\subject{My Topic}
%\keywords{keyterm1; keyterm2; keyterm3; ...}
\committeeperson{Petru Andrei}{Professor Directing Dissertation}
\committeeperson{Joseph B. Schlenoff}{University Representative}
\committeeperson{Jim P. Zheng}{Committee Member}
\committeeperson{Pedro Moss}{Committee Member}
\committeeperson{Hui Li}{Committee Member}

\clubpenalty=9999
\widowpenalty=9999

\begin{document}

\frontmatter
\maketitle
\makecommitteepage	

\begin{dedication}
Dedicated to My Beloved Parents
%
%	Text width = \uselengthunit{in}\printlength{\textwidth} Text height = \printlength{\textheight}
%
\end{dedication}

\begin{acknowledgments}
I am first and foremost grateful to my advisor, Petru Andrei, for all his guidance, support and most importantly his patience. I would also like to thank Prof. Jim Zheng for allowing me to perform experiments on Li-air batteries with his students. I would also like to thank Dr. Xujie Chen for building Li-air batteries and performing impedance measurements in order to validate the idea of measuring impedance spectra during discharge. Dr. Xujie Chen also was of great help in guiding and assisting me in performing the EIS experiments.

I would also take this opportunity to thank all my friends for their support and constant encouragement.
\end{acknowledgments}

\tableofcontents
\listoftables
\listoffigures

\begin{listofsymbols}
\subfile{components/SymbolsList}
\end{listofsymbols}

%\begin{listofabbrevs}
%\end{listofabbrevs}

\begin{abstract}
	There has been a growing interest in electrochemical storage devices such as batteries, fuel cells and supercapacitors in recent years. This interest is due to our increasing dependence on portable electronic devices and due to the high demand for energy storage from the electric transport vehicles and electrical power grid industries. As, we transition towards cleaner renewable fuel sources such as solar and wind the need for energy storage devices in the power grid industry will continue to grow. Among the electrochemical storage devices, Li-air offers the highest theoretical specific capacity and energy density. However, these batteries suffer from a number of issues such as low cyclability, low practical energy densities, and low specific capacities. The deposition of lithium peroxide on the surface of the cathode is one of the main causes for the low practical specific capacity of lithium-air batteries with organic electrolyte.

	Electrochemical impedance spectroscopy (EIS) is a powerful analytical tool that has been used in the past to extract physical parameters such as chemical diffusion coefficient, effective diffusion coefficient, and Faradaic reaction rate and to determine the stability and the extent of degradation in an electrochemical device. In this dissertation, a physics-based analytical model is developed to study the EIS of Li-air batteries, in which the mass transport inside the cathode is limited by oxygen diffusion, during charge and discharge. The model takes into consideration the effects of double layer, Faradaic processes, and oxygen diffusion in the cathode, but neglects the effects of anode, separator, conductivity of the deposit layer, and Li-ion transport. The analytical model predicts that the effects of Faradaic processes and oxygen diffusion can be masked by the double layer capacitance. Therefore, the dissertation focuses on the two cases: 1) when the Faradaic and the diffusion processes are separate and can are easily observed as two different semicircles on the Nyquist plot and 2) when the Faradaic and the diffusion processes are shadowed by the double layer capacitance and is observed on the Nyquist plot as a single combined semicircle. 

	In order to extract physical parameters such as the diffusion coefficient of oxygen and Faradaic reaction rate a simple mathematical expression is developed. The diffusion coefficient is determined by using the value of the resistances (real impedance intercept on the Nyquist plot) of both the semicircles for the first case and by using the value of the combined resistance for the second case. Once, the effective oxygen diffusion coefficient is estimated, it can be used to estimate the value of the reaction constant. This method of extracting physical parameters of the cathode in a Li-air battery can serve as a tool in identifying an effective electrolyte and a cathode material. In addition, it can also serve as a noninvasive technique to identify and also quantify the use of the catalyst to improve the reaction kinetics in an electrochemical system. 

	Finally, finite element simulations are used to validate the analytical model and to study the effects of discharge products on the total impedance of Li-air batteries with organic electrolyte. The finite element simulations are based on the theory of concentrated solutions and the complex impedance spectra are computed by linearizing the partial differential equations that describe the mass and charge transport in Li-air batteries. These equations include the oxygen diffusion equation, the Li drift-diffusion equation, and the electron conduction equation. The reaction at the anode and cathode are described by Butler-Volmer kinetics. The finite element simulation show that the total impedance of a Li-air battery increases by more than $200\%$ when the impedance spectra is measured near the end of the discharge cycle as compared to on a fresh battery. The impedance spectra and the deposition profile of lithium peroxide is significantly affected by the resistivity of the deposition layer. The finite element simulations also show that using electrolytes with high oxygen solubility and concentrated \ce{O2} gas at high pressures will reduce the total impedance of Li-air batteries.
\end{abstract}
\mainmatter

\newcommand{\main}{./}
%\include{components/1_Introduction}
\subfile{components/1_Introduction}
\subfile{components/2_dc_discharge}
\subfile{components/3_dc_charge}
\subfile{components/4_exp_verification}
\subfile{components/5_diss_kinetics}
\subfile{components/6_FEM}
\subfile{components/7_Conclusions}
%
%\appendix
%\subfile{components/LR_Diffusion_Impedances}
%\include{components/2_dc_discharge}


\renewcommand*{\bibname}{References}
\bibliographystyle{ieeetr}
\bibliography{library}


\begin{biosketch}
Mohit Mehta received his Bachelors of Engineering in Electronics and Telecommunications from Pune University, India in the summer of 2010. He joined Florida State University in Fall 2011 for his Doctorate of Philosophy in Electrical Engineering.
He is currently working on Modeling and simulation of electrochemical impedance spectra in Li-air batteries under the guidance of Dr. Petru Andrei.
\end{biosketch}

\end{document}
