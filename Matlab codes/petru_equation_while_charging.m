function [ eta ] = petru_equation_while_charging(n,F,A,a,kf0,kr0,beta,Vt,Deff,c_int,l,I_dis)
% Diffusion length calculation based on Petru's Theory to
% compute EIS in Li-air batteries. The return value is lambda.
% Lambda^2 = Deff./k
% I_F * lam= nFA*Deff*co2*tanh(l/lam);
% Here, n = number of charge transfer electrons
% F = Faradaic constant
% A = cross sectional area of the cathode
% Deff = Effective oxygen diffusion coefficient in the organic electrolyte
% l = Length of the cathode
% I_dis = Value of the dc discharge current
% F is the  inline function which is solved by numerical method to oxygen
% c_int is the inital concentration of oxygen dissolved in the electrolyte
% diffusion length. This diffusion length is needed to compute the Voltage,
% impedance response of a Li-air battery.
% Here the return function is double.
x0 = 0.01.*ones(size(I_dis)); 
kr = @(x)(a.*kr0.*exp((1-beta).*n./Vt.*x));
kf = @(x)(a.*kf0.*exp((-beta).*n./Vt.*x));
c_chg = @(x)(c_int - (kr(x)./kf(x)));
% f_w_kr =
% @(x)((n.*F.*A.*sqrt(kf(x).*Deff).*c_chg(x).*tanh(sqrt(kf(x)./Deff).*l))-(I_dis));
f_w_sin_w_kr = @(x)((n.*F.*A.*sqrt(kf(x).*Deff).*c_chg(x).*tan(sqrt(kf(x)./Deff).*l))+(I_dis));
options = optimoptions('fsolve','Display','off');
options.TolFun = 1e-30;
options.MaxIter = 10000;
options.TolX = 1e-15;
[eta,~,EXITFLAG,output,~] = fsolve(f_w_sin_w_kr,x0,options); % Call solver
% display(output);
if (EXITFLAG >3 ||EXITFLAG <1)
    save('error_file');
    display(['FOUND ERROR: Error code = ' num2str(EXITFLAG)]);
end
%     1  FSOLVE converged to a root.
%     2  Change in X too small.
%     3  Change in residual norm too small.
%     4  Computed search direction too small.
%     0  Too many function evaluations or iterations.
%    -1  Stopped by output/plot function.
%    -2  Converged to a point that is not a root.
%    -3  Trust region radius too small (Trust-region-dogleg).
end