function [P,cur_max,vol_max,P_e_max,P_a_max,P_c_max,P_r_max,P_max] = at_max_pwer_wrt_R_Do2 (E0,n,F,A,Vt,Deff_multiple,po2,l,i0a,kc,a,R_multiple,I_dis,kh)
cur_max = zeros(length(R_multiple),1);
vol_max = zeros(length(R_multiple),1);
P = zeros(length(I_dis),length(R_multiple),length(Deff_multiple));
P_max = zeros(length(R_multiple),length(Deff_multiple));
P_e_max = zeros(length(R_multiple),length(Deff_multiple));
P_a_max = zeros(length(R_multiple),length(Deff_multiple));
P_c_max = zeros(length(R_multiple),length(Deff_multiple));
P_r_max = zeros(length(R_multiple),length(Deff_multiple));
c_int=kh*po2/1000;
% figure(3);
% subplot(2,1,1);
% an = annotation('textbox',...
%     [0.418312661498708 0.822525597269625 0.0829793281653747 0.0580204778156999],...
%     'String',['D_{eff} ='],...
%     'FitBoxToText','off');
% cc = distinguishable_colors(length(Deff_multiple),'w');
cc = jet(length(Deff_multiple));
for i=1:length(Deff_multiple)
Deff = Deff_multiple(i);
lamad=petru_equation(n,F,A,Deff,c_int,l,I_dis);
% [row,col] = ind2sub(size(lamad),find(lamad == min(lamad(:))));
% display(['lamda_min(' num2str(row) ',' num2str(col) ') = ' num2str(lamad(row,col))]);
Ea_sinh = @(x)(Vt.*2.*asinh(x./2./i0a));
Ec_sinh = @(x)(2.*Vt./n.*asinh(Deff./2./kc./a./lamad.^2)');
figure(3);
subplot(1,2,1)
% plot(1e3.*I_dis,imag(log10(lamad)));hold on;
plot(1e3.*I_dis,(log10(lamad)),'Color',cc(i,:));hold on;
if sum(lamad<0)~=0
% set(an,'String',...
%     ['D_{eff} =' num2str(Deff)],...
%     'FitBoxToText','off');
display(['Deff(' num2str(i) ') = ' num2str(Deff)])
end
axis square;
xlabel('I_d_i_s [mA/cm^2]');
ylabel('\lambda [cm/s]');
title('\lambda vs I_d_i_s');
subplot(1,2,2)
plot(1e3.*I_dis,Ec_sinh(I_dis),'Color',cc(i,:));hold on;
axis square;
xlabel('I_d_i_s [mA/cm^2]');
ylabel('E_c [V]');
title('\eta_c vs I_d_i_s');
drawnow;
parfor j=1:length(R_multiple)
R = R_multiple(j);
% c_int = co2_multiple(j);
V = @(x)(E0-(Ea_sinh(x))-(Ec_sinh(x))-(R.*x));           
Y = (V(I_dis'))';
P(:,j,i)=I_dis.*Y;
[row,col] = ind2sub(size(P(:,j,i)),find(P(:,j,i) == max(P(:,j,i))));
cur_max(j) = I_dis(row);
vol_max(j) = Y(col);
% display(['lamda(' num2str(row) ') = ' num2str(lamad(row))]);
P_max(j,i)   = max(P(:,j,i));
P_e_max(j,i) = cur_max(j).*E0;
P_a_max(j,i) = cur_max(j).*((Ea_sinh(cur_max(j)'))');
P_c_max(j,i) = cur_max(j).*(Ec_sinh_max(n,F,1,Vt,Deff,c_int,l,cur_max(j),kc,a)');
P_r_max(j,i) = cur_max(j).*((R.*cur_max(j)));

end
end
figure(3)
subplot(1,2,1)
hold off;
subplot(1,2,2)
hold off;
cc = distinguishable_colors(6);
figure(1);
subplot(3,2,1);
plot(R_multiple,1e3.*P_max,'-o','Color',cc(1,:));
title('Total Power');
xlabel('Resistance [\Omega]');
ylabel('Power [mW]');
subplot(3,2,2);plot(R_multiple,1e3.*cur_max,'->','Color',cc(2,:));
title('Current at Max Power');
xlabel('Resistance [\Omega]');
ylabel('Max Current [mA]');
subplot(3,2,3);plot(R_multiple,1e3.*P_e_max,'-v','Color',cc(3,:));
title('I_m_a_x\times E_0');
xlabel('Resistance [\Omega]');
ylabel('Power [mW]');
subplot(3,2,4);plot(R_multiple,1e3.*P_c_max,'-^','Color',cc(4,:));
title('I_m_a_x\times E_c');
xlabel('Resistance [\Omega]');
ylabel('Power [mW]');
subplot(3,2,5);plot(R_multiple,1e3.*P_a_max,'-s','Color',cc(5,:));
title('I_m_a_x\timesE_a');
xlabel('Resistance [\Omega]');
ylabel('Power [mW]');
subplot(3,2,6);plot(R_multiple,1e3.*P_r_max,'-x','Color',cc(6,:));
title('I^2_m_a_x\timesR');
xlabel('Resistance [\Omega]');
ylabel('Power [mW]');

figure(2);
clf
hold on;
P_max_plot = plot(R_multiple,1e3.*P_max,'-o','Color',cc(1,:));
% P_e_max_plot = plot(R_multiple,1e3.*P_e_max,'-v','Color',cc(3,:));
P_c_max_plot = plot(R_multiple,1e3.*P_c_max,'-^','Color',cc(4,:));
P_a_max_plot = plot(R_multiple,1e3.*P_a_max,'-s','Color',cc(5,:));
P_r_max_plot = plot(R_multiple,1e3.*P_r_max,'-x','Color',cc(6,:));  
hold off;
xlabel('Resistance [\Omega]');
ylabel('Power [mW]');
P_max_plot_group = hggroup;
% P_e_max_plot_group = hggroup;
P_c_max_plot_group = hggroup;
P_a_max_plot_group = hggroup;
P_r_max_plot_group = hggroup;
set(P_max_plot,'Parent',P_max_plot_group);
% set(P_e_max_plot,'Parent',P_e_max_plot_group);
set(P_c_max_plot,'Parent',P_c_max_plot_group);
set(P_a_max_plot,'Parent',P_a_max_plot_group);
set(P_r_max_plot,'Parent',P_r_max_plot_group);

set(P_max_plot_group,'Displayname','Total Power');
% set(P_e_max_plot_group,'Displayname','I_m_a_x\times E_0');
set(P_c_max_plot_group,'Displayname','I_m_a_x\times E_c');
set(P_a_max_plot_group,'Displayname','I_m_a_x\timesE_a');
set(P_r_max_plot_group,'Displayname','I^2_m_a_x\timesR');
% s=legend([P_max_plot_group P_e_max_plot_group P_c_max_plot_group P_a_max_plot_group P_r_max_plot_group]);
s=legend([P_max_plot_group P_c_max_plot_group P_a_max_plot_group P_r_max_plot_group]);
set(s,'Interpreter','tex','Location','NorthEast');
% legend('Total Power','I_m_a_x\times E_0','I_m_a_x\times E_c','I_m_a_x\timesE_a','I^2_m_a_x\timesR');

figure(4)
clf
hold on;
P_max_plot = errorbar(R_multiple,1e3.*mean(P_max(:,:)'),1e3.*(max(P_max(:,:)')-mean(P_max(:,:)')),'Color',cc(1,:));
P_e_max_plot = errorbar(R_multiple,1e3.*mean(P_e_max(:,:)'),1e3.*(max(P_e_max(:,:)')-mean(P_e_max(:,:)')),'Color',cc(3,:));
P_c_max_plot = errorbar(R_multiple,1e3.*mean(P_c_max(:,:)'),1e3.*(max(P_c_max(:,:)')-mean(P_c_max(:,:)')),'Color',cc(4,:));
P_a_max_plot = errorbar(R_multiple,1e3.*mean(P_a_max(:,:)'),1e3.*(max(P_a_max(:,:)')-mean(P_a_max(:,:)')),'Color',cc(5,:));
P_r_max_plot = errorbar(R_multiple,1e3.*mean(P_r_max(:,:)'),1e3.*(max(P_r_max(:,:)')-mean(P_r_max(:,:)')),'Color',cc(6,:));  
hold off;
xlabel('Resistance [\Omega]');
ylabel('Power [mW]');
P_max_plot_group = hggroup;
P_e_max_plot_group = hggroup;
P_c_max_plot_group = hggroup;
P_a_max_plot_group = hggroup;
P_r_max_plot_group = hggroup;
set(P_max_plot,'Parent',P_max_plot_group);
set(P_e_max_plot,'Parent',P_e_max_plot_group);
set(P_c_max_plot,'Parent',P_c_max_plot_group);
set(P_a_max_plot,'Parent',P_a_max_plot_group);
set(P_r_max_plot,'Parent',P_r_max_plot_group);

set(P_max_plot_group,'Displayname','Total Power');
set(P_e_max_plot_group,'Displayname','I_m_a_x\times E_0');
set(P_c_max_plot_group,'Displayname','I_m_a_x\times E_c');
set(P_a_max_plot_group,'Displayname','I_m_a_x\timesE_a');
set(P_r_max_plot_group,'Displayname','I^2_m_a_x\timesR');
s=legend([P_max_plot_group P_e_max_plot_group P_c_max_plot_group P_a_max_plot_group P_r_max_plot_group]);
end