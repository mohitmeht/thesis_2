function [ lamad,Co ] = petru_equation_w_diffusion_kinetics(n,F,A,Deff,po2,l,kh,I_dis,kf)
% In this function, the diffusion length is computed based on the equation given
% by Petru Andrei. The effect of diffusion kinetics is added to the
% original equation. This diffusion kinetics was mentioned by Horstmann et
% al.
% Here, n = number of charge transfer electrons
% F = Faradaic constant
% A = cross sectional area of the cathode
% Deff = Effective oxygen diffusion coefficient in the organic electrolyte
% l = Length of the cathode
% I_dis = Value of the dc discharge current
% po2 = Partial pressure of oxygen in the atmosphere (just above the cathode surface)
% kf is the forward reaction rate parameter of the diffusion kinetics eqn.
% N = kf - kr (N = flux of oxygen, rate of oxygen entering the battery and kr 
% is the rate of oxygen exiting the system.)
% kf = f(2*pi*Mo2*RT)^(-0.5) Mo2 = Molar mass, f = % of gas molecule
% hitting the interface enter the liquid
% Default values for Li-air batteries with aqueous electrolyte (LiOH specifically)
% n= 4;
% A = 1;
% F = 96485;
% Deff = 2e-5*(0.8^1.5);
% kf = 45.2467;
% kh = 769.23*1e3;
% l = 0.04;
% po2 = 0.21;
% I_dis=1e-3;
x0 = 0.01.*ones(size(I_dis));  % Make a starting guess at the solution
co = @(x)((x.*kf.*po2)./((tanh(l./x).*Deff)+(x.*kf.*kh)));
% f = @(x)(((n.*F.*A.*Deff.*po2.*tanh(l./x))./(I_dis./kf))-((tanh(l./x).*Deff) + (kf.*inv(kh).*x))); % Old model
f = @(x)(((n.*F.*A.*Deff.*co(x).*tanh(l./x))-(I_dis.*x)));
options = optimoptions('fsolve','Display','off');
options.TolFun = 1e-15;
options.TolX = 1e-15;
[lamad,~,EXITFLAG,output,~] = fsolve(f,x0,options); % Call solver
Co = co(lamad);
% display(output);
if (EXITFLAG >3 ||EXITFLAG <1)
    save('error_file');
    display(['FOUND ERROR: Error code = ' num2str(EXITFLAG)]);
end
%     1  FSOLVE converged to a root.
%     2  Change in X too small.
%     3  Change in residual norm too small.
%     4  Computed search direction too small.
%     0  Too many function evaluations or iterations.
%    -1  Stopped by output/plot function.
%    -2  Converged to a point that is not a root.
%    -3  Trust region radius too small (Trust-region-dogleg).
end

function [kf] = kf_default()
kf = 45.2467;
end