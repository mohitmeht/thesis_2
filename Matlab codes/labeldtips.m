%function output_txt = labeldtips(obj,event_obj)
function [output_txt_from_plot] = labeldtips(obj,event_obj,...
                      freq,Z_re,Z_im,hdt,disp)                  
pos = get(event_obj,'Position');
x = pos(1); y = pos(2);
[row,col] = find(Z_re == x);  % Find index to retrieve obs. name
% The find is reliable only if there are no duplicate x values
%[row,col] = ind2sub(size(Z),idx);
%labeldtips_test(hdt,disp);  % To calculate slope
% output_txt_from_data = {['Z'': ',num2str(Z_re(row),4),'[ohm]'] ['-Z'''': ',num2str(Z_im(row),4),'[ohm]'] ['Freq: ' num2eng(freq(row)) 'Hz'] ['Pos: ' num2str(row)]};
output_txt_from_plot = {['Z'': ',num2eng(x),'[ohm]'] ['-Z'''': ',num2eng(y),'[ohm]'] ['Freq: ' num2eng(freq(col)) 'Hz'] ['Pos: ' num2str(col)]};
end