function [output]=Ec_sinh_max(n,F,A,Vt,Deff,c_int,l,I_dis,kc,a)
lamad=petru_equation(n,F,A,Deff,c_int,l,I_dis);
output = (2.*Vt./n.*asinh(Deff./2./kc./a./lamad.^2)');
end