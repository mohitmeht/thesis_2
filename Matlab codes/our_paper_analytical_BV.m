function [Z,f,eta]=our_paper_analytical_BV(I_dis,kc,n,Deff,c_int,l,Cd,a,A,poro,Vt,~,f)

% I_dis=1e-3;             %  _______     
% k0=1.3e-8;              % |               
% n=3.2;                  % |initial            
% Do=7e-6;                % |  or 
% c_int=3.26e-6;          % |standard 
% l=0.02;                 % |_______
% beta=0.57;
% Vt=40e-3; 
% a=1e4;
% poro=0.6;
% f=logspace(-3,log10(200e3),200);
%%%%%%%% eta=-0.3;     %% using a constant eta value instead of calculations
F=phys_constants('F');
%% petru's model calculations using Butler-Volmer equation with sinh
lam=petru_equation(n,F,A,Deff,c_int,l,I_dis);
k=Deff./lam.^2;
eta=(2.*Vt/n)*asinh(-k./(2.*kc*a));
w=2.*pi.*f;
im=1i.*w.*poro;
an=1;
ad=1;
a_var = (an./ad);
bn=k;
bd=im;
b = (bn./bd);
cn=k.*sqrt(k).*tanh(sqrt((k+im)./Deff).*l);
cd=im.*sqrt(k+im).*tanh(sqrt(k./Deff).*l);
c = (cn./cd);
Z0=-2.*Vt.*tanh(n/2./Vt.*eta)/(n.*I_dis);
Zf=Z0./(a_var- b + c);
Zd=1./(1i.*w.*a.*A.*l.*Cd);
Z=(Zf.*Zd)./(Zf+Zd);
end