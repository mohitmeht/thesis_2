
I_dis=logspace(-4,-3,10);             %  _______     
k0=1.3e-8;              % |               
n=2;                  % |initial            
Do=7e-6;                % |  or 
c_int=3.26e-6;          % |standard 
l=0.02;                 % |_______
beta=0.5;
Vt=26e-3; 
a=1e4;
poro=0.75;
A= 1;
brugg = 1.5;
Deff=Do*(poro^brugg);
Cd = 1e-8;
%%%%%%% eta=-0.3;     %% using a constant eta value instead of calculations
F=96500;
Z0=Vt./(n.*beta.*I_dis);
lam=petru_equation(n,F,A,Deff,c_int,l,I_dis);
eta=(Vt./n./beta).*log(k0.*a.*(lam.^2./Deff));
% eta=-0.4;
f=logspace(-3,6,200)';
w=2.*pi.*f;
k=k0.*a.*exp(-beta.*n.*eta./Vt);
im=1i.*poro.*(w*ones(size(k)));
k = repmat(k,size(w));
Z0 = repmat(Z0,size(w));
an=1;
ad=1;
bn=k;
bd=im;
cn=k.*sqrt(k).*tanh(sqrt((k+im)./Deff).*l);
cd=im.*sqrt(k+im).*tanh(sqrt(k./Deff).*l);
Zf=Z0./((an./ad)-(bn./bd)+(cn./cd));
Zd=1./(1i.*w.*a.*A.*l.*Cd);
Zd = repmat(Zd,1,length(I_dis));
Z=(Zf.*Zd)./(Zf+Zd);
Z_plot = zeros(size([real(Z) imag(Z)]));
Z_plot(:,1:2:end) = real(Z);
Z_plot(:,2:2:end) = -imag(Z);
plot_length = (size(Z,2));
cc= jet(plot_length);
for i= 1:plot_length
plot(Z_plot(:,(2*i)-1),Z_plot(:,2*i),'color',cc(i,:));hold on;
end
hold off;