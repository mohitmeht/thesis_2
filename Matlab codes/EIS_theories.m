function EIS_theories(varargin)
switch varargin{length(varargin)}
    case 2
        handles = varargin{1};
        f = varargin{4};
        try
            set(handles.output_window_input,'String',[get(handles.output_window_input,'String');...
                {['Starting simulation of EIS with Tafel kinetics....']}]);              
            Z = EIS_with_tafel(varargin{1:end-1});
            set(handles.output_window_input,'String',[get(handles.output_window_input,'String');...
                {['Completed simulation of EIS with Tafel kinetics....']}]);                
        catch        
        set(handles.output_window_input,'String',[get(handles.output_window_input,'String');...
                {['EIS_with_tafel function failed....']}]);                       
        end
        plot_figure(handles,Z,f);
    
    case 3
        handles = varargin{1};
        try
            set(handles.output_window_input,'String',[get(handles.output_window_input,'String');...
                {['Starting simulation of EIS with Butler-Volmer kinetics with sinh....']}]);               
            Z = EIS_with_sinh(varargin{1:end-1});
            set(handles.output_window_input,'String',[get(handles.output_window_input,'String');...
                {['Completed simulation of EIS with Butler-Volmer kinetics with sinh....']}]);                
        catch        
        set(handles.output_window_input,'String',[get(handles.output_window_input,'String');...
                {['EIS_with_tafel function failed....']}]);                       
        end
        plot_figure(handles,Z);

    case 4
        handles = varargin{1};
        try
            set(handles.output_window_input,'String',[get(handles.output_window_input,'String');...
                {['Starting simulation of EIS with Butler-Volmer kinetics....']}]);              
            Z = EIS_with_beta(varargin{1:end-1});
            set(handles.output_window_input,'String',[get(handles.output_window_input,'String');...
                {['Completed simulation of EIS with Butler-Volmer kinetics....']}]);                 
        catch        
        set(handles.output_window_input,'String',[get(handles.output_window_input,'String');...
                {['EIS_with_FBV function failed....']}]);                       
        end
        plot_figure(handles,Z);          

    otherwise
        handles = varargin{1};
        set(handles.output_window_input,'String',[get(handles.output_window_input,'String');...
                {['Sorry, this option is not yet implemented...']}]);
end
end

function [Z] = EIS_with_tafel(varargin)
        handles = varargin{1};
        constants = varargin{2};
        var = varargin{3};
        f = varargin{4};
        Vt = Vt_compute(var.T);
        Deff = var.Dbulk*(var.poro^var.brugg);
        lam = petru_equation(var.n,constants.F,var.A,Deff,var.c_int,var.l,var.I_dis);
        k = Deff./lam.^2;
        eta = -Vt/var.n/var.alpha*log(k/var.kc/var.a);
        w=2.*pi.*f;
        im=1i.*w.*var.poro;
        an=1;
        ad=1;
        bn=k;
        bd=im;
        cn=k.*sqrt(k).*tanh(sqrt((k+im)./Deff).*var.l);
        cd=im.*sqrt(k+im).*tanh(sqrt(k./Deff).*var.l);
        Z0=Vt/(var.n*var.alpha*var.I_dis);
        Zf=Z0./((an./ad)-(bn./bd)+(cn./cd));
        Zd=1./(1i.*w.*var.a.*var.A.*var.l.*var.Cd);
        Z=(Zf.*Zd)./(Zf+Zd);
        set(handles.eta_output,'String',num2str(2.956 + eta));     
end
function [Z] = EIS_with_sinh(varargin)
        handles = varargin{1};
        constants = varargin{2};
        var = varargin{3};
        f = varargin{4};
        Vt = Vt_compute(var.T);
        Deff = var.Dbulk*(var.poro^var.brugg);
        lam = petru_equation(var.n,constants.F,var.A,Deff,var.c_int,var.l,var.I_dis);
        k = Deff./lam.^2;
        eta=(2.*Vt/var.n)*asinh(-k./(2.*var.kc*var.a));
        w=2.*pi.*f;
        im=1i.*w.*var.poro;
        an=1;
        ad=1;
        bn=k;
        bd=im;
        cn=k.*sqrt(k).*tanh(sqrt((k+im)./Deff).*var.l);
        cd=im.*sqrt(k+im).*tanh(sqrt(k./Deff).*var.l);
        Z0=-2.*Vt.*tanh(var.n/2./Vt.*eta)/(var.n.*var.I_dis);
        Zf=Z0./((an./ad)-(bn./bd)+(cn./cd));
        Zd=1./(1i.*w.*var.a.*var.A.*var.l.*var.Cd);
        Z=(Zf.*Zd)./(Zf+Zd);
        set(handles.eta_output,'String',num2str(2.956 + eta));            
end
function [Z] = EIS_with_beta(varargin)
        handles = varargin{1};
        constants = varargin{2};
        var = varargin{3};
        f = varargin{4};
        Vt = Vt_compute(var.T);
        Deff = var.Dbulk*(var.poro^var.brugg);
        lam = petru_equation(var.n,constants.F,var.A,Deff,var.c_int,var.l,var.I_dis);
        k = Deff./lam.^2;
        w=2.*pi.*f;
        im=1i.*w.*var.poro;
        eta_eqn= @(x)(exp(-var.alpha.*var.n.*x./Vt) - ...
                    exp((1-var.alpha).*var.n.*x./Vt) - ...
                    (k./(var.kc.*var.a)));
        eta = fzero(eta_eqn,[0 -4]);
        an=1;
        ad=1;
        bn=k;
        bd=im;
        cn=k.*sqrt(k).*tanh(sqrt((k+im)./Deff).*var.l);
        cd=im.*sqrt(k+im).*tanh(sqrt(k./Deff).*var.l);
        Z0=Vt.*k./(var.n.*var.I_dis.*((var.alpha.*k) + ...
                (var.kc.*var.a.*exp((1-var.alpha).*var.n.*eta./Vt))));
        Zf=Z0./((an./ad)-(bn./bd)+(cn./cd));
        Zd=1./(1i.*w.*var.a.*var.A.*var.l.*var.Cd);
        Z=(Zf.*Zd)./(Zf+Zd);
        set(handles.eta_output,'String',num2str(2.956 + eta));         
end

function plot_figure(varargin)
handles = varargin{1};
Z = varargin{2};
if get(handles.hold_axis,'value')
    hold on;
else
    hold off;
end
    if get(handles.constant_axis,'value')
        temp = get(handles.axes1);
        plot(real(Z),-imag(Z));
        xlabel('Z'' [\Omega]');
        ylabel('-Z'''' [\Omega]');
        set(handles.axes1,'xlim',temp.XLim);
        set(handles.axes1,'ylim',temp.YLim);        
    else
        plot(real(Z),-imag(Z));
        xlabel('Z'' [\Omega]');
        ylabel('-Z'''' [\Omega]');    
    end
    datacursormode on
if length(varargin)>2
    f = varargin{3};
    hdt = datacursormode;
    set(hdt,'DisplayStyle','datatip','Enable','on');
    set(hdt,'UpdateFcn',{@labeldtips_3D,f,real(Z),imag(Z)});
end
end