function [ lamad,eta ]  = petru_equation_with_HLA_W_no_assum(n,F,A,a,kc,beta,Vt,Deff,c_int,l,I_dis)
% This is to solve the analytical solution without any assumptions
% This equation solves the Butler-Volmer equation completely wo assumptions
% This function return lambda as well as eta, DEFAUT IS LAMBDA
% Diffusion length calculation based on Petru's Theory to
% compute EIS in Li-air batteries.
% Here, n = number of charge transfer electrons
% F = Faradaic constant
% A = cross sectional area of the cathode
% Deff = Effective oxygen diffusion coefficient in the organic electrolyte
% l = Length of the cathode
% I_dis = Value of the dc discharge current
% F is the  inline function which is solved by numerical method to oxygen
% c_int is the inital concentration of oxygen dissolved in the electrolyte
% diffusion length. This diffusion length is needed to compute the Voltage,
% impedance response of a Li-air battery.
% Original Code
for i=1:length(I_dis)
[lamad(i),eta(i)] = main_prog(n,F,A,a,kc,beta,Vt,Deff,c_int,l,I_dis(i));
end
% x0 = 0.01.*ones(size(I_dis));  % Make a starting guess at the solution
% F = @(x)((n.*F.*A.*Deff.*c_int.*tanh(l./x))-(I_dis.*x));
% options = optimoptions('lsqnonlin','Display','off');
% % options = optimoptions('fsolve');
% options.TolFun = 1e-30;
% options.MaxIter = 10000;
% options.TolX = 1e-15;
% % options.FunValCheck = 'on';
% [lamad,~] = lsqnonlin(F,x0,1e-100,1e100); % Call solver
end

function [ lamad_temp,eta_temp ]  = main_prog(n,F,A,a,kc,beta,Vt,Deff,c_int,l,I_dis)
x0 = 0.01.*ones(size(I_dis));  % Make a starting guess at the solution
kf = @(x)(a*kc*exp(-beta*n/Vt*x));
kr = @(x)(a*kc*c_int*exp((1-beta)*n/Vt*x));
lam = @(x)(sqrt(Deff/kf(x)));
F = @(x)((n*F*A*Deff*(c_int-(kr(x)/kf(x)))*tanh(l/lam(x)))-(I_dis*lam(x)));
options = optimoptions('fsolve','Display','off');
% options = optimoptions('fsolve');
options.TolFun = 1e-30;
options.MaxIter = 10000;
options.TolX = 1e-15;
% options.FunValCheck = 'on';
[eta_temp,~] = fsolve(F,x0,options); % Call solver
lamad_temp = lam(eta_temp);
end
