clear all;clc;close all;
if ismac
    var_flder='/Users/mohitmehta/Copy/work/Phd/Data/Xujie Chen_data_on_Li-air/EIS paper/Saved Variables/';
elseif ~isunix
    var_flder='H:\Copy\work\Phd\Data\Xujie Chen_data_on_Li-air\EIS paper\Saved Variables\';
end
load([var_flder 'All_EIS_data.mat']);
experiment=1;           % Experiment number
ZreZimohm=cell2mat(Impedance(3,experiment));
names=char(Impedance{4,experiment});
num=cell2mat(Impedance(1,experiment));
ZreZimohmX(:,:)=ZreZimohm(:,1,:);
ZreZimohmY(:,:)=ZreZimohm(:,2,:);
ocv=num(:,10:11:end);
frq(:,:)=Impedance{1,experiment}(:,3:11:end);
%%
xlabel('Time [minutes]','Fontsize',15,'FontName','Times New Roman');
ylabel('Open Circuit Voltage [v]','Fontsize',15,'FontName','Times New Roman');
box off;
% axis square;
set(0,'ScreenPixelsPerInch',90);
set(gcf,'Color','w');
set(gca,'FontSize',15,'Color','w','FontName','Times New Roman');
%%




exp_total_time=[1./frq(:,1)/60;...
    (1/frq(end,1)+1./frq(:,2))/60;...
    (1/frq(end,1)+1/frq(end,2)+1./frq(:,3))/60;...
    (1/frq(end,1)+1/frq(end,2)+1/frq(end,3)+1./frq(:,4))/60;...
    (1/frq(end,1)+1/frq(end,2)+1/frq(end,3)+1/frq(end,4)+1./frq(:,5))/60];
voltages=[ocv(:,1);ocv(:,2);ocv(:,3);ocv(:,4);ocv(:,5)];
%%
voltages=[ocv(:,1);ocv(:,2);ocv(:,3);ocv(:,4);ocv(:,5)];
% clearvars -except exp_total_time voltages
%%
% plot(1:(39*5),[ocv(:,1);ocv(:,2);ocv(:,3);ocv(:,4);ocv(:,5)])

close all;
plot(exp_total_time,voltages);
%%
%%  Saving Figures
% filename=['Experiment_' num2str(experiment) '_j_' names(i,1:4) ];
filename='exp_9_all';
if ismac
      foldername='/Users/mohitmehta/Viivo/Figures_date/';
%     foldername='/Users/mohitmehta/Copy/My Figures/Comparison_figures/';
elseif isunix
    display('Sorry No path');
    break;
else
    foldername='H:\Copy\work\Phd\Show_to_Dr_Petru\EIS_with_discharge_curve\';
end
save_figures_with_cnt([foldername filename],gcf);
%%

%% Plot Database data 
close all;
cc=distinguishable_colors(6,'w');
plotlinewidth=1.5;
markertype='O';
markersize=7;
fontsize = 20;
figure(1);
% i=4;
for j=1:5
%     figure(j)
[AX,H1,H2] =plotyy(ZreZimohmX(:,j),ZreZimohmY(:,j),ZreZimohmX(:,j),ocv(:,j),'plot'); hold on;
set(H1,'LineStyle','O','LineWidth',plotlinewidth,'MarkerFaceColor',cc(j,:),'Color',cc(j,:));
set(H2,'LineStyle','O','LineWidth',plotlinewidth,'MarkerFaceColor',cc(j+1,:),'Color',cc(j+1,:));
set(H1,'LineStyle',':');
set(H2,'LineStyle',':');
ocv_min=2;
ocv_max=3.5;
figure_steps=4;
ocv_tick_diff=(ocv_max-ocv_min)/figure_steps;
z_tick_diff=(z_max-z_min)/figure_steps;

for n=1:figure_steps+1
ocv_ticks(1,n)=ocv_min+((n-1)*ocv_tick_diff);
z_ticks(1,n)=z_min+((n-1)*z_tick_diff);
end
z_min=0;
z_max=450;
set(AX(2),'ylim',[ocv_min ocv_max],'YTick',ocv_ticks,'YColor', cc(j+1,:));
set(AX(1),'ylim',[z_min z_max],'YTick',z_ticks);
% 
set(AX(1),'xlim',[0 500]);
set(AX(2),'xlim',[0 500]);
title(names(j,:),'Interpreter','none');
grid off; box on;
axis square;               % Axis Size and font type
set(gcf,'Color','w'); 
set(gcf,'Position',[136,322,560,420]);                            % Opening the figure on second screen with full size
set(0,'ScreenPixelsPerInch',90); 
set(get(AX(1),'Ylabel'),'String','-Z'''' [\Omega]','FontSize',fontsize);
set(get(AX(2),'Ylabel'),'String','OCV','FontSize',fontsize);
set(get(AX(1),'Xlabel'),'String','Z'' [\Omega]','FontSize',fontsize);
set(get(AX(2),'Xlabel'),'String','Z'' [\Omega]','FontSize',fontsize);
set(AX(1),'FontSize',fontsize);
set(AX(2),'FontSize',fontsize);  
set(AX(1),'PlotBoxAspectRatio',[1 50/220 1]);
set(AX(2),'PlotBoxAspectRatio',[1 50/220 1]);
end