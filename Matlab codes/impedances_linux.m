function [p,Z_out,tmp]=impedances_linux( number, tmp_plot, tmp_j, tmp_m, tmp_o, tmp_q)
% close all;
cur=pwd;
tmp=logspace(-3,6,100);
w=2.*pi.*tmp;
Vt=26e-3;
b=0.5;
F=96500;
A=1;
n=2;
mul=[1e-2,1e-1,0.5,1e0,2,1e1,1e2];
% mul=[0.5,1e0,2];
cc=distinguishable_colors(size(mul,2),'w');
font_shape={'-x','-O','-^','-v','-*','-p','-ms'};
switch number
    case 1      % one specie semi-infinite
      plot_anno_nos=[1 4 5];
      plot_name_nos=[1 2 3];  
      i_min=1;
      i_max=size(mul,2);      
      j_min=1;
      j_max=size(mul,2);      
      m_min=1;
      m_max=size(mul,2);      
      o_min=4;
      o_max=4;    
      q_min=4;
      q_max=4;      
    case 2      % two species semi-infinite
      plot_name_nos=[4 5 6];   
      plot_anno_nos=[2 4 6];
      i_min=1;
      i_max=size(mul,2);      
      j_min=1;
      j_max=size(mul,2);      
      m_min=1;
      m_max=size(mul,2);      
      o_min=4;
      o_max=4;    
      q_min=4;
      q_max=4;           
    case 3      % one specie transmissive
      plot_name_nos=[7 8 9 10];   
      plot_anno_nos=[1 4 5 8];
      i_min=1;
      i_max=size(mul,2);      
      j_min=1;
      j_max=size(mul,2);      
      m_min=1;
      m_max=size(mul,2);      
      o_min=1;
      o_max=size(mul,2);      
      q_min=4;
      q_max=4;  
    case 4      % one specie reflective
      plot_name_nos=[7 8 9 10];   
      plot_anno_nos=[1 4 5 8];
      i_min=1;
      i_max=size(mul,2);      
      j_min=1;
      j_max=size(mul,2);      
      m_min=1;
      m_max=size(mul,2);      
      o_min=1;
      o_max=size(mul,2);      
      q_min=4;
      q_max=4;      
    case 5      % two species transmissive
       plot_name_nos=[11 12 13 14]; 
       plot_anno_nos=[2 4 6 8];
      i_min=1;
      i_max=size(mul,2);      
      j_min=1;
      j_max=size(mul,2);      
      m_min=1;
      m_max=size(mul,2);      
      o_min=1;
      o_max=size(mul,2);      
      q_min=4;
      q_max=4;         
    case 6      % two species reflective
      plot_name_nos=[11 12 13 14]; 
      plot_anno_nos=[2 4 6 8];
      i_min=1;
      i_max=size(mul,2);      
      j_min=1;
      j_max=size(mul,2);      
      m_min=1;
      m_max=size(mul,2);      
      o_min=1;
      o_max=size(mul,2);      
      q_min=4;
      q_max=4;       
    case 7      % one specie gerischer
      plot_name_nos=[15 16 17 18];
      plot_anno_nos=[1 4 5 7];
      i_min=1;
      i_max=size(mul,2);      
      j_min=1;
      j_max=size(mul,2);      
      m_min=1;
      m_max=size(mul,2);      
      o_min=1;
      o_max=size(mul,2);      
      q_min=4;
      q_max=4;   
    case 8      % one specie finite gerischer
      plot_name_nos=[19 20 21 22 23]; 
      plot_anno_nos=[1 4 5 7 8];
      i_min=1;
      i_max=size(mul,2);      
      j_min=1;
      j_max=size(mul,2);      
      m_min=1;
      m_max=size(mul,2);      
      o_min=1;
      o_max=size(mul,2);      
      q_min=1;
      q_max=size(mul,2);   
    case 9      % two species transmissive reflective
      plot_name_nos=[7 8 9 10];
      plot_anno_nos=[2 4 6 8];
      i_min=1;
      i_max=size(mul,2);      
      j_min=1;
      j_max=size(mul,2);      
      m_min=1;
      m_max=size(mul,2);      
      o_min=1;
      o_max=size(mul,2);      
      q_min=4;
      q_max=4;  
    otherwise
        disp('other value');
end
% for plot_name_pos=1:size(plot_name_nos,2)
for plot_name_pos=tmp_plot:tmp_plot
for q=tmp_q:tmp_q
for o=tmp_o:tmp_o
for m=tmp_m:tmp_m
for j=tmp_j:tmp_j
figure;
% clf;
set(gcf,'Visible','on');
% for i=i_min:i_max
for i=4:4
switch plot_name_nos(plot_name_pos)
    case 1      % one specie semi-infinite
        Do=7e-6*mul(i);
        Co=3.26e-6*mul(m);
        Cd=40e-6*mul(j);
        l=1e-4;
        eta=-0.5;
        ko=1.3e-8;
        Dr=7e-6;
        Cr=3.26e-6;
        name_part_variable='Do';     % write variable name with 'i' or inner loop
    case 2      % one specie semi-infinite 
        Do=7e-6*mul(m);
        Co=3.26e-6*mul(j);
        Cd=40e-6*mul(i);
        l=1e-4;
        eta=-0.5;
        ko=1.3e-8;
        Dr=7e-6;
        Cr=3.26e-6;
        name_part_variable='Cd';     % write variable name with 'i' or inner loop 
    case 3      % one specie semi-infinite 
        Do=7e-6*mul(j);
        Co=3.26e-6*mul(i);
        Cd=40e-6*mul(m);
        l=1e-4;
        eta=-0.5;
        ko=1.3e-8;
        Dr=7e-6;
        Cr=3.26e-6;
        name_part_variable='Co';     % write variable name with 'i' or inner loop     
    case 4      % two species semi-infinite 
        Do=7e-6;
        Co=3.26e-6;
        Cd=40e-6*mul(j);
        l=1e-4;
        eta=-0.5;
        ko=1.3e-8;
        Dr=7e-6*mul(i);
        Cr=3.26e-6*mul(m);
        name_part_variable='Dr';     % write variable name with 'i' or inner loop     
    case 5      % two species semi-infinite 
        Do=7e-6;
        Co=3.26e-6;
        Cd=40e-6*mul(i);
        l=1e-4;
        eta=-0.5;
        ko=1.3e-8;
        Dr=7e-6*mul(m);
        Cr=3.26e-6*mul(j);
        name_part_variable='Cd';     % write variable name with 'i' or inner loop     
    case 6      % two species semi-infinite 
        Do=7e-6;
        Co=3.26e-6;
        Cd=40e-6*mul(m);
        l=1e-2;
        eta=-0.5;
        ko=1.3e-8;
        Dr=7e-6*mul(j);
        Cr=3.26e-6*mul(i);
        name_part_variable='Cr';     % write variable name with 'i' or inner loop         
    case 7      % one specie transmissive/reflective 
        Do=7e-6*mul(i);
        Dr=7e-6;        
        Cd=40e-6*mul(j);        
        Co=3.26e-6*mul(m);
        Cr=3.26e-6;
        ko=1.3e-8;        
        l=1e-2*mul(o);
        eta=-0.5;
        name_part_variable='Do';     % write variable name with 'i' or inner loop     
    case 8      % one specie transmissive/reflective
        Do=7e-6*mul(o);
        Dr=7e-6;        
        Cd=40e-6*mul(i);        
        Co=3.26e-6*mul(j);
        Cr=3.26e-6;
        ko=1.3e-8;        
        l=1e-2*mul(m);
        eta=-0.5;
        name_part_variable='Cd';     % write variable name with 'i' or inner loop     
    case 9      % one specie transmissive/reflective
        Do=7e-6*mul(m);
        Dr=7e-6;        
        Cd=40e-6*mul(o);        
        Co=3.26e-6*mul(i);
        Cr=3.26e-6;
        ko=1.3e-8;        
        l=1e-2*mul(j);
        eta=-0.5;
        name_part_variable='Co';     % write variable name with 'i' or inner loop         
    case 10      % one specie transmissive/reflective
        Do=7e-6*mul(j);
        Dr=7e-6;        
        Cd=40e-6*mul(m);        
        Co=3.26e-6*mul(o);
        Cr=3.26e-6;
        ko=1.3e-8;        
        l=1e-2*mul(i);
        eta=-0.5;
        name_part_variable='l';     % write variable name with 'i' or inner loop                 
    case 11      % two species transmissive/reflective 
        Do=7e-6;
        Dr=7e-6*mul(i);        
        Cd=40e-6*mul(j);        
        Co=3.26e-6;
        Cr=3.26e-6*mul(m);
        ko=1.3e-8;        
        l=1e-2*mul(o);
        eta=-0.5;
        name_part_variable='Dr';     % write variable name with 'i' or inner loop                     
    case 12      % two species transmissive/reflective 
        Do=7e-6;
        Dr=7e-6*mul(o);        
        Cd=40e-6*mul(i);        
        Co=3.26e-6;
        Cr=3.26e-6*mul(j);
        ko=1.3e-8;        
        l=1e-2*mul(m);
        eta=-0.5;
        name_part_variable='Cd';     % write variable name with 'i' or inner loop                         
    case 13      % two species transmissive/reflective 
        Do=7e-6;
        Dr=7e-6*mul(m);        
        Cd=40e-6*mul(o);        
        Co=3.26e-6;
        Cr=3.26e-6*mul(i);
        ko=1.3e-8;        
        l=1e-2*mul(j);
        eta=-0.5;
        name_part_variable='Cr';     % write variable name with 'i' or inner loop                             
    case 14      % two species transmissive/reflective 
        Do=7e-6;
        Dr=7e-6*mul(j);        
        Cd=40e-6*mul(m);        
        Co=3.26e-6;
        Cr=3.26e-6*mul(o);
        ko=1.3e-8;        
        l=1e-2*mul(i);
        eta=-0.5;
        name_part_variable='l';     % write variable name with 'i' or inner loop                                 
    case 15      % one specie gerischer semi-infinite
        Do=7e-6*mul(i);
        Dr=7e-6;        
        Cd=40e-6*mul(j);        
        Co=3.26e-6*mul(m);
        Cr=3.26e-6;
        ko=1.3e-8*mul(o);        
        l=1e-2;
        eta=-0.5;
        name_part_variable='Do';     % write variable name with 'i' or inner loop                                     
    case 16      % one specie gerischer semi-infinite
        Do=7e-6*mul(o);
        Dr=7e-6;        
        Cd=40e-6*mul(i);        
        Co=3.26e-6*mul(j);
        Cr=3.26e-6;
        ko=1.3e-8*mul(m);        
        l=1e-2;
        eta=-0.5;
        name_part_variable='Cd';     % write variable name with 'i' or inner loop                                         
    case 17      % one specie gerischer semi-infinite
        Do=7e-6*mul(m);
        Dr=7e-6;        
        Cd=40e-6*mul(o);        
        Co=3.26e-6*mul(i);
        Cr=3.26e-6;
        ko=1.3e-8*mul(j);        
        l=1e-2;
        eta=-0.5;
        name_part_variable='Co';     % write variable name with 'i' or inner loop                                     
    case 18      % one specie gerischer semi-infinite
        Do=7e-6*mul(j);
        Dr=7e-6;        
        Cd=40e-6*mul(m);        
        Co=3.26e-6*mul(o);
        Cr=3.26e-6;
        ko=1.3e-8*mul(i);        
        l=1e-2;
        eta=-0.5;
        name_part_variable='ko';     % write variable name with 'i' or inner loop                                                     
    case 19      % one specie gerischer finite
        Do=7e-6*mul(i);
        Dr=7e-6;        
        Cd=40e-6*mul(j);        
        Co=3.26e-6*mul(m);
        Cr=3.26e-6;
        ko=1.3e-8*mul(o);        
        l=1e-2*mul(q);
        eta=-0.5;
        name_part_variable='Do';     % write variable name with 'i' or inner loop                                     
    case 20      % one specie gerischer finite
        Do=7e-6*mul(q);
        Dr=7e-6;        
        Cd=40e-6*mul(i);        
        Co=3.26e-6*mul(j);
        Cr=3.26e-6;
        ko=1.3e-8*mul(m);        
        l=1e-2*mul(o);
        eta=-0.5;
        name_part_variable='Cd';     % write variable name with 'i' or inner loop                                         
    case 21      % one specie gerischer finite
        Do=7e-6*mul(o);
        Dr=7e-6;        
        Cd=40e-6*mul(q);        
        Co=3.26e-6*mul(i);
        Cr=3.26e-6;
        ko=1.3e-8*mul(j);        
        l=1e-2*mul(m);
        eta=-0.5;
        name_part_variable='Co';     % write variable name with 'i' or inner loop                                     
    case 22      % one specie gerischer finite
        Do=7e-6*mul(m);
        Dr=7e-6;        
        Cd=40e-6*mul(o);        
        Co=3.26e-6*mul(q);
        Cr=3.26e-6;
        ko=1.3e-8*mul(i);        
        l=1e-2*mul(j);
        eta=-0.5;
        name_part_variable='ko';     % write variable name with 'i' or inner loop                                                     
    case 23      % one specie gerischer finite
        Do=7e-6*mul(j);
        Dr=7e-6;        
        Cd=40e-6*mul(m);        
        Co=3.26e-6*mul(o);
        Cr=3.26e-6;
        ko=1.3e-8*mul(q);        
        l=1e-2*mul(i);
        eta=-0.5;
        name_part_variable='l';     % write variable name with 'i' or inner loop                                                            
    otherwise
        disp('other value');
end
    
% Cd=40e-6*mul(m);
% Do=7e-6*mul(i);
% Co=3.26e-6*mul(j);
% eta=-0.5;
% ko=1.3e-8;
% Dr=Do;
% Cr=Co;
i0=n*F*A*ko;        % constant i0
const0=Vt/(n*i0);   % constant0 
const1=Vt/((n^2)*F*A);
kr=ko;
k=ko*exp(-b*n*eta/Vt)-kr*exp((1-b)*n*eta/Vt);
deltax=sqrt(2.*Do./tmp);
deltay=sqrt(2.*Dr./tmp);
switch number
    case 1      % one specie semi-infinite
       const2=(1./(sqrt(1i.*w)));
       const3=1/(Co*sqrt(Do));
       name_part_name='one_specie_semi_infinite';
    case 2      % two species semi-infinite
       const2=(1./(sqrt(1i.*w)));
       const3=((1/(Co*sqrt(Do)))+(1/(Cr*sqrt(Dr))));
       name_part_name='two_species_semi_infinite';
    case 3      % one specie transmissive
       const2=(tanh(sqrt(1i.*w./Do).*l))./(sqrt(1i.*w));
       const3=1/(Co*sqrt(Do));
       name_part_name='one_specie_transmissive';
    case 4      % one specie reflective
       const2=(coth(sqrt(1i.*w./Do).*l))./(sqrt(1i.*w));
       const3=1/(Co*sqrt(Do)); 
       name_part_name='one_specie_reflective';
    case 5      % two species transmissive
       const2=((tanh(sqrt(1i.*w./Do).*l))./(Co.*sqrt(Do)))+((tanh(sqrt(1i.*w./Dr).*l))./(Cr.*sqrt(Dr)));
       const3=(1./(sqrt(1i.*w))); 
       name_part_name='two_species_transmissive';
    case 6      % two species reflective
       const2=((coth(sqrt(1i.*w./Do).*l))./(Co.*sqrt(Do)))+((coth(sqrt(1i.*w./Dr).*l))./(Cr.*sqrt(Dr)));
       const3=(1./(sqrt(1i.*w))); 
       name_part_name='two_species_reflective';
    case 7      % one specie gerischer
       const2=(1./(sqrt(k+(1i.*w))));
       const3=1/(Co*sqrt(Do));
       name_part_name='one_specie_gerishcer';
    case 8      % one specie finite gerischer
       const2=(tanh(sqrt((k+(1i.*w))./Do).*l))./(sqrt(k+(1i.*w)));
       const3=1/(Co*sqrt(Do)); 
       name_part_name='one_specie_transmissive_gerishcer';
    case 9      % two species transmissive reflective
       const2=((tanh(sqrt(1i.*w./Do).*l))./(Co.*sqrt(Do)))+((coth(sqrt(1i.*w./Dr).*l))./(Cr.*sqrt(Dr)));
       const3=(1./(sqrt(1i.*w)));
       name_part_name='two_species_transmissive_reflective';
    otherwise
        disp('other value');
end
Zf=const0+(const1.*const2.*const3); 
Z=Zf./(1+(1i.*w.*Zf.*Cd));
Z_out(i,:)=Z;
l=l*1e-2;
name_stack={['$D_o=' num2str(Do/10^(floor(log10(Do)))) '\times 10^{' num2str(floor(log10(Do))) '}\,\textrm{cm}^2/\textrm{s}$'],...
            ['$D_r=' num2str(Dr/10^(floor(log10(Dr)))) '\times 10^{' num2str(floor(log10(Dr))) '}\,\textrm{cm}^2/\textrm{s}$'],...
            ['$n=' num2str(n) '$'],...
            ['$C_d=' num2eng(Cd) ' F/cm^2$'],...
            ['$c_o=' num2str(Co/10^(floor(log10(Co)))) '\times 10^{' num2str(floor(log10(Co))) '}\,\textrm{mol/cm}^3$'],...
            ['$c_r=' num2str(Cr/10^(floor(log10(Cr)))) '\times 10^{' num2str(floor(log10(Cr))) '}\,\textrm{mol/cm}^3$'],...
            ['$k_o=' num2str(ko/10^(floor(log10(ko)))) '\times 10^{' num2str(floor(log10(ko))) '}\,\textrm{cm}/\textrm{s}$'],...
            ['$l=' num2eng(l) ' m$']};

p(number)=plot(real(Z),-imag(Z),'-','Color',cc(i,:),'DisplayName',name_stack(plot_anno_nos(plot_name_pos)));
% p(number)=plot(real(Z),-imag(Z),'-','Color',cc(i,:));
set(p(number),'LineWidth',1.5);
% figure (2)
% plot(tmp,sqrt((real(Z).^2)+(imag(Z).^2)),font_shape{i},'Color',cc(i,:),'DisplayName',name_stack(plot_anno_nos(plot_name_pos)));
hold on;
end
xlim([0 20]);ylim([0 20]);
xlabel('Z''[\Omega]','Fontsize',20,'FontName','Times New Roman');
ylabel('-Z''''[\Omega]','Fontsize',20,'FontName','Times New Roman');
box off;
axis square;
set(gcf,'Position',[134   218   560   420])
set(0,'ScreenPixelsPerInch',120);
set(gcf,'Color','w');

set(gca,'FontSize',20,'Color','w','FontName','Times New Roman');
set(gcf,'Position',[825,61,720,720]);
set(legend('-DynamicLegend'),'Interpreter','latex','FontSize',15,'FontName','Times New Roman','Location','NorthWest');
% annotation(gcf,'textbox',...
%     [0.589359788359788 0.768915343915345 0.763417989417991 0.200529100529099],...
%     'String',{['$n=' num2str(n) '$'],...
%     ['$C_d=' num2eng(Cd) ' F/cm^2$'],...
%     ['$c_o=' num2str(Co/10^(floor(log10(Co)))) '\times 10^{' num2str(floor(log10(Co))) '}\,\textrm{mol/cm}^3$'],...
%     ['$c_r=' num2str(Cr/10^(floor(log10(Cr)))) '\times 10^{' num2str(floor(log10(Cr))) '}\,\textrm{mol/cm}^3$'],...
%     ['$l=' num2eng(l) ' m$'],...
%     ['$D_r=' num2str(Dr/10^(floor(log10(Dr)))) '\times 10^{' num2str(floor(log10(Dr))) '}\,\textrm{cm}^2/\textrm{s}$'],...
%     },...
%     'FontSize',15,...
%     'FitBoxToText','off',...
%     'LineStyle','none','FontName','Times New Roman','Interpreter','latex');
annotation(gcf,'textbox',...
    [0.589359788359788 0.768915343915345 0.763417989417991 0.200529100529099],...
    'String',name_stack(plot_anno_nos(plot_anno_nos~=plot_anno_nos(plot_name_pos))),...
    'FontSize',15,...
    'FitBoxToText','off',...
    'LineStyle','none','FontName','Times New Roman','Interpreter','latex');
% hold off;
sdf('EIS_plots');
% if ismac==1
%      mkdir(['/Users/mohitmehta/Copy/My Figures/Pospectus_figures_' datestr(date)]);
%     cd(['/Users/mohitmehta/Copy/My Figures/Pospectus_figures_' datestr(date)]);
% elseif isunix==1
% cd('/panfs/storage.local/ame/home/mrm11s/my_matlab_codes/figures');
% else
%     mkdir (['H:\Copy\My Figures\Pospectus_figures_' datestr(date)]);
%     cd(['H:\Copy\My Figures\Pospectus_figures_' datestr(date)]);
% % cd('C:\Users\Mohit\Desktop\test');
% end
% title(['j' num2str(j) ' m' num2str(m) ' o' num2str(o) ' q' num2str(q)])
% file_name=['j_' num2str(j) '_m_' num2str(m) '_o_' num2str(o) '_q_' num2str(q) '_' name_part_name '_' name_part_variable];
% save_figures(file_name);
% close all;
end
end
end
end
end
% if isunix~=1
%     combine_figures(name_part_name);
% end
hold off;
end
function save_figures(file_name)
if ismac==1
    files=dir([file_name '_*.pdf']);
elseif isunix==1
	files=dir([file_name '_*.fig']);
else
    files=dir([file_name '_*.pdf']);
end

[~,idx] = sort([files.datenum]);
if size(files,1)==0
    cnt=0;
else
    for num=6:-1:4
        if ~isnan(str2double(files(idx(size(idx,2))).name(end-num:end-4)))
            cnt=str2double(files(idx(size(idx,2))).name(end-num:end-4));
            break;
        end
    end
end
cnt=cnt+1;
sdf('EIS_plots');
if ismac==1
    a=[file_name '_' num2str(cnt) '.pdf'];
    export_fig(gcf, a);
elseif isunix==1
    a=[file_name '_' num2str(cnt) '.fig'];
    savefig(gcf, a);
else
    a=[file_name '_' num2str(cnt) '.eps'];
    export_fig(gcf, a);
end
          
display([a ' has been written']);
end
function combine_figures(name_part_name)
% cd('H:\Copy\My Figures\Pospectus_figures_12152013');
cd('C:\Users\Mohit\Desktop\test')
files=dir(['*' name_part_name '_*.pdf']);
[~,idx] = sort([files.datenum]);
if size(files,1)==0
    cnt=0;
else
    for num=6:-1:4
        if ~isnan(str2double(files(idx(size(idx,2))).name(end-num:end-4)))
            cnt=str2double(files(idx(size(idx,2))).name(end-num:end-4));
            break;
        end
    end

end
cnt=cnt+1;
	a=['cmb_' name_part_name '_' num2str(cnt) '.pdf'];
  append_pdfs(a,files(idx).name);  
display([a ' has been written']);
end