function co2 = c_ext_calculator(pressure,perc_o2,T)
% Returns ambient oxygen concentration
% Pressure is in Pascals
% R = 8.314 J/mol K
% T in Kelvins
% 1e-6 converts mol/m3 to mol/cm3
% perc_o2 is in percentage (eg. O2 = 21%)
co2 = pressure./8.314./T.*perc_o2.*1e-6/100;    
end