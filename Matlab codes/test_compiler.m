%% Working test
clc;
E0=Y(1);
Vt=26e-3;
beta=0.5;
n=4;
F=96500;
c_int=3.26e-6;
ka=1.3e-5;
% ka_paper=nFc(0,t)ka_simulation
Deff=3e-6;
l=0.03;
a=5e4;
kc=1.4e-9;
beta0 = [ka;kc];
for i=1:length(X)
lamad(i)=petru_equation(n,F,Deff,c_int,l,X(i));
end
% clearvars -except E0 anode_overpotential cathode_overpotential other_resistances X Y
V=E0-(Vt./beta.*asinh(X./2./n./F./c_int./ka))-(Vt./n./beta.*log(Deff./kc./a./lamad.^2)')-(500.*X);
modelfun = @(b,x)(E0-(Vt./beta.*asinh(x./2./n./F./c_int./b(1)))-(Vt./n./beta.*log(Deff./b(2)./a./lamad.^2)')-(500.*x));
k1(:,:) = nlinfit(X,Y,modelfun,beta0);
display(['Predicted ka = ' num2str(k1(1)) ' cm/s']);
display(['Predicted kc = ' num2str(k1(2)) ' cm/s']);
plot(X*1e3,modelfun(k1,X(:)),'-');hold on;
plot(X*1e3,Y,'O','Color','red');
legend('Model','Actual');
hold off
set(0,'ScreenPixelsPerInch',90);
set(gcf,'Color','w');
set(gca,'FontSize',15,'Color','w','FontName','Times New Roman');
xlabel('Current Density [mA/cm^2]')
ylabel('OCV [Volts]')
if exist('disp','var')
    delete(disp);
end;
disp=annotation('textbox',...
    [0.331524880708932 0.786329913015213 0.247784594410361 0.0926998841251448],'String', {['Predicted ka = ' num2str(k1(1)) ' cm/s'],...
    ['Predicted kc = ' num2str(k1(2)) ' cm/s']},'FitBoxToText','on','FontSize',20,...
    'FontName','Times New Roman','LineStyle','none');
axis square