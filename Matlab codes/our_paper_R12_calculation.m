clear all;
close all;
clc;
%%
% R12=Vt/n/beta/I*(2*sinh(2*l)/(sinh(2*l)+(2*l)));
a=5e4;
epsi=0.75;
n=2;
beta=0.5;
Vt=26e-3;
I=0.1e-3;
eta=0.6;
% Cd=10e-6;
L=0.01;
A=1;
co=69.7865e-6;
F=96500;
brugg=1.5;
%% R12 (High cathode specific area)
clc;
Min=Vt/(n*beta*I);
display('---*---*---*---*---*---*---*---')
display(['Min R12 =' num2str(Min)]);
display(['Max R12 =' num2str(2*Min)]);

% for R12=551:100:1000
clc;
R12=860;

syms lam;
%lambda=vpasolve(n*A*F*Deff*co*tanh(L/lam)/lam-I, lam,1.5);
lambda=vpasolve((n*R12*beta*I/(2*Vt))-(sinh(2*lam)/(sinh(2*lam)+(2*lam))), lam,1.5);
l=double(lambda);
display('---*---*---*---*---*---*---*---')
display(['R12=' num2str(R12)]);
display(['l=' num2str(l)]);
clear lambda
% end
%% R1 and R2 (Low cathode specific area)
R1=980;
R2=710;
syms lam;
lambda=vpasolve((R2/R1)-((sinh(2*lam)-(2*lam))/(sinh(2*lam)+(2*lam))), lam,1.5);
lambda=double(lambda);
clc;
display('---*---*---*---*---*---*---*---')
display(['R1=' num2str(R1)]);
display(['R2=' num2str(R2)]);
display(['l=' num2str(lambda)]);


% l=L/lambda;
% display(l);
% R12=Vt/beta/I
% R12_full=Vt/n/beta/I*(2*sinh(2*l)/(sinh(2*l)+(2*l)))
%%
lam=1.96;
(sinh(2*lam)-(2*lam))/(sinh(2*lam)+(2*lam))
%%
clc;
lamda=L/l;
Deff=I*lamda/n/F/A/(co)/tanh(l);
display('---*---*---*---*---*---*---*---')
display(['Deff=' num2str(Deff)]) 
D=Deff/(epsi^brugg);
display(['D=' num2str(D)])
k=Deff/(lamda^2*a)*exp(n*beta/Vt*eta);
display(['k=' num2str(k)])
%%
clc;
clear all;
close all;
P_air=170e3;
R=8.314;
T=293;
R12=860;
a=5e4;
% Rlist=
% list=[0.18:-0.005:0.003];
% cc=distinguishable_colors(length(list),'w');
% for i=1:length(list)
epsi=0.75;
n=1;
beta=0.5;
Vt=26e-3;
I=0.1e-3;
eta=15e-3;
% Cd=10e-6;
L=0.01;
A=3.14;
F=96500;
brugg=1.5;
c_air=P_air/R/T/1e6;
c_o2=c_air*1;  % concentration of oxygen in air
% sol=0.345;
% co=c_o2*list(i);
co=c_o2*0.005;
Min=Vt/(n*beta*I);
syms lam;
if (R12<Min || R12>(2*Min))
    display('FUCK FUCK'); break;
end
lambda=vpasolve((n*R12*beta*I/(2*Vt))-(sinh(2*lam)/(sinh(2*lam)+(2*lam))), lam,1.5);
l=double(lambda);
clear lambda
lamda=L/l;
Deff=I*lamda/n/F/A/(co)/tanh(l);
D=Deff/(epsi^brugg);
k=Deff/(lamda^2*a)*exp(n*beta/Vt*eta);
display('---*---*---*---*---*---*---*---')
display('---*---*---*---*---*---*---*---')
display(['i= ' num2str(i)]);
display(['Min R12 =' num2str(Min)]);
display(['Max R12 =' num2str(2*Min)]);
display('---*---*---*---*---*---*---*---')
display(['concentration of air is ' num2eng(c_air)]);
display(['concentration of oxygen (100% of O2) in air is ' num2eng(c_o2)]);
display('---*---*---*---*---*---*---*---')
display(['R12=' num2str(R12)]);
display(['l=' num2str(l)]);
display('---*---*---*---*---*---*---*---')
display(['Deff=' num2str(Deff)]) 
display(['D=' num2str(D)])
display(['k=' num2str(k)])
% dplot(i)=D;
% % plot(i,D,'Color',cc(ceil(i*10),:),'LineWidth',4,'DisplayName',num2str(co));hold on;
% plot(i,D,'Color',cc(i,:),'LineWidth',4,'DisplayName',[num2str(list(i)) ' Do= ' num2str(D)]);hold on;
% end
% set(legend('-DynamicLegend'),'Interpreter','none','FontSize',15,'FontName','Times New Roman');
% % axis([0.3 0.65 1e-7 1e-8])