function  [ZreZimohm,names,ZreZimohmX,ZreZimohmY,ocv,frq,t]=ld_eis_msrmnt(Impedance,experiment )
ZreZimohm=cell2mat(Impedance(3,experiment));
names=char(Impedance{4,experiment});
ZreZimohmX(:,:)=ZreZimohm(:,1,:);
ZreZimohmY(:,:)=ZreZimohm(:,2,:);
num=cell2mat(Impedance(1,experiment));
ocv=num(:,10:11:end);
frq(:,:)=Impedance{1,experiment}(:,3:11:end);
t=num(:,2:11:end);
end

