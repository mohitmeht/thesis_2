clear
kb= 1.3806488e-23;      % Boltzman constant
q=1.602176e-19;         % Electron Energy
F=96485;                % Faraday's constant
% l=0.04;     % Length in cm % from the paper
l_multiple = logspace(-5,0,100);
temp_in_c = 30;
T= 273.15 + temp_in_c;
Vt=kb*T/q;
c_int=3.26e-6;     % converting to mol/cm3
% c_int_multiple  = logspace(-10,-4,100);
a=4e4;      % Calculated using Pt distribution and cathode physical parameters
% a_multiple=[4e3 4e4 4e5 4e6 4e7];    
epsi=0.75;
k0 = logspace(-10,-4,100);
% k0 = 1e-10;
beta=0.5;
n=2;
E0=2.956; % First value of the voltage from the data % given in the paper
% Dbulk_multiple  = logspace(-10,-4,100);
Dbulk  = [7e-6];
A = 1;
Deff=epsi^1.5*Dbulk;                          % comparison simulations
I_dis_multiple=logspace(-5,-2.30102999566398,100)';
x0 = 0.01;  % Make a starting guess at the solution
% lamda = zeros(length(I_dis_multiple),1);
% fval = zeros(length(I_dis_multiple),1);
% eta_c = zeros(length(I_dis_multiple),length(Dbulk_multiple));
tic
% for j=1:length(Dbulk_multiple)

Length_to_write_csv_files = length(I_dis_multiple);
for k=1:length(l_multiple)
    l=l_multiple(k);
    variable_parameter = 1;
matrix_b_dim = length(variable_parameter);
x0 = 0.01.*ones(length(I_dis_multiple),length(variable_parameter));  % Make a starting guess at the solution
mehta_eqn = @(x)(n.*F*A.*c_int.*tanh(l./x)*diag(Deff)-(repmat(I_dis_multiple,1,matrix_b_dim).*x));
options = optimoptions('fsolve','Display','off');
options.TolFun = 1e-20;

[lamda,fval] = fsolve(mehta_eqn,x0,options); % Call solver
eta_c =Vt./n.*repmat((beta.^-1),length(1),1).*log(lamda.^2./Deff*repmat((k0),length(1),1).*a);
list_of_acceptable_values = find(min(eta_c)<-0.25);
for j=1:Length_to_write_csv_files
    for i=1:Length_to_write_csv_files
        X(((Length_to_write_csv_files*(j-1))+i),1) = log10(I_dis_multiple(i));
        Y(((Length_to_write_csv_files*(j-1))+i),1) = log10(k0(j));
        Z(((Length_to_write_csv_files*(j-1))+i),1) = eta_c(i,j);
    end
end
csvwrite_with_headers(['Idis_k0_l_' num2str(k) '.csv'],[X,Y,Z],{'I_dis','k0','eta_c'})
end
toc
%% With Temperature
clear

kb= 1.3806488e-23;      % Boltzman constant
q=1.602176e-19;         % Electron Energy
F=96485;                % Faraday's constant
l=0.04;     % Length in cm % from the paper
% temp_in_c = 30;
temp_in_c_multiple = linspace(-30,50,100);
% T= 273.15 + temp_in_c;
% Vt=kb*T/q;
c_int=3.26e-6;     % converting to mol/cm3
% c_int_multiple  = logspace(-10,-4,100);
a=4e4;      % Calculated using Pt distribution and cathode physical parameters
epsi=0.75;
k0 = logspace(-10,-4,300);
% k0 = 1e-10;
beta=0.5;
n=2;
E0=2.956; % First value of the voltage from the data % given in the paper
% Dbulk_multiple  = logspace(-10,-4,100);
Dbulk  = [7e-6];
A = 1;
Deff=epsi^1.5*Dbulk;                          % comparison simulations

I_dis_multiple=logspace(-5,-2.30102999566398,300)';
x0 = 0.01;  % Make a starting guess at the solution
% lamda = zeros(length(I_dis_multiple),1);
% fval = zeros(length(I_dis_multiple),1);
% eta_c = zeros(length(I_dis_multiple),length(Dbulk_multiple));
tic
% for j=1:length(Dbulk_multiple)


    variable_parameter = 1;
matrix_b_dim = length(variable_parameter);
x0 = 0.01.*ones(length(I_dis_multiple),length(variable_parameter));  % Make a starting guess at the solution
mehta_eqn = @(x)(n.*F*A.*c_int.*tanh(l./x)*diag(Deff)-(repmat(I_dis_multiple,1,matrix_b_dim).*x));
options = optimoptions('fsolve','Display','off');
options.TolFun = 1e-20;
[lamda,fval] = fsolve(mehta_eqn,x0,options); % Call solver
for k=1:length(temp_in_c_multiple)
    temp_in_c = temp_in_c_multiple(k);
    T= 273.15 + temp_in_c;
    Vt=kb*T/q;
eta_c =Vt./n.*repmat((beta.^-1),length(1),1).*log(lamda.^2./Deff*repmat((k0),length(1),1).*a);

for j=1:length(k0)
    for i=1:length(I_dis_multiple)
        X(((length(I_dis_multiple)*(j-1))+i),1) = log10(I_dis_multiple(i));
        Y(((length(I_dis_multiple)*(j-1))+i),1) = log10(k0(j));
        Z(((length(I_dis_multiple)*(j-1))+i),1) = eta_c(i,j);
    end
end
csvwrite_with_headers(['Idis_k0_T_' num2str(k) '.csv'],[X,Y,Z],{'I_dis','k0','eta_c'})
end

toc

