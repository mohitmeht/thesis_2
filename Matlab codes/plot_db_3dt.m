function plot_db_3dt()
global ZreZimohm names ZreZimohmX ZreZimohmY ocv frq t
cc=jet(300);
for j=1:size(ZreZimohmX,2)
for i=1:size(ZreZimohmX,1)
    plot3(ZreZimohmX(i,j),t(i,j)./60,ZreZimohmY(i,j),'O','LineWidth',5,'color',cc(ceil(t(i,j)./60),:));hold on;
end
end
end

