%% Plot 1
close all;
clear all;
clc;
for i=1:1
tmp_plot=i;
tmp_j=4;
tmp_m=4;
tmp_o=4;
tmp_q=4;
[a2,Z,tmp]=impedances_linux(8, tmp_plot, tmp_j, tmp_m, tmp_o, tmp_q);
end
% set(a2(2),'Color','blue')
% set(a2(2),'markers',4); 4526 4726
%% Bode plot
close all
[AX,hline1,hline2] = plotyy(log10(2*pi()*tmp),abs(Z(4,:)),log10(2*pi()*tmp),180*angle(Z(4,:))/pi(),'plot','plot');
set(get(AX(1),'Ylabel'),'String','|Z|','Fontsize',20,'FontName','Times New Roman');
set(get(AX(2),'Ylabel'),'String','\theta^\circ','Fontsize',20,'FontName','Times New Roman');
set(get(AX(2),'Xlabel'),'String','log(\omega)','Fontsize',20,'FontName','Times New Roman');
set(gcf,'Color','w'); 
set(AX(2),'xlim',[-2.5 7]);
set(AX(1),'xlim',[-2.5 7]);
% set(hline2,'LineWidth',1.5);
% figure (2)
% plot(tmp,sqrt((real(Z).^2)+(imag(Z).^2)),font_shape{i},'Color',cc(i,:),'DisplayName',name_stack(plot_anno_nos(plot_name_pos)));
hold on;
box off;

set(0,'ScreenPixelsPerInch',120);
set(gcf,'Color','w');
% set(AX(1),'FontSize',20,'Color','w','FontName','Times New Roman');
set(gcf,'Position',[825,61,720,720]);


%% Plot 2
[a1,Z(2,:),~]=impedances_linux(1);
Z_re=real(Z);
set(a1,'Color',[0.850980392156863 0.325490196078431 0.0980392156862745]);
set(a1,'markers',4)
display('Plotting Complete');
%% Data cursors
datacursormode on
hdt = datacursormode;
set(hdt,'DisplayStyle','datatip');
set(hdt,'UpdateFcn',{@labeldtips,tmp,real(Z(4,:))})
%% change plot properties before saving
filename=['bode_plot'];
% filename=['j_' num2str(tmp_j) '_m_' num2str(tmp_m) '_o_' num2str(tmp_o) '_q_' num2str(tmp_q) '_one_specie_transmissive_Co'];
if ismac==1
    foldername='/Users/mohitmehta/Copy/My Figures/Final Plots/';
elseif isunix==1
    display('Sorry No path');
    break;
else
%     foldername='H:\Copy\My Figures\Final Plots\';
    foldername='H:\Copy\My Figures\Pospectus_figures_01-Feb-2014\';
end
save_figures_with_cnt([foldername filename],gcf);