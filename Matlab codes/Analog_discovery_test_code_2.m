%% Import data from text file.
% This code is used to extract information of sample number and the sample
% sample[i]=value .... Format...
clear all;
close all;
clc;
% %% Initialize variables.
filename = '/Users/mohitmehta/Dropbox/result.txt';
delimiter = '\t';

% %% Format string for each line of text:
%   column1: double (%f)
%	column2: double (%f)
% For more information, see the TEXTSCAN documentation.
formatSpec = '%f%f%[^\n\r]';

% %% Open the text file.
fileID = fopen(filename,'r');

% %% Read columns of data according to format string.
% This call is based on the structure of the file used to generate this
% code. If an error occurs for a different file, try regenerating the code
% from the Import Tool.
dataArray = textscan(fileID, formatSpec, 'Delimiter', delimiter,  'ReturnOnError', false);

% %% Close the text file.
fclose(fileID);

% %% Post processing for unimportable data.
% No unimportable data rules were applied during the import, so no post
% processing code is included. To generate code which works for
% unimportable data, select unimportable cells in a file and regenerate the
% script.

% %% Allocate imported array to column variable names
VarName1 = dataArray{:, 1};
V2 = dataArray{:, 2};

% %% Clear temporary variables
clearvars filename delimiter formatSpec fileID dataArray ans VarName1;
%% 
plot(V2(1:end));axis([0 inf -3 3]);