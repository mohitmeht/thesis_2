clear all;
clc;
filename = 'H:\Copy\work\Phd\All EIS plots\Digitized_data_fig\Potentiostatic_13.dat';
delimiter = ',';
startRow = 2;
formatSpec = '%f%f%[^\n\r]';
fileID = fopen(filename,'r');
dataArray = textscan(fileID, formatSpec, 'Delimiter', delimiter, 'HeaderLines' ,startRow-1, 'ReturnOnError', false);
fclose(fileID);
ZreZimohmX = dataArray{:, 1};
ZreZimohmY = dataArray{:, 2};
clearvars -except ZreZimohmX ZreZimohmY;

%%
figure(4)
clf;
plot(ZreZimohmX,ZreZimohmY,'LineWidth',1)
datacursormode on
angle=annotation('textbox',...
    [0.647248151149183 0.895096348544802 0.179874213836478 0.060377358490566],'String',...
    {['$\theta=\textrm{N.A.}$']},...
    'FitBoxToText','on','FontSize',20,...
    'FontName','Times New Roman','LineStyle','none','Interpreter','latex');
hdt = datacursormode;
set(hdt,'DisplayStyle','datatip','Enable','on');
set(hdt,'UpdateFcn',{@labeldtips_o_slope,...
    ZreZimohmX,...
    ZreZimohmY,...
    hdt,...
    angle});

%%
z_min=0;
z_max=5;
y_min=0;
y_max=40;
x_min = 0;
x_max=80;
set(gcf,'Position',[613 89 795 (y_max-y_min)/(x_max-x_min)*795])
set(0,'ScreenPixelsPerInch',90);
set(gcf,'Color','w');
set(gca,'FontSize',20,'Color','w','FontName','Times New Roman');
xlabel('Z''[ohm]','FontSize',20,'FontName','Times New Roman')
ylabel('Z''''[ohm]','FontSize',20,'FontName','Times New Roman')
% ylabel('time elapsed [min]','FontSize',20,'FontName','Times New Roman')
set(gca,'PlotBoxAspectRatio',[1 (y_max-y_min)/(x_max-x_min) 1]);
% set(gca,'PlotBoxAspectRatio',[1 1 (z_max-z_min)/(x_max-x_min)]);
axis([x_min x_max y_min y_max])
% axis([x_min x_max -inf inf z_min z_max])
%%
title(['DOI: 10.1016/j.jpowsour.2014.05.063 Figure 10D '],'Interpreter','none')
% title(['FLW 2 specie transmissive'],'Interpreter','none')
%%
clc;
num_to_plot=1;
found_peaks=[1;find(diff(sign(diff(ZreZimohmY(:,num_to_plot))))~=0)+1];
Re=ZreZimohmX(found_peaks(1),num_to_plot);
R1=ZreZimohmX(found_peaks(3),num_to_plot)-ZreZimohmX(found_peaks(1),num_to_plot);
% f_peaks=frq(found_peaks,num_to_plot);
ratio12=ZreZimohmY(found_peaks(2),num_to_plot)/ZreZimohmY(found_peaks(3),num_to_plot);
% ratio13=ZreZimohmY(found_peaks(2),num_to_plot)/ZreZimohmY(end,num_to_plot); % Need to edit if another semicircle is formed
angle_1= 50.1 ;
%%
% clear anno_final
% delete(anno_final)
anno_final(1)=annotation(gcf,'textbox',...
    [0.161052106478355 0.418294804323118 0.306918238993711 0.471698113207547],...
    'Interpreter','latex',...
    'String',{['$\theta_1=' num2str(angle_1) '^\circ$'],... %     ['$\theta_2=' num2str(angle_1-angle_2) '^\circ$'],...
    ['$R_e=' num2str(Re) '\,\Omega$'],...
    ['$R_1=' num2str(R1) '\,\Omega$'],...%     ['$f_1=' num2eng(f_peaks(1)) '\textrm{Hz}$'],...%     ['$f_2=' num2eng(f_peaks(2)) '\textrm{Hz}$'],...%     ['$f_3=' num2eng(f_peaks(3)) '\textrm{Hz}$'],...
    ['$\textrm{Ratio}_{12}=' num2str(ratio12) '$'],...%     ['$\textrm{Ratio}_{13}=' num2str(ratio13) '$']
    },...
    'FontSize',20,...
    'FontName','Times New Roman',...
    'LineStyle','none');
anno_final(2)=annotation(gcf,'textbox',...
    [0.801303678805399 0.121536541691739 0.0855345911949685 0.087248322147651],...
    'Interpreter','latex',...
    'String',{'$\theta_1$',''},...
    'FontSize',20,...
    'FontName','Times New Roman',...
    'LineStyle','none');

delete(angle)
r=R1/2;
theta=-90:10:90;
x=r.*sind(theta)+ZreZimohmX(found_peaks(1))-r.*sind(theta(1));
y=r.*cosd(theta);
hold on;
figure(4);
% clf;
% set(p,'Visible','off')
plot(x,y,'--','Color','red','LineWidth',1);
%%
% filename=['Das2014'];
filename=['Potentiostatic'];
if ismac
      foldername='/Users/mohitmehta/Copy/work/Phd/All EIS plots/Analysis/Slopes/';
%     foldername='/Users/mohitmehta/Copy/My Figures/Comparison_figures/';
elseif isunix
    display('Sorry No path');
    break;
else
    foldername='H:\Copy\work\Phd\All EIS plots\Digitized_data_fig\';
%     foldername='H:\Dropbox\Apps\Texpad\Notes\figures\';
end
save_figures_with_cnt([foldername filename],gcf);