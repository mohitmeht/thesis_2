function  labeldtips_test(hdt,disp)
info_struct = getCursorInfo(hdt);
if (length(getCursorInfo(hdt))==2)
   if length(info_struct(1).Position)==2
        slpe_Z_imag=[info_struct(1).Position(2) info_struct(2).Position(2)];
        slpe_Z_real=[info_struct(1).Position(1) info_struct(2).Position(1)];
   else
        slpe_Z_imag=[info_struct(1).Position(3) info_struct(2).Position(3)];
        slpe_Z_real=[info_struct(1).Position(1) info_struct(2).Position(1)];
   end
ang=atand(diff(slpe_Z_imag)/diff(slpe_Z_real));
set(disp,'String',['$\theta =' num2str(ang) '^\circ$'])
end
end

