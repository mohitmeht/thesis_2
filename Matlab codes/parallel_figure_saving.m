clear all;
close all;
clc;
delete(gcp);
files=dir('*.fig');
parpool(4);
parfor i=1:5
open(files(i).name);
sdf('EIS_plots');
%  print(figure(i),'-deps2',['Myfile' num2str(i)]);
set(figure(i),'Visible','off');
saveas(figure(i),['tp_' num2str(i) '.jpg']);
end
