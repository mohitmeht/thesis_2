
%% Experimental Data
clear all;
if ismac==1 
    load('/Users/mohitmehta/Copy/work/Phd/Data/Xujie Chen_data_on_Li-air/EIS paper/Saved Variables/All_EIS_data.mat');
else
    load('H:\Copy\work\Phd\Data\Xujie Chen_data_on_Li-air\EIS paper\Saved Variables\All_EIS_data');
end
experiment=6;           % Experiment number
ZreZimohm=cell2mat(Impedance(3,experiment));
names=char(Impedance{4,experiment});
num=cell2mat(Impedance(1,experiment));
ZreZimohmX(:,:)=ZreZimohm(:,1,:);
ZreZimohmY(:,:)=ZreZimohm(:,2,:);
%% Computed Data
cc=distinguishable_colors(13,'w');
mul=[1e-1,0.5,1e0,2,1e1];
I_dis=1e-3.*[2e-1 0.5 0.7 1 2 5];
k0=1.3e-8.*mul;                       
n=[1:0.1:4];                              
Do=7e-6.*mul;                
c_int=3.26e-6.*[5e-2:0.05:1e0 1.5e0:0.5:10];          
l=1e-2.*mul;
sp_area=1e4*mul;
font_shape={'x','O','^','v','*','p','s'};
Cd=1e-6.*[1 10 20 30];

 i=2;    % I_dis
 j=[3];    % k0
 p=[4];    % n
 m=[2];    % Do
 o=[3];    % c_int
 q=[2];    % l
 r=[3];    % Cd
s=[4];    % sp_area
    clf;
% for p=1:1
number=o;
% for i=1:3       
  [Z,tmp]=our_paper_analytical(I_dis(i),k0(j),n(p),Do(m),c_int(o),l(q),Cd(r)); 