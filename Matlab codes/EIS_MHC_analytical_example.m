clear;
% close all;
clc;
lam_dim = 1200;     % This is the reorgaization energy
Cd = 10e-6;
A = 1;
Vt= Vt_compute(300);
eta_dim = -0.05;
f=logspace(-3,log10(200e3),200);
I_dis=0.5e-3;
k0=1.7e-6;                        
n=2;
sp_area=5e4;
poro=0.75;
brugg=1.5;
Do=7e-6;
Deff=Do.*(poro^brugg);   
ext_conc=9.375e-6;
sol=0.2508;
c_int=sol*ext_conc;  
l=0.02;
F = phys_constants('F');
Diff_length = petru_equation(n,F,A,Deff,c_int,l,I_dis); % This is the sqrt(Deff/k) ratio

[Z,f]=EIS_MHC_analytical(I_dis,lam_dim,Deff,k0,l,Cd,sp_area,A,poro,Vt,eta_dim,f);
plot(real(Z),-imag(Z));hold on;
% axis([0 30000 0 30000])
%%
[Z,f] = EIS_MHC_analytical_with_iF(I_dis,lam_dim,Diff_length,Deff,k0,l,Cd,sp_area,A,poro,Vt,f);
plot(real(Z),-imag(Z));hold on;