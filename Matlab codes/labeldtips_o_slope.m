function [output_txt] = labeldtips_o_slope(obj,event_obj,...
                      Z_re,Z_im,hdt,disp)                  
pos = get(event_obj,'Position');
x = pos(1); y = pos(2);
[row,col] = find(Z_re == x);  % Find index to retrieve obs. name
% The find is reliable only if there are no duplicate x values
%[row,col] = ind2sub(size(Z),idx);
labeldtips_test(hdt,disp);
output_txt = {['Z'': ',num2str(Z_re(row),4),'[ohm]'] ['-Z'''': ',num2str(Z_im(row),4),'[ohm]'] ['Pos: ' num2str(row)]};
end