function [ lamad ] = petru_equation_usingVPASolve(n,F,Deff,c_int,l,I_dis)
syms lamda;
lam=vpasolve((n*F*Deff*c_int*tanh(l/lamda))-(I_dis*lamda), lamda,0.1);
lamad=double(lam);
end

