%% Plot 1
if ismac==1
     mkdir(['/Users/mohitmehta/Copy/My Figures/Pospectus_figures_' datestr(date)]);
elseif isunix==1
    mkdir(['/panfs/storage.local/ame/home/mrm11s/my_matlab_codes/figures_' datestr(date)]);
else
    mkdir (['H:\Copy\My Figures\Pospectus_figures_' datestr(date)]);
end
% close all;
% clear all;
% clc;
cc=distinguishable_colors(13,'w');
mul=[1e-1,0.5,1e0,2,1e1];
I_dis=1e-3.*[2e-1 0.5 0.7 1 2 5];
k0=1.3e-8.*mul;                       
n=[1:0.1:4];                              
Do=7e-6.*mul;                
c_int=3.26e-6.*[5e-2:0.05:1e0 1.5e0:0.5:10];          
l=1e-2.*mul;
% sp_area=1e4*mul;
font_shape={'x','O','^','v','*','p','s'};
Cd=1e-6.*[1 10 20 30];

 i=2;    % I_dis
 j=[3];    % k0
 p=[4];    % n
 m=[2];    % Do
%  o=[3];    % c_int
 q=[2];    % l
 r=[3];    % Cd
% for s=[4];    % sp_area
%   close all;
%     figure(1);
%   clf;
%   set(gcf,'Visible','on');
h=figure(1);
% clf;
% movegui(h, 'onscreen');
% rect = get(h,'Position'); 
% rect(1:2) = [0 0]; 
    writerObj = VideoWriter(['Do_dep_wo_label_p_' num2str(p) '.mp4'],'MPEG-4');
    writerObj.FrameRate = 10;
    open(writerObj);
for o=[1:length(c_int)]
    clf;
% for p=1:1
number=o;
% for i=1:3       
  [Z,tmp]=our_paper_analytical(I_dis(i),k0(j),n(p),Do(m),c_int(o),l(q),Cd(r)); 
% figure (1);
% set(gcf,'Visible','off');
sa=plot(real(Z),-imag(Z),'-');
xlabel('Z''[\Omega]','Fontsize',20,'FontName','Times New Roman');
ylabel('-Z''''[\Omega]','Fontsize',20,'FontName','Times New Roman');
box off;
axis square;
set(gcf,'Position',[134   218   560   420])
set(0,'ScreenPixelsPerInch',120);
set(gcf,'Color','w');
set(sa,'LineWidth',2);
set(gca,'FontSize',20,'Color','w','FontName','Times New Roman');
set(gcf,'Position',[825,61,720,720]);
[myx,myy]=extrema(-imag(Z));
[mxx,mxy]=extrema(imag(Z));
% if length(mxy)==2
%     fct='-----';
%     fmin=num2eng(str2double(num2str(tmp(mxy(2)),'%.3f')));
% %     num2eng(tmp(mxy(2)));
% else
%     fct=num2eng(str2double(num2str(tmp(mxy(3)),'%.3f')));
%     fmin=num2eng(str2double(num2str(tmp(myy(2)),'%.3f')));
% %     fmin=num2eng(tmp(mxy(3)));
% end
% 
% % Create textbox
% annotation(gcf,'textbox',...
%     [0.552500000000002 0.652055555555558 0.381944444444443 0.0958333333333333],...
%     'String',{['f_{min}=' fmin 'Hz']},...
%     'FontSize',20,...
%     'FitBoxToText','off',...
%     'LineStyle','none');
% 
% % Create textbox
% annotation(gcf,'textbox',...
%     [0.549166666666669 0.727611111111113 0.381944444444443 0.0958333333333333],...
%     'String',{['f_{ct}=' fct 'Hz']},...
%     'FontSize',20,...
%     'FitBoxToText','off',...
%     'LineStyle','none');
% 
% % Create textbox
% annotation(gcf,'textbox',...
%     [0.545833333333335 0.805944444444446 0.445833333333331 0.0958333333333333],...
%     'String',{['f_{max}=' num2eng(str2double(num2str(tmp(myy(1)),'%.3f'))) 'Hz']},...
%     'FontSize',20,...
%     'FitBoxToText','off',...
%     'LineStyle','none');
% annotation(gcf,'textbox',...
%     [0.427777777777779 0.597611111111112 0.2375 0.0958333333333333],...
%     'String',{[num2eng(tmp(myy(1))) 'Hz']},...
%     'FontSize',20,...
%     'LineStyle','none');
    axis([0 200 0 200]);
%        frame = getframe;
   writeVideo(writerObj,getframe(gcf));
%     title(['i ' num2str(i) ' j ' num2str(j) ' p ' num2str(p)...
%         ' m ' num2str(m) ' o ' num2str(o) ' q ' num2str(q)...
%         ' r ' num2str(r) ' s ' num2str(s)])
%     title(['i_' num2str(i) '_j_' num2str(j) '_p_' num2str(p)...
%         '_m_' num2str(m) '_o_' num2str(o) '_q_' num2str(q)...
%         '_r_' num2str(r)]);
%     axis([0 160 0 160]);
% cur=pwd;
% cd(cur);
% cd('C:\Users\Mohit\Desktop\test');
% waitforbuttonpress;
end
close(writerObj);
% set(gcf,'Position',[134   218   560   420])
% set(0,'ScreenPixelsPerInch',90);
% set(gcf,'Color','w');
% set(p,'LineWidth',1);
% set(gca,'FontSize',12,'Color','w','FontName','Times New Roman');


% name_stack={['$I_{dis}=' num2eng(I_dis(i)) '\textrm{A/cm}^2$'],...
%             ['$D_o=' num2str(Do(m)/10^(floor(log10(Do(m))))) '\times 10^{' num2str(floor(log10(Do(m)))) '}\,\textrm{cm}^2/\textrm{s}$'],... 
%             ['$n=' num2str(n(p)) '$'],...
%             ['$C_d=' num2eng(Cd(r)) ' F/cm^2$'],...
%             ['$c_o=' num2str(c_int(o)/10^(floor(log10(c_int(o))))) '\times 10^{' num2str(floor(log10(c_int(o)))) '}\,\textrm{mol/cm}^3$'],...
%             ['$k=' num2str(k0(j)/10^(floor(log10(k0(j))))) '\times 10^{' num2str(floor(log10(k0(j)))) '}\,\textrm{cm}/\textrm{s}$'],...
%             ['$L=' num2eng(1e-2*l(q)) ' m$'],...
%             ['$a=' num2str(sp_area(s)/10^(floor(log10(sp_area(s))))) '\times 10^{' num2str(floor(log10(sp_area(s)))) '}\,\textrm{cm}^2/\textrm{cm}^{-3}$']}; 
name_stack={['$I_{dis}=' num2eng(I_dis(i)) '\textrm{A/cm}^2$'],...
            ['$D_o=' num2str(Do(m)/10^(floor(log10(Do(m))))) '\times 10^{' num2str(floor(log10(Do(m)))) '}\,\textrm{cm}^2/\textrm{s}$'],... 
            ['$n=' num2str(n(p)) '$'],...
            ['$C_d=' num2eng(Cd(r)) ' F/cm^2$'],...
            ['$c_o=' num2str(c_int(o)/10^(floor(log10(c_int(o))))) '\times 10^{' num2str(floor(log10(c_int(o)))) '}\,\textrm{mol/cm}^3$'],...
            ['$k=' num2str(k0(j)/10^(floor(log10(k0(j))))) '\times 10^{' num2str(floor(log10(k0(j)))) '}\,\textrm{cm}/\textrm{s}$'],...
            ['$l=' num2eng(1e-2*l(q)) ' m$']};
% annotation(gcf,'textbox',...
%     [0.589359788359788 0.768915343915345 0.763417989417991 0.200529100529099],...
%     'String',name_stack,...
%     'FontSize',15,...
%     'FitBoxToText','off',...
%     'LineStyle','none','FontName','Times New Roman','Interpreter','latex');
% % save_figures_with_cnt(['our_paper_ana_i_all_others_1'],figure(1));
% close all;
% set(legend('-DynamicLegend','Location','NorthWest'),'Interpreter','latex');

%     if ismac==1
%     cd(['/Users/mohitmehta/Copy/My Figures/Pospectus_figures_' datestr(date)]);
%     elseif isunix==1
%     cd(['/panfs/storage.local/ame/home/mrm11s/my_matlab_codes/figures_' datestr(date)]);
%     else
%     cd(['H:\Copy\My Figures\Pospectus_figures_' datestr(date)]);    
%     end
%     waitforbuttonpress;
%     save_figures_with_cnt(['our_paper_ana_j_' num2str(j) '_m_' num2str(m)...
%         '_o_' num2str(o) '_n_' num2str(p) '_l_' num2str(q) '_s_' num2str(s)],figure(1));

% end
% if ismac==1
%     cd(['/Users/mohitmehta/Copy/My Figures/Pospectus_figures_' datestr(date)]);
% elseif isunix==1
%     cd(['/panfs/storage.local/ame/home/mrm11s/my_matlab_codes/figures_' datestr(date)]);
% else
%     cd(['H:\Copy\My Figures\Pospectus_figures_' datestr(date)]);    
% end
% %     save_figures_with_cnt(['our_paper_ana_j_' num2str(j) '_m_' num2str(m)...
% %         '_o_' num2str(o) '_n_' num2str(p) '_l_' num2str(q)],figure(1));
% %% Data cursors
datacursormode on
hdt = datacursormode;
set(hdt,'DisplayStyle','datatip');
set(hdt,'UpdateFcn',{@labeldtips,tmp,real(Z)})
% Create textbox

%% 
filename=['one_specie_andrei_l'];
if ismac==1
    foldername='/Users/mohitmehta/Copy/My Figures/Final Plots/';
elseif isunix==1
    display('Sorry No path');
    break;
else
    foldername='H:\Copy\My Figures\Pospectus_figures_01-Feb-2014\';
end
save_figures_with_cnt([foldername filename],gcf);
%% Data cursors
datacursormode on
hdt = datacursormode;
set(hdt,'DisplayStyle','datatip');
set(hdt,'UpdateFcn',{@labeldtips,tmp,real(Z)})