function save_prospectus_figs(src,eventdata)
cd('H:\Copy\My Figures\');
files=dir('*two_species_transmissive_reflective_diff_l_*.pdf');
[~,idx] = sort([files.datenum]);
if size(files,1)==0
    cnt=0;
elseif isnan(str2double(files(idx(size(idx,2))).name(end-6:end-4)))
    if isnan(str2double(files(idx(size(idx,2))).name(end-5:end-4)))
        cnt=str2double(files(idx(size(idx,2))).name(end-4:end-4));
    else
        cnt=str2double(files(idx(size(idx,2))).name(end-5:end-4));
    end
else
    cnt=str2double(files(idx(size(idx,2))).name(end-6:end-4));
% exist(files(size(idx,2)).name,'file');
end
cnt=cnt+1;
sdf('EIS_plots');
if ismac==1
	a=['/Users/mohitmehta/Copy/My Figures/two_species_transmissive_reflective_diff_l_' num2str(cnt) '.pdf'];        %np- no colored points
else
	a=['H:\Copy\My Figures\two_species_transmissive_reflective_diff_l_' num2str(cnt) '.pdf'];
end
export_fig(gcf, a);          
display([a ' has been written']);
end
