%% Load Data from the database
 clear all;
Impedance=ld_eis_db();
experiment=4;           % Experiment number
ZreZimohm=cell2mat(Impedance(3,experiment));
names=char(Impedance{4,experiment});
ZreZimohmX(:,:)=ZreZimohm(:,1,:);
ZreZimohmY(:,:)=ZreZimohm(:,2,:);
frq(:,:)=Impedance{1,experiment}(:,3:11:end);
clearvars -except ZreZimohmX ZreZimohmY frq names
%% PLOTTING
cc=distinguishable_colors(5,'w');
close all;
for i=3:4
% plot(ZreZimohmX(:,i),ZreZimohmY(:,i),'o','Color',cc(i,:),'MarkerFaceColor',cc(i,:),...
%    'DisplayName',['DC:-' num2str(dc_current(i))]);
 plot(ZreZimohmX(:,i),ZreZimohmY(:,i),'o','Color',cc(i,:),'MarkerFaceColor',cc(i,:),...
   'DisplayName',names(i,:));
hold on;
axis equal;
axis([20 600 0 580]);
pause(1);
end
hold off;
set(legend('-DynamicLegend'),'Interpreter','none','FontSize',15,'FontName','Times New Roman','Location','NorthWest');
%% Cursors for Experiment Data
hdt = datacursormode;
set(hdt,'DisplayStyle','datatip');
set(hdt,'UpdateFcn',{@labeldtips,frq(:,4),ZreZimohmX(:,4)})
datacursormode on
% title(names(i,:),'Interpreter','none');
%% testing
figure;
i=2;
plot(ZreZimohmX(:,i),ZreZimohmY(:,i),'o','Color','black','MarkerFaceColor','black',...
    'DisplayName',['DC:-' num2str(dc_current(i))]);
axis([0 450 0 450]);
set(legend('-DynamicLegend'),'Interpreter','latex','FontSize',15,'FontName','Times New Roman','Location','NorthWest');
%% 
cc=distinguishable_colors(10,'w');
a=1;
%%
exp=7;
% close all
%% Names displayed
Impedance{4,9}'
%%

i=7;
clear ZreZimohm ZreZimohmX ZreZimohmY
ZreZimohm=cell2mat(Impedance(3,exp));
ZreZimohmX(:,:)=ZreZimohm(:,1,:);
ZreZimohmY(:,:)=ZreZimohm(:,2,:);
plot(ZreZimohmX(1:end-1,i),ZreZimohmY(1:end-1,i),'o','Color',cc(a,:));
a=a+1;
hold on;
%%
set(0,'ScreenPixelsPerInch',90);
set(gcf,'Color','w');
set(gca,'FontSize',20,'Color','w','FontName','Times New Roman','LineWidth',1);  
xlabel('Z'' [\Omega]','FontSize',20,'FontName','Times New Roman');
ylabel('-Z'''' [\Omega]','FontSize',20,'FontName','Times New Roman');
axis square
axis([25 270 0 245])
legend('xc041714','xc112113')