function printfig(src,eventdata,ZreZimohmX,frq,names,i,Date,Time)
cursorMode = datacursormode(gcf);
cursors=cursorMode.DataCursors;
num_cursors=size(cursors,1);
for i_cur=1:num_cursors
    Z(i_cur,1)=str2double(cursors(i_cur).string{1}(5:end-6));
    Z(i_cur,2)=str2double(cursors(i_cur).string{2}(6:end-6));
    [row,experiment]=(find(ZreZimohmX==Z(i_cur,1)));
    Z(i_cur,3)=frq(row);
end
Rct=abs(Z(1,1)-Z(3,1));
disp(' ');disp('--------------------------------------- ');
disp(['Plot Number:' num2str(i)]);
% disp(['Name:-' names(i,:)]);
disp(['Name:-' names{i}])
disp('High Frequency:');
disp(['Freq:' num2eng(Z(1,3)) 'Hz']);
disp(['Z_real:' num2eng(Z(1,1)) 'ohm']);
disp(['Z_imag:' num2eng(Z(1,2)) 'ohm']);
disp(' ');
disp('Middle Frequency:');
disp(['Freq:' num2eng(Z(2,3)) 'Hz']);
disp(['Z_real:' num2eng(Z(2,1)) 'ohm']);
disp(['Z_imag:' num2eng(Z(2,2)) 'ohm']);
disp(' ');
disp('Low Frequency Start:');
disp(['Freq:' num2eng(Z(3,3)) 'Hz']);
disp(['Z_real:' num2eng(Z(3,1)) 'ohm']);
disp(['Z_imag:' num2eng(Z(3,2)) 'ohm']);
disp(['Rct:' num2eng(Rct) 'ohm']);
disp(' ');
if num_cursors>3
disp('Low Frequency Middle:');
disp(['Freq:' num2eng(Z(4,3)) 'Hz']);
disp(['Z_real:' num2eng(Z(4,1)) 'ohm']);
disp(['Z_imag:' num2eng(Z(4,2)) 'ohm']);
disp(' ');
if num_cursors>4
disp('Low Frequency end:');
disp(['Freq:' num2eng(Z(5,3)) 'Hz']);
disp(['Z_real:' num2eng(Z(5,1)) 'ohm']);
disp(['Z_imag:' num2eng(Z(5,2)) 'ohm']);
Rdiff=abs(Z(3,1)-Z(5,1));
disp(['Rdiff:' num2eng(Rdiff) 'ohm'])
end
end

if ismac==1
 filename='/Users/mohitmehta/Copy/work/Phd/Data/EIS_database.xlsx';        %np- no colored points
else
filename = 'H:\Copy\work\Phd\Data\EIS_database.xlsx';
end
 
load('/Users/mohitmehta/Copy/work/Phd/Data/EIS_database_row_num');
xlscol_num=xlscol_num+1;
save('/Users/mohitmehta/Copy/work/Phd/Data/EIS_database_row_num','xlscol_num');
% try
% [numbers,txt] =xlsread(filename);
% catch err
%     rethrow(err);
% end
% 
% if exist('err')
%     if strcmp(err.message,'Subscript indices must either be real positive integers or logicals.')
%          display('found error');
%         load('/Users/mohitmehta/Copy/work/Phd/Data/EIS_database_row_num');
%          xlscol_num=xlscol_num+1;
%         save('/Users/mohitmehta/Copy/work/Phd/Data/EIS_database_row_num','xlscol_num');
%     end
% elseif isempty(txt) && isempty(numbers)
%     xlscol_num=2;
% else
%    xlscol_num=size(txt,2)+1; 
% end
%         
% display(xlscol_num)

xlwrite(filename,{pwd;names{i}},1,[xlscol(xlscol_num),'1']);  % Write Filepath and filename
xlwrite(filename,{Date{i};Time{i}},1,[xlscol(xlscol_num),'3']);  % Write Date and time of the experiment
row_num=[8,12,16,22,26];
for i_write=1:num_cursors
xlwrite(filename,[Z(i_write,3);Z(i_write,1);Z(i_write,2)],1,[xlscol(xlscol_num),num2str(row_num(i_write))]);	% Write data, freq, Z_real and,Zimag
end
end