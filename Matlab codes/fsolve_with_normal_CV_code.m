
close all;
clc;
dbulk_multiple = [1e-4 1e-5 1e-6 1e-7];
% cc=distinguishable_colors(length(dbulk_multiple),'w');
parfor j=1:length(dbulk_multiple)
kb= 1.3806488e-23;      % Boltzman constant
q=1.602176e-19;         % Electron Energy
F=96485;                % Faraday's constant
l=0.04;     % Length in cm % from the paper
atm=1;	% Pressure in atmospheres % if partial pressure is given, do not use oxygen concentration term
oxygen_percentage=0.21;
partial_pressure = oxygen_percentage*atm;
partial_pressure_multiple = linspace(0.1,2,100);
temp_in_c = 30;
T= 273.15 + temp_in_c;
Vt=kb*T/q;
kh=1.3e-3;        % Electrochemical Oxygen technology 0.7M H3PO4
standard_temp=298.15;
temp=(T^-1)-(standard_temp^-1);
C = 1700; % wikipedia and http://webbook.nist.gov/cgi/cbook.cgi?ID=C7782447&Units=SI&Mask=10#Refs
kh_new=kh*exp(C*temp);
c_int=kh_new.*partial_pressure_multiple./1000;     % converting to mol/cm3
a=4e4;      % Calculated using Pt distribution and cathode physical parameters
epsi=0.8;
i0a=1e-4; % ka_paper=nFc(0,t)ka_simulation
kc=1e-10;
beta=0.5;
n=4;
R =linspace(1,200,100);
E0=3.967; % First value of the voltage from the data % given in the paper
% Dbulk=2e-5; % Lee2002 1M LiPF6 in EC/DEC   % Use in normal or 
Dbulk  = dbulk_multiple(j);
Deff=epsi^1.5*Dbulk;                          % comparison simulations


% parfor i=1:length(X)
% lamad(i)=petru_equation(n,F,Deff,c_int,l,X(i));
% end

I_dis_multiple=linspace(0,30e-3,100)';

matrix_b_dim = length(c_int);
x0 = 0.01.*ones(length(I_dis_multiple),length(c_int));  % Make a starting guess at the solution
F = @(x)(n.*F*1.*Deff.*tanh(l./x0)*repmat(c_int,matrix_b_dim,1)-(repmat(I_dis_multiple,1,matrix_b_dim).*x));


options = optimoptions('fsolve','Display','off');
options.TolFun = 1e-20;
tic
[x,fval] = fsolve(F,x0,options); % Call solver
toc
lamda(:,j) = x;

% Ea_sinh = @(x)(Vt.*2.*asinh(x./2./i0a));
% Ec_sinh = @(x)(2.*Vt./n.*asinh(Deff./2./kc./a./lamad(find(x==X)).^2));
% % Ec_sinh = @(x)(lamad(find(x==X)));
% Rh = @(x)(R.*x);
% with_E0 = @(x)(E0);            
% with_Ea = @(x)(E0-(Ea_sinh(x)));
% with_Ec = @(x)(E0-(Ec_sinh(x)));
% with_Ea_Ec = @(x)(E0-(Ea_sinh(x))-(Ec_sinh(x)));
% with_R = @(x)(E0-(R.*x));
% with_Deff_w_sinh = @(x)(E0-(Ea_sinh(x))-(Ec_sinh(x))-(R.*x));
% P_with_E0 = X.*with_E0(X);
% P_with_Ea = X.*with_Ea(X);
% P_with_Ec = X.*with_Ec(X);
% P_with_Ea_Ec = X.*with_Ea_Ec(X);
% P_with_R = X.*with_R(X);
% P_with_Deff_w_sinh = X.* (with_Deff_w_sinh(X));
end