clear all;
clc;
f=flip(logspace(-3,0,100));
w=2.*pi.*f;
% Z=(1./(sqrt(1i.*w))); % Warburg Impedance
% Z=(tanh(sqrt(1i.*w)))./sqrt(1i.*1*w); % FLW (Transmissive)
% Z=(coth(sqrt(1i.*w)))./(sqrt(1i.*w));   % FLW (Reflective)
Z=((tanh(sqrt(1i.*w).*5))+(tanh(sqrt(1i.*w).*1)))./(sqrt(1i.*w));
cc= distinguishable_colors(10,'w');
% hold on;
figure(1);
% clf;
plot3(real(Z),log10(f),-imag(Z),'color',cc(length(get(gca,'children'))+1,:));hold on;
% axis([0 1 0 0.5])
view([0 -1 0])
angle=annotation('textbox',...
    [0.647248151149183 0.895096348544802 0.179874213836478 0.060377358490566],'String',...
    {['$\theta=\textrm{N.A.}$']},...
    'FitBoxToText','on','FontSize',20,...
    'FontName','Times New Roman','LineStyle','none','Interpreter','latex');
% hdt = datacursormode;
% set(hdt,'DisplayStyle','datatip','Enable','on');
% set(hdt,'UpdateFcn',{@labeldtips,...
%     f,...
%     real(Z),...
%     -imag(Z),...
%     hdt,...
%     angle});

%%
figure(2);
% clf;
% Z=((tanh(sqrt(1i.*w).*20))+(tanh(sqrt(1i.*w).*1)))./(sqrt(1i.*w));
% plot(real(fft(Z)),imag(fft(Z)),'o')
plot(log10(f),real(fft(Z)),'-','color',cc(length(get(gca,'children'))+1,:));hold on;
figure(3);
plot(log10(f),imag(fft(Z)),'-','color',cc(length(get(gca,'children'))+1,:));hold on;
%%
clc;
angle=calculate_slope(real(Z),-imag(Z));
%%
legend('1:1','10:1','5:1')