%%
clf;
clearvars -except tmpX_andrei tmpY_andrei SS_Re SS_Im 
%%
filename='Deff_diffusion_equation.mp4';
% Make_video(filename,10,1);
writerObj = VideoWriter(writerObj,filename,'MPEG-4');
writerObj.FrameRate = 1;
open(writerObj);

%%
clf;
window_size=[405.000000000000e+000 242.000000000000e+000 2.04700000000000e+003 1.07700000000000e+003];
legend_position=[730.672560018697e-003 708.734488317589e-003 108.780760626398e-003 75.2257799671593e-003];
% yaxis_location=[-7.42811501597441e+000 48.4824281150160e+000 1.00010919874411e+000];
yaxis_location=[-14.2573833086573e+000 48.1165744564794e+000 1.00010919874411e+000];
cc(:,3)=[0.600000023841858 0.200000002980232 0];
cc(:,2)=[0.26274511218071 0.34901961684227 0.847058832645416];
cc(:,1)=[0 0.400000005960464 0.141176477074623];
% cc(:,)=[0 0 0];
set(gcf,'Position',window_size);
for i=[3 2 1]
plot_location=i;

plot(SS_Re{plot_location}(1:1:end),-SS_Im{plot_location}(1:1:end),'^','MarkerFaceColor',cc(:,plot_location),...
    'LineWidth',1,'Color',cc(:,plot_location), 'MarkerSize',4);
hold on;
plot(tmpX_andrei{plot_location}-1,tmpY_andrei{plot_location},...
    'LineWidth',2,'Color',cc(:,plot_location));
set(gca,'FontSize',25,'Color','w','FontName','Times New Roman','LineWidth',1);  
xlabel('Re(Z) (ohm)','FontSize',25,'FontName','Times New Roman');
y_pos=ylabel('-Im(Z) (ohm)','FontSize',25,'FontName','Times New Roman');
axis square
box off;
 lnd=legend('Simulation','Analytical');
 set(y_pos,'Position',yaxis_location);
 set(lnd,'Position',legend_position);
end
set(0,'ScreenPixelsPerInch',180);
set(gcf,'Color','w');
set(gca,'FontSize',25,'Color','w','FontName','Times New Roman','LineWidth',1);  
% set(gca,'Color',[1 0.992156862745098 0.945098039215686]);     %  Background color 
% set(gcf,'Color',[1 0.992156862745098 0.945098039215686]);     %  for the conference presentation
% set(lnd,'Color',[1 0.992156862745098 0.945098039215686]);
set(gca,'Color','w');     
set(gcf,'Color','w');     
set(lnd,'Color','w');
box off;
 axis([0 150 0 60]);
 pbaspect([1 60/150 1])
  %%
 set(gca,'PlotBoxAspectRatio',[1 330/600 1]);
%% Pictures creater
window_size=[405.000000000000e+000 242.000000000000e+000 2.04700000000000e+003 1.07700000000000e+003];
legend_position=[730.672560018697e-003 708.734488317589e-003 108.780760626398e-003 75.2257799671593e-003];
yaxis_location=[-7.42811501597441e+000 48.4824281150160e+000 1.00010919874411e+000];
cc(:,1)=[0.600000023841858 0.200000002980232 0];
cc(:,2)=[0.26274511218071 0.34901961684227 0.847058832645416];
cc(:,3)=[0 0.400000005960464 0.141176477074623];
loop=0;
 axis([0 150 0 80]);
 pbaspect([1 80/150 1]);
for j=1:5:25
clf;
loop=loop+1;
for plot_location=3:-1:1

plot(SS_Re{plot_location},-SS_Im{plot_location},'^','MarkerFaceColor',[0.600000023841858 0.200000002980232 0],...
    'LineWidth',4,'Color',cc(:,plot_location));
hold on;
plot(tmpX_andrei{plot_location}-1+j,tmpY_andrei{plot_location},...
    'LineWidth',2,'Color',cc(:,plot_location));
set(gca,'FontSize',25,'Color','w','FontName','Times New Roman','LineWidth',1);  
xlabel('Z'' [\Omega]','FontSize',25,'FontName','Times New Roman');
y_pos=ylabel('-Z'''' [\Omega]','FontSize',25,'FontName','Times New Roman');
axis square
box off;
 lnd=legend('Simulation','Analytical');
 set(y_pos,'Position',yaxis_location);
 set(lnd,'Position',legend_position);
 annotation(gcf,'textbox',...
    [0.236683174450655 0.305576190918378 0.246478873239437 0.0429009193054137],...
    'Interpreter','latex',...
    'String',{'$\textrm{D}_{\textrm{o}_2}=7\times10^{-4}\,\textrm{cm}^2/\textrm{s}$'},...
    'FontSize',20,...
    'LineStyle','none','Color',cc(:,1));
 annotation(gcf,'textbox',...
    [0.447070498394317 0.350520011143097 0.246478873239437 0.0429009193054137],...
    'Interpreter','latex',...
    'String',{'$\textrm{D}_{\textrm{o}_2}=7\times10^{-5}\,\textrm{cm}^2/\textrm{s}$'},...
    'FontSize',20,...
    'LineStyle','none','Color',cc(:,2));
annotation(gcf,'textbox',...     
    [0.638971906845021 0.403635435045038 0.246478873239437 0.0429009193054137],...
    'Interpreter','latex',...
    'String',{'$\textrm{D}_{\textrm{o}_2}=7\times10^{-6}\,\textrm{cm}^2/\textrm{s}$'},...
    'FontSize',20,...
    'LineStyle','none','Color',cc(:,3));
end
set(0,'ScreenPixelsPerInch',90);
set(gcf,'Color','w');
set(gca,'FontSize',25,'Color','w','FontName','Times New Roman','LineWidth',1);  

box off;
 filename=['Video_to_make' num2str(loop)];
if ismac
      foldername='/Users/mohitmehta/Dropbox/work/Phd/Conferences/ECS 2014/Data_simulation/';
%     foldername='/Users/mohitmehta/Copy/My Figures/Comparison_figures/';
elseif isunix
    display('Sorry No path');
    break;
else
    foldername='H:\Copy\My Figures\Comparison_figures\';
end
save_figures_with_cnt([foldername filename],gcf);
end




%% Video creater
window_size=[405.000000000000e+000 242.000000000000e+000 2.04700000000000e+003 1.07700000000000e+003];
outer_pos=[405.000000000000e+000 242.000000000000e+000 2.04700000000000e+003 1.15000000000000e+003];
% legend_position=[730.672560018697e-003 708.734488317589e-003 108.780760626398e-003 75.2257799671593e-003];
legend_position=[628.003829437896e-003 716.054899192535e-003 185.148998534441e-003 170.148560817085e-003];
yaxis_location=[-14.2573833086573e+000 48.1165744564794e+000 1.00010919874411e+000];
% yaxis_location=[-7.42811501597441e+000 48.4824281150160e+000 1.00010919874411e+000];
cc(:,1)=[0.600000023841858 0.200000002980232 0];
cc(:,2)=[0.26274511218071 0.34901961684227 0.847058832645416];
cc(:,3)=[0 0.400000005960464 0.141176477074623];
filename='/Users/mohitmehta/Dropbox/work/Phd/Conferences/ECS 2014/Data_simulation/Deff_diffusion_equation';
% Make_video(filename,10,1);
writerObj = VideoWriter(filename,'MPEG-4');
writerObj.FrameRate = 60;
open(writerObj);
for j=1:0.2:25
clf;
set(gcf,'Position',window_size);
set(gcf,'OuterPosition',outer_pos);
for plot_location=3:-1:1

plot(SS_Re{plot_location},-SS_Im{plot_location},'^','MarkerFaceColor',cc(:,plot_location),...
    'LineWidth',4,'Color',cc(:,plot_location));
hold on;
plot(tmpX_andrei{plot_location}-1+j,tmpY_andrei{plot_location},...
    'LineWidth',2,'Color',cc(:,plot_location));
end
set(gca,'FontSize',25,'Color','w','FontName','Times New Roman','LineWidth',1);  

xlabel('Re(Z) (ohm)','FontSize',25,'FontName','Times New Roman');
y_pos=ylabel('-Im(Z) (ohm)','FontSize',25,'FontName','Times New Roman');
axis square
box off;
 lnd=legend('Simulation','Analytical');
 set(y_pos,'Position',yaxis_location);
 set(lnd,'Position',legend_position)

set(0,'ScreenPixelsPerInch',180);
set(gcf,'Color','w');
set(gca,'FontSize',25,'Color','w','FontName','Times New Roman','LineWidth',2);  
set(gca,'Color',[1 0.992156862745098 0.945098039215686]);
set(gcf,'Color',[1 0.992156862745098 0.945098039215686]);
set(lnd,'Color',[1 0.992156862745098 0.945098039215686]);
box off;
 axis([0 150 0 80]);
 pbaspect([1 80/150 1])
 annotation(gcf,'textbox',...
    [0.246942089936732 0.535106636686188 0.198827552515877 0.0519962859795729],...
    'Interpreter','latex',...
    'String',{'$\textrm{D}_{\textrm{o}_2}=7\times10^{-4}\,\textrm{cm}^2/\textrm{s}$'},...
    'FontSize',15,...
    'LineStyle','none','Color',cc(:,1));
 annotation(gcf,'textbox',...
    [0.405546316664956 0.498342007514435 0.136785539814362 0.0389972144846797],...
    'Interpreter','latex',...
    'String',{'$\textrm{D}_{\textrm{o}_2}=7\times10^{-5}\,\textrm{cm}^2/\textrm{s}$'},...
    'FontSize',15,...
    'LineStyle','none','Color',cc(:,2));
annotation(gcf,'textbox',...     
    [0.582792131564123 0.44746485945723 0.198827552515877 0.0519962859795729],...
    'Interpreter','latex',...
    'String',{'$\textrm{D}_{\textrm{o}_2}=7\times10^{-6}\,\textrm{cm}^2/\textrm{s}$'},...
    'FontSize',15,...
    'LineStyle','none','Color',cc(:,3));
hold off;
writeVideo(writerObj,getframe());
end
close(writerObj);
display(['File ' filename ' written.']);



%%

% linkdata on
% xlabel('Z'' [ohm]');
% ylabel('-Z'''' [ohm]');
set(0,'ScreenPixelsPerInch',90);
set(gca,'FontSize',25,'FontName','Times New Roman','LineWidth',1);  


box off;
 axis([0 150 0 100]);
 pbaspect([1 100/150 1])
 
 %%  Saving Figures
% filename=['Experiment_' num2str(experiment) '_j_' names(i,1:4) ];
filename='Idis_all_equations';
if ismac
      foldername='/Users/mohitmehta/Dropbox/work/Phd/Conferences/ECS 2014/Data_simulation/';
%     foldername='/Users/mohitmehta/Copy/My Figures/Comparison_figures/';
elseif isunix
    display('Sorry No path');
    break;
else
    foldername='H:\Copy\My Figures\Comparison_figures\';
end
save_figures_with_cnt([foldername filename],gcf);