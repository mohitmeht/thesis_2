close all;
clc;
clear all;
%% Load Data from the database
if ismac
    var_flder='/Users/mohitmehta/Copy/work/Phd/Data/Xujie Chen_data_on_Li-air/EIS paper/Saved Variables/';
elseif ~isunix
    var_flder='H:\Copy\work\Phd\Data\Xujie Chen_data_on_Li-air\EIS paper\Saved Variables\';
end
load([var_flder 'All_EIS_data.mat']);
experiment=7;           % Experiment number
ZreZimohm=cell2mat(Impedance(3,experiment));
names=char(Impedance{4,experiment});
num=cell2mat(Impedance(1,experiment));
ZreZimohmX(:,:)=ZreZimohm(:,1,:);
ZreZimohmY(:,:)=ZreZimohm(:,2,:);
ocv=num(:,10:11:end);
frq(:,:)=Impedance{1,experiment}(:,3:11:end);
Zre_Abs(:,:)=Impedance{1,experiment}(:,7:11:end);
Zre_Phase(:,:)=Impedance{1,experiment}(:,8:11:end);
%% Compute the analytical results
i=5;
cc=distinguishable_colors(7,'w');
mul=[1e-3,1e-2,0.5,1.6,1e1,1e2,1e3];
I_dis=0.1e-3;
k0=1.3e-8.*mul;                       
n=2;
poro=0.7;
brugg=1.5;
Vt=26e-3;
Do=7e-6.*mul;                
c_int=3.26e-6.*mul;          
l=140e-4;
beta=0.475;
sp_area=26e4;
Cd=.01e-6;
 j=[7];    % k0
 p=[3];    % n
 m=[4];    % Do
 o=[3];    % c_int
[Z,tmp]=our_paper_analytical(I_dis,k0(j),n,Do(m),c_int(o),l,Cd,sp_area,brugg,poro,Vt,beta); 
%% Computing existing models        
        f_em=logspace(-3,6,100);
        w_em=2.*pi.*f_em;
        k=k0(j)*exp(-beta*n*eta/Vt)-kr*exp((1-beta)*n*eta/Vt);
% One specie semi-infinite gerishcer
       i0=n*F*A*ko;        % constant i0
       const0=Vt/(n*i0);   % constant0 
       const1=Vt/((n^2)*F*A);
       const2=(1./(sqrt(k+(1i.*w_em))));
       const3=1/(c_int(o)*sqrt(Do(m)));
       Zf=const0+(const1.*const2.*const3); 
%% Plot the two plots
shift=25;
plot(ZreZimohmX(:,i),ZreZimohmY(:,i),'o','Color',cc(i,:));hold on;
plot(real(Z)+shift,-imag(Z),'-*');
hold off;
%%
axis([370 480 15 60])
%% Cursors for Experiment Data
datacursormode on
hdt = datacursormode;
set(hdt,'DisplayStyle','datatip');
set(hdt,'UpdateFcn',{@labeldtips,frq(:,i)',ZreZimohmX(:,i)'})
%% Calculate slope 
info_struct = getCursorInfo(hdt);
y=[info_struct(1).Position(2) info_struct(2).Position(2)];
x=[info_struct(1).Position(1) info_struct(2).Position(1)];
ang=atand(diff(y)/diff(x));
display(['Experimental angle: ' num2str(ang) ' deg'])
%% Cursors for Computed
datacursormode on
hdt = datacursormode;
set(hdt,'DisplayStyle','datatip','Enable','on');
set(hdt,'UpdateFcn',{@labeldtips,tmp,real(Z)+shift})
%% Calculate slope 
info_struct = getCursorInfo(hdt);
y=[info_struct(1).Position(2) info_struct(2).Position(2)]; 
x=[info_struct(1).Position(1) info_struct(2).Position(1)];
ang=atand(diff(y)/diff(x));
display(['Computed angle: ' num2str(ang) ' deg'])
%%  Saving Figures
filename=['Xujhe_comparison_1mA_exp_7_wo_labels'];
if ismac
    foldername='/Users/mohitmehta/Copy/My Figures/Comparison_figures/';
elseif isunix
    display('Sorry No path');
    break;
else
    foldername='H:\Copy\My Figures\Comparison_figures\';
end
save_figures_with_cnt([foldername filename],gcf);