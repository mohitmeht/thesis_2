function varargout = EIS(varargin)
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @EIS_OpeningFcn, ...
                   'gui_OutputFcn',  @EIS_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end

function EIS_OpeningFcn(hObject, eventdata, handles, varargin)
handles.output = hObject;
guidata(hObject, handles);
if strcmp(get(hObject,'Visible'),'off')
    plot(rand(1));
    
end
cla;

function varargout = EIS_OutputFcn(hObject, eventdata, handles) 
varargout{1} = handles.output;
gui_input = struct('n',str2double(get(handles.n_input,'String')),...
                   'A',str2double(get(handles.A_input,'String')),...
                   'a',str2double(get(handles.sp_area_input,'String')),...
                   'poro',str2double(get(handles.poro_input,'String')),...
                   'Dbulk',str2double(get(handles.Dbulk_input,'String')),...
                   'po2',str2double(get(handles.po2_input,'String')),...
                   'l', str2double(get(handles.l_input,'String')),...
                   'kc', str2double(get(handles.kc_input,'String')),...
                   'T',str2double(get(handles.T_input,'String')),...
                   'alpha',str2double(get(handles.alpha_input,'String')),...
                   'I_dis',str2double(get(handles.I_dis_input,'String')),...
                   'brugg',str2double(get(handles.brugg_input,'String')),...
                   'c_int',str2double(get(handles.co2_sol_input,'String'))*str2double(get(handles.c_ext_input,'String')),...
                   'Cd',str2double(get(handles.Cd_input,'String')),...
                   'f_min',str2double(get(handles.freq_min_input,'String')),...
                   'f_max',str2double(get(handles.freq_max_input,'String')),...
                   'pts',str2double(get(handles.pts_input,'String')));
varargout{2} = gui_input;


% --- Executes on button press in simulate_button.
function simulate_button_Callback(hObject, eventdata, handles)
constants = struct('F', 96485);
gui_input = struct('n',str2double(get(handles.n_input,'String')),...
                   'A',str2double(get(handles.A_input,'String')),...
                   'a',str2double(get(handles.sp_area_input,'String')),...
                   'poro',str2double(get(handles.poro_input,'String')),...
                   'Dbulk',str2double(get(handles.Dbulk_input,'String')),...
                   'po2',str2double(get(handles.po2_input,'String')),...
                   'l', str2double(get(handles.l_input,'String')),...
                   'kc', str2double(get(handles.kc_input,'String')),...
                   'T',str2double(get(handles.T_input,'String')),...
                   'alpha',str2double(get(handles.alpha_input,'String')),...
                   'I_dis',str2double(get(handles.I_dis_input,'String')),...
                   'brugg',str2double(get(handles.brugg_input,'String')),...
                   'c_int',str2double(get(handles.co2_sol_input,'String'))*str2double(get(handles.c_ext_input,'String')),...
                   'Cd',str2double(get(handles.Cd_input,'String')),...
                   'f_min',str2double(get(handles.freq_min_input,'String')),...
                   'f_max',str2double(get(handles.freq_max_input,'String')),...
                   'pts',str2double(get(handles.pts_input,'String')));
f = logspace(log10(gui_input.f_min),log10(gui_input.f_max),gui_input.pts);               
% Other code
popup_sel_index = get(handles.eqn_selector_input, 'Value');
switch popup_sel_index
    case 1
        set(handles.output_window_input,'String',[get(handles.output_window_input,'String');...
                {['Please select an equation to solve...']}]);
            drawnow;
    otherwise
        try
        EIS_theories(handles,constants,gui_input,f,popup_sel_index);
        catch
            set(handles.output_window_input,'String',[get(handles.output_window_input,'String');...
                {['EIS_theories function failed...']}]);
            drawnow;  
        end    
end




% --- Executes on selection change in eqn_selector_input.
function eqn_selector_input_Callback(hObject, eventdata, handles)
function eqn_selector_input_CreateFcn(hObject, eventdata, handles)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
set(hObject, 'String', {'Select an equation to solve', ...
                        'EIS using tafel','EIS using sinh','EIS using beta'});


function n_input_Callback(hObject, eventdata, handles)
function n_input_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit2_Callback(hObject, eventdata, handles)
function edit2_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function A_input_Callback(hObject, eventdata, handles)
function A_input_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function sp_area_input_Callback(hObject, eventdata, handles)
function sp_area_input_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function poro_input_Callback(hObject, eventdata, handles)
function poro_input_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function alpha_input_Callback(hObject, eventdata, handles)
function alpha_input_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function l_input_Callback(hObject, eventdata, handles)
function l_input_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function Cd_input_Callback(hObject, eventdata, handles)
function Cd_input_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function I_dis_input_Callback(hObject, eventdata, handles)
function I_dis_input_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function kc_input_Callback(hObject, eventdata, handles)
function kc_input_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function c_int_input_Callback(hObject, eventdata, handles)
function c_int_input_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function freq_min_input_Callback(hObject, eventdata, handles)
function freq_min_input_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function freq_max_input_Callback(hObject, eventdata, handles)
function freq_max_input_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function T_input_Callback(hObject, eventdata, handles)
function T_input_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function Dbulk_input_Callback(hObject, eventdata, handles)
function Dbulk_input_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function brugg_input_Callback(hObject, eventdata, handles)
% hObject    handle to brugg_input (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of brugg_input as text
%        str2double(get(hObject,'String')) returns contents of brugg_input as a double


% --- Executes during object creation, after setting all properties.
function brugg_input_CreateFcn(hObject, eventdata, handles)
% hObject    handle to brugg_input (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function po2_input_Callback(hObject, eventdata, handles)
% hObject    handle to po2_input (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of po2_input as text
%        str2double(get(hObject,'String')) returns contents of po2_input as a double


% --- Executes during object creation, after setting all properties.
function po2_input_CreateFcn(hObject, eventdata, handles)
% hObject    handle to po2_input (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

