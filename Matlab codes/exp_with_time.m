clearvars -except ratio1 ratio2 ratio3;
clc;
global ZreZimohm names ZreZimohmX ZreZimohmY ocv frq t
cc=distinguishable_colors(size(t,2),'w');
%%
for (i=1:10)
    ld_expt(i);
    display(['Experiment ' num2eng(i)]);
display(names);
end;
%%

%%
% clear all;
% close all;
figure(1)
clf;
exp_number=8;
num_to_plot=4;
ld_expt(exp_number);
plot3(ZreZimohmX(:,num_to_plot),t(:,num_to_plot)./60,ZreZimohmY(:,num_to_plot),'o','Color',cc(length(get(gca,'children'))+1,:),...
    'MarkerFaceColor',cc(length(get(gca,'children'))+1,:),'DisplayName',names(num_to_plot,:));hold on;
% set(legend('-DynamicLegend'),'Interpreter','none','FontSize',20,'FontName','Times New Roman','Location','NorthEast');
view(gca,[0 0]);
%%
ld_expt(8);
% clf;

cc=distinguishable_colors(size(t,2),'w');
for num_to_plot=1:size(t,2)
figure(2);
    %     for num_to_plot=1:2
% num_to_plot=7;
% ld_expt(exp_number);
% plot3(ZreZimohmX(:,num_to_plot),t(:,num_to_plot)./60,ZreZimohmY(:,num_to_plot),'-','Color',cc(length(get(gca,'children'))+1,:),'MarkerFaceColor',cc(length(get(gca,'children'))+1,:),'DisplayName',num2str(exp_number));hold on;
plot3(ZreZimohmX(:,num_to_plot),t(:,num_to_plot)./60,ZreZimohmY(:,num_to_plot),'-','Color',cc(num_to_plot,:),'MarkerFaceColor',cc(num_to_plot,:),'DisplayName',names(num_to_plot,:));pause(2);hold on;
end
set(legend('-DynamicLegend'),'Interpreter','none','FontSize',20,'FontName','Times New Roman','Location','NorthEast');

%% Fit
close all;
clc;
for i=1:size(t,2)
f=fit( ZreZimohmX(:,i), ZreZimohmY(:,i), 'splineinterp');
set(plot(f),'Color',cc(i,:))
% plot(ZreZimohmX(:,1),ZreZimohmY(:,1),'s');
end
%% Custom cursor
clear angle
if exist('angle','var')
    delete(angle)
end
set(gcf,'Position',[613 89 795 300/450*795])
set(0,'ScreenPixelsPerInch',90);
set(gcf,'Color','w');
set(gca,'FontSize',20,'Color','w','FontName','Times New Roman');
xlabel('Z''[ohm]','FontSize',20,'FontName','Times New Roman')
zlabel('Z''''[ohm]','FontSize',20,'FontName','Times New Roman')
ylabel('time elapsed [min]','FontSize',20,'FontName','Times New Roman')
set(gca,'PlotBoxAspectRatio',[1 1 300/450]);
axis([0 450 0 inf 0 300])
angle=annotation('textbox',...
    [0.647248151149183 0.895096348544802 0.179874213836478 0.060377358490566],'String',...
    {['$\theta=\textrm{N.A.}$']},...
    'FitBoxToText','on','FontSize',20,...
    'FontName','Times New Roman','LineStyle','none','Interpreter','latex');
datacursormode on
hdt = datacursormode;
set(hdt,'DisplayStyle','datatip','Enable','on');
set(hdt,'UpdateFcn',{@labeldtips,frq(:,num_to_plot),ZreZimohmX(:,num_to_plot),ZreZimohmY(:,num_to_plot),hdt,angle});

found_peaks=[find(diff(sign(ZreZimohmY(:,num_to_plot)))~=0)+1;find(diff(sign(diff(ZreZimohmY(:,num_to_plot))))~=0)+1];
Re=ZreZimohmX(found_peaks(1),num_to_plot);
R1=ZreZimohmX(found_peaks(3),num_to_plot)-ZreZimohmX(found_peaks(1),num_to_plot);
f_peaks=frq(found_peaks,num_to_plot);
ratio12=ZreZimohmY(found_peaks(2),num_to_plot)/ZreZimohmY(found_peaks(3),num_to_plot);
ratio13=ZreZimohmY(found_peaks(2),num_to_plot)/ZreZimohmY(end,num_to_plot); % Need to edit if another semicircle is formed
%%
% Create textbox
% anno_final(1)=annotation(gcf,'textbox',...
%     [0.162407057813936 0.737584304564256 0.242767295597484 0.052863436123348],...
%     'Interpreter','latex',...
%     'String',{'$\theta_1=72.3004^\circ$',...
%     '$\theta_2=15.33^\circ$'},...
%     'FontSize',20,...
%     'FontName','Times New Roman',...
%     'LineStyle','none');
anno_final(1)=annotation(gcf,'textbox',...
    [0.161052106478355 0.418294804323118 0.306918238993711 0.471698113207547],...
    'Interpreter','latex',...
    'String',{'$\theta_1=72.3004^\circ$',...
    '$\theta_2=15.33^\circ$',...
    ['$R_e=' num2str(Re) '\,\Omega$'],...
    ['$R_1=' num2str(R1) '\,\Omega$'],...
    ['$f_1=' num2eng(f_peaks(1)) '\textrm{Hz}$'],...
    ['$f_2=' num2eng(f_peaks(2)) '\textrm{Hz}$'],...
    ['$f_3=' num2eng(f_peaks(3)) '\textrm{Hz}$'],...
    ['$\textrm{Ratio}_{12}=' num2str(ratio12) '$'],...
    ['$\textrm{Ratio}_{13}=' num2str(ratio13) '$']},...
    'FontSize',20,...
    'FontName','Times New Roman',...
    'LineStyle','none');
%% Calculate Slope

%% Calculate peak ratios
if exist('disp','var')
    delete(disp);
end;
info_struct = getCursorInfo(hdt);
disp=annotation('textbox',...
    [0.641 0.781900452488688 0.12275 0.0751131221719478],'String', {['Ratio: ' num2str(info_struct(2).Position(3)/info_struct(1).Position(3))],...
    ['Ratio Inverted: ' num2str(info_struct(1).Position(3)/info_struct(2).Position(3))],...
    ['Time difference: ' num2str(info_struct(1).Position(2)-info_struct(2).Position(2)) ' min(s)']},'FitBoxToText','on','FontSize',20,...
    'FontName','Times New Roman');

% display(' ')
% display(['Ratio: ' num2str(info_struct(2).Position(3)/info_struct(1).Position(3))]) 
% display(['Ratio Inverted: ' num2str(info_struct(1).Position(3)/info_struct(2).Position(3))]) 
% Calculate Measured time difference
% display(['Time difference: ' num2str(info_struct(1).Position(2)-info_struct(2).Position(2)) ' min(s)'])
%%
close all;
 length(get(gca,'children'))
 %%
clf;
cc=distinguishable_colors(10,'w');
ld_expt(2);
num_to_plot=1;
plot3(ZreZimohmX(:,num_to_plot)/max(ZreZimohmX(:,num_to_plot)),t(:,num_to_plot)/max(t(:,num_to_plot)),ZreZimohmY(:,num_to_plot)/max(ZreZimohmY(:,num_to_plot)),'Color',cc(length(get(gca,'children'))+1,:));hold on;
%%
ld_expt(4);
num_to_plot=1;
plot3(ZreZimohmX(:,num_to_plot)/max(ZreZimohmX(:,num_to_plot)),t(:,num_to_plot)/max(t(:,num_to_plot)),ZreZimohmY(:,num_to_plot)/max(ZreZimohmY(:,num_to_plot)),'Color',cc(length(get(gca,'children'))+1,:));hold on;
axis([0 1 0 0.01 0 1])

%%
leg=legend('first 50\mu','after 0dc 50\mu','the 0 dc','after 10\mu and 20\mu');
set(legend('first 50\mu','after 0dc 50\mu','the 0 dc','after 10\mu and 20\mu'),'FontSize',18,'Position',[0.794973846885402 0.573243801652893 0.0889205896338564 0.0759297520661157])
%%
figure(2);plot(frq(:,1),t(:,1).*frq(:,1))
%%
set(legend('-DynamicLegend'),'Interpreter','none','FontSize',20,'FontName','Times New Roman','Location','NorthEast');
%%
set(gcf,'Position',[603 261 795 753])
set(0,'ScreenPixelsPerInch',90);
set(gcf,'Color','w');
set(gca,'FontSize',20,'Color','w','FontName','Times New Roman');
disp=annotation('textbox',...
    [0.149134943602013 0.148544977968982 0.562424242424242 0.172715894868586],'String',...
    {['$\theta =' num2str(ang) '\times 10^{' num2str(floor(log10(k1(2)))) '}\,\textrm{cm/s}$']},...
    'FitBoxToText','on','FontSize',20,...
    'FontName','Times New Roman','LineStyle','none','Interpreter','latex');
%%
filename=['plot_' num2str(num_to_plot)];
if ismac
      foldername='/Users/mohitmehta/Copy/work/Phd/All EIS plots/Analysis/Slopes/';
%     foldername='/Users/mohitmehta/Copy/My Figures/Comparison_figures/';
elseif isunix
    display('Sorry No path');
    break;
else
    foldername='H:\Copy\work\Phd\All EIS plots\Analysis\Slopes\Exp 8\';
end
save_figures_with_cnt([foldername filename],gcf);
%%
ylim([0 15])
zlim([0 150])
xlim([0 650])
%%
xlabel('Z''[ohm]','FontSize',20,'FontName','Times New Roman')
zlabel('Z''''[ohm]','FontSize',20,'FontName','Times New Roman')
ylabel('time elapsed [min]','FontSize',20,'FontName','Times New Roman')