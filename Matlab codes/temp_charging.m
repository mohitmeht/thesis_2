clearvars;
clc;
kf0 = 1.7e-8;

n = 2; 
beta = 0.5;
poro = 0.75;
Deff = (poro^1.5).*7e-6;
c_ext = 40e-6;
kr0 = kf0*c_ext;
c_int = 0.3*c_ext;
l = 0.02;
Cd = 10e-6;
a = 5e4;
A = 1;
Vt = Vt_compute(300);
b = 0.5;
F = phys_constants('F');
I_dis = 0.1e-3; 
% %%
eta = petru_equation_while_charging(n,F,A,a,kf0,kr0,beta,Vt,Deff,c_int,l,I_dis);
sprintf('%0.5f V',2.96 + eta)
% %%
% eta = -0.086932  ;
kr = @(x)(a.*kr0.*exp((1-beta).*n./Vt.*x));
kf = @(x)(a.*kf0.*exp((-beta).*n./Vt.*x));
c_chg_int = (c_int-(kr(eta)./kf(eta)));
k_Deff = sqrt(kf(eta)./Deff);
x = 0:l/100:l;
co = @(x)((c_chg_int.*(cos(k_Deff.*x) + (tan(k_Deff.*l)*sin(k_Deff.*x)))) ...
            + (kr(eta)./kf(eta)));
subplot(2,1,1);    
plot(-x,co(x));
% %%
f=logspace(-3,log10(200e3),200);
[Z_charging,f,eta_charging]=our_paper_analytical_w_charging(I_dis,kf0,kr0,n,Deff,c_int,l,Cd,a,A,poro,Vt,beta,f);

subplot(2,1,2)
plot(real(Z_charging'),imag(Z_charging'),'-');
% hold on;