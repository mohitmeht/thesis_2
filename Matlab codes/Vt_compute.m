function [Vt] = Vt_compute(T)
kb = 1.3806488e-23;
q = 1.602176e-19;
Vt = kb*T/q;
end