function [kh_T] = kh_at_diff_temperature (T)
% kh is assumed to be kh = c/p
% kh_T for a given temperature is Henry's constant 
% T is any given temperature, in K
% Tstd refers to the standard temperature (298 K).
% kh_std refers to Henry's constant at the standard temperature (298 K). 
% C is the constant (in Kelvins)
% Default values for oxygen in water are
% kh_std = 1.3e-6; % mol/atm/cm3; 
% Tstd = 298 ; % K
% C = 1700;
%
% The values given below are for O2 in water. 
% change this function if other gas or solvents are required
kh_std = 1.3e-6;
Tstd = 298.15;
C = 1700;
temp=(T^-1)-(Tstd^-1);
kh_T=kh_std.*exp(C*temp);
end