%% Working for linearization and substitution
clear;
clc;
syms x beta n Vt k0 a v dv k lam
% f = k0*a*(exp((-beta)*n*x/Vt)-exp((1-beta)*n*x/Vt));
f = sqrt(pi*lam)/(1+exp(x))*erfc(lam-sqrt(1+sqrt(lam)+x^2)/(2*sqrt(lam)));
% f = -2*a*k0*sinh((0.5*n*x)/Vt);
% f = k0*a*(exp((-beta)*n*x/Vt));
f1 = subs(f,x,v);
t = taylor(f,'ExpansionPoint', v,'Order', 2);
% display(t-f1);
dk = subs(t-f1,x-v,dv)

% subs(dk,f)
% t1 = subs(t-f1,f,k);
% display(subs(t1,x-v,dv));
%% Working for expansion about a point.. and plot it using ezplot function

clear
k0 = 1e-10;
a=4e4;
beta = 0.1;
n = 2;
Vt = 26e-3;
syms x 
expansion_pt = 1e-3;
small_signal_ampli = 100e-6;
% f = -2*k0*a*sinh(n*x/Vt/2);
f = k0*a*(exp((-beta)*n*x/Vt)-exp((1-beta)*n*x/Vt));
t1 = taylor(f,x,'ExpansionPoint', expansion_pt,'Order', 1);
t2 = taylor(f,x,'ExpansionPoint', expansion_pt,'Order', 2);
% display(t2-t1);
clf;
hold on;
h=ezplot(t2,[expansion_pt-small_signal_ampli,expansion_pt+small_signal_ampli]);
set(h,'Color','red');

h=ezplot(f,[expansion_pt-small_signal_ampli,expansion_pt+small_signal_ampli]) ;
set(h,'Color','black');
legend ('5','function')
hold off;


%%
clear
syms x
k0 = 1e-10;
n = 2;
beta = 0.5;
Vt = 26e-3;
small_signal_ampli = 500e-6;
f1 = exp(-beta*n/Vt*x);
f2 = exp((1-beta)*n/Vt*x);
ezplot(f1/f2,[-0.093,0])
%%
clear;
clc;
syms x beta n Vt k0 a v dv k
% f = k0*a*(exp((-beta)*n*x/Vt)-exp((1-beta)*n*x/Vt));
f = -2*a*k0*sinh((0.5*n*x)/Vt);
% f = k0*a*(exp((-beta)*n*x/Vt));
f1 = subs(f,x,v);
t = taylor(f,'ExpansionPoint', v,'Order', 2);
dk = subs(t-f1,x-v,dv);
subs(dk,-a*k0,k/2/sinh(n*v/Vt))
% t1 = subs(t-f1,f,k);
% display(subs(t1,x-v,dv));

%%
clear
% syms x n beta a k0 Vt
syms x
a=2;
k0 = 1e-10;
a=4e4;
beta = 0.5;
n = 2;
Vt = 26e-3;
f_w_sinh = sinh(x);
f_w_exp = -exp(x);
% f_w_sinh = k0*a*sinh(n*x/2/Vt);
% f_w_exp = -k0*a*exp(-n*beta/Vt*x);
at_x_eq_to = -1e-6;
% exp_pnt_eq_to_min = at_x_eq_to-1e-1;
% exp_pnt_eq_to_max = at_x_eq_to+1e-1;
exp_pnt_eq_to_min = -0.5;
exp_pnt_eq_to_max = 0.5;
% taylor(f_w_sinh, x, 'ExpansionPoint', at_x_eq_to);
t1_w_sinh = taylor(f_w_sinh, x, 'ExpansionPoint', at_x_eq_to, 'Order', 1);
t2_w_sinh = taylor(f_w_sinh, x, 'ExpansionPoint', at_x_eq_to, 'Order', 2);
t3_w_sinh = taylor(f_w_sinh, x, 'ExpansionPoint', at_x_eq_to, 'Order', 4);
t1_w_exp = taylor(f_w_exp, x, 'ExpansionPoint', at_x_eq_to, 'Order', 1);
t2_w_exp = taylor(f_w_exp, x, 'ExpansionPoint', at_x_eq_to, 'Order', 2);
t3_w_exp = taylor(f_w_exp, x, 'ExpansionPoint', at_x_eq_to, 'Order', 4);
clc;
% display(t2_w_sinh)
% display(t2_w_exp)
% %%
clf;
hold on
h=ezplot(t2_w_exp, [exp_pnt_eq_to_min,exp_pnt_eq_to_max]);
set(h,'Color','red');
h=ezplot(t3_w_exp, [exp_pnt_eq_to_min,exp_pnt_eq_to_max]);
set(h,'Color','black');
h=ezplot(t3_w_sinh, [exp_pnt_eq_to_min,exp_pnt_eq_to_max]);
set(h,'Color','blue');
% h=ezplot(f, [exp_pnt_eq_to_min,exp_pnt_eq_to_max]);
% set(h,'Color','black');
legend('2','4')

title('Taylor Series Expansion')
hold off

%%
clear
clf
clc
syms x
f_w_sinh = 2*sinh(x);
f_w_exp = (-exp(-x));
exp_pnt_eq_to_min = -10;
exp_pnt_eq_to_max = 10;
hold on
h=ezplot(f_w_sinh, [exp_pnt_eq_to_min,exp_pnt_eq_to_max]);
set(h,'Color','red');
h=ezplot(f_w_exp, [exp_pnt_eq_to_min,exp_pnt_eq_to_max]);
hold off
%%
clear
clc
clf
syms x
k0 = 1e-10;
a=4e4;
beta = 0.4;
n = 2;
Vt = 26e-3;
at_x_eq_to =-0.5e-3;
% exp_pnt_eq_to_min = -0.3;
% exp_pnt_eq_to_max = 0.3;
exp_pnt_eq_to_min = at_x_eq_to-10e-6;
exp_pnt_eq_to_max = at_x_eq_to+10e-6;
f1 = exp((-beta)*n*x/Vt);
f2 = exp((1-beta)*n*x/Vt);
f_w_exp = k0*a*(exp((-beta)*n*x/Vt)-exp((1-beta)*n*x/Vt));
f_w_sinh = -2*k0*a*sinh(n*x/2/Vt);
t1_w_sinh = taylor(f_w_sinh, x, 'ExpansionPoint', at_x_eq_to, 'Order', 1);
t2_w_sinh = taylor(f_w_sinh, x, 'ExpansionPoint', at_x_eq_to, 'Order', 2);
t3_w_sinh = taylor(f_w_sinh, x, 'ExpansionPoint', at_x_eq_to, 'Order', 4);
t1_w_exp = taylor(f_w_exp, x, 'ExpansionPoint', at_x_eq_to, 'Order', 1);
t2_w_exp = taylor(f_w_exp, x, 'ExpansionPoint', at_x_eq_to, 'Order', 2);
t3_w_exp = taylor(f_w_exp, x, 'ExpansionPoint', at_x_eq_to, 'Order', 4);
hold on
% h=ezplot(t3_w_exp,[exp_pnt_eq_to_min,exp_pnt_eq_to_max]);hold on;
% set(h,'Color','red');
h=ezplot(f_w_exp,[exp_pnt_eq_to_min,exp_pnt_eq_to_max]);
set(h,'Color','blue','MarkerSize',2,'Marker','o','LineStyle','none');
h=ezplot(t2_w_sinh,[exp_pnt_eq_to_min,exp_pnt_eq_to_max]);
set(h,'Color','red','LineWidth',2,'LineStyle','-');
hold off