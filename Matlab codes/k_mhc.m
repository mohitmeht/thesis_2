function [k] = k_mhc(lam,eta,k0,sp_area) 
MHC_short_int = sqrt(1+sqrt(lam)+eta.^2);
MHC_long_int = (lam-MHC_short_int)./(2.*sqrt(lam));
k = -k0.*sp_area./k_mhc_0(lam).*(sqrt(pi.*lam).*tanh(eta/2)).*erfc(MHC_long_int); % Full MHC using tanh.
% k = -k0./k_mhc_0(lam).*(sqrt(pi.*lam).*tanh(eta/2)).*erfc(MHC_long_int); % Full MHC using tanh.

end

function [k_0] = k_mhc_0(lam)
k_0 = (sqrt(pi.*lam)./2.*erfc((lam-sqrt(1+sqrt(lam)))./(2.*sqrt(lam))));  % k_MHC at 0, do not delete.. Its not equal to k_mhc(lam,0)
end