#!/bin/bash
#MOAB -N Matlab_prospectus_plots
#MOAB -l nodes=4
#MOAB -l walltime=02:00:00
#MOAB -j oe
module load intel
module load gnu-openmpi
cd /panfs/storage.local/ame/home/mrm11s
export MATLAB_PREFDIR=/panfs/storage.local/ame/home/mrm11s/my_matlab_codes/pref
matlab </panfs/storage.local/ame/home/mrm11s/my_matlab_codes/matlab_pref/top_file_impedances_linux.m
echo "end"