clear all;
% close all;
% clc;
exp_number=5;
num_to_plot=6;
% clc;
global ZreZimohm names ZreZimohmX ZreZimohmY ocv frq t
ld_expt(exp_number);
cc=distinguishable_colors(size(t,2),'w');
%%
% clf;
% num_to_plot=6;
time=t(:,num_to_plot)./60;
plot3(ZreZimohmX(time<5,num_to_plot),t(time<5,num_to_plot)./60,ZreZimohmY(time<5,num_to_plot),'o','Color',cc(num_to_plot,:),...
    'MarkerFaceColor',cc(num_to_plot,:),'DisplayName',names(num_to_plot,:));hold on;
set(legend('-DynamicLegend'),'Interpreter','none','FontSize',10,'FontName','Times New Roman','Location','NorthEast');
view(gca,[0 0 1]);
xlabel('Z''[ohm]','FontSize',15,'FontName','Times New Roman')
zlabel('Z''''[ohm]','FontSize',15,'FontName','Times New Roman')
ylabel('time elapsed [min]','FontSize',15,'FontName','Times New Roman')
%%
ZreZimohmX(time<2,num_to_plot)