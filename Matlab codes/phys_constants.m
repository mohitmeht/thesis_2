function [value,unit,name]=phys_constants(var)
% Various Physical Constants
% Enter the variable for a physical constant and 
% the function will return its value
% k_B, k_B_in_SI, R, e, F, N_A
switch var
    case 'k_B'
        name = 'Boltzman constant';
        value = 8.6173324e-5;
        unit = 'eV/K';
    case 'k_B_in_SI'
        name = 'Boltzman constant';
        value = 1.3806488e-23;
        unit = 'eV/K';        
    case 'R'
        name = 'Gas constant';
        value = 8.3144621;
        unit = 'J/K/mol';
    case 'e'
        name = 'electron';
        value = 1.602176565e-19;
        unit = 'eV';
    case 'F'
        name = 'Faradays constant';
        value = 96485;
        unit = 'C/mol';
    case 'N_A'
        name = 'Avogardo number';
        value = 6.0221413e23;
        unit = '1/mol';
    otherwise 
        name = 'Not available';
        value = NaN;
end
end