function  ld_expt( experiment )
Impedance=ld_eis_db;
global ZreZimohm names ZreZimohmX ZreZimohmY ocv frq t
[ZreZimohm,names,ZreZimohmX,ZreZimohmY,ocv,frq,t]=ld_eis_msrmnt(Impedance,experiment);
end

