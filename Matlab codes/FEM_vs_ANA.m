clear;
clc;
% Loading data from the current folder (Assuming it is the thesis matlab code folder)
filename = 'FEM_vs_ANA_sim_xy.dat';
fileID = fopen(filename,'r');
dataArray = textscan(fileID, '%f%f%f%f%f%f%f%f%f%f%[^\n\r]', 'Delimiter', ',',  'ReturnOnError', false);
fclose(fileID);
VarName1 = dataArray{:, 1};
VarName2 = dataArray{:, 2};
VarName3 = dataArray{:, 3};
VarName4 = dataArray{:, 4};
VarName5 = dataArray{:, 5};
VarName6 = dataArray{:, 6};
VarName7 = dataArray{:, 7};
VarName8 = dataArray{:, 8};
VarName9 = dataArray{:, 9};
VarName10 = dataArray{:, 10};
clearvars filename delimiter formatSpec fileID dataArray ans;
% Listing common parameters
T = 300;
o2_perc = 100;
Cd = 10e-6;
A = 1;
f=logspace(-3,log10(2),200);
k0=1.7e-10;    
n=2;
beta = 0.5;
sp_area=5e5;
poro=0.75;
brugg=1.5;
Do=7e-6;
Deff=Do.*(poro^brugg);   
ext_conc=40.375e-6;
sol=0.3;
c_int=sol*ext_conc;  
l=0.019995;
F = phys_constants('F');
Vt = Vt_compute(T);

% Plotting for 0.1mA/cm2
I_dis=0.1e-3;
[Z_1,~,eta] = our_paper_analytical_BV(I_dis,k0,n,Deff,c_int,l,Cd,sp_area,A,poro,Vt,beta,f);
display(['OCV = ' num2str(2.956 + eta) ' V']);
figure
hold on;
plot(min(VarName1)+real(Z_1'),imag(Z_1'),'-', 'DisplayName','Analytical @ 0.1 mA/cm^2','LineWidth',1); 
plot(VarName1,VarName2,'V', 'DisplayName','Simulation @ 0.1 mA/cm^2','MarkerFaceColor','w');

% Plotting for 0.2mA/cm2
I_dis=0.2e-3;
[Z_2,~,eta] = our_paper_analytical_BV(I_dis,k0,n,Deff,c_int,l,Cd,sp_area,A,poro,Vt,beta,f);
display(['OCV = ' num2str(2.956 + eta) ' V']);
plot(min(VarName3)+real(Z_2'),imag(Z_2'),'-', 'DisplayName','Analytical @ 0.2 mA/cm^2','LineWidth',1); 
plot(VarName3,VarName4,'V', 'DisplayName','Simulation @ 0.2 mA/cm^2','MarkerFaceColor','w');

% Plotting for 0.3mA/cm2
I_dis=0.3e-3;
[Z_3,~,eta] = our_paper_analytical_BV(I_dis,k0,n,Deff,c_int,l,Cd,sp_area,A,poro,Vt,beta,f);
display(['OCV = ' num2str(2.956 + eta) ' V']);
plot(VarName5,VarName6,'V', 'DisplayName','Simulation @ 0.3 mA/cm^2','MarkerFaceColor','w');
plot(min(VarName5)+real(Z_3'),imag(Z_3'),'-', 'DisplayName','Analytical @ 0.3 mA/cm^2','LineWidth',1); 

% Plotting for 0.5mA/cm2
I_dis=0.5e-3;
[Z_5,~,eta] = our_paper_analytical_BV(I_dis,k0,n,Deff,c_int,l,Cd,sp_area,A,poro,Vt,beta,f);
display(['OCV = ' num2str(2.956 + eta) ' V']);
plot(VarName7,VarName8,'V', 'DisplayName','Simulation @ 0.5 mA/cm^2','MarkerFaceColor','w');
plot(min(VarName7)+real(Z_5'),imag(Z_5'),'-', 'DisplayName','Analytical @ 0.5 mA/cm^2','LineWidth',1); 

% Plotting for 0.5mA/cm2
I_dis=1e-3;
[Z_10,~,eta] = our_paper_analytical_BV(I_dis,k0,n,Deff,c_int,l,Cd,sp_area,A,poro,Vt,beta,f);
display(['OCV = ' num2str(2.956 + eta) ' V']);
plot(VarName9,VarName10,'V', 'DisplayName','Simulation @ 1 mA/cm^2','MarkerFaceColor','w');
plot(min(VarName7)+real(Z_10'),imag(Z_10'),'-', 'DisplayName','Analytical @ 1 mA/cm^2','LineWidth',1); 

legend('-DynamicLegend');
xlabel('Re(Z) [\Omega]','FontName','TimesNewRoman','FontSize',24);
ylabel('-Im(Z) [\Omega]','FontName','TimesNewRoman','FontSize',24);
hold off;
set(gca,'FontName','TimesNewRoman')
set(gca,'FontSize',24);
box on
set(gca,'Color','w')
set(gcf,'Color','w') 
xmax = 300;
ymax = 150;
axis([0 xmax 0 ymax])
set(gca,'PlotBoxAspectRatio',[1 ymax/xmax 1]);
%% Writing Data to csv
mat_2_write = [real(Z_1') imag(Z_1') real(Z_2') imag(Z_2') real(Z_3') imag(Z_3') real(Z_5') imag(Z_5') real(Z_10') imag(Z_10')];
csvwrite('FEM_vs_ANA_ana_xy.dat',mat_2_write);

%% Saving the resultant figure
filename=['Ana_vs_FEM'];
if ismac
      foldername='/Users/mohitmehta/Dropbox/Apps/Texpad/Thesis/figures/';
elseif isunix
    display('Sorry No path');
else
    foldername='E:\Dropbox\Apps\Texpad\Thesis\figures\';
end
save_figures_with_cnt([foldername filename],gcf);
