function [p,Z,f]=Our_model_1_andrei_model(I,k,n,Do,co,L,Cd,a)
Deff=Do*(0.75^1.5);
f=logspace(-3,6,100);
w=2.*pi.*f;
% k=1.3e-7;
% a=1e4;
% n=2;
epsi=0.75;
beta=0.5;
Vt=26e-3;
% I=1e-3;
eta=0.5;
% Cd=10e-6;
% L=0.01;

A=1;
CD=a*A*L*Cd;
% co=3.26e-6;
F=96500;
syms lam;
lambda=vpasolve(n*A*F*Deff*co*tanh(L/lam)/lam-I, lam,1.5);
lambda=double(lambda);
cal_eta=(Vt/(n*beta))*log((lambda^2)*k*a/Deff);
display([num2eng(cal_eta) 'V']);

% lambda=sqrt(Deff/(k*a*exp(n*beta*eta/Vt)));
omega=w.*epsi.*(lambda.^2)./Deff;
l=L/lambda;
Zf_a=Vt/(n*beta*I);
Zf_b_n=(omega.*1i);
Zf_b_b_b=omega.*1i;
Zf_b_b_c=-1;
Zf_b_b_a_n=tanh(sqrt(1+(omega.*1i)).*l);
Zf_b_b_a_b=sqrt(1+(omega.*1i)).*tanh(l);
Zf_b=Zf_b_n./((Zf_b_b_a_n./Zf_b_b_a_b)+Zf_b_b_b+Zf_b_b_c);
Zf=Zf_a.*Zf_b;
Zd=1./(1i.*w.*CD);
Z=(Zf.*Zd)./(Zf+Zd);
p=plot(real(Z),-imag(Z),'o');
axis equal;
set(gcf,'Position',[134   218   560   420])
set(0,'ScreenPixelsPerInch',90);
set(gcf,'Color','w');
set(p,'LineWidth',1);
set(gca,'FontSize',12,'Color','w','FontName','Times New Roman');
box off;
xlabel('Z''[\Omega]');
ylabel('-Z''''[\Omega]');
end