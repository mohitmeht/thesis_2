clear all;
clc;
kb = phys_constants('k_B');      % Boltzman constant
q = phys_constants('e');         % Electron Energy
F = phys_constants('F');               % Faraday's constant
l=0.04;     % Length in cm % from the paper
temp_in_c = 30;
T= 273.15 + temp_in_c;
Vt=Vt_compute(T);
c_int=3.26e-6;     % converting to mol/cm3
a=4e4;      % Calculated using Pt distribution and cathode physical parameters
epsi=0.75;
% k0 = [1e-4 1e-10];
beta=0.5;
n=2;
E0=2.956; % First value of the voltage from the data % given in the paper
% Dbulk  = logspace(-10,-4,10);
Dbulk  = 7e-6;
A = 1;
% Deff=epsi^1.5*Dbulk;                          % comparison simulations
variable_parameter = epsi^1.5*Dbulk;
I_dis_multiple=linspace(1e-6,5e-3,200)';
lamda = petru_equation(n,F,A,variable_parameter,c_int,l,I_dis_multiple);
k = variable_parameter./lamda.^2;
clc
% cc=distinguishable_colors(100,'w');
k0 = logspace(-10,-4,200);
eta_c = Vt./n./beta.*log((1./k)*(k0.*a));
%%
% eta_sinh = (2.*Vt./n)*asinh(-k*(1./k0)./(2.*a));

figure(2)
clf;
[C3,h3] = contourf((I_dis_multiple),(k0),eta_c',200);hold on;
[C1,h1]=contour((I_dis_multiple),(k0),eta_c',[0,-0.075]); % Values inside the square brackets are exact overpotential values
[C2,h2]=contour((I_dis_multiple),(k0),eta_c',[0,-0.075]); % Values inside the square brackets are exact overpotential values
h1.LevelList = [-0.075];
h2.LevelList = [0];
h2.LineStyle = '--';
h1.LineStyle = '-';
h1.LineColor = 'Black';
h2.LineColor = 'Black';

h3.LineStyle = 'none';
hold off;
caxis([min(min(eta_c)),max(max(eta_c))])
% colormap('jet');
 set(gca,'XScale','log')
set(gca,'YScale','log')
c=colorbar;
xlabel('Faradaic current [A]','FontSize',12,'FontName','Times New Roman')
ylabel('Reaction rate constant [cm/s]','FontSize',12,'FontName','Times New Roman')
ylabel(c,'\eta (over-potential) [V]','FontSize',12);
set(gcf,'Color','w');
set(gca,'FontSize',12,'Color','w','FontName','Times New Roman');
axis square
t1= text(1e-5,1e-6,'-\eta < 0','Rotation',0,'FontName','Times New Roman','FontSize',12);
t2= text(3.6e-5,6e-9,{'Tafel approximation','      breaks down'},'Rotation',49,'FontName','Times New Roman','FontSize',12);
t3= text(0.55e-3,1e-6,{'(0 < -\eta < 3V_T)'},'Rotation',49,'FontName','Times New Roman','FontSize',12);
t4= text(0.1e-3,5e-10,{'Tafel approximation','             holds','         (-\eta > 3V_T)'},'Rotation',0,'FontName','Times New Roman','FontSize',12,'Color', 'White');
t5= text(0.1e-3,5e-10,{'Discharge'},'Rotation',45,'FontName','Times New Roman','FontSize',12,'Color', 'White');
t6= text(1e-5,1e-6,{'Charge'},'Rotation',45,'FontName','Times New Roman','FontSize',12);
%%
clearvars -except I_dis_multiple k0 eta_c
contourf(log10(I_dis_multiple),log10(k0),eta_c',600)
xlabel('Discharge current [mA]','FontSize',24,'FontName','Times New Roman')
ylabel('k_0 [cm/s]','FontSize',24,'FontName','Times New Roman')
c=colorbar;
ylabel(c,'\eta (over-potential) [V]','FontSize',24)
%%
set(gcf,'Position',[603 261 795 753])
set(0,'ScreenPixelsPerInch',90);
set(gcf,'Color','w');
set(gca,'FontSize',24,'Color','w','FontName','Times New Roman');
%%
filename=['Contour_for_Tafel_Breakdown'];
if ismac
      foldername='/Users/mohitmehta/Dropbox/Apps/Texpad/Dissertation/figures/';
elseif isunix
    display('Sorry No path');
    break;
else
    foldername='C:\Users\Samira\Dropbox\Apps\Texpad\Thesis\figures\';
end
save_figures_with_cnt([foldername filename],gcf);