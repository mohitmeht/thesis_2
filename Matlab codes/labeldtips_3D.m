function [output_txt] = labeldtips_3D(obj,event_obj,...
                      freq,Z_re,Z_im,hdt,disp)                  
pos = get(event_obj,'Position');
if size(Z_re,2)>1
    Z_re = Z_re';
    Z_im = Z_im';
end
x = pos(1); y = pos(2);

[row,col] = find(Z_re == x);  % Find index to retrieve obs. name
% The find is reliable only if there are no duplicate x values
%[row,col] = ind2sub(size(Z),idx);
% labeldtips_test(hdt,disp);
output_txt = {['Z'': ',num2str(Z_re(row),4),'[ohm]'] ['-Z'''': ',num2str(Z_im(row),4),'[ohm]'] ['Freq: ' cell2mat(num2eng(freq(row))) 'Hz'] ['Pos: ' num2str(row)] ['Time: ' num2str(y) ' min']};
end