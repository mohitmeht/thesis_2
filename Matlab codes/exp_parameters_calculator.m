%% This calculates the parameters for the experiment of performing simultaneously impedance spectra and the discharge curve

clearvars -except frq f1;
f=logspace(-3,6,46);
f1=frq(:,2);
t=sum(1./f)/60;
t1=sum(1./f1)/60;
(t1-t)/t1*100
%%
clc;
low_freq=1e-3;
hig_freq=200e3;
total_pts=611;
num_per_dec=log10(hig_freq/low_freq)/log10(10);
pts_per_dec=total_pts/num_per_dec;

f=logspace(log10(low_freq),log10(hig_freq),total_pts);
t=sum(1./f)/60;
time_in_sec=sum(1./f);
time_in_min=time_in_sec/60;
time_in_hrs=time_in_min/60;

if(ceil(time_in_hrs)-floor(time_in_hrs))~=0
    display(['Total time:- ' num2str(ceil(t)) ' mins'...
    ' or ' num2str(floor(t/60)) ':' num2str(floor((time_in_hrs-floor(time_in_hrs))*60)) ' hrs']);
end
% display(['Total time:- ' num2str(ceil(t)) ' mins' ' or ' num2str(floor(t/60)) ':' num2str(floor(((t/60)-floor(t/60))*60)) ' hrs']);
% display(['Total time:- ' num2str(ceil(t/60)) ' hrs']);
display(['Points/decade = '  num2str(ceil(pts_per_dec))...
    ' or ' num2str((pts_per_dec))]);

