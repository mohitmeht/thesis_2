close all;
clc;
clear all;
%% Load Data from the database
global ZreZimohm names ZreZimohmX ZreZimohmY ocv frq t
ld_expt(10);
cc=jet(300);
names

%% Database plotting


%% Constants for computation 
cc=distinguishable_colors(7,'w');
I_dis=0.1e-3;
k0=13e-8;                       
n=2;
poro=0.5;
brugg=1.5;
beta=0.5;
Vt=26e-3;
Do=7e-6;  
Deff=Do.*(poro^brugg);   
cross_area=2.5;
c_int=5e-7;  
l=0.0064;
sp_area=5e4;
Cd=7e-6;
% %% Compute the analytical results
%  [Z,tmp]=our_paper_analytical(I_dis,k0(j),n,Do(m),c_int(o),l,Cd,sp_area,brugg,poro,Vt,beta);
% Deff is not computed but given directly 

[Z,tmp]=our_paper_analytical(I_dis,k0,n,Deff,c_int,l,Cd,sp_area,cross_area,poro,Vt,beta,1); 
% [Z,tmp]=our_paper_analytical(I_dis,ko,n,Deff,Co,l,Cd,sp_area,brugg,poro,Vt,beta); 
tmpX_andrei=real(Z)+28;
tmpY_andrei=-imag(Z);

plot(tmpX_andrei,tmpY_andrei,'^','color','black','MarkerFaceColor','black');
axis([0 1.1*max(tmpX_andrei) 0 1.1*max(tmpX_andrei)])

%%
close all;
plot(tmpX_andrei,tmpY_andrei,'^','color','black','MarkerFaceColor','black');hold on;
plot(ZreZimohmX(:,2),ZreZimohmY(:,2),'V','color','blue','MarkerFaceColor','blue');hold off;
% plot(tmpX_liter,tmpY_liter,'V','color','blue','MarkerFaceColor','blue');hold off;
linkdata on;
hold off;
%%
datacursormode on
hdt = datacursormode;
set(hdt,'DisplayStyle','datatip','Enable','on');
set(hdt,'UpdateFcn',{@labeldtips,tmp,tmpX})
%% Computing existing models   
F=96500;
A=1;
beta=0.5;
n=2;
l=10e-6;
Vt=26e-3;
eta=-0.2;
Cd=0.03e-6;
f_em=logspace(-2,6,100);
w_em=2.*pi.*f_em;
ko=1.3e-8;
% k=ko*exp(-beta*n*eta/Vt)-ko*exp((1-beta)*n*eta/Vt);
% k=ko*exp(-beta*n*eta/Vt);
% One specie semi-infinite gerishcer
% i0=n*F*A*ko;        % constant i0
i0=0.01e-3;
% Deff=Do(m)*(poro^brugg);
Deff=10e-7;
Co=3.26e-8;
const0=Vt/(n*i0);   % constant0 
const1=Vt/((n^2)*F*A);
i_omega=1i.*w_em;
under_sqrt=k+i_omega;
const2=(tanh(sqrt(under_sqrt./Deff).*l)./...
        (sqrt(under_sqrt)));
const3=1/(Co*sqrt(Deff)); 

% const2=(1./(sqrt(k+(1i.*w_em))));
% const3=1/(Co*sqrt(Deff));
Zf=const0+(const1.*const2.*const3); 
Z_liter=Zf./(1+(1i.*w_em.*Zf.*Cd));
tmpX_liter=real(Z_liter);
tmpY_liter=-imag(Z_liter);
plot(tmpX_liter,tmpY_liter)
% clear Z;
axis([0 100 0 100])
%%
x=real(Z);
y=-imag(Z);
hdt = datacursormode;
set(hdt,'DisplayStyle','datatip','Enable','on');
set(hdt,'UpdateFcn',{@labeldtips,tmp,tmpX});
datacursormode on
% axis square
%% Plot the two plots
% close all;
figure (1)
% shift=-115;
shift=400;
plot(ZreZimohmX(:,i),ZreZimohmY(:,i),'o','Color',cc(i,:),'MarkerFaceColor',cc(i,:));hold on;
% plot(real(Z)+shift,-imag(Z)+20,'-*');
hold off;
 axis([0 inf 0 inf])
xlabel('Z''[\Omega]','Fontsize',15,'FontName','Times New Roman');
ylabel('-Z''''[\Omega]','Fontsize',15,'FontName','Times New Roman');
box off;
% axis square;
set(0,'ScreenPixelsPerInch',90);
set(gcf,'Color','w');
set(gca,'FontSize',15,'Color','w','FontName','Times New Roman');
% linkdata on;
%%
% close all;
i=5;
rnge_strt=1;
tmpX=ZreZimohmX(rnge_strt:end,i);
tmpY=ZreZimohmY(rnge_strt:end,i);
tmpfrq=frq(rnge_strt:end,i);
set(gcf,'Color','w');
set(gca,'FontSize',15,'Color','w','FontName','Times New Roman');    
% figure (2)
%    plot(tmpX,tmpY,'o','Color','black','MarkerFaceColor','black');hold on;
clc;

%%
plot(x,y);linkdata on
hold off;
% axis([0 550 0 550])
% axis([370 480 15 60])

%% Plot using Linked DATA


a=50;
b=430;
c=0;
x=b-a:0.1:b+0.98*a;
y=sqrt((a.^2)-((x-b).^2))+c;

%% Cursors for Experiment Data
% i=4;

hdt = datacursormode;
set(hdt,'DisplayStyle','datatip');
set(hdt,'UpdateFcn',{@labeldtips,frq(:,i)',ZreZimohmX(:,i)'})
datacursormode on
title(names(i,:),'Interpreter','none');
%% Calculate slope 
info_struct = getCursorInfo(hdt);
slpe_y=[info_struct(1).Position(2) info_struct(2).Position(2)];
slpe_x=[info_struct(1).Position(1) info_struct(2).Position(1)];
ang=atand(diff(slpe_y)/diff(slpe_x));
display(['Experimental angle:' num2str(ang) ' deg'])
title(['DC: ' names(i,17:21) 'A/cm^2 ','Experimental angle: ' num2str(ang) ' deg'],'Fontsize',15);
% title(['DC: ' names{i},'      Experimental angle: ' num2str(ang) ' deg'],'Fontsize',15, 'Interpreter','none');
% title(names(i,:),'Interpreter','none');
%% Cursors for Computed
datacursormode on
hdt = datacursormode;
set(hdt,'DisplayStyle','datatip','Enable','on');
set(hdt,'UpdateFcn',{@labeldtips,tmp,comp_X})
%% Calculate slope for computed
info_struct = getCursorInfo(hdt);
y=[info_struct(1).Position(2) info_struct(2).Position(2)]; 
x=[info_struct(1).Position(1) info_struct(2).Position(1)];
ang=atand(diff(y)/diff(x));
title(['Computed angle: ' num2str(ang) ' deg'])
% refreshdata();
%%  Saving Figures
% filename=['Experiment_' num2str(experiment) '_j_' names(i,1:4) ];
filename='discharge_exp_1_all_cmb';
if ismac
      foldername='/Users/mohitmehta/Viivo/Figures_date/';
%     foldername='/Users/mohitmehta/Copy/My Figures/Comparison_figures/';
elseif isunix
    display('Sorry No path');
    break;
else
    foldername='H:\Copy\My Figures\Comparison_figures\';
end
save_figures_with_cnt([foldername filename],gcf);