function [search_result, array_idx]= find_closest_value(search_array,search_value,delta)
tmp = abs(search_array-search_value);
[idx idy] = min(tmp); %index of closest value
tmp_result = search_array(sub2ind(size(search_array), idy, 1:length(idy))); %closest value
search_result=tmp_result(tmp_result>search_value-delta & tmp_result<search_value+delta);
array_idx=find(tmp_result>search_value-delta & tmp_result<search_value+delta);
end