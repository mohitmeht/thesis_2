k0 = 5e-9;
n = 2;
poro = 0.75;
Deff = (poro^1.5).*7e-6;
c_ext = c_ext_calculator(101325,21,300);
c_int = 0.3*c_ext;
l = 0.02;
Cd = 10e-6;
a = 1e5;
A = 1;
Vt = Vt_compute(300);
b = 0.5;
eta = 0.1;
k = a*k0*(exp((1-b)*n/Vt*eta)-exp(-b*n*eta/Vt));
% k = 0.01;
lam = sqrt(k./Deff);
% %%
x = 0:0.001:0.2;
co = @(x)(c_int.*(cosh(lam.*x) - (tanh(lam.*l)*sinh(lam.*x))));
plot(-x,co(x));
%%
clf
lam_inv = sqrt(k./Deff);
x = 0:l/1000:l;
co = @(x)(c_int.*(cosh(lam_inv.*x) - (tanh(lam_inv.*l)*sinh(lam_inv.*x))));
plot(-x,co(x)); hold on;
%%

k0 = 1.7e-8;
n = 2;
poro = 0.75;
Deff = (poro^1.5).*7e-6;
c_ext = c_ext_calculator(101325,21,300);
c_int = 0.3*c_ext;
l = 0.02;
Cd = 10e-6;
a = 1e5;
A = 1;
Vt = Vt_compute(300);
beta = 0.5;
f=logspace(-3,log10(20),200);
I_dis = 1e-3; 
% [Z_charging,~,eta_charging] = our_paper_analytical_w_charging(I_dis,k0,n,Deff,c_int,l,Cd,a,A,poro,Vt,beta,f);
[Z,f,eta_BV] = our_paper_analytical_BV(I_dis,k0,n,Deff,c_int,l,Cd,a,A,poro,Vt,beta,f);


lam_inv = sqrt(k./Deff);
x = 0:l/1000:l;
co = @(x)(c_int.*(cosh(lam_inv.*x) - (tanh(lam_inv.*l)*sinh(lam_inv.*x))));
plot(-x,co(x)); hold on;
%%

clc;
k0 = 5e-9;
n = 2;
beta = 0.5;
poro = 0.75;
Deff = (poro^1.5).*7e-6;
c_ext = c_ext_calculator(101325,21,300);
c_int = 0.3*c_ext;
l = 0.02;
Cd = 10e-6;
a = 1e5;
A = 1;
Vt = Vt_compute(300);
b = 0.5;
F = phys_constants('F');
I_dis = 0.5e-3; 
lam = petru_equation_while_charging(n,F,A,Deff,c_int,l,I_dis);
k = Deff./lam^2;
%%
clc;
% eta_eqn= @(x)(exp((1-beta).*n.*x./Vt) - ...
%                     exp(-beta.*n.*x./Vt) - ...
%                     (k./(k0.*a)));
eta_eqn= @(x)(exp((1-beta).*n.*x./Vt) - ...
                    exp(-beta.*n.*x./Vt) - ...
                    (k./(k0.*a)));
eta = fzero(eta_eqn,0.1);
%%
k_Deff = sqrt(k./Deff);
x = 0:l/1000:l;
co = @(x)(c_int.*(cos(k_Deff.*x) + (tan(k_Deff.*l)*sin(k_Deff.*x))));
plot(-x,co(x));