%%
clear all;clc;close all;
if ismac
    var_flder='/Users/mohitmehta/Copy/work/Phd/Data/Xujie Chen_data_on_Li-air/EIS paper/Saved Variables/';
elseif ~isunix
    var_flder='H:\Copy\work\Phd\Data\Xujie Chen_data_on_Li-air\EIS paper\Saved Variables\';
end
load([var_flder 'All_EIS_data.mat']);
experiment=9;           % Experiment number
ZreZimohm=cell2mat(Impedance(3,experiment));
names=char(Impedance{4,experiment});
num=cell2mat(Impedance(1,experiment));
ZreZimohmX(:,:)=ZreZimohm(:,1,:);
ZreZimohmY(:,:)=ZreZimohm(:,2,:);
ocv=num(:,10:11:end);
frq(:,:)=Impedance{1,experiment}(:,3:11:end);
time=1./frq./60;
cc=distinguishable_colors(size(ocv,2),'w');
frq_clean=frq(isfinite(frq(:, 1)), :);
ocv_clean=ocv(isfinite(ocv(:, 1)), :);
time_clean=time(isfinite(time(:, 1)), :);
clear frq ocv time
frq=frq_clean;
ocv=ocv_clean;
time=time_clean;
names
%%
figure(1);
% plot(time,voltages,'O','MarkerFaceColor','blue','Color','blue');
for i=1:size(ocv,2)
% for i=[2 4 6]
    plot(time(:,i),ocv(:,i),'-','Color',cc(i,:),'MarkerFaceColor',cc(i,:),'LineWidth',2,'DisplayName',names(i,:));hold on;
end
hold off;
set(legend('-DynamicLegend'),'Interpreter','none','FontSize',15,'FontName','Times New Roman','Location','NorthEast');


xlabel('Time [minutes]','Fontsize',15,'FontName','Times New Roman');
ylabel('Open Circuit Voltage [v]','Fontsize',15,'FontName','Times New Roman');
box off;
% axis square;
set(0,'ScreenPixelsPerInch',90);
set(gcf,'Color','w');
set(gca,'FontSize',15,'Color','w','FontName','Times New Roman');
ylim([2 3.6]);
axis square
%% Combining All discharge curves into 1
exp_total_time=zeros(size(ocv_clean,2)*size(ocv_clean,1),1);
% voltages=zeros(size(ocv,2)*2,1);
% Assigning first array of total time
exp_total_time(1:size(ocv_clean,1),1)=1./frq_clean(:,1)./60;
for i=2:size(ocv_clean,2)
exp_total_time(((i-1)*size(ocv_clean,1))+1:(i*size(ocv_clean,1)),1)=exp_total_time((i-1)*size(ocv_clean,1),1)+(1./frq_clean(:,i)./60);
end
voltages=reshape(ocv_clean,[],1);
%% Ploting combined discharge curves 
figure(2);
plo_num_low=2;
plo_num_high=3;
for i=2:size(ocv,2)
plot(exp_total_time(((i-1)*size(ocv_clean,1))+1:(i)*size(ocv_clean,1),1),...
    voltages(((i-1)*size(ocv_clean,1))+1:(i)*size(ocv_clean,1),1),'-','Color',cc(i,:),'MarkerFaceColor',cc(i,:),'LineWidth',2);hold on;
end
hold off;
xlabel('Time [minutes]','Fontsize',15,'FontName','Times New Roman');
ylabel('Open Circuit Voltage [v]','Fontsize',15,'FontName','Times New Roman');
box off;
% axis square;
set(0,'ScreenPixelsPerInch',90);
set(gcf,'Color','w');
set(gca,'FontSize',15,'Color','w','FontName','Times New Roman');

axis square
%%
filename=['experiment_' num2str(experiment) '_comb'];
if ismac
      foldername='/Users/mohitmehta/Copy/work/Phd/Discharge curve experiment/Preliminary-show xujie/';
%     foldername='/Users/mohitmehta/Copy/My Figures/Comparison_figures/';
elseif isunix
    display('Sorry No path');
    break;
else
    foldername='H:\Copy\My Figures\Comparison_figures\';
end
save_figures_with_cnt([foldername filename],gcf);

