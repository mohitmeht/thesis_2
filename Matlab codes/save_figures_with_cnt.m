function save_figures_with_cnt(file_name,figure_handle)

extension_to_be_used = '.pdf';
if ismac
    files=dir([file_name '_*' extension_to_be_used]);
elseif isunix
    files=dir([file_name '_*' extension_to_be_used]);
else
    files=dir([file_name '_*' extension_to_be_used]);
end  


[~,idx] = sort([files.datenum]);
if size(files,1)==0
    cnt=0;
else
    for num=6:-1:4
        if ~isnan(str2double(files(idx(size(idx,2))).name(end-num:end-4)))
            cnt=str2double(files(idx(size(idx,2))).name(end-num:end-4));
            break;
        end
    end
end
cnt=cnt+1;
if ismac
    a=[file_name '_' num2str(cnt) extension_to_be_used];
    export_fig(figure_handle, a);
elseif isunix
    a=[file_name '_' num2str(cnt) extension_to_be_used];
    savefig(figure_handle, a);
else
    a=[file_name '_' num2str(cnt) extension_to_be_used];
    export_fig(figure_handle, a);
end   
display([a ' has been written']);
end