% EIS plotter for my thesis, all plots should be plotted using this file
%% Nyquist vs Bode Plotter
% Load Data from the database
clear all;
clc;
Impedance=ld_eis_db();
experiment=6;           % Experiment number
ZreZimohm=cell2mat(Impedance(3,experiment));
names=char(Impedance{4,experiment});
num=cell2mat(Impedance(1,experiment));
ZreZimohmX(:,:)=ZreZimohm(:,1,:);
ZreZimohmY(:,:)=ZreZimohm(:,2,:);
ocv=num(:,10:11:end);
frq(:,:)=Impedance{1,experiment}(:,3:11:end);
Zre_Abs(:,:)=Impedance{1,experiment}(:,7:11:end);
Zre_Phase(:,:)=Impedance{1,experiment}(:,8:11:end);

% Nyquist Plot
hold off
cc=[0,0,0;0,0,1];
i=5;            % Plot number/reading number
tot_readings=2*size(ZreZimohm,3);
prnt=0;
plotlinewidth=1.5;
markertype='-';
figvis='off';
markersize=7;
fontsize = 20;
range=1:size(ZreZimohm,1);

figure(1);
subplot(1,2,1);
plot(ZreZimohmX(:,i),ZreZimohmY(:,i),'-h','LineWidth',plotlinewidth,'MarkerFaceColor','w','Color',mean(cc));
grid off; box on;
axis square;               % Axis Size and font type
set(gcf,'Color','w'); 
ylabel('-Z'''' [\Omega]','FontSize',fontsize,'FontName','Times New Roman');
xlabel('Z'' [\Omega]','FontSize',fontsize,'FontName','Times New Roman');
set(gca,'FontSize',fontsize,'FontName','Times New Roman');
xmax = 350;
ymax = 350;
axis([0 xmax 0 ymax])
set(gca,'YTick',[0 100 200 300 400])
set(gca,'PlotBoxAspectRatio',[1 ymax/xmax 1]);

% Bode Plot
tot_readings=2*size(ZreZimohm,3);

prnt=0;
plotlinewidth=1.5;
figvis='off';
markersize=7;

range=1:size(ZreZimohm,1);
plot_freq = frq(:,i);
plot_Abs = Zre_Abs(:,i);
plot_phase = Zre_Phase(:,i);
subplot(1,2,2);
[AX,H1,H2] =plotyy(frq(:,i),Zre_Abs(:,i),frq(:,i),Zre_Phase(:,i),'semilogx');
set(H1,'Marker','v','LineWidth',plotlinewidth,'MarkerFaceColor','w','Color',cc(1,:));
set(H2,'Marker','o','LineWidth',plotlinewidth,'MarkerFaceColor','w','Color',cc(2,:));
set(H1,'LineStyle','-');
set(H2,'LineStyle','-');
set(AX(1),'xlim',[10e-3 1e6],'XTick',[10e-3 1 1e2 1e4 1e6]);
set(AX(2),'xlim',[10e-3 1e6],'XTick',[10e-3 1 1e2 1e4 1e6]);
set(AX(1),'ylim',[0 350],'YTick',[100 200 300 400 500 600 700]);
set(AX(2),'ylim',[-60 30],'YTick',[-60 -30 0 30],'YColor', cc(2,:));
set(AX(1),'YColor',cc(1,:));
set(AX(2),'YColor',cc(2,:));

grid off; box on;
axis square;               % Axis Size and font type
set(gcf,'Color','w'); 
set(get(AX(1),'Ylabel'),'String','\midZ\mid [\Omega]','FontSize',fontsize,'FontName','Times New Roman');
set(get(AX(2),'Ylabel'),'String','Arg (Z) [\circ]','FontSize',fontsize,'FontName','Times New Roman');
set(get(AX(1),'Xlabel'),'String','Frequency [Hz]','FontSize',fontsize,'FontName','Times New Roman');
set(get(AX(2),'Xlabel'),'String','Frequency [Hz]','FontSize',fontsize,'FontName','Times New Roman');
set(AX(1),'FontSize',fontsize,'FontName','Times New Roman');
set(AX(2),'FontSize',fontsize,'FontName','Times New Roman');  
set(AX(1),'PlotBoxAspectRatio',[1 1 1]);
set(AX(2),'PlotBoxAspectRatio',[1 1 1]);
%% BV vs Tafel for different discharge currents
clear;
clc;
k0 = 5e-9;
n = 2;
poro = 0.75;
Deff = (poro^1.5).*7e-6;
c_ext = c_ext_calculator(101325,21,300);
c_int = 0.3*c_ext;
l = 0.02;
Cd = 10e-6;
a = 1e5;
A = 1;
Vt = Vt_compute(300);
beta = 0.5;
f=logspace(-3,log10(20),200);
I_dis = 1e-3.*[0.01 0.02 0.05 0.1]; 
figure(1);
clf;
hold on;
for i= 1:length(I_dis)
[Z,f,eta_BV] = our_paper_analytical_BV(I_dis(i),k0,n,Deff,c_int,l,Cd,a,A,poro,Vt,beta,f);
plot(real(Z'),imag(Z'),'-');
[Z,f,eta_Tafel] = our_paper_analytical(I_dis(i),k0,n,Deff,c_int,l,Cd,a,A,poro,Vt,beta,f(1:2:end));
plot(real(Z'),imag(Z'),'V');
display(['OCV_BV = ' num2str(2.956 + eta_BV) ' V']);
display(['OCV_Tafel = ' num2str(2.956 + eta_Tafel) ' V']);
end
hold off;
xmax=2800;
ymax=1500;   
axis([0 xmax 0 ymax])
xlabel('Re(Z) /\Omega','FontName','Times New Roman','FontSize',18)
ylabel('-Im(Z) /\Omega','FontName','Times New Roman','FontSize',18)
set(gca,'PlotBoxAspectRatio',[1 ymax/xmax 1]);
set(gca,'Color','w','FontName','Times New Roman','FontSize',18)
set(gcf,'Color','w');
box on;
cac=gca;
cac.XMinorTick = 'on';
cac.YMinorTick = 'on';
%% BV vs Tafel for different reaction rate constants
clear;
clc;
n = 2;
poro = 0.75;
Deff = (poro^1.5).*7e-6;
c_ext = c_ext_calculator(101325,21,300);
c_int = 0.3*c_ext;
l = 0.02;
Cd = 10e-6;
a = 1e5;
A = 1;
Vt = Vt_compute(300);
beta = 0.5;
f=logspace(-3,log10(1),200);
I_dis = 10e-6; 
k0 = 1e-8.*[0.01 0.2 0.5 1 2 5];
figure(1);
clf;
% g1 = hggroup;
% g2 = hggroup;
hold on;
for i= 1:length(k0)
[Z_BV,~,eta_BV] = our_paper_analytical_BV(I_dis,k0(i),n,Deff,c_int,l,Cd,a,A,poro,Vt,beta,f);
% plot(real(Z_BV'),imag(Z_BV'),'-','DisplayName',num2str(k0(i)),'Parent',g1);
plot(real(Z_BV'),imag(Z_BV'),'-','DisplayName',num2str(k0(i)));
[Z_T,~,eta_Tafel] = our_paper_analytical(I_dis,k0(i),n,Deff,c_int,l,Cd,a,A,poro,Vt,beta,f(1:3:end));
display(['OCV_BV = ' num2str(2.956 + eta_BV) ' V']);
display(['OCV_Tafel = ' num2str(2.956 + eta_Tafel) ' V']);
end
plot(real(Z_T'),imag(Z_T'),'V','DisplayName','Tafel');
% plot(real(Z_T'),imag(Z_T'),'V','DisplayName','Tafel','Parent',g2);
hold off;
xmax=3000;
ymax=1500;   
axis([0 xmax 0 ymax])
xlabel('Re(Z) /\Omega','FontName','Times New Roman','FontSize',18)
ylabel('-Im(Z) /\Omega','FontName','Times New Roman','FontSize',18)
set(gca,'PlotBoxAspectRatio',[1 ymax/xmax 1]);
set(gca,'Color','w','FontName','Times New Roman','FontSize',18)
set(gcf,'Color','w');
box on;
cac=gca;
cac.XMinorTick = 'on';
cac.YMinorTick = 'on';
%% BV vs Tafel for different reaction rate constants b~=0.5
clear;
clc;
n = 2;
poro = 0.75;
Deff = (poro^1.5).*7e-6;
c_ext = c_ext_calculator(101325,21,300);
c_int = 0.3*c_ext;
l = 0.02;
Cd = 10e-6;
a = 1e5;
A = 1;
Vt = Vt_compute(300);
beta = 0.55;
f=logspace(-3,log10(1),200);
I_dis = 10e-6; 
k0 = 1e-8.*[0.01 0.5 1 2 5];
figure(1);
clf;
g1 = hggroup;
g2 = hggroup;
g3 = hggroup;
hold on;
for i= 1:length(k0)
[Z_FBV,~,eta_FBV] = our_paper_analytical_FBV(I_dis,k0(i),n,Deff,c_int,l,Cd,a,A,poro,Vt,beta,f);
plot(real(Z_FBV'),imag(Z_FBV'),'-','DisplayName',num2str(k0(i)),'Parent',g1);
[Z_BV,~,eta_BV] = our_paper_analytical_BV(I_dis,k0(i),n,Deff,c_int,l,Cd,a,A,poro,Vt,beta,f(1:5:end));
plot(real(Z_BV'),imag(Z_BV'),'o','DisplayName',num2str(k0(i)),'Parent',g2);
[Z_T,~,eta_Tafel] = our_paper_analytical(I_dis,k0(i),n,Deff,c_int,l,Cd,a,A,poro,Vt,beta,f(1:5:end));
display(['OCV_BV = ' num2str(2.956 + eta_BV) ' V']);
display(['OCV_FBV = ' num2str(2.956 + eta_FBV) ' V']);
display(['OCV_Tafel = ' num2str(2.956 + eta_Tafel) ' V']);
end
plot(real(Z_T'),imag(Z_T'),'V','DisplayName','Tafel','Parent',g3);
hold off;
xmax=3000;
ymax=1500;   
axis([0 xmax 0 ymax])
xlabel('Re(Z) /\Omega','FontName','Times New Roman','FontSize',18)
ylabel('-Im(Z) /\Omega','FontName','Times New Roman','FontSize',18)
set(gca,'PlotBoxAspectRatio',[1 ymax/xmax 1]);
set(gca,'Color','w','FontName','Times New Roman','FontSize',18)
set(gcf,'Color','w');
box on;
cac=gca;
cac.XMinorTick = 'on';
cac.YMinorTick = 'on';
g2.Children(5).Color = [0 0.5 0];
g2.Children(5).LineWidth = 1;
g2.Children(5).MarkerFaceColor = 'w'; 
g2.Children(4).Color = [0.08 0.17 0.55];
g2.Children(4).LineWidth = 1;
g2.Children(4).MarkerFaceColor = 'w';
g2.Children(3).Color = [0.6 0.2 0];
g2.Children(3).LineWidth = 1;
g2.Children(3).MarkerFaceColor = 'w';
g2.Children(2).Color = [0.49 0.18 0.56];
g2.Children(2).LineWidth = 1;
g2.Children(2).MarkerFaceColor = 'w';
g2.Children(1).Color = 'r';
g2.Children(1).LineWidth = 1;
g2.Children(1).MarkerFaceColor = 'w';
g2.Children(1).HandleVisibility = 'off';
g1.Children(5).Color = [0 0.5 0];
g1.Children(5).LineWidth = 1;
g1.Children(4).Color = [0.08 0.17 0.55];
g1.Children(4).LineWidth = 1;
g1.Children(3).Color = [0.6 0.2 0];
g1.Children(3).LineWidth = 1;
g1.Children(2).Color = [0.49 0.18 0.56];
g1.Children(2).LineWidth = 1;
g1.Children(1).Color = 'r';
g1.Children(1).LineWidth = 1;
g1.Children(1).HandleVisibility = 'off';
g3.Children.Color = 'black';
g3.Children.LineWidth = 1;
g3.Children.MarkerFaceColor = 'w'; 
legend([g1,g2,g3],'BV (\beta = 0.55)','BV (\beta = 0.5) ','Tafel (\beta = 0.55)')
%% BV vs Tafel vs FBV for different values of beta 
clear;
clc;
n = 2;
poro = 0.75;
Deff = (poro^1.5).*7e-6;
c_ext = c_ext_calculator(101325,21,300);
c_int = 0.3*c_ext;
l = 0.02;
Cd = 10e-6;
a = 4e4;
A = 1;
Vt = Vt_compute(300);
beta = [0.3 0.4 0.5 0.6 .7];
f=logspace(-3,log10(1),200);
I_dis = 30e-6; 
k0 = 1.3e-8;
figure(1);
clf;
g1 = hggroup;
g2 = hggroup;
g3 = hggroup;
hold on;
for i= 1:length(beta)
[Z_FBV,~,eta_FBV] = our_paper_analytical_FBV(I_dis,k0,n,Deff,c_int,l,Cd,a,A,poro,Vt,beta(i),f);
plot(real(Z_FBV'),imag(Z_FBV'),'-','DisplayName',num2str(beta(i)),'Parent',g1);
[Z_BV,~,eta_BV] = our_paper_analytical_BV(I_dis,k0,n,Deff,c_int,l,Cd,a,A,poro,Vt,beta(i),f(1:7:end));
[Z_T,~,eta_Tafel] = our_paper_analytical(I_dis,k0,n,Deff,c_int,l,Cd,a,A,poro,Vt,beta(i),f(1:7:end));
plot(real(Z_T'),imag(Z_T'),'V','DisplayName',num2str(beta(i)),'Parent',g2);
display(['OCV_BV = ' num2str(2.956 + eta_BV) ' V']);
display(['OCV_FBV = ' num2str(2.956 + eta_FBV) ' V']);
display(['OCV_Tafel = ' num2str(2.956 + eta_Tafel) ' V']);
end
plot(real(Z_BV'),imag(Z_BV'),'o','DisplayName','BV (\beta = 0.5)','Parent',g3);
hold off;
xmax=1700;
ymax=900;   
axis([0 xmax 0 ymax])
xlabel('Z'' [\Omega]','FontName','Times New Roman','FontSize',18)
ylabel('-Z''''[\Omega]','FontName','Times New Roman','FontSize',18)
set(gca,'PlotBoxAspectRatio',[1 ymax/xmax 1]);
set(gca,'Color','w','FontName','Times New Roman','FontSize',18)
set(gcf,'Color','w');
box on;
cac=gca;
cac.XMinorTick = 'on';
cac.YMinorTick = 'on';
g2.Children(5).Color = [0 0.5 0];
g2.Children(5).LineWidth = 1;
g2.Children(5).MarkerFaceColor = 'w'; 
g2.Children(4).Color = [0.08 0.17 0.55];
g2.Children(4).LineWidth = 1;
g2.Children(4).MarkerFaceColor = 'w';
g2.Children(3).Color = [0.6 0.2 0];
g2.Children(3).LineWidth = 1;
g2.Children(3).MarkerFaceColor = 'w';
g2.Children(2).Color = [0.49 0.18 0.56];
g2.Children(2).LineWidth = 1;
g2.Children(2).MarkerFaceColor = 'w';
g2.Children(1).Color = 'r';
g2.Children(1).LineWidth = 1;
g2.Children(1).MarkerFaceColor = 'w';
g2.Children(1).HandleVisibility = 'off';
g1.Children(5).Color = [0 0.5 0];
g1.Children(5).LineWidth = 1;
g1.Children(4).Color = [0.08 0.17 0.55];
g1.Children(4).LineWidth = 1;
g1.Children(3).Color = [0.6 0.2 0];
g1.Children(3).LineWidth = 1;
g1.Children(2).Color = [0.49 0.18 0.56];
g1.Children(2).LineWidth = 1;
g1.Children(1).Color = 'r';
g1.Children(1).LineWidth = 1;
g1.Children(1).HandleVisibility = 'off';
g3.Children.Color = 'black';
g3.Children.LineWidth = 1;
g3.Children.MarkerFaceColor = 'w'; 
legend([g1,g2,g3],'BV (\beta = 0.55)','BV (\beta = 0.5) ','Tafel (\beta = 0.55)')
%% FBV charging (THIS IS INCORRECT, the solution still needs to be found)
clear;
clc;
k0 = 1.7e-8;
n = 2;
poro = 0.75;
Deff = (poro^1.5).*7e-6;
c_ext = c_ext_calculator(101325,21,300);
c_int = 0.3*c_ext;
l = 0.02;
Cd = 10e-6;
a = 1e5;
A = 1;
Vt = Vt_compute(300);
beta = 0.5;
f=logspace(-3,log10(20),200);
I_dis = 1e-3; 
[Z_charging,~,eta_charging] = our_paper_analytical_w_charging(I_dis,k0,n,Deff,c_int,l,Cd,a,A,poro,Vt,beta,f);
figure(1);
plot(real(Z_charging'),imag(Z_charging'),'-');
display(['OCV = ' num2str(2.956 + eta_charging) ' V']);
%% Dissolution kinetics 
clear;
clc;

%--------Dont change these values-----------------------------------------
T0 = 273.15;        % Standard Temperature
R = 82.05736;       % Gas constant
alpha = 0.028494; % Bunsen coefficient for 1M LiPF6 PC:DME (1:2)
kh = R.*T0/alpha;
%-------------------------------------------------------------------------

k0 = 1.7e-6;
n = 2;
poro = 0.75;
Deff = (poro^1.5).*7e-6;
po2 = 0.21;

kf = 1;
l = 0.02;
Cd = 10e-6;
a = 5e4;
A = 1;
T = 300;

Vt = Vt_compute(T);

beta = 0.5;
f=logspace(-3,log10(20),200);
I_dis = 10e-3; 

[Z_diss,~,eta_diss,lam,Co] = our_paper_analytical_diss_kinetics(I_dis,k0,n,Deff,l,Cd,a,A,poro,Vt,po2,(kh),kf,f);
x = 0:l/10000:l/100;
co2 = @(x)((Co.*(cosh(x./lam) - (tanh(l./lam)*sinh(x./lam)))));
subplot(2,1,1);    
plot(-x,co2(x));
co2(0)
% figure(1);
subplot(2,1,2);
plot(real(Z_diss'),imag(Z_diss'),'-');hold on;
display(['OCV = ' num2str(2.956 + eta_diss) ' V']);
hold off;
%% CSV Writer
clear Z_to_write
Z_to_write = [real(Z_BV{1})',-imag(Z_BV{1})',real(Z_BV{2})',-imag(Z_BV{2})',...
            real(Z_BV{3})',-imag(Z_BV{3})',real(Z_BV{4})',-imag(Z_BV{4})',...
            real(Z_BV{5})',-imag(Z_BV{5})',real(Z_BV{6})',-imag(Z_BV{6})',...
            {real(Z_T)'},{-imag(Z_T)'}];
csvwrite('BV_Tafel_different_k0_part_1.csv',Z_to_write(1:12))
csvwrite('BV_Tafel_different_k0_part_2.csv',Z_to_write(13:14))
%% Saving Figures
filename='BV_vs_Tafel_vs_FBV_EIS_diff_k0';
% filename='BV_vs_Tafel_EIS_diff_k0';
if ismac
      foldername='/Users/mohitmehta/Dropbox/Apps/Texpad/Thesis/figures/';
%     foldername='/Users/mohitmehta/Copy/My Figures/Comparison_figures/';
elseif isunix
    display('Sorry No path');

else
    foldername='E:\Dropbox\Apps\Texpad\Thesis\figures\';
end
save_figures_with_cnt([foldername filename],gcf);