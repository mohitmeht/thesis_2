clear all;
close all;
display('Part A');
lpc_design_C=[47E-9 0.47E-6 1E-6 33E-9 10E-6 2.2E-6 47E-6 100E-6 220E-6];
lpc_design_R=[3900 4300 100 1000 2000 620 560 160 180] ;
(lpc_design_R'*lpc_design_C.*2*pi).^-1;
[r c]=find(ans>1000 & ans<6000);
for i=1:length(r)
display(i);
display(lpc_design_R(1,r(i)));
display(lpc_design_C(1,c(i)));
display(ans(r(i),c(i)));
end

display('Part B');
hpc_R=[3900 4300 100 1000 2000 620 560 160 180 200 220 240 270 36 33] ;
hpc_L=[2.2E-3 3.3E-3 10E-3 100E-3 1E-3];
a=diag(hpc_L)
b=a^-1
t=(diag(b)*hpc_R)/(2*pi)
[r c]= find(t<7000 & t>1000)
for i=1:length(r)
display(i);
display(hpc_L(1,r(i)));
display(hpc_R(1,c(i)));
display(t(r(i),c(i)));
end



% display('Part C')
% C=[47E-9 0.47E-6 1E-6 33E-9 10E-6 2.2E-6 47E-6 100E-6 220E-6];
% L=[2.2E-3 3.3E-3 10E-3 100E-3 1E-3];
% fn=1./(2*pi.*sqrt(L'*C));
% R=[3900 4300 100 1000 2000 620 560 160 180 200 220 240 270 36 33] ;
% tp=(R/2)*sqrt(C/L)
% [r c]= find(fn<7000 & fn>1000);
%  for i=1:length(r)
% display(i);
% display(L(1,r(i)));
% display(C(1,c(i)));
% display(fn(r(i),c(i)));
% end