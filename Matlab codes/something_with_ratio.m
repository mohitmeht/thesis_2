clf;
exp_number=8;
num_to_plot=5;
ld_expt(exp_number);
plot3(ZreZimohmX(:,num_to_plot),t(:,num_to_plot)./60,ZreZimohmY(:,num_to_plot),'-','Color',cc(length(get(gca,'children'))+1,:),...
    'MarkerFaceColor',cc(length(get(gca,'children'))+1,:),'DisplayName',names(num_to_plot,:));hold on;
set(legend('-DynamicLegend'),'Interpreter','none','FontSize',20,'FontName','Times New Roman','Location','NorthEast');
datacursormode on
hdt = datacursormode;
set(hdt,'DisplayStyle','datatip','Enable','on');
set(hdt,'UpdateFcn',{@labeldtips,frq(:,num_to_plot),ZreZimohmX(:,num_to_plot),ZreZimohmY(:,num_to_plot)});
view(gca,[0 0])
%%
if exist('disp','var')
    delete (disp)
end;
info_struct = getCursorInfo(hdt);
disp=annotation('textbox',...
    [0.641 0.781900452488688 0.12275 0.0751131221719478],'String', {['Ratio: ' num2str(info_struct(2).Position(3)/info_struct(1).Position(3))],...
    ['Ratio Inverted: ' num2str(info_struct(1).Position(3)/info_struct(2).Position(3))],...
    ['Time difference: ' num2str(info_struct(1).Position(2)-info_struct(2).Position(2)) ' min(s)']},'FitBoxToText','on','FontSize',20,...
    'FontName','Times New Roman');
%%
clc
exp_number=8;
for num_to_plot=1:size(t,2)
ld_expt(exp_number);

found_peaks=find(diff(sign(diff(ZreZimohmY(:,num_to_plot))))~=0)+1;
peaks=ZreZimohmY(found_peaks,num_to_plot);
time_diff=diff(t(found_peaks))/60;
ratio(1)=peaks(1)/peaks(2);

if ratio(1)<0
    ratio=1/ratio;
end
display(' ');
display(['Number ' num2str(num_to_plot)])
display(['i = ' names(num_to_plot,17:end-9) ' mA/cm^2']);
display(['Ratio = ' num2str(ratio)]);
display(['Time difference = ' num2str(time_diff) ' min']);
end