clc;
clear;
kf0 = 1.7e-10;
kr0 = 1.8e-14;
n = 2; 
beta = 0.5;
poro = 0.75;
Deff = (poro^1.5).*5.7e-6;
c_ext = 40e-6;
c_int = 0.3*c_ext;
l = 0.02;
Cd = 10e-6;
a = 5e4;
A = 1;
Vt = Vt_compute(300);
b = 0.5;
F = phys_constants('F');
I_dis = 0.1e-3; 

x0 = 0.01.*ones(size(I_dis)); 
kr = @(x)(a.*kr0.*exp((1-beta).*n./Vt.*x));
kf = @(x)(a.*kf0.*exp((-beta).*n./Vt.*x));
c_chg_int = @(x)((kr(x)./kf(x)) - c_int);
f_w_kr = @(x)((n.*F.*A.*sqrt(kf(x).*Deff).*c_chg_int(x).*tanh(sqrt(kf(x)./Deff).*l))-(I_dis));
[eta] = fzero(f_w_kr,x0); % Call solver

sprintf('%0.5f',2.96 + eta)
% %%
% eta = -0.086932  ;
kr_f = @(x)(a.*kr0.*exp((1-beta).*n./Vt.*x));
kf_f = @(x)(a.*kf0.*exp((-beta).*n./Vt.*x));
c_chg_int = (c_int-(kr_f(eta)./kf_f(eta)));
k_Deff = sqrt(kf(eta)./Deff);
x = 0:l/100:l;
co = @(x)((c_chg_int.*(cosh(k_Deff.*x) - (tanh(k_Deff.*l)*sinh(k_Deff.*x)))) ...
            + (kr(eta)./kf(eta)));
plot(-x,co(x));


F=phys_constants('F');
kr = kr_f(eta);
kf = kf_f(eta);
c_chg = (c_int-(kr./kf));
f=logspace(-3,log10(200e3),200);
w=2.*pi.*f;
im=1i.*w.*poro;
an=1;
ad=1;
a_1 = an./ad;
bn=kf;
bd=im;
b_1 = bn./bd;
dkf = kf + im;
cn = kf.*sqrt(kf).*tanh(sqrt(dkf./Deff).*l);
cd = im.*sqrt(dkf).*tanh(sqrt(kf./Deff).*l);
c_1 = cn./cd;
dn = kr.*sqrt(kf).*tanh(sqrt(dkf./Deff).*l);
dd = beta.*c_chg.*dkf.*sqrt(dkf).*tanh(sqrt(kf./Deff).*l);
d_1 = dn./dd;
e_1 = ((kr0./kf0.*exp(n.*eta./Vt)) - (kr./dkf));
e_2_n = sqrt(kf).*l;
e_2_d = beta.*c_chg.*sqrt(Deff).*tanh(sqrt(kf./Deff).*l);
e_2 = e_2_n./e_2_d;
e_comb = e_1.*e_2;
Z0=-Vt./(n.*I_dis.*beta);
Zf=Z0./((a_1)-(b_1)+(c_1)+(d_1)+(e_comb));
Zd=1./(1i.*w.*a.*A.*l.*Cd);
Z_charging=(Zf.*Zd)./(Zf+Zd);
plot(real(Z_charging'),imag(Z_charging'),'-');