%% Data files from the simualtions are required.... 
% Perform clear before using this matlab file. So that all the similar
% variables are removed...
clc;
clearvars -except tmpX_SIM tmpY_SIM tmpX_ANA tmpY_ANA save_location;
I_dis_list = 1e-3.*[0.1,0.2,0.3,0.5,1];     % Add list of currents for which the simulations are performed    
I_dis = I_dis_list(save_location);          % save location is used to make and store different curves
k0 = 1.7e-8;
n =2;
poro = 0.75;
Deff = (poro^1.5).*7e-6;
c_int = 10.126e-6;
l = 0.015;
Cd = 10e-6;
a = 35e4;
A = 1;
Vt = Vt_compute(298);
beta = 0.5;
f=logspace(-3,0,50);

[Z,f] = our_paper_analytical(I_dis,k0,n,Deff,c_int,l,Cd,a,A,poro,Vt,beta,f);
tmpX_ANA(save_location)={real(Z)+25'};
tmpY_ANA(save_location)={-imag(Z)'};
plot(tmpX_ANA{save_location},tmpY_ANA{save_location},'LineWidth',2,'Color',[0.0431372560560703 0.517647087574005 0.780392169952393]);
hold on;
plot(tmpX_SIM{save_location},tmpY_SIM{save_location},'LineWidth',2,'Color','Black');
hold off;
legend('Analytical','Simulation');
axis([0 320 0 320])
%%
datacursormode on
hdt = datacursormode;
set(hdt,'DisplayStyle','datatip','Enable','on');
set(hdt,'UpdateFcn',{@labeldtips_3D,f,tmpX_ANA{save_location},tmpY_ANA{save_location}});
%%
save_location=5;
fileID = fopen(['/Users/mohitmehta/OneDrive/Backup/Work_ongoing/ECS S2015/FEM_vs_ANA_' num2str(save_location) '.dat'],'r');
dataArray = textscan(fileID, '%f%f%[^\n\r]', 'Delimiter', ',',  'ReturnOnError', false);
fclose(fileID);
tmpX_SIM(save_location) = flip(dataArray(:, 1));
tmpY_SIM(save_location) = flip(dataArray(:, 2));
clearvars fileID dataArray ans;
%%
cc= distinguishable_colors(5,'w');
plot_style = ['sq'];
for i= 1:5
plot(tmpX_ANA{i},tmpY_ANA{i},'LineWidth',2,'Color',cc(i,:));
hold on;
plot(tmpX_SIM{i},tmpY_SIM{i},'sq','Color',cc(i,:));
legend('Analytical','Simulation');
end
axis([0 320 0 320])
hold off