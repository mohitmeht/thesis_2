function [ Impedance ] = ld_eis_db()
database_name='All_EIS_data';
if ismac
    base_path='/Users/mohitmehta/Copy/work/Phd/Data/Xujie Chen_data_on_Li-air/EIS paper/Saved Variables/';
elseif ~isunix
    base_path='E:\Copy\work\Phd\Data\Xujie Chen_data_on_Li-air\EIS paper\Saved Variables\';
end
load([base_path database_name]);

end

