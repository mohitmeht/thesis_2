function [Z,f]=EIS_MHC_analytical_with_iF(I_dis,n,lam_dim,Diff_length,Deff,k0,l,Cd,sp_area,A,poro,T,f)
% MHC with iF calculates k based on discharge current, eta is calculated
% using iF equation from petru's equation
% lam_dim = 1000;%in kJ/mol	% Here lam is reorganization energy and not
% sqrt(Deff/k)
% Vt=40e-3;                 %  _______     
% k0=1.3e-8; % cm/s         % |               
% Do=7e-6;                  % |initial            
% c_int=3.26e-6;            % |  or 
%                           % |standard 
% l=0.02;                   % |_______
% beta=0.57;
% Vt=40e-3; 
% a=1e4;
% poro=0.6;
% f=logspace(-3,log10(200e3),200);
% % eta=-0.3;     %% using a constant eta value instead of calculations
%% model calculations
Vt = Vt_compute(T);
lam = lam_dimless(lam_dim,T); % lambda in kJ/mol is converted to dimensionless
k_mhc_temp = Deff./Diff_length^2;
eta = k_mhc_solve(k_mhc_temp,lam,k0,sp_area); % eta_dim is in V and is converted to dimensionless with Vt (V)
eta_dim = 2.96+(eta.*Vt);
display(['eta[' num2str(lam) '] = ' num2str(eta_dim)]);
w=2.*pi.*f;
im=1i.*w.*poro;
dk_mhc_temp = k_mhc_temp + im;
MHC_short_int = sqrt(1+sqrt(lam)+eta.^2);
MHC_long_int = (lam-MHC_short_int)./(2.*sqrt(lam));

a = Vt./n./I_dis;
bn = 1;
bd1 = 1;

bd2 = k_mhc_temp./im;

bd3_1n = k_mhc_temp.*sqrt(k_mhc_temp);
bd3_1d = im.*sqrt(dk_mhc_temp);
bd3_1 = bd3_1n./bd3_1d;
bd3_2n = tanh(sqrt(dk_mhc_temp./Deff).*l);
bd3_2d = tanh(sqrt(k_mhc_temp./Deff).*l);
bd3_2 = bd3_2n./bd3_2d;
bd3 = bd3_1.*bd3_2;
bd = bd1 - bd2 + bd3;
b = bn./bd;

cn = sinh(-eta);
cd1 = 1;
cd2_1n = eta.*sinh(eta);
cd2_1d = erfc(MHC_long_int);
cd2_1 = cd2_1n./cd2_1d;
cd2_2n = exp(-MHC_long_int.^2);
cd2_2d = MHC_short_int.*sqrt(pi.*lam);
cd2_2 = cd2_2n./cd2_2d;
cd2 = cd2_1.*cd2_2;
cd = cd1+cd2;
c = cn./cd;
Zf=a.*b.*c;
Zd=1./(1i.*w.*sp_area.*A.*l.*Cd);
Z=(Zf.*Zd)./(Zf+Zd);
%% Old (Both the forms yield the same result)
% % Deff=Do*(poro^brugg);
% Z0=Vt/(n*beta*I_dis);
% lam=petru_equation(n,F,A,Deff,c_int,l,I_dis);
% eta=(Vt/n/beta)*log(k0*a*(lam^2/Deff));
% k=k0*a*exp(-beta*n*eta/Vt);
% w=2.*pi.*f;
% im=1i.*w.*poro;
% Zd=1./(1i.*w.*a.*A.*l.*Cd);
% Z=(Zf.*Zd)./(Zf+Zd);


end

function [k] = k_mhc(lam,eta,k0,sp_area) 
MHC_short_int = sqrt(1+sqrt(lam)+eta.^2);
MHC_long_int = (lam-MHC_short_int)./(2.*sqrt(lam));
k = -k0.*sp_area./k_mhc_0(lam).*(sqrt(pi.*lam).*tanh(eta/2)).*erfc(MHC_long_int); % Full MHC using tanh.
% k = -k0./k_mhc_0(lam).*(sqrt(pi.*lam).*tanh(eta/2)).*erfc(MHC_long_int); % Full MHC using tanh.
end

function [result] = k_mhc_solve(k,lam,k0,sp_area)
MHC_short_int = @(eta)(sqrt(1+sqrt(lam)+(eta.^2)));
MHC_long_int = @(eta)((lam-MHC_short_int(eta))./(2.*sqrt(lam)));
x0 = [-50 -0.000001];  % Make a starting guess at the solution
F = @(eta)((k_mhc_0(lam)./sp_area./k0./sqrt(pi.*lam)./erfc(MHC_long_int(eta))./tanh(-eta/2))-(1./k));
options = optimset('Display','none'); % show iterations
% options = optimoptions('fsolve');
% options.TolFun = 1e-30;
% options.MaxIter = 10000;
options.FunValCheck = 'on';
[result,~,exit_check] = fzero(F,x0,options); % Call solver
%   1 Converged
%   -1   Terminated
%   -3  nan or inf
%   -4 complex value
%   -5 singular point
%   -6 no sign change
end

function [lam] = lam_dimless(lam_dim,T)
lam = (lam_dim*1e3/phys_constants('N_A')/phys_constants('k_B_in_SI')/T); % converts kJ/mol to dimensionless
end

function [k_0] = k_mhc_0(lam)
k_0 = (sqrt(pi.*lam)./2.*erfc((lam-sqrt(1+sqrt(lam)))./(2.*sqrt(lam))));  % k_MHC at 0, do not delete.. Its not equal to k_mhc(lam,0)
end