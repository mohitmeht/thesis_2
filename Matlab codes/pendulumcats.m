function dy = pendulumcats(t,y)
dy = zeros(2,1);
omega = 1;
dy(1) = y(2);
dy(2) = -omega*omega*sin(y(1));
end