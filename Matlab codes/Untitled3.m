%% Plot 1
if ismac==1
     mkdir(['/Users/mohitmehta/Copy/My Figures/Pospectus_figures_' datestr(date)]);
elseif isunix==1
    mkdir(['/panfs/storage.local/ame/home/mrm11s/my_matlab_codes/figures_' datestr(date)]);
else
    mkdir (['H:\Copy\My Figures\Pospectus_figures_' datestr(date)]);
end
close all;
clear all;
clc;
cc=distinguishable_colors(13,'w');
mul=[1e-2,1e-1,0.5,1e0,2,1e1,1e2];
I_dis=1e-3.*[2e-1,0.5,1e0,2,5];
k0=1.3e-8.*mul;                       
n=[1 2 2.1 2.2 2.5 3 3.1 3.2 3.3 ];                              
Do=7e-6.*mul;                
c_int=3.26e-6.*mul;          
l=1e-4.*mul;
font_shape={'x','O','^','v','*','p','s'};
Cd=40e-6.*mul;





for  i=3:4
for r=[2 4 6];
for  q=[2 4 6]
for  p=[2 4 6]  
for  o=[2 4 6]    
for  m=[2 4 6] 
    
    
  figure(1);
  clf;
  set(gcf,'Visible','off');
for  j=[2 4 6]
number=j;
% for i=1:3
name_stack={['$I_{dis}=' num2eng(I_dis(i)) '\textrm{A/cm}^2$'],...
            ['$D_o=' num2str(Do(m)/10^(floor(log10(Do(m))))) '\times 10^{' num2str(floor(log10(Do(m)))) '}\,\textrm{cm}^2/\textrm{s}$'],... 
            ['$n=' num2str(n(p)) '$'],...
            ['$C_d=' num2eng(Cd(r)) ' F/cm^2$'],...
            ['$c_{int}=' num2str(c_int(o)/10^(floor(log10(c_int(o))))) '\times 10^{' num2str(floor(log10(c_int(o)))) '}\,\textrm{mol/cm}^3$'],...
            ['$k_0=' num2str(k0(j)/10^(floor(log10(k0(j))))) '\times 10^{' num2str(floor(log10(k0(j)))) '}\,\textrm{cm}/\textrm{s}$'],...
            ['$l=' num2eng(l(q)) ' m$']};
        
       
    h=our_paper_analytical(I_dis(i),k0(j),n(p),Do(m),c_int(o),l(q),Cd(r));
    
    
    
    hold on;
    set(h,'Color',cc(number,:),'MarkerFaceColor',cc(number,:));
    set(h,'DisplayName',['$k_0=' num2str(k0(j)/10^(floor(log10(k0(j))))) '\times 10^{' num2str(floor(log10(k0(j)))) '}\,\textrm{cm}/\textrm{s}$']);
    set(h,'Marker',font_shape{number});
    set(h,'LineStyle','-');
    set(h,'LineWidth',1.5);
    title(['i ' num2str(i) ' j ' num2str(j) ' p ' num2str(p)...
        ' m ' num2str(m) ' o ' num2str(o) ' q ' num2str(q)...
        ' r ' num2str(r)])
%     axis([0 160 0 160]);
% cur=pwd;
% cd(cur);
% cd('C:\Users\Mohit\Desktop\test');
% waitforbuttonpress;
end
if ismac==1
    cd(['/Users/mohitmehta/Copy/My Figures/Pospectus_figures_' datestr(date)]);
elseif isunix==1
    cd(['/panfs/storage.local/ame/home/mrm11s/my_matlab_codes/figures_' datestr(date)]);
else
    cd(['H:\Copy\My Figures\Pospectus_figures_' datestr(date)]);    
end
%     save_figures_with_cnt(['our_paper_ana_j_' num2str(j) '_m_' num2str(m)...
%         '_o_' num2str(o) '_n_' num2str(p) '_l_' num2str(q)],figure(1));

annotation(gcf,'textbox',...
    [0.589359788359788 0.768915343915345 0.763417989417991 0.200529100529099],...
    'String',name_stack,...
    'FontSize',15,...
    'FitBoxToText','off',...
    'LineStyle','none','FontName','Times New Roman','Interpreter','latex');
% % save_figures_with_cnt(['our_paper_ana_i_all_others_1'],figure(1));
% close all;
set(legend('-DynamicLegend','Location','NorthWest'),'Interpreter','latex');
axis([0 200 0 200]);
    save_figures_with_cnt(['i_' num2str(i) '_j_' num2str(j) '_p_' num2str(p)...
        '_m_' num2str(m) '_o_' num2str(o) '_q_' num2str(q)...
        '_r_' num2str(r) '_l'],figure(1));
end
end
end
end
end
end