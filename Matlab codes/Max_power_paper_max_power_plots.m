clear all;
close all;
clc;
% l_multiple=[1e-1 1e-2 1e-3 1e-4];
l=0.04;
kb= 1.3806488e-23;
q=1.602176e-19;
atm=0.2;  % Pressure in atmospheres

% c_int_multiple=[linspace(1e-9,1e-7,30) linspace(5e-7,1e-4,70)];
% c_int_multiple=[1e-6 10e-6 20e-6 40e-6 60e-6]; 
% R=547.4; % Ping
% R=132;  % 2010_YonggangWang_IV.dat
R=157;  % 
% R_multiple=[linspace(0,800,30) linspace(900,5000,10)];
E0=3.959; % First value of the voltage from the data
% Vt=26e-3;
beta=0.5;
n=4;
F=96500;
% ka=8e-4; % ka_paper=nFc(0,t)ka_simulation
ka_multiple=[linspace(1e-2,1e-3,10) linspace(9e-4,1e-7,50)];
Deff_multiple=[3e-4 3e-5 3e-6 3e-7 3e-8 3e-9];
% Deff=3e-6;
a=5e4;
% a_multiple=[5e3 5e4 5e5 5e6];
kc=8e-10;
% kc_multiple=[8e-7 8e-8 8e-9 8e-10 8e-11 8e-12];
X=linspace(10e-6,30e-3,100);
temp_in_c = 25;
T= 274.15 + temp_in_c;
c_air=atm*101.32500e3/8.314/T;
c_int=0.21*c_air;
Vt=kb*T/q;
    %%
    P_max=zeros(1,length(ka_multiple));
    P_plot=zeros(length(Deff_multiple),length(ka_multiple));
%     P_plot=zeros(1,length(ka_multiple));   % use 1 when you are testing a new variable and do not have multiple values of any other variable
for k=1:length(Deff_multiple)
% for k=1:1         % Change this when multiple values of the other variable is found
%     Deff=Deff_multiple(k);
% a=a_multiple(k);
% l=l_multiple(k);
Deff=Deff_multiple(k);
% kc=kc_multiple(k);
% ka=ka_multiple(k);
% R=R_multiple(k);
% c_int=c_int_multiple(k);
for j=1:length(ka_multiple)
ka=ka_multiple(j);
parfor i=1:length(X)
lamad(i)=petru_equation(n,F,Deff,c_int,l,X(i));
end
with_Deff = @(x)(E0-(Vt./beta.*asinh(x./2./n./F./c_int./ka))-(Vt./n./beta.*asinh(Deff./2./kc./a./lamad.^2))-(R.*x));
Y = with_Deff(X);
P=X.*Y;
% figure(1);
% plot(X.*1e3,Y);
% figure(2);
% plot(X.*1e3,P,'O');
P_max(j)=max(P);
end
P_plot(k,:)=P_max;
end
errordlg('Simulation Finished');
% plot(1e3.*X(1:length(X)/length(P_max):end),1e3.*P_max);
% plot(1e6.*c_int_multiple,1e3.*P_max);
% distFig();
%%
% xlabel('Current density mA/cm^2')
% xlabel('Oxygen concentration (10^-^6) [mol/cm^3]')
% xlabel('Resistance [\Omega]')
xlabel('Anodic reaction rate (10^-^3) [cm/s]')
ylabel('Maximum power density [mW/cm^2]')
set(gcf,'Position',[603 261 795  753])
set(0,'ScreenPixelsPerInch',90);
set(gcf,'Color','w');
set(gca,'FontSize',20,'Color','w','FontName','Times New Roman');
title('Variation of power density w.r.t Anodic reaction rate')
%%
clf;
cc=distinguishable_colors(size(P_plot,1),'w');
for i=1:size(P_plot,1)
% for i=2:4
% plot(1e6.*c_int_multiple,1e3.*P_plot(i,:),'-','color',cc(i,:),'LineWidth',1);hold on;
% plot(log10(1e7.*ka_multiple),1e3.*P_plot(i,:),'-','color',cc(i,:),'LineWidth',1);hold on;
% plot(1e3.*ka_multiple,1e3.*P_plot(i,:),'-','color',cc(i,:),'LineWidth',1);hold on;
plot(1e3.*ka_multiple,1e3.*P_plot(i,:),'-',...
    'color',cc(i,:),'LineWidth',1,...
    'DisplayName',['$D_{\textrm{eff}}=' num2str(Deff_multiple(i)/10^(floor(log10(Deff_multiple(i))))) '\times 10^{' num2str(floor(log10(Deff_multiple(i)))) '}\,\textrm{cm}^2\textrm{/s}$']);hold on;
% plot(1e3.*ka_multiple,1e3.*P_plot(i,:),'-',...
%     'color',cc(i,:),'LineWidth',1,...
%     'DisplayName',['$L_{c}=' num2eng(1e-2.*l_multiple(i)) ' m$']);hold on;
end
set(legend('-DynamicLegend'),...
    'Interpreter','latex',...
    'FontSize',18,...
    'FontName','Times New Roman',...
    'Location','NorthEast');
%% Save figure

filename=['P_vs_ka_Deff'];
if ismac
      foldername='/Users/mohitmehta/Copy/My Articles/CM_discharge_curve_with_vamsci/';
%     foldername='/Users/mohitmehta/Copy/My Figures/Comparison_figures/';
elseif isunix
    display('Sorry No path');
    break;
else
    foldername='H:\Copy\My Articles\CM_discharge_curve_with_vamsci\';
end
save([foldername filename],'ka_multiple','Deff_multiple','P_plot');
save_figures_with_cnt([foldername filename],gcf);

