function [Z,f,eta]=our_paper_analytical_FBV(I_dis,kc,n,Deff,c_int,l,Cd,a,A,poro,Vt,beta,f)
% I_dis=1e-3;             %  _______     
% k0=1.3e-8;              % |               
% n=3.2;                  % |initial            
% Do=7e-6;                % |  or 
% c_int=3.26e-6;          % |standard 
% l=0.02;                 % |_______
% beta=0.57;
% Vt=40e-3; 
% a=1e4;
% poro=0.6;
% f=logspace(-3,log10(200e3),200);
%%%%%%%% eta=-0.3;     %% using a constant eta value instead of calculations
F=phys_constants('F');
%% petru's model calculations using Butler-Volmer equation with sinh
lam=petru_equation(n,F,A,Deff,c_int,l,I_dis);
k=Deff./lam.^2;
eta_eqn= @(x)(exp(-beta.*n.*x./Vt) - ...
                    exp((1-beta).*n.*x./Vt) - ...
                    (k./(kc.*a)));
eta = fzero(eta_eqn,[0 -4]);
w=2.*pi.*f;
im=1i.*w.*poro;
an=1;
ad=1;
bn=k;
bd=im;
cn=k.*sqrt(k).*tanh(sqrt((k+im)./Deff).*l);
cd=im.*sqrt(k+im).*tanh(sqrt(k./Deff).*l);
Z0=Vt.*k./(n.*I_dis.*((beta.*k) + ...
                (kc.*a.*exp((1-beta).*n.*eta./Vt))));
Zf=Z0./((an./ad)-(bn./bd)+(cn./cd));
Zd=1./(1i.*w.*a.*A.*l.*Cd);
Z=(Zf.*Zd)./(Zf+Zd);
end