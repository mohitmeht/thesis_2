clear
kb= 1.3806488e-23;      % Boltzman constant
q=1.602176e-19;         % Electron Energy
F=96485;                % Faraday's constant
l=0.04;     % Length in cm % from the paper
temp_in_c = 30;
T= 273.15 + temp_in_c;
Vt=kb*T/q;
c_int=3.26e-6;     % converting to mol/cm3
a=4e4;      % Calculated using Pt distribution and cathode physical parameters
epsi=0.75;
k0 = logspace(-10,-4,300);
beta=0.5;
n=2;
E0=2.956; % First value of the voltage from the data % given in the paper
Dbulk  = [1e-10];
A = 1;
Deff=epsi^1.5*Dbulk;                          % comparison simulations

I_dis_multiple=logspace(-5,-2.30102999566398,300)';
x0 = 0.01;  % Make a starting guess at the solution

Length_to_write_csv_files = length(I_dis_multiple);

    variable_parameter = 1;
matrix_b_dim = length(variable_parameter);
x0 = 0.01.*ones(length(I_dis_multiple),length(variable_parameter));  % Make a starting guess at the solution
mehta_eqn = @(x)(n.*F*A.*c_int.*tanh(l./x)*diag(Deff)-(repmat(I_dis_multiple,1,matrix_b_dim).*x));
options = optimoptions('fsolve','Display','off');
options.TolFun = 1e-20;

[lamda,fval] = fsolve(mehta_eqn,x0,options); % Call solver
eta_c =2*Vt./n.*asinh(Deff./(lamda.^2*repmat((k0),length(1),1).*a));
%%
colormap jet
contourf(log10(I_dis_multiple),log10(k0),eta_c',100,'LineStyle','none');


