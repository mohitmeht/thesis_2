function [Z,f,eta]=our_paper_analytical(I_dis,k0,n,Deff,c_int,l,Cd,a,A,poro,Vt,beta,f)

% I_dis=1e-3;             %  _______     
% k0=1.3e-8;              % |               
% n=3.2;                  % |initial            
% Do=7e-6;                % |  or 
% c_int=3.26e-6;          % |standard 
% l=0.02;                 % |_______
% beta=0.57;
% Vt=40e-3; 
% a=1e4;
% poro=0.6;
% f=logspace(-3,log10(200e3),200);
%%%%%%%% eta=-0.3;     %% using a constant eta value instead of calculations
F=phys_constants('F');
%% petru's model calculations
Z0=Vt./(n.*beta.*I_dis);
lam=petru_equation(n,F,A,Deff,c_int,l,I_dis);
k=Deff./lam.^2;
eta = -Vt./n./beta.*log(k./k0./a);
w=2.*pi.*f;
im=1i.*w.*poro;
an = Z0;
ad1 = 1;
ad2 = k./im;
ad3n = k.*sqrt(k).*tanh(sqrt((k+im)./Deff).*l);
ad3d = im.*sqrt(k+im).*tanh(sqrt(k./Deff).*l);
ad3 = ad3n./ad3d;
ad = ad1 - ad2 + ad3;
Zf = an./ad;
Zd = 1./(1i.*w.*a.*A.*l.*Cd);
Z = (Zf.*Zd)./(Zf+Zd);
end